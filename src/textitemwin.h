/* * C o p y r i g h t *(also read COPYING)* * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *  Copyright (C) 2003  Philip Van Hoof <me@freax.org>                         *
 *                                                                             *
 *  This program is free software; you can redistribute it and/or modify       *
 *  it under the terms of the GNU General Public License as published by       *
 *  the Free Software Foundation; either version 2 of the License, or          *
 *  (at your option) any later version.                                        *
 *                                                                             *
 *  This program is distributed in the hope that it will be useful,            *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *  GNU General Public License for more details.                               *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License          *
 *  along with this program; if not, write to the Free Software                *
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA  *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <regex.h>
#include <strings.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>
#ifdef HAVE_CONFIG_H
#  include "../config.h"
#endif

/*#include <libgtkhtml/gtkhtml.h>*/
#include <gnome.h>

#ifdef __cplusplus
extern "C" {
#endif

#define GNOME_TYPE_TEXTITEMWIN (textitemwin_get_type())
#define GNOME_TEXTITEMWIN(obj) (GTK_CHECK_CAST((obj), GNOME_TYPE_TEXTITEMWIN, TextItemWin))
#define GNOME_TEXTITEMWIN_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass), GNOME_TYPE_TEXTITEMWIN, TextItemWinClass))
#define GNOME_IS_TEXTITEMWIN(obj) (GTK_CHECK_TYPE((obj), GNOME_TYPE_TEXTITEMWIN))
#define GNOME_IS_TEXTITEMWIN_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GNOME_TYPE_TEXTITEMWIN))
#define MAXLENSEARCHREPLACE 5000

typedef struct _TextItemWin TextItemWin;
typedef struct _TextItemWinClass TextItemWinClass;
typedef struct _TextItemTab TextItemTab;

typedef struct _TextItemTextTargetWidget TextItemTextTargetWidget;
typedef struct _TextItemHtmlTargetWidget TextItemHtmlTargetWidget;
typedef struct _TextItemTargetsTargetWidget TextItemTargetsTargetWidget;
typedef struct _TextItemTimeStampTargetWidget TextItemTimeStampTargetWidget;

struct Push {
    char *string;  
    size_t len;   
    size_t alloc;
};


struct _TextItemHtmlTargetWidget {
	TextItemWin *twin;
	TextItemTab *tab;

	GtkWidget *htmltargetvbox;
	GtkWidget *textview;

	GtkTextBuffer *buffer;

	GtkWidget *notebook;
	GtkWidget *htmlsrclabel;
	GtkWidget *htmlprevvbox;
	GtkWidget *htmlpreviewlabel;
	GtkWidget *htmlsrcvbox;

	/*Temporary solution*/
	GtkWidget *htmlviewer;
	/*HtmlDocument *doc;*/
	GtkWidget *scrolled_window;

};


struct _TextItemTimeStampTargetWidget {
	TextItemWin *twin;
	TextItemTab *tab;

	GtkWidget *tarstargetvbox;
	GtkWidget *textview;
	GtkWidget *scrolled_window;
};

struct _TextItemTargetsTargetWidget {
	TextItemWin *twin;
	TextItemTab *tab;

	GtkWidget *tarstargetvbox;
	GtkWidget *textview;
	GtkWidget *scrolled_window;


};

struct _TextItemTextTargetWidget {
  TextItemWin *twin;
  TextItemTab *tab;

  GtkWidget *notebook;
  
  GtkWidget *txttargetvbox;
  GtkWidget *txttargetscrolledwindow;
  GtkWidget *textview;
  GtkTextBuffer *buffer;
  GtkWidget *txttargetnotebook;
  GtkWidget *txttargettopvbox;
  GtkWidget *txttargettosearchvbox1;
  GtkWidget *txttargettosearchhbox1;
  GtkWidget *txttargettosearchhbox2;
  GtkWidget *txttargettosearchvbox2;
  GtkWidget *txttargettosearchhbox3;
  GtkWidget *txttargetsearchlabel;
  GtkWidget *txttargetfillerlabel1;
  GtkWidget *txttargettosearchhbox4;
  GtkWidget *txttargetfillerlabel2;
  GtkWidget *txttosearchentry;
  GtkWidget *txttargettoreplacevbox1;
  GtkWidget *txttargettoreplacehbox1;
  GtkWidget *txttargettoreplacelabel;
  GtkWidget *txttargetfillerlabel3;
  GtkWidget *txttargettoreplacehbox2;
  GtkWidget *txttargetfillerlabel4;
  GtkWidget *txttargetreplaceentry;
  GtkWidget *txttargethseparator;
  GtkWidget *txttargethbuttonbox;
  GtkWidget *txttargetreplacebutton;
  GtkWidget *txttargetsearcandreplace;

  
  GtkWidget *txttargetother;
  
  GtkWidget *misc_vbox;
  GtkWidget *tblMisc01;
  GtkWidget *btnRemLastChars;
  GtkWidget *btnRemFirstChars;
  GtkWidget *lblTlastChars;
  GtkWidget *lblTfirstChars;
  GtkWidget *lblRFirstChars;
  GtkWidget *lblRLastChars;
  GtkObject *sbtnLastChars_adj;
  GtkWidget *sbtnLastChars;
  GtkObject *sbtnFirstChars_adj;
  GtkWidget *sbtnFirstChars;
  GtkWidget *btnRemFirstLines;
  GtkWidget *btnRemLastLines;
  GtkWidget *lblTFirstLines;
  GtkWidget *lblTLastLines;
  GtkObject *sbtnFirstLines_adj;
  GtkWidget *sbtnFirstLines;
  GtkObject *sbtnLastLines_adj;
  GtkWidget *sbtnLastLines;
  GtkWidget *lblRFirstLines;
  GtkWidget *lblRLastLines;
  GtkWidget *btnInvert;
  GtkWidget *lblRev;



};

struct _TextItemTab {
	GtkWidget *tabwidget;
	GtkWidget *tablabel;
	GtkWidget *tabparent;
	GtkWidget *textview;

	gint tabnum;
	gboolean changed;
	guchar *newdata;
	gchar *codeset;
	GtkSelectionData *data;
};

struct _TextItemWin {
  GtkWindow window;
  gpointer *item;
  GtkWidget *textview;
  GList *tabs;

  GtkWidget *toplevelvbox;
  GtkWidget *menubar;
  GtkWidget *notebook;

  GtkWidget *empty_notebook_page;
  GtkWidget *txttargetother;
  GtkWidget *TARGETHOLDER1;
  GtkWidget *TARGETHOLDER2;
  GtkWidget *hbuttonbox;
  GtkWidget *cancel_button;
  GtkWidget *ok_button;

  gboolean changed;

  GtkWidget *windowparent;


  TextItemTextTargetWidget * (*plugin_create_texttargetwidget) (TextItemWin *twin,TextItemTab *tab, GtkSelectionData *data);
  TextItemHtmlTargetWidget * (*plugin_create_htmltargetwidget) (TextItemWin *twin,TextItemTab *tab, GtkSelectionData *data);
  TextItemTargetsTargetWidget * (*plugin_create_tarstargetwidget) (TextItemWin *twin,TextItemTab *tab, GtkSelectionData *data);
  TextItemTimeStampTargetWidget * (*plugin_create_timetargetwidget) (TextItemWin *twin,TextItemTab *tab, GtkSelectionData *data);
  /* 
  void (*plugin_prepare_menus) (TextItemWin *twin);
  void (*plugin_createhintsfor_menus) (TextItemWin *twin);

  void (*plugin_revstr) (gchar *input_buf, gint buflen, gchar *output_buf);
  void (*plugin_remove_first_x_chars_from_each_line) (gchar *s1, gint x);
  void (*plugin_remove_last_x_chars_from_each_line) (gchar *s1, gint x);
  void (*plugin_remove_first_x_lines) (gchar *s1, gint x);
  void (*plugin_remove_last_x_lines) (gchar *s1, gint x);

  gchar* (*plugin_search_replace_using_sed) (gchar *buffer, gchar *search, gchar *replace);
  gchar * (*plugin_string_replace) (gchar *gstring, gchar *goldpiece, gchar *gnewpiece);
*/
};

struct _TextItemWinClass {
   GtkWindowClass parent_class;
};


TextItemTextTargetWidget * create_texttargetwidget (TextItemWin *twin,TextItemTab *tab, GtkSelectionData *data);
TextItemHtmlTargetWidget * create_htmltargetwidget (TextItemWin *twin,TextItemTab *tab, GtkSelectionData *data);
TextItemTargetsTargetWidget * create_tarstargetwidget (TextItemWin *twin,TextItemTab *tab, GtkSelectionData *data);
TextItemTimeStampTargetWidget * create_timetargetwidget (TextItemWin *twin,TextItemTab *tab, GtkSelectionData *data);

void textitemwin_prepare_menus(TextItemWin *twin);
void textitemwin_createhintsfor_menus(TextItemWin *twin);
GtkType textitemwin_get_type(void);
GtkWidget* textitemwin_new (GtkWidget *windowparent, gpointer *user_data);

void revstr(gchar *input_buf, gint buflen, gchar *output_buf);
void remove_first_x_chars_from_each_line(gchar *s1, gint x);
void remove_last_x_chars_from_each_line(gchar *s1, gint x);
void remove_first_x_lines(gchar *s1, gint x);
void remove_last_x_lines(gchar *s1, gint x);

void texitemwin_ok (TextItemWin *twin, gpointer *user_data);
void textitemwin_cancel_and_close (TextItemWin *twin, gpointer *user_data);

/* Textmanipulation functions */
gchar* search_replace_using_sed (gchar *buffer, gchar *search, gchar *replace);

char * myreplace(char *string, char *oldpiece, char *newpiece);
gchar * g_string_replace (gchar *gstring, gchar *goldpiece, gchar *gnewpiece);
char *PushString(struct Push *push, const char *append);
char *PushNString(struct Push *push, const char *append, int size);


#ifdef __cplusplus /* cpp compatibility */
}
#endif
