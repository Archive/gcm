/* * C o p y r i g h t *(also read COPYING)* * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *  Copyright (C) 2003  Philip Van Hoof <me@freax.org>                         *
 *                                                                             *
 *  This program is free software; you can redistribute it and/or modify       *
 *  it under the terms of the GNU General Public License as published by       *
 *  the Free Software Foundation; either version 2 of the License, or          *
 *  (at your option) any later version.                                        *
 *                                                                             *
 *  This program is distributed in the hope that it will be useful,            *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *  GNU General Public License for more details.                               *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License          *
 *  along with this program; if not, write to the Free Software                *
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA  *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */


/*  A lot pieces of this code come from the XMMS sources (http://www.xmms.org)
 * 
 *  XMMS - Cross-platform multimedia player
 *  Copyright (C) 1998-2002  Peter Alm, Mikael Alm, Olle Hallnas,
 *                           Thomas Nilsson and 4Front Technologies
 *  Copyright (C) 1999-2002  Haavard Kvaalen
 */


#include "mainwin.h"
#include "plugin.h"
#include "mainwin_callbacks.h"
#include "controlsocket.h"
#include "../libgcm/libgcm.h"

#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <dirent.h>
#include <errno.h>
#include <pthread.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>


static gint session_id = 0;
static gint ctrl_fd = 0;
static gchar *socket_name;

static void *ctrlsocket_func(void *);
static pthread_t ctrlsocket_thread;
static gboolean going = TRUE, started = FALSE;
MainWin *mwin;


typedef struct
{
	ClientPktHeader hdr;
	gpointer data;
	gint fd;
}
PacketNode;

static GList *packet_list;
static pthread_mutex_t packet_list_mutex = PTHREAD_MUTEX_INITIALIZER, start_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t start_cond = PTHREAD_COND_INITIALIZER;

gboolean setup_ctrlsocket(gpointer *mwinp)
{
	struct sockaddr_un saddr;
	gboolean retval = FALSE;
	gint i;

	if (mwinp!=NULL) {
		mwin = (MainWin*) mwinp;
	} else return;


	if ((ctrl_fd = socket(AF_UNIX, SOCK_STREAM, 0)) != -1)
	{
		for (i = 0;; i++)
		{

			saddr.sun_family = AF_UNIX;
			g_snprintf(saddr.sun_path, 108, "%s/gcm_%s.%d",
				   g_get_tmp_dir(), g_get_user_name(), i);
			
			if (!gcm_remote_is_running(i))
			{
				g_print("GCM SocketSetup: Death socket (%s) found\n", saddr.sun_path);
				if ((unlink(saddr.sun_path) == -1) && errno != ENOENT)
				{
					g_log(NULL, G_LOG_LEVEL_CRITICAL,
					      "setup_ctrlsocket(): Failed to unlink %s (Error: %s)",
					      saddr.sun_path, strerror(errno));
				} else {
					g_print("GCM SocketSetup: Socket (%s) removed successfully\n", saddr.sun_path);
				}
			}
			else
			{

				g_print("GCM: Another instance of GNOME Clipboard Manager is already running\n");
				exit(0);
				break;
			}
			if (bind(ctrl_fd, (struct sockaddr *) &saddr, sizeof (saddr)) != -1)
			{
				session_id = i;
				listen(ctrl_fd, 100);
				going = TRUE;

				pthread_create(&ctrlsocket_thread, NULL, ctrlsocket_func, NULL);
				socket_name = g_strdup(saddr.sun_path);
				retval = TRUE;
				break;
			}
			else
			{
				g_log(NULL, G_LOG_LEVEL_CRITICAL,
				      "setup_ctrlsocket(): Failed to assign %s to a socket (Error: %s)",
				      saddr.sun_path, strerror(errno));
				break;
			}
		}
	}
	else
		g_log(NULL, G_LOG_LEVEL_CRITICAL,
		      "setup_ctrlsocket(): Failed to open socket: %s", strerror(errno));

	if (!retval)
	{
		if (ctrl_fd != -1)
			close(ctrl_fd);
		ctrl_fd = 0;
	}
	return retval;
}

gint ctrlsocket_get_session_id(void)
{
	return session_id;
}

void cleanup_ctrlsocket(void)
{
	if (ctrl_fd)
	{
		pthread_mutex_lock(&start_mutex);
		going = FALSE;
		pthread_cond_signal(&start_cond);
		pthread_mutex_unlock(&start_mutex);
		pthread_join(ctrlsocket_thread, NULL);
		close(ctrl_fd);
		unlink(socket_name);
		g_free(socket_name);
		ctrl_fd = 0;
	}
}

void start_ctrlsocket(void)
{
	pthread_mutex_lock(&start_mutex);
	started = TRUE;
	pthread_cond_signal(&start_cond);
	pthread_mutex_unlock(&start_mutex);
}

static void ctrl_write_packet(gint fd, gpointer data, gint length)
{
	ServerPktHeader pkthdr;

	pkthdr.version = GCM_PROTOCOL_VERSION;
	pkthdr.data_length = length;
	write(fd, &pkthdr, sizeof (ServerPktHeader));
	if (data && length > 0)
		write(fd, data, length);
}

static void ctrl_write_gint(gint fd, gint val)
{
	ctrl_write_packet(fd, &val, sizeof (gint));
}

/* Unused a.t.m.
static void ctrl_write_gfloat(gint fd, gfloat val)
{
	ctrl_write_packet(fd, &val, sizeof (gfloat));
}

static void ctrl_write_gboolean(gint fd, gboolean bool)
{
	ctrl_write_packet(fd, &bool, sizeof (gboolean));
}

*/
static void ctrl_write_string(gint fd, gchar * string)
{
	ctrl_write_packet(fd, string, string ? strlen(string) + 1 : 0);
}

static void ctrl_ack_packet(PacketNode * pkt)
{
	ctrl_write_packet(pkt->fd, NULL, 0);
	close(pkt->fd);
	if (pkt->data)
		g_free(pkt->data);
	g_free(pkt);
}

void *ctrlsocket_func(void *arg)
{
	fd_set set;
	struct timeval tv;
	struct sockaddr_un saddr;
	gint fd;
	PacketNode *pkt;
	gint len;

	pthread_mutex_lock(&start_mutex);
	while (!started && going)
		pthread_cond_wait(&start_cond, &start_mutex);
	pthread_mutex_unlock(&start_mutex);

	while (going)
	{
		FD_ZERO(&set);
		FD_SET(ctrl_fd, &set);
		tv.tv_sec = 0;
		tv.tv_usec = 100000;
		len = sizeof (saddr);
		if ((select(ctrl_fd + 1, &set, NULL, NULL, &tv) <= 0) ||
		    ((fd = accept(ctrl_fd, (struct sockaddr *) &saddr, &len)) == -1))
			continue;

		pkt = g_malloc0(sizeof (PacketNode));
		read(fd, &pkt->hdr, sizeof (ClientPktHeader));
		if (pkt->hdr.data_length)
		{
			pkt->data = g_malloc0(pkt->hdr.data_length);
			read(fd, pkt->data, pkt->hdr.data_length);
		}
		pkt->fd = fd;
		
		switch (pkt->hdr.command)
		{
			case CMD_GET_VERSION:
				ctrl_write_gint(pkt->fd, 0x09a3);
				ctrl_ack_packet(pkt);
				break;

			case CMD_GET_ROWDATE:
				if (pkt->data) {
					gchar *prv;
					gint result = 
							gtk_clist_get_text ( GTK_CLIST(mwin->cliplist),
							(*(guint32 *) pkt->data), CLIST_DATE_FIELD, &prv);
					if (result == 1) {
						ctrl_write_string(pkt->fd, prv);
					} else ctrl_write_string(pkt->fd, NULL);
				} else ctrl_write_string(pkt->fd, NULL);
				ctrl_ack_packet(pkt);
				break;
			case CMD_GET_ROWFROM:
				if (pkt->data) {
					gchar *prv;
					gint result = 
							gtk_clist_get_text ( GTK_CLIST(mwin->cliplist),
							(*(guint32 *) pkt->data), CLIST_FROM_FIELD, &prv);
					
					if (result == 1) {
						ctrl_write_string(pkt->fd, prv);
					} else ctrl_write_string(pkt->fd,NULL);
				} else ctrl_write_string(pkt->fd, NULL);
				ctrl_ack_packet(pkt);
				break;
			case CMD_GET_ROWPREVIEW:
				if (pkt->data) {
					gchar *prv;
					gint result = 
							gtk_clist_get_text ( GTK_CLIST(mwin->cliplist),
							(*(guint32 *) pkt->data), CLIST_DATA_FIELD, &prv);
					if (result == 1) {
						ctrl_write_string(pkt->fd, prv);
					} else ctrl_write_string(pkt->fd, NULL);
				} else ctrl_write_string(pkt->fd, NULL);
				ctrl_ack_packet(pkt);
				break;
			case CMD_GET_ROWCNT:
				ctrl_write_gint(pkt->fd, GTK_CLIST(mwin->cliplist)->rows);
				ctrl_ack_packet(pkt);
				break;
			case CMD_PING:
				ctrl_ack_packet(pkt);
				break;
			default:
				pthread_mutex_lock(&packet_list_mutex);
				
				packet_list = g_list_append(packet_list, pkt);
				ctrl_write_packet(pkt->fd, NULL, 0);
				close(pkt->fd);
				pthread_mutex_unlock(&packet_list_mutex);
				break;
/*
			case CMD_PLAYLIST_ADD_URL_STRING:
				playlist_add_url_string(pkt->data);
				ctrl_ack_packet(pkt);
				break;

			case CMD_PLAYLIST_CLEAR:
				GDK_THREADS_ENTER();
				playlist_clear();
				GDK_THREADS_LEAVE();
				ctrl_ack_packet(pkt);
				break;*/
		}
	}
	pthread_exit(NULL);
}

void check_ctrlsocket(void)
{
	GList *pkt_list, *next;
	PacketNode *pkt;
	gpointer data;


	pthread_mutex_lock(&packet_list_mutex);
	for (pkt_list = packet_list; pkt_list; pkt_list = next)
	{
		pkt = pkt_list->data;
		data = pkt->data;

		switch (pkt->hdr.command)
		{
			case CMD_ADD_TEXTSEL:
				app_add_text_selection (mwin, "Text", "New", data);
				break;
			case CMD_EXIT_GCM:
				if (pluginsystem_ready())
					cleanup_plugins ();
				/* question: should we use gtk_main_quit (); ? */
					exit(*((guint32 *) data));
				break;
			case CMD_SELECT_NONE:
				on_mainwinmenu_selectnone_activate (NULL, (gpointer) mwin);
				break;
			case CMD_SELECT_ALL:
				on_mainwinmenu_selectall_activate (NULL, (gpointer) mwin);
				break;
			case CMD_CLEAR:
				on_mainwinmenu_clear_activate (NULL, (gpointer) mwin);
				break;
			case CMD_GET_NEW:
				app_get_new_item (mwin);
				break;
			case CMD_DELETE_SELECTED:
				app_delete_selected_items (mwin, GTK_CLIST(mwin->cliplist));
				break;
			case CMD_MERGE_SELECTED:
				app_merge_selectedrows (mwin);
				break;
			case CMD_SAVE_ALL:
				on_mainwinmenu_file_savealltodisk_activate (NULL, (gpointer) mwin);
				break;
			case CMD_SAVE:
				on_mainwinmenu_file_savetodisk_activate (NULL, (gpointer) mwin);
				break;
			case CMD_SELECT_ROW:
				gtk_clist_select_row (GTK_CLIST(mwin->cliplist), *((guint32 *) data), -1);
				break;
			case CMD_NEWWIN_SHOW:
				app_create_and_show_newitemwin(mwin);
				break;
			case CMD_NEWWIN_HIDE:
				if (mwin->newitemwin != NULL)
					gtk_widget_hide(GTK_WIDGET(mwin->newitemwin));
				break;
			case CMD_ABOUT_SHOW:
				app_create_and_show_aboutwin(mwin);
				break;
			case CMD_SHOW_TEXTITEMWIN:
				app_create_and_show_textitemwin (mwin, *((guint32 *) data));
				break;
			case CMD_PREFS_SHOW:
				app_show_prefswin(mwin);
				break;
			case CMD_PREFS_HIDE:
				if (mwin->prefswin != NULL)
					gtk_widget_destroy(GTK_WIDGET(mwin->prefswin));
				break;
			case CMD_MOVE_SELECTED_DOWN :
			{
				GList *remaining;
				remaining = g_list_copy(GTK_CLIST(mwin->cliplist)->selection);
				if (remaining!=NULL) {
					gtk_clist_freeze (GTK_CLIST(mwin->cliplist));
					app_move_selected_down (mwin, remaining, -1);
					g_list_free(remaining);
					gtk_clist_thaw (GTK_CLIST(mwin->cliplist));
				}
			}
			break;
			case CMD_MOVE_SELECTED_UP:
			{
				GList *remaining;
				remaining = g_list_copy(GTK_CLIST(mwin->cliplist)->selection);
				if (remaining!=NULL) {
					gtk_clist_freeze (GTK_CLIST(mwin->cliplist));
					app_move_selected_up (mwin, remaining, -1);
					gtk_clist_thaw (GTK_CLIST(mwin->cliplist));
					g_list_free(remaining);
				}
			}
			break;
			case CMD_MWIN_HIDE:
				gtk_widget_hide(GTK_WIDGET(mwin));
				break;
			case CMD_MWIN_SHOW:
				gtk_widget_show(GTK_WIDGET(mwin));
				break;
		}
		next = g_list_next(pkt_list);
		packet_list = g_list_remove_link(packet_list, pkt_list);
		g_list_free_1(pkt_list);
		if (pkt->data)
			g_free(pkt->data);
		g_free(pkt);
	}
	pthread_mutex_unlock(&packet_list_mutex);
}
