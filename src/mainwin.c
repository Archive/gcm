/* * C o p y r i g h t *(also read COPYING)* * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *  Copyright (C) 2003  Philip Van Hoof <me@freax.org>                         *
 *                                                                             *
 *  This program is free software; you can redistribute it and/or modify       *
 *  it under the terms of the GNU General Public License as published by       *
 *  the Free Software Foundation; either version 2 of the License, or          *
 *  (at your option) any later version.                                        *
 *                                                                             *
 *  This program is distributed in the hope that it will be useful,            *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *  GNU General Public License for more details.                               *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License          *
 *  along with this program; if not, write to the Free Software                *
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA  *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */



#include <sys/time.h>
#include <time.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <dirent.h>
#include <errno.h>
#include <unistd.h>
#include <gconf/gconf.h>
#include <gconf/gconf-client.h>

#include "mainwin.h"
#include "newitemwin.h"
#include "textitemwin.h"
#include "prefswin.h"
#include "mainwin_callbacks.h"
#include "plugin.h"
#include "controlsocket.h"
#include "../libgcm/libgcm.h"



#define MAXIMUM_WM_REPARENTING_DEPTH 4
static void mainwin_class_init(MainWinClass *klass);
static void mainwin_init(MainWin *mwin);
static void mainwin_destroy(GtkObject *object);



static GtkWindowClass *parent_class = NULL;


/* If you change a menu, also change it in the popmainwinmenu*/
/* And set the datapointer in the mainwin_new function to mwin !*/
static GnomeUIInfo mainwinmenu_file_menu_uiinfo[] =
{
#define MAINWINMENU_FILE_NEWITEM 0
  GNOMEUIINFO_MENU_NEW_ITEM (N_("_New..."), NULL, on_mainwinmenu_newitem_activate, NULL),
#define MAINWINMENU_FILE_OPENITEMFROMDISK 1
  {
    GNOME_APP_UI_ITEM, N_("_Load items..."),
    NULL,
    (gpointer) on_mainwinmenu_file_openfromdisk_activate, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_OPEN,
    GDK_O, GDK_CONTROL_MASK, NULL
  },
 #define MAINWINMENU_FILE_SAVEITEMSTODISK 2
  {
    GNOME_APP_UI_ITEM, N_("_Save selected..."),
    NULL,
    (gpointer) on_mainwinmenu_file_savetodisk_activate, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_SAVE_AS,
    GDK_S, GDK_CONTROL_MASK, NULL
  },
#define MAINWINMENU_FILE_SAVEALLITEMSTODISK 3
  {
    GNOME_APP_UI_ITEM, N_("Save _all..."),
    NULL,
    (gpointer) on_mainwinmenu_file_savealltodisk_activate, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_SAVE_AS,
    GDK_A, GDK_CONTROL_MASK, NULL
  },

#define MAINWINMENU_FILE_SEPARATOR 4
  GNOMEUIINFO_SEPARATOR,
#define MAINWINMENU_FILE_EXIT 5
  GNOMEUIINFO_MENU_EXIT_ITEM (on_mainwinmenu_exit_activate, NULL),
#define MAINWINMENU_FILE_END 6
  GNOMEUIINFO_END
};


static GnomeUIInfo mainwinmenu_edit_menu_uiinfo[] =
{
#define MAINWINMENU_EDIT_CUT 0
  GNOMEUIINFO_MENU_CUT_ITEM (on_mainwinmenu_cut_activate, NULL),
#define MAINWINMENU_EDIT_COPY 1
  GNOMEUIINFO_MENU_COPY_ITEM (on_mainwinmenu_copy_activate, NULL),
#define MAINWINMENU_EDIT_PASTE 2
  GNOMEUIINFO_MENU_PASTE_ITEM (on_mainwinmenu_paste_activate, NULL),
#define MAINWINMENU_EDIT_CLEAR 3
  GNOMEUIINFO_MENU_CLEAR_ITEM (on_mainwinmenu_clear_activate, NULL),
#define MAINWINMENU_EDIT_SEPARATORX 4
  GNOMEUIINFO_SEPARATOR,
#define MAINWINMENU_EDIT_SELECT_ALL 5
  GNOMEUIINFO_MENU_SELECT_ALL_ITEM (on_mainwinmenu_selectall_activate, NULL),
{
#define MAINWINMENU_EDIT_SELECT_NONE 6
    GNOME_APP_UI_ITEM, N_("_Select none"),
    NULL,
    (gpointer) on_mainwinmenu_selectnone_activate, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_UNDO,
    0, (GdkModifierType) 0, NULL
  },
/*#define MAINWINMENU_EDIT_SEPARATOR 7
  GNOMEUIINFO_SEPARATOR,*/
#define MAINWINMENU_EDIT_END 7  
  GNOMEUIINFO_END
};

static GnomeUIInfo mainwinmenu_settings_menu_uiinfo[] =
{
#define MAINWINMENU_SETTINGS_PREFERENCES 0
  GNOMEUIINFO_MENU_PREFERENCES_ITEM (on_mainwinmenu_preferences_activate, NULL),
#define MAINWINMENU_SETTINGS_END 1
  GNOMEUIINFO_END
};

static GnomeUIInfo mainwinmenu_help_menu_uiinfo[] =
{
#define MAINWINMENU_HELP_ABOUT 0
  GNOMEUIINFO_MENU_ABOUT_ITEM (on_mainwinmenu_about_activate, NULL),
#define MAINWINMENU_HELP_END 1
  GNOMEUIINFO_END
};

static GnomeUIInfo mainwinmenu_uiinfo[] =
{
#define MAINWINMENU_FILE 0
  GNOMEUIINFO_MENU_FILE_TREE (mainwinmenu_file_menu_uiinfo),
#define MAINWINMENU_EDIT 1
  GNOMEUIINFO_MENU_EDIT_TREE (mainwinmenu_edit_menu_uiinfo),
#define MAINWINMENU_SETTINGS 2
  GNOMEUIINFO_MENU_SETTINGS_TREE (mainwinmenu_settings_menu_uiinfo),
#define MAINWINMENU_HELP 3
  GNOMEUIINFO_MENU_HELP_TREE (mainwinmenu_help_menu_uiinfo),
#define MAINWINMENU_END 4
  GNOMEUIINFO_END
};

static GnomeUIInfo popmainwinmenu_file_menu_uiinfo[] =
{
#define POPMAINWINMENU_FILE_NEWITEM 0
  GNOMEUIINFO_MENU_NEW_ITEM (N_("_New..."), NULL, on_mainwinmenu_newitem_activate, NULL),
  {
#define POPMAINWINMENU_FILE_DELETEITEM 1
    GNOME_APP_UI_ITEM, N_("_Delete"),
    NULL,
    (gpointer) on_mainwinmenu_deleteitem_activate, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_CLOSE,
    0, (GdkModifierType) 0, NULL
  },
#define POPMAINWINMENU_FILE_SAVEITEMSTODISK 2
  {
    GNOME_APP_UI_ITEM, N_("_Save selected..."),
    NULL,
    (gpointer) on_mainwinmenu_file_savetodisk_activate, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_SAVE_AS,
    0, (GdkModifierType) 0, NULL
  },
#define POPMAINWINMENU_FILE_SAVEALLITEMSTODISK 3
  {
    GNOME_APP_UI_ITEM, N_("_Save all..."),
    NULL,
    (gpointer) on_mainwinmenu_file_savealltodisk_activate, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_SAVE_AS,
    GDK_A, GDK_CONTROL_MASK, NULL
  },
 #define POPMAINWINMENU_FILE_OPENITEMFROMDISK 4
  {
    GNOME_APP_UI_ITEM, N_("_Load items..."),
    NULL,
    (gpointer) on_mainwinmenu_file_openfromdisk_activate, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_OPEN,
    0, (GdkModifierType) 0, NULL
  },

#define POPMAINWINMENU_FILE_SEPARATOR 5
  GNOMEUIINFO_SEPARATOR,
#define POPMAINWINMENU_FILE_EXIT 6
  GNOMEUIINFO_MENU_EXIT_ITEM (on_mainwinmenu_exit_activate, NULL),
#define POPMAINWINMENU_FILE_END 7
  GNOMEUIINFO_END
};

static GnomeUIInfo popmainwinmenu_edit_menu_uiinfo[] =
{
#define POPMAINWINMENU_EDIT_CUT 0
  GNOMEUIINFO_MENU_CUT_ITEM (on_mainwinmenu_cut_activate, NULL),
#define POPMAINWINMENU_EDIT_COPY 1
  GNOMEUIINFO_MENU_COPY_ITEM (on_mainwinmenu_copy_activate, NULL),
#define POPMAINWINMENU_EDIT_PASTE 2
  GNOMEUIINFO_MENU_PASTE_ITEM (on_mainwinmenu_paste_activate, NULL),
#define POPMAINWINMENU_EDIT_CLEAR 3
  GNOMEUIINFO_MENU_CLEAR_ITEM (on_mainwinmenu_clear_activate, NULL),
#define POPMAINWINMENU_EDIT_SEPARATOR 4
  GNOMEUIINFO_SEPARATOR,
#define POPMAINWINMENU_EDIT_SELECT_ALL 5
  GNOMEUIINFO_MENU_SELECT_ALL_ITEM (on_mainwinmenu_selectall_activate, NULL),
{
#define POPMAINWINMENU_EDIT_SELECT_NONE 6
    GNOME_APP_UI_ITEM, N_("_Select none"),
    NULL,
    (gpointer) on_mainwinmenu_selectnone_activate, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_UNDO,
    0, (GdkModifierType) 0, NULL
  },	
  
#define POPMAINWINMENU_EDIT_SEPARATOR2 7
  GNOMEUIINFO_SEPARATOR,

#define POPMAINWINMENU_EDIT_END 8

  GNOMEUIINFO_END
};

static GnomeUIInfo popmainwinmenu_settings_menu_uiinfo[] =
{
#define POPMAINWINMENU_SETTINGS_PREFERENCES 0
  GNOMEUIINFO_MENU_PREFERENCES_ITEM (on_mainwinmenu_preferences_activate, NULL),
#define POPMAINWINMENU_SETTINGS_END 1
  GNOMEUIINFO_END
};

static GnomeUIInfo popmainwinmenu_help_menu_uiinfo[] =
{
#define POPMAINWINMENU_HELP_ABOUT 0
  GNOMEUIINFO_MENU_ABOUT_ITEM (on_mainwinmenu_about_activate, NULL),
#define POPMAINWINMENU_HELP_END 1
  GNOMEUIINFO_END
};



static GnomeUIInfo popmainwinmenu_uiinfo[] =
{
#define POPMAINWINMENU_FILE 0
  GNOMEUIINFO_MENU_FILE_TREE (popmainwinmenu_file_menu_uiinfo),
#define POPMAINWINMENU_EDIT 1
  GNOMEUIINFO_MENU_EDIT_TREE (popmainwinmenu_edit_menu_uiinfo),
#define POPMAINWINMENU_SETTINGS 2
  GNOMEUIINFO_MENU_SETTINGS_TREE (popmainwinmenu_settings_menu_uiinfo),
#define POPMAINWINMENU_HELP 3
  GNOMEUIINFO_MENU_HELP_TREE (popmainwinmenu_help_menu_uiinfo),
#define POPMAINWINMENU_END 4
  GNOMEUIINFO_END
};

static GnomeUIInfo cliplistpopupmenu_uiinfo[] =
{
#define CLIPLISTPOPUPMENU_NEWITEM 0
  GNOMEUIINFO_MENU_NEW_ITEM (N_("_New"), NULL, on_cliplistpopupmenu_new_activate, NULL),
#define CLIPLISTPOPUPMENU_EDITITEM 1
{
    GNOME_APP_UI_ITEM, N_("_Edit"),
    NULL,
    (gpointer) on_cliplistpopupmenu_edit_activate, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_SAVE_AS,
    0, (GdkModifierType) 0, NULL
  },
#define CLIPLISTPOPUPMENU_F_SEPARATOR 2
  GNOMEUIINFO_SEPARATOR,  
#define CLIPLISTPOPUPMENU_CUTITEM 3
  GNOMEUIINFO_MENU_CUT_ITEM (on_cliplistpopupmenu_cut_activate, NULL),
#define CLIPLISTPOPUPMENU_COPYITEM 4
  GNOMEUIINFO_MENU_COPY_ITEM (on_cliplistpopupmenu_copy_activate, NULL),
#define CLIPLISTPOPUPMENU_PASTEITEM 5
  GNOMEUIINFO_MENU_PASTE_ITEM (on_cliplistpopupmenu_paste_activate, NULL),
#define CLIPLISTPOPUPMENU_FILEMENU 6
  GNOMEUIINFO_SUBTREE (N_("_Menu"), popmainwinmenu_uiinfo),
#define CLIPLISTPOPUPMENU_S_SEPARATOR 7
  GNOMEUIINFO_SEPARATOR,  

#define CLIPLISTPOPUPMENU_MOVE_UP 8
  {
    GNOME_APP_UI_ITEM, N_("Move item _up"),
    NULL,
    (gpointer) on_mainwinmenu_moveup_activate, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_UP,
    0, (GdkModifierType) 0, NULL
  },
#define CLIPLISTPOPUPMENU_MOVE_DOWN 9
  {
    GNOME_APP_UI_ITEM, N_("Move item _down"),
    NULL,
    (gpointer) on_mainwinmenu_movedown_activate, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_DOWN,
    0, (GdkModifierType) 0, NULL
  },
  
  
#define CLIPLISTPOPUPMENU_EDIT_SEPARATOR 10
  GNOMEUIINFO_SEPARATOR,  
#define CLIPLISTPOPUPMENU_MERGE 11
  {
    GNOME_APP_UI_ITEM, N_("_Merge items to one"),
    NULL,
    (gpointer) on_cliplistpopupmenu_merge_activate, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK,GNOME_STOCK_MENU_CONVERT ,
    0, (GdkModifierType) 0, NULL
  },

#define CLIPLISTPOPUPMENU_CONVERT 12
  {
    GNOME_APP_UI_ITEM, N_("C_onvert data to items"),
    NULL,
    (gpointer) on_cliplistpopupmenu_convert_activate, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_CONVERT,
    0, (GdkModifierType) 0, NULL
  },

#define CLIPLISTPOPUPMENU_EDIT_CLEAR 13
  GNOMEUIINFO_MENU_CLEAR_ITEM (on_mainwinmenu_clear_activate, NULL), 
#define CLIPLISTPOPUPMENU_EDIT_SELECT_ALL 14
  GNOMEUIINFO_MENU_SELECT_ALL_ITEM (on_mainwinmenu_selectall_activate, NULL),
{
#define CLIPLISTPOPUPMENU_EDIT_SELECT_NONE 15
    GNOME_APP_UI_ITEM, N_("S_elect none"),
    NULL,
    (gpointer) on_mainwinmenu_selectnone_activate, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_UNDO,
    0, (GdkModifierType) 0, NULL
  },	
  
#define CLIPLISTPOPUPMENU_END 16
  GNOMEUIINFO_END
};





void app_prepare_menus(MainWin *mwin)
{
	gint i;

	for (i=0; i<MAINWINMENU_FILE_END; i++) {
		mainwinmenu_file_menu_uiinfo[i].user_data = mwin;
	}
	for (i=0; i<MAINWINMENU_EDIT_END; i++) {
		mainwinmenu_edit_menu_uiinfo[i].user_data = mwin;
	}

	for (i=0; i<MAINWINMENU_SETTINGS_END; i++) {
		mainwinmenu_settings_menu_uiinfo[i].user_data = mwin;
	}

	for (i=0; i<MAINWINMENU_HELP_END; i++) {
		mainwinmenu_help_menu_uiinfo[i].user_data = mwin;
	}

	for (i=0; i<POPMAINWINMENU_FILE_END; i++) {
		popmainwinmenu_file_menu_uiinfo[i].user_data = mwin;
	}

	for (i=0; i<POPMAINWINMENU_EDIT_END; i++) {
		popmainwinmenu_edit_menu_uiinfo[i].user_data = mwin;
	}

	for (i=0; i<POPMAINWINMENU_SETTINGS_END; i++) {
		popmainwinmenu_settings_menu_uiinfo[i].user_data = mwin;
	}
	for (i=0; i<POPMAINWINMENU_HELP_END; i++) {
		popmainwinmenu_help_menu_uiinfo[i].user_data = mwin;
	}

	for (i=0; i<CLIPLISTPOPUPMENU_END; i++) {
		cliplistpopupmenu_uiinfo[i].user_data = mwin;
	}
}


void app_update_menus(MainWin *mwin)
{
	GList *selected;

		/* This needs documentation for users */
	if (mwin->prefs->show_menu == FALSE) {
		gtk_widget_show (GTK_WIDGET (cliplistpopupmenu_uiinfo[CLIPLISTPOPUPMENU_FILEMENU].widget));
	} else {
		gtk_widget_hide (GTK_WIDGET (cliplistpopupmenu_uiinfo[CLIPLISTPOPUPMENU_FILEMENU].widget));
	}

	
	if (mwin->openfilelist_inuse==TRUE) {
		/* If the openfile dialog is in use */
		gtk_widget_set_sensitive (GTK_WIDGET (mainwinmenu_file_menu_uiinfo[MAINWINMENU_FILE_OPENITEMFROMDISK].widget), FALSE);
		gtk_widget_set_sensitive (GTK_WIDGET (popmainwinmenu_file_menu_uiinfo[POPMAINWINMENU_FILE_OPENITEMFROMDISK].widget), FALSE);
	} else {
		gtk_widget_set_sensitive (GTK_WIDGET (mainwinmenu_file_menu_uiinfo[MAINWINMENU_FILE_OPENITEMFROMDISK].widget), TRUE);
		gtk_widget_set_sensitive (GTK_WIDGET (popmainwinmenu_file_menu_uiinfo[POPMAINWINMENU_FILE_OPENITEMFROMDISK].widget), TRUE);
	}
	
	if (mwin->savefilelist_inuse==TRUE) {
		/* If the savefile dialog is in use */
		gtk_widget_set_sensitive (GTK_WIDGET (mainwinmenu_file_menu_uiinfo[MAINWINMENU_FILE_SAVEITEMSTODISK].widget), FALSE);
		gtk_widget_set_sensitive (GTK_WIDGET (popmainwinmenu_file_menu_uiinfo[POPMAINWINMENU_FILE_SAVEITEMSTODISK].widget), FALSE);	
		
		gtk_widget_set_sensitive (GTK_WIDGET (mainwinmenu_file_menu_uiinfo[MAINWINMENU_FILE_SAVEALLITEMSTODISK].widget), FALSE);
		gtk_widget_set_sensitive (GTK_WIDGET (popmainwinmenu_file_menu_uiinfo[POPMAINWINMENU_FILE_SAVEALLITEMSTODISK].widget), FALSE);
		
	} else {

		gtk_widget_set_sensitive (GTK_WIDGET (mainwinmenu_file_menu_uiinfo[MAINWINMENU_FILE_SAVEITEMSTODISK].widget), TRUE);
		gtk_widget_set_sensitive (GTK_WIDGET (popmainwinmenu_file_menu_uiinfo[POPMAINWINMENU_FILE_SAVEITEMSTODISK].widget), TRUE);			
		/* the saveallitems menuitem is set sensitive in next if statement */
	}
	
	if ((GTK_CLIST(mwin->cliplist)->rows > 0) && (mwin->savefilelist_inuse==FALSE)) {
		gtk_widget_set_sensitive (GTK_WIDGET (mainwinmenu_file_menu_uiinfo[MAINWINMENU_FILE_SAVEALLITEMSTODISK].widget), TRUE);
		gtk_widget_set_sensitive (GTK_WIDGET (popmainwinmenu_file_menu_uiinfo[POPMAINWINMENU_FILE_SAVEALLITEMSTODISK].widget), TRUE);
	} else {
		gtk_widget_set_sensitive (GTK_WIDGET (mainwinmenu_file_menu_uiinfo[MAINWINMENU_FILE_SAVEALLITEMSTODISK].widget), FALSE);
		gtk_widget_set_sensitive (GTK_WIDGET (popmainwinmenu_file_menu_uiinfo[POPMAINWINMENU_FILE_SAVEALLITEMSTODISK].widget), FALSE);
	}

	if (mwin->copypaste) {
		if (g_list_first (mwin->copypaste)) {
			gtk_widget_set_sensitive (GTK_WIDGET (mainwinmenu_edit_menu_uiinfo[MAINWINMENU_EDIT_PASTE].widget), TRUE);
			gtk_widget_set_sensitive (GTK_WIDGET (popmainwinmenu_edit_menu_uiinfo[POPMAINWINMENU_EDIT_PASTE].widget), TRUE);
			gtk_widget_set_sensitive (GTK_WIDGET (cliplistpopupmenu_uiinfo[CLIPLISTPOPUPMENU_PASTEITEM].widget), TRUE);
			goto ddis;
		} else goto dis;
	} else goto dis;

dis:
	gtk_widget_set_sensitive (GTK_WIDGET (mainwinmenu_edit_menu_uiinfo[MAINWINMENU_EDIT_PASTE].widget), FALSE);
	gtk_widget_set_sensitive (GTK_WIDGET (popmainwinmenu_edit_menu_uiinfo[POPMAINWINMENU_EDIT_PASTE].widget), FALSE);
	gtk_widget_set_sensitive (GTK_WIDGET (cliplistpopupmenu_uiinfo[CLIPLISTPOPUPMENU_PASTEITEM].widget), FALSE);
ddis:	
	
	selected = GTK_CLIST(mwin->cliplist)->selection;
	if (selected) {
		selected = g_list_first (selected);
		if (selected) {
			gtk_widget_set_sensitive (GTK_WIDGET (popmainwinmenu_file_menu_uiinfo[POPMAINWINMENU_FILE_SAVEITEMSTODISK].widget), TRUE);

			gtk_widget_set_sensitive (GTK_WIDGET (popmainwinmenu_edit_menu_uiinfo[POPMAINWINMENU_EDIT_CUT].widget), TRUE);
			gtk_widget_set_sensitive (GTK_WIDGET (popmainwinmenu_edit_menu_uiinfo[POPMAINWINMENU_EDIT_COPY].widget), TRUE);
			gtk_widget_set_sensitive (GTK_WIDGET (mainwinmenu_edit_menu_uiinfo[MAINWINMENU_EDIT_CUT].widget), TRUE);
			gtk_widget_set_sensitive (GTK_WIDGET (mainwinmenu_edit_menu_uiinfo[MAINWINMENU_EDIT_COPY].widget), TRUE);
	
			gtk_widget_set_sensitive (GTK_WIDGET (cliplistpopupmenu_uiinfo[CLIPLISTPOPUPMENU_CUTITEM].widget), TRUE);
			gtk_widget_set_sensitive (GTK_WIDGET (cliplistpopupmenu_uiinfo[CLIPLISTPOPUPMENU_COPYITEM].widget), TRUE);
			gtk_widget_set_sensitive (GTK_WIDGET (cliplistpopupmenu_uiinfo[CLIPLISTPOPUPMENU_EDITITEM].widget), TRUE);
			gtk_widget_set_sensitive (GTK_WIDGET (cliplistpopupmenu_uiinfo[CLIPLISTPOPUPMENU_MOVE_DOWN].widget), TRUE);
			gtk_widget_set_sensitive (GTK_WIDGET (cliplistpopupmenu_uiinfo[CLIPLISTPOPUPMENU_MOVE_UP].widget), TRUE);
			gtk_widget_set_sensitive (GTK_WIDGET (cliplistpopupmenu_uiinfo[CLIPLISTPOPUPMENU_CONVERT].widget), TRUE);

			gtk_widget_set_sensitive (GTK_WIDGET (mwin->toolbar_deleteitem), TRUE);
			gtk_widget_set_sensitive (GTK_WIDGET (mwin->toolbar_edititem), TRUE);
		} else goto deactivate;
	} else goto deactivate;
	return;

deactivate:
	
	gtk_widget_set_sensitive (GTK_WIDGET (popmainwinmenu_edit_menu_uiinfo[POPMAINWINMENU_EDIT_CUT].widget), FALSE);
	gtk_widget_set_sensitive (GTK_WIDGET (popmainwinmenu_edit_menu_uiinfo[POPMAINWINMENU_EDIT_COPY].widget), FALSE);
	gtk_widget_set_sensitive (GTK_WIDGET (mainwinmenu_edit_menu_uiinfo[MAINWINMENU_EDIT_CUT].widget), FALSE);
	gtk_widget_set_sensitive (GTK_WIDGET (mainwinmenu_edit_menu_uiinfo[MAINWINMENU_EDIT_COPY].widget), FALSE);

	gtk_widget_set_sensitive (GTK_WIDGET (cliplistpopupmenu_uiinfo[CLIPLISTPOPUPMENU_CUTITEM].widget), FALSE);
	gtk_widget_set_sensitive (GTK_WIDGET (cliplistpopupmenu_uiinfo[CLIPLISTPOPUPMENU_COPYITEM].widget), FALSE);
	gtk_widget_set_sensitive (GTK_WIDGET (cliplistpopupmenu_uiinfo[CLIPLISTPOPUPMENU_EDITITEM].widget), FALSE);
	gtk_widget_set_sensitive (GTK_WIDGET (cliplistpopupmenu_uiinfo[CLIPLISTPOPUPMENU_MOVE_DOWN].widget), FALSE);
	gtk_widget_set_sensitive (GTK_WIDGET (cliplistpopupmenu_uiinfo[CLIPLISTPOPUPMENU_MOVE_UP].widget), FALSE);
	gtk_widget_set_sensitive (GTK_WIDGET (cliplistpopupmenu_uiinfo[CLIPLISTPOPUPMENU_CONVERT].widget), FALSE);

	gtk_widget_set_sensitive (GTK_WIDGET (popmainwinmenu_file_menu_uiinfo[POPMAINWINMENU_FILE_SAVEITEMSTODISK].widget), FALSE);
	gtk_widget_set_sensitive (GTK_WIDGET (mainwinmenu_file_menu_uiinfo[MAINWINMENU_FILE_SAVEITEMSTODISK].widget), FALSE);

	gtk_widget_set_sensitive (GTK_WIDGET (mwin->toolbar_deleteitem), FALSE);
	gtk_widget_set_sensitive (GTK_WIDGET (mwin->toolbar_edititem), FALSE);
	return;
}








GtkType mainwin_get_type(void)
{
	static GtkType mainwin_type = 0;
	if (!mainwin_type)
	{
		static const GtkTypeInfo mainwin_info =
		{	
			"MainWin",
			sizeof (MainWin),
			sizeof (MainWinClass),
			(GtkClassInitFunc) mainwin_class_init,
			(GtkObjectInitFunc) mainwin_init,
			NULL,
			NULL,
			(GtkClassInitFunc) NULL,
		};
		mainwin_type = gtk_type_unique (GTK_TYPE_WINDOW, &mainwin_info);
	}
	return (mainwin_type);
}

static void mainwin_class_init(MainWinClass *klass)
{
	GtkObjectClass *object_class;
	object_class = (GtkObjectClass*)klass;
	object_class->destroy = mainwin_destroy;
	parent_class = gtk_type_class(GTK_TYPE_WINDOW);
}

static void mainwin_init(MainWin *mwin)
{

}

static void mainwin_destroy(GtkObject *object)
{
	GtkWidget *mwin;

	g_return_if_fail(object != NULL);
	g_return_if_fail(GNOME_IS_MAINWIN(object));
	mwin = GTK_WIDGET(object);
	/*gtk_widget_destroy(mwin);
	gtk_exit(0);*/
	gtk_widget_hide(mwin);
}


gint idle_func(gpointer data)
{
	GDK_THREADS_ENTER();
	check_ctrlsocket();
	GDK_THREADS_LEAVE();
	return TRUE;
}



GtkWidget* mainwin_new (GtkWidget *windowparent)
{
  MainWin *mwin;

  mwin = gtk_type_new(GNOME_TYPE_MAINWIN);
  g_signal_connect(G_OBJECT(mwin), "show", G_CALLBACK(mainwin_show), mwin);
  g_signal_connect(G_OBJECT(mwin), "hide", G_CALLBACK(mainwin_hide), mwin);
  mwin->windowparent = windowparent;

  mwin->prefs = getprefs_fromfile ();
  mwin->prefswin = prefswin_new(GTK_WIDGET(mwin));
  /* Set some pointers to functions that are made available for plugins */
  mwin->plugin_update_item = app_update_item;
  mwin->plugin_update_row = app_update_row;
  mwin->plugin_get_new_item = app_get_new_item;
  mwin->plugin_delete_selected_items = app_delete_selected_items;
  mwin->plugin_add_selection = app_add_selection;
  mwin->plugin_add_text_selection = app_add_text_selection;
  mwin->plugin_update_menus = app_update_menus;
  mwin->plugin_reload_prefs = reload_prefs;
  mwin->plugin_claim_selection = app_claim_selection;

  mwin->plugin_enable_plugin = app_enable_plugin;
  mwin->plugin_disable_plugin = app_disable_plugin;

  mwin->plugin_update_text_selection = app_update_text_selection;
  mwin->plugin_move_selected_up = app_move_selected_up;
  mwin->plugin_move_selected_down = app_move_selected_down;

  mwin->plugin_selection_free = selection_free;
  mwin->plugin_prefs_free = prefs_free;
  mwin->plugin_try_to_get_text_target= app_try_to_get_text_target;
  mwin->plugin_saveasdata_free = saveasdata_free;
  mwin->plugin_show_prefswin = app_show_prefswin;

  mwin->primary_poller=FALSE;
  mwin->primary_poller_is_running = FALSE;

  /* Build the UI */
  gtk_widget_set_size_request(GTK_WIDGET(mwin), 670, 400);
  gtk_window_set_title (GTK_WINDOW (mwin), _("Clipboard Manager"));
  gtk_window_set_policy (GTK_WINDOW (mwin), TRUE, TRUE, FALSE);
  mwin->accel_group = gtk_accel_group_new ();
  gtk_window_add_accel_group (GTK_WINDOW (mwin), mwin->accel_group);


  app_prepare_menus(mwin);

  mwin->topvbox = gtk_vbox_new (FALSE, 0);
  gtk_widget_show (mwin->topvbox);
  gtk_container_add (GTK_CONTAINER (mwin), mwin->topvbox);

  mwin->vbox = gtk_vbox_new (FALSE, 0);
  gtk_widget_show (mwin->vbox); 
  
  gtk_box_pack_start (GTK_BOX (mwin->topvbox), mwin->vbox, TRUE, TRUE, 0);

  mwin->mainwinmenu = gtk_menu_bar_new ();
  gtk_widget_show (mwin->mainwinmenu);

  mwin->menuhandlebox = gtk_handle_box_new ();
  gtk_widget_show (mwin->menuhandlebox);
  
  gtk_box_pack_start (GTK_BOX (mwin->vbox), mwin->menuhandlebox, FALSE, TRUE, 0);

  gtk_container_add (GTK_CONTAINER (mwin->menuhandlebox), mwin->mainwinmenu);

  gnome_app_fill_menu (GTK_MENU_SHELL (mwin->mainwinmenu), mainwinmenu_uiinfo,
                       NULL, FALSE, 0);

  mwin->cliplistpopupmenu = gtk_menu_new ();
  gnome_app_fill_menu (GTK_MENU_SHELL (mwin->cliplistpopupmenu), cliplistpopupmenu_uiinfo,
                       NULL, FALSE, 0);

  mwin->toolbarhandlebox = gtk_handle_box_new ();
  gtk_widget_show (mwin->toolbarhandlebox);
  
  gtk_box_pack_start (GTK_BOX (mwin->vbox), mwin->toolbarhandlebox, FALSE, TRUE, 0);


  mwin->toolbar = gtk_toolbar_new ();

  gtk_toolbar_set_orientation(GTK_TOOLBAR(mwin->toolbar),GTK_ORIENTATION_HORIZONTAL);
  gtk_widget_show (mwin->toolbar);
  gtk_container_add (GTK_CONTAINER (mwin->toolbarhandlebox), mwin->toolbar);

  gtk_container_set_border_width (GTK_CONTAINER (mwin->toolbar), 1);
 
  mwin->tmp_toolbar_icon = gtk_image_new_from_stock (GNOME_STOCK_PIXMAP_NEW, GTK_ICON_SIZE_MENU);

  mwin->toolbar_newitem = gtk_toolbar_append_element (GTK_TOOLBAR (mwin->toolbar),
                                GTK_TOOLBAR_CHILD_BUTTON, NULL, _("New"), _("New item"), NULL,
                                mwin->tmp_toolbar_icon, NULL, NULL);
  gtk_widget_show (mwin->toolbar_newitem);
  mwin->tmp_toolbar_icon = gtk_image_new_from_stock (GNOME_STOCK_PIXMAP_CLOSE, GTK_ICON_SIZE_MENU);

  mwin->toolbar_deleteitem = gtk_toolbar_append_element (GTK_TOOLBAR (mwin->toolbar),
                                GTK_TOOLBAR_CHILD_BUTTON, NULL, _("Delete"), _("Delete items"), NULL,
                                mwin->tmp_toolbar_icon, NULL, NULL);
  gtk_widget_show (mwin->toolbar_deleteitem);
  mwin->tmp_toolbar_icon = gtk_image_new_from_stock (GNOME_STOCK_PIXMAP_SAVE_AS, GTK_ICON_SIZE_MENU);

  mwin->toolbar_edititem = gtk_toolbar_append_element (GTK_TOOLBAR (mwin->toolbar),
                                GTK_TOOLBAR_CHILD_BUTTON, NULL, _("Edit"), _("Edit item"), NULL,
                                mwin->tmp_toolbar_icon, NULL, NULL);
  gtk_widget_show (mwin->toolbar_edititem);
  mwin->tmp_toolbar_icon = gtk_image_new_from_stock (GNOME_STOCK_PIXMAP_CLEAR, GTK_ICON_SIZE_MENU);

  mwin->toolbar_clear = gtk_toolbar_append_element (GTK_TOOLBAR (mwin->toolbar),
                                GTK_TOOLBAR_CHILD_BUTTON, NULL, _("Clear"), _("Clear list"), NULL,
                                mwin->tmp_toolbar_icon, NULL, NULL);
  gtk_widget_show (mwin->toolbar_clear);
  mwin->tmp_toolbar_icon = gtk_image_new_from_stock (GNOME_STOCK_PIXMAP_PREFERENCES, GTK_ICON_SIZE_MENU);

  mwin->toolbar_preferences = gtk_toolbar_append_element (GTK_TOOLBAR (mwin->toolbar),
                                GTK_TOOLBAR_CHILD_BUTTON, NULL, _("Preferences"), _("Preferences"), NULL,
                                mwin->tmp_toolbar_icon, NULL, NULL);
  gtk_widget_show (mwin->toolbar_preferences);
  mwin->tmp_toolbar_icon = gtk_image_new_from_stock (GNOME_STOCK_PIXMAP_REFRESH, GTK_ICON_SIZE_MENU);

  mwin->toolbar_refresh = gtk_toolbar_append_element (GTK_TOOLBAR (mwin->toolbar),
                                GTK_TOOLBAR_CHILD_BUTTON, NULL, _("Get current"),
                                _("Get the current clipboard"), NULL, mwin->tmp_toolbar_icon, NULL, NULL);
  gtk_widget_show (mwin->toolbar_refresh);
  mwin->tmp_toolbar_icon = gtk_image_new_from_stock (GNOME_STOCK_PIXMAP_EXIT, GTK_ICON_SIZE_MENU);

  mwin->toolbar_quit = gtk_toolbar_append_element (GTK_TOOLBAR (mwin->toolbar),
                                GTK_TOOLBAR_CHILD_BUTTON, NULL, _("Exit"), _("Quit application"), NULL,
                                mwin->tmp_toolbar_icon, NULL, NULL);
  gtk_widget_show (mwin->toolbar_quit);

  mwin->scrolledwindow = gtk_scrolled_window_new (NULL, NULL);
  gtk_widget_show (mwin->scrolledwindow);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (mwin->scrolledwindow), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

  gtk_box_pack_start (GTK_BOX (mwin->vbox), mwin->scrolledwindow, TRUE, TRUE, 0);


  app_create_cliplist(mwin);

  gtk_container_add (GTK_CONTAINER (mwin->scrolledwindow), mwin->cliplist);
  g_signal_connect (G_OBJECT (mwin->cliplist), "event",
                   G_CALLBACK (on_cliplist_event), mwin);

  mwin->appbar = gnome_appbar_new (TRUE, TRUE, GNOME_PREFERENCES_NEVER);
  gtk_widget_show (mwin->appbar);
  

  gtk_box_pack_start (GTK_BOX (mwin->topvbox), mwin->appbar, FALSE, TRUE, 0);



  g_signal_connect (G_OBJECT (mwin), "destroy",
                      G_CALLBACK (mainwin_destroy),
                      mwin);

  g_signal_connect (G_OBJECT (mwin->toolbar_newitem), "clicked",
                      G_CALLBACK (on_toolbar_newitem_clicked),
                      mwin);
  g_signal_connect (G_OBJECT (mwin->toolbar_deleteitem), "clicked",
                      G_CALLBACK (on_toolbar_deleteitem_clicked),
                      mwin);
  g_signal_connect (G_OBJECT (mwin->toolbar_edititem), "clicked",
                      G_CALLBACK (on_toolbar_edititem_clicked),
                      mwin);
  g_signal_connect (G_OBJECT (mwin->toolbar_clear), "clicked",
                      G_CALLBACK (on_toolbar_clear_clicked),
                      mwin);
  g_signal_connect (G_OBJECT (mwin->toolbar_preferences), "clicked",
                      G_CALLBACK (on_toolbar_preferences_clicked),
                      mwin);
  g_signal_connect (G_OBJECT (mwin->toolbar_refresh), "clicked",
                      G_CALLBACK (on_toolbar_refresh_clicked),
                      mwin);
  g_signal_connect (G_OBJECT (mwin->toolbar_quit), "clicked",
                      G_CALLBACK (on_toolbar_quit_clicked),
                      mwin);

  g_signal_connect (G_OBJECT (mwin), "event",
                      G_CALLBACK (on_mainwin_event),
                      mwin);
  g_signal_connect (G_OBJECT (mwin->topvbox), "event",
                      G_CALLBACK (on_mainwin_event),
                      mwin);
  g_signal_connect (G_OBJECT (mwin->vbox), "event",
                      G_CALLBACK (on_mainwin_event),
                      mwin);
  g_signal_connect (G_OBJECT (mwin->scrolledwindow), "event",
                      G_CALLBACK (on_mainwin_event),
                      mwin);

  gtk_window_set_wmclass (GTK_WINDOW (mwin), "mainwin", "gcm");

  
  app_createhintsfor_menus(mwin);

  /* Initialize libgcm */
  mwin->timeout = gtk_timeout_add(10, idle_func, NULL); 
  setup_ctrlsocket((gpointer)mwin);
  start_ctrlsocket();


  reload_prefs(mwin);

  prefswin_fill_in_prefs (GNOME_PREFSWIN(mwin->prefswin), mwin->prefs);

  gnome_window_icon_set_from_file (GTK_WINDOW (mwin), PIXMAPPATH"/gcm-app-icon.png");

  gtk_widget_set_sensitive (GTK_WIDGET(mwin), FALSE);
  return GTK_WIDGET(mwin);
}


void app_show_prefswin (MainWin *mwin) 
{
	if (mwin->prefswin==NULL) {
		reload_prefs(mwin);
		mwin->prefswin = prefswin_new(GTK_WIDGET(mwin));
		prefswin_fill_in_prefs (GNOME_PREFSWIN(mwin->prefswin), (gpointer*)mwin->prefs);
	}

	gtk_widget_show(mwin->prefswin);
}


void app_create_cliplist(MainWin *mwin) 
{


  mwin->cliplist = gtk_clist_new (CLIST_FIELD_COUNT);
  gtk_widget_show (mwin->cliplist);
  
  gtk_clist_set_selection_mode (GTK_CLIST(mwin->cliplist), GTK_SELECTION_MULTIPLE);

  gtk_clist_set_column_width (GTK_CLIST (mwin->cliplist), CLIST_DATE_FIELD, 60);

  gtk_clist_set_column_width (GTK_CLIST (mwin->cliplist), CLIST_FROM_FIELD, 65);

  gtk_clist_set_column_auto_resize (GTK_CLIST (mwin->cliplist), CLIST_FROM_FIELD, TRUE);

  gtk_clist_set_column_width (GTK_CLIST (mwin->cliplist), CLIST_DATA_FIELD, 80);
  gtk_clist_column_titles_show (GTK_CLIST (mwin->cliplist));

  mwin->cliplistlabel_time = gtk_label_new (_("Time"));
  gtk_widget_show (mwin->cliplistlabel_time);
  gtk_clist_set_column_widget (GTK_CLIST (mwin->cliplist), CLIST_DATE_FIELD, mwin->cliplistlabel_time);

  mwin->cliplistlabel_from = gtk_label_new (_("From"));
  gtk_widget_show (mwin->cliplistlabel_from);
  gtk_clist_set_column_widget (GTK_CLIST (mwin->cliplist), CLIST_FROM_FIELD, mwin->cliplistlabel_from);

  mwin->cliplistlabel_data = gtk_label_new (_("Data preview"));
  gtk_widget_show (mwin->cliplistlabel_data);
  gtk_clist_set_column_widget (GTK_CLIST (mwin->cliplist), CLIST_DATA_FIELD, mwin->cliplistlabel_data);


  g_signal_connect (G_OBJECT (mwin->cliplist), "select_row",
                      G_CALLBACK (on_cliplist_select_row),
                      mwin);
  g_signal_connect (G_OBJECT (mwin->cliplist), "unselect_row",
                      G_CALLBACK (on_cliplist_unselect_row),
                      mwin);

}

void app_createhintsfor_menus(MainWin *mwin)
{
	gint i;
	/*GDK_CONTROL_MASK | GDK_SHIFT_MASK | GDK_MOD1_MASK*/
	gtk_widget_add_accelerator (mainwinmenu_file_menu_uiinfo[MAINWINMENU_FILE_NEWITEM].widget, "activate",
							  mwin->accel_group, GDK_N, GDK_CONTROL_MASK,
                              GTK_ACCEL_VISIBLE);

	gtk_widget_add_accelerator (mainwinmenu_file_menu_uiinfo[MAINWINMENU_FILE_OPENITEMFROMDISK].widget, "activate",
							  mwin->accel_group, GDK_O, GDK_CONTROL_MASK,
                              GTK_ACCEL_VISIBLE);


	gtk_widget_add_accelerator (mainwinmenu_file_menu_uiinfo[MAINWINMENU_FILE_SAVEITEMSTODISK].widget, "activate",
							  mwin->accel_group, GDK_S, GDK_CONTROL_MASK,
                              GTK_ACCEL_VISIBLE);
	
	gtk_widget_add_accelerator (mainwinmenu_file_menu_uiinfo[MAINWINMENU_FILE_SAVEALLITEMSTODISK].widget, "activate",
							  mwin->accel_group, GDK_S, GDK_CONTROL_MASK | GDK_MOD1_MASK,
                              GTK_ACCEL_VISIBLE);

	gtk_widget_add_accelerator (mainwinmenu_file_menu_uiinfo[MAINWINMENU_FILE_EXIT].widget, "activate",
							  mwin->accel_group, GDK_Q, GDK_CONTROL_MASK,
                              GTK_ACCEL_VISIBLE);

	gtk_widget_add_accelerator (mainwinmenu_edit_menu_uiinfo[MAINWINMENU_EDIT_CUT].widget, "activate",
							  mwin->accel_group, GDK_X, GDK_CONTROL_MASK,
                              GTK_ACCEL_VISIBLE);

	gtk_widget_add_accelerator (mainwinmenu_edit_menu_uiinfo[MAINWINMENU_EDIT_COPY].widget, "activate",
							  mwin->accel_group, GDK_C, GDK_CONTROL_MASK,
                              GTK_ACCEL_VISIBLE);

	gtk_widget_add_accelerator (mainwinmenu_edit_menu_uiinfo[MAINWINMENU_EDIT_PASTE].widget, "activate",
							  mwin->accel_group, GDK_V, GDK_CONTROL_MASK,
                              GTK_ACCEL_VISIBLE);

	gtk_widget_add_accelerator (mainwinmenu_edit_menu_uiinfo[MAINWINMENU_EDIT_SELECT_ALL].widget, "activate",
							  mwin->accel_group, GDK_A, GDK_CONTROL_MASK,
                              GTK_ACCEL_VISIBLE);

	for (i=0; i<MAINWINMENU_FILE_END; i++) {
		if (mainwinmenu_file_menu_uiinfo[i].widget) {
			gtk_widget_ref (mainwinmenu_file_menu_uiinfo[i].widget);
		}
	}
	for (i=0; i<MAINWINMENU_EDIT_END; i++) {
		if (mainwinmenu_edit_menu_uiinfo[i].widget) {
			gtk_widget_ref (mainwinmenu_edit_menu_uiinfo[i].widget);
		}
	}

	for (i=0; i<MAINWINMENU_SETTINGS_END; i++) {
		if (mainwinmenu_settings_menu_uiinfo[i].widget) {
			gtk_widget_ref (mainwinmenu_settings_menu_uiinfo[i].widget);
		}
	}

	for (i=0; i<MAINWINMENU_HELP_END; i++) {
		if (mainwinmenu_help_menu_uiinfo[i].widget) {
			gtk_widget_ref (mainwinmenu_help_menu_uiinfo[i].widget);
		}
	}

	for (i=0; i<POPMAINWINMENU_FILE_END; i++) {
		if (popmainwinmenu_file_menu_uiinfo[i].widget) {
			gtk_widget_ref (popmainwinmenu_file_menu_uiinfo[i].widget);
		}
	}

	for (i=0; i<POPMAINWINMENU_EDIT_END; i++) {
		if (popmainwinmenu_edit_menu_uiinfo[i].widget) {
			gtk_widget_ref (popmainwinmenu_edit_menu_uiinfo[i].widget);
		}
	}

	for (i=0; i<POPMAINWINMENU_SETTINGS_END; i++) {
		if (popmainwinmenu_settings_menu_uiinfo[i].widget) {
			gtk_widget_ref (popmainwinmenu_settings_menu_uiinfo[i].widget);
		}
	}
	for (i=0; i<POPMAINWINMENU_HELP_END; i++) {
		if (popmainwinmenu_help_menu_uiinfo[i].widget) {
			gtk_widget_ref (popmainwinmenu_help_menu_uiinfo[i].widget);
		}
	}

	for (i=0; i<CLIPLISTPOPUPMENU_END; i++) {
		if (cliplistpopupmenu_uiinfo[i].widget) {
			gtk_widget_ref (cliplistpopupmenu_uiinfo[i].widget);
		}
	}
}


GtkWidget* app_create_and_show_newitemwin (MainWin *mwin)
{
	if (mwin->newitemwin==NULL) {
		mwin->newitemwin = newitemwin_new(GTK_WIDGET(mwin));
	}
	gtk_widget_show(mwin->newitemwin);
	return mwin->newitemwin;
}


GtkWidget* app_create_and_show_textitemwin (MainWin *mwin, gint row)
{
	GtkWidget *textitemwin;
	Selection *item;

	item = gcm_decompress_selection((Selection*)gtk_clist_get_row_data (GTK_CLIST(mwin->cliplist), row));

	if ((item != NULL) && (item->compressed==FALSE)) {
		textitemwin = textitemwin_new(GTK_WIDGET(mwin), (gpointer) item);
		gtk_widget_show(GTK_WIDGET(textitemwin));
	}
	return textitemwin;
}


GtkWidget* app_create_and_show_aboutwin (MainWin *mwin)
{
  GtkWidget *thewin;
  GdkPixbuf        *pixbuf;
  GError           *error = NULL;
  gchar *comment;
  const gchar *documenters[] = {
	  "Philip Van Hoof <me@freax.org>",
	  "Daisy Van Lissum <daisy.vanlissum@student.kuleuven.ac.be>",
	  /*"Jamaal Sanford <jamaal@sanfordonline.org>", */
	  /*"Steele <mazeltov@magma.ca>",*/
	  NULL	  
  };
  const gchar *translator_credits = _("translator_credits");
  const gchar *authors[] = {
	"Philip Van Hoof <me@freax.org>",
	/*"Jonas Heylen <jonas.heylen@pandora.be>",*/
	"Dmitry Koval <koval@korris.com.ua>",
	"Greg <greg@reddfish.co.nz>",
    NULL
  };
  
  pixbuf = gdk_pixbuf_new_from_file (PIXMAPPATH"/gcm-app-icon.png", &error);

  if (error) {
         g_warning (G_STRLOC ": cannot open "PIXMAPPATH"/gcm-app-icon.png : %s", error->message);
         g_error_free (error);
  }

  comment = g_strdup (_("This program is responsible for managing "
                        "your selections (a.k.a. clipboards)."));

  thewin = gnome_about_new ( _("GNOME Clipboard Manager"), VERSION,
                        "(C) 2003",
                        _("Released under the GNU General Public License.\n"),
                        authors,
                        documenters,
                        strcmp (translator_credits, "translator_credits") != 0 ? translator_credits : NULL,
                        pixbuf);

  if (pixbuf) gdk_pixbuf_unref (pixbuf);
  gnome_window_icon_set_from_file (GTK_WINDOW (thewin), PIXMAPPATH"/gcm_icon32x32.png");

  gtk_widget_show(thewin);
}


void app_add_plugin (MainWin *mwin, gpointer pe)
{
	GcmPlugin *p = (GcmPlugin*) pe;
#ifdef DEVELOPMENT
		g_print("GCM PLUGIN LOADER: Adding plugin %s\n", p->description);
#endif
	p->init(GTK_WIDGET(mwin));
	mwin->plugin_list = g_list_prepend(mwin->plugin_list, p);

	gtk_list_store_append (GNOME_PREFSWIN(mwin->prefswin)->plugstore, &GNOME_PREFSWIN(mwin->prefswin)->plugiter);

	gtk_list_store_set (GNOME_PREFSWIN(mwin->prefswin)->plugstore, &GNOME_PREFSWIN(mwin->prefswin)->plugiter,
					ENABLED_COLUMN, p->enabled,
					DES_COLUMN, p->description,
					FILEN_COLUMN, p->filename,
					VISIBLE_COLUMN, TRUE,
					WORLD_COLUMN, TRUE,
					-1);

	plugins_on_plugin_added(GTK_WIDGET(mwin), p);
}


void app_remove_plugin (MainWin *mwin, gpointer pe)
{
	GcmPlugin *p = (GcmPlugin*) pe;
	PrefsWin *pwin = GNOME_PREFSWIN(mwin->prefswin);
	gboolean valid;
	GtkTreeIter iter;
	GtkTreeModel *model = GTK_TREE_MODEL(pwin->plugstore);

#ifdef DEVELOPMENT
		g_print("GCM PLUGIN REMOVAL: Removing plugin %s\n", p->description);
#endif

	valid = gtk_tree_model_get_iter_first (model, &iter);
	while (valid)
	{
		gchar *filename_data, *des_data;
		gboolean enabled_data;

		gtk_tree_model_get (model, &iter, 
			DES_COLUMN, &des_data,
			FILEN_COLUMN, &filename_data, 
			ENABLED_COLUMN, &enabled_data, -1);

		if (!(strcmp(p->filename, filename_data))) {
			gtk_list_store_remove (GTK_LIST_STORE (model), &iter);
			break;
		}
		valid = gtk_tree_model_iter_next (model, &iter);
	}

	if (p && p->cleanup)
	{
		/* Start the cleanup function of the plugin */
		p->cleanup();
		GDK_THREADS_LEAVE();
		while(g_main_iteration(FALSE));
		GDK_THREADS_ENTER();
	}
	close_dynamic_lib(p->handle);

}

void app_set_plugin_status (MainWin *mwin, gpointer pe, gboolean status)
{
	GcmPlugin *p = (GcmPlugin*) pe;
	PrefsWin *pwin = GNOME_PREFSWIN(mwin->prefswin);
	gboolean valid;
	GtkTreeIter iter;
	GtkTreeModel *model = GTK_TREE_MODEL(pwin->plugstore);

	p->enabled = status;
#ifdef DEVELOPMENT
	if (status)
		g_print("GCM PLUGIN ENABLE: Changing status of plugin %s\n", p->description);
	else 
		g_print("GCM PLUGIN DISABLE: Changing status of plugin %s\n", p->description);
#endif

	valid = gtk_tree_model_get_iter_first (model, &iter);
	while (valid)
	{
		gchar *filename_data, *des_data;
		gboolean enabled_data;

		gtk_tree_model_get (model, &iter, 
			DES_COLUMN, &des_data,
			FILEN_COLUMN, &filename_data, 
			ENABLED_COLUMN, &enabled_data, -1);

		if (!(strcmp(p->filename, filename_data))) {
			gtk_list_store_set (GNOME_PREFSWIN(mwin->prefswin)->plugstore, &iter,
					ENABLED_COLUMN, p->enabled,
					DES_COLUMN, p->description,
					FILEN_COLUMN, p->filename, -1);
			break;
		}
		valid = gtk_tree_model_iter_next (model, &iter);
	}


}

void app_enable_plugin (MainWin *mwin, gpointer pe)
{
	app_set_plugin_status (mwin, pe, TRUE);
}
void app_disable_plugin(MainWin *mwin,  gpointer pe)
{
	app_set_plugin_status (mwin, pe, FALSE);
}


void saveasdata_free (SaveAsData *sa) 
{
	g_list_free(sa->items);
	g_free(sa->path);
	g_free(sa);
}

void selection_free (Selection *item) 
{
	/* Important! don't lose memory here :) */
	/* Is this necessary? item=gcm_decompress_selection(item); */
	/* Note that data->data can be compressed */

	if (item!=NULL) {
		GList *targets=g_list_copy(item->targets);
		
		if (item->time!=NULL) g_free (item->time);
		if (item->from!=NULL) g_free (item->from);

		while(targets)
		{
			GtkSelectionData *data = (GtkSelectionData*)targets->data;
			if (data==NULL) continue;
			gtk_selection_data_free (data);
			targets=g_list_next(targets);
		}
		g_list_free(targets);
		if (item->targets!=NULL) g_list_free(item->targets);
		if (item->csizes!=NULL) g_list_free (item->csizes);
		if (item!=NULL) g_free(item);
	}
}



void prefs_free (Prefs *p) 
{
	g_free (p);
}


gboolean saveprefs_tofile (Prefs *prefs)
{
	GConfClient *client;
	GError *err=NULL;

	if (prefs != NULL) {
		if (gconf_is_initialized ()) {
			client = gconf_client_get_default();

			if (!gconf_client_set_int (client, "/apps/gcm/prefs/selection_setting", prefs->selection_setting, &err)) return FALSE;
			if (!gconf_client_set_bool (client, "/apps/gcm/prefs/notfirst_time", TRUE, &err)) return FALSE;
			if (!gconf_client_set_bool (client, "/apps/gcm/prefs/beep_on_collect", prefs->beep_on_collect, &err)) return FALSE;
			if (!gconf_client_set_bool (client, "/apps/gcm/prefs/auto_collect", prefs->auto_collect, &err)) return FALSE;
#ifdef DEVELOPMENT
			if (!gconf_client_set_bool (client, "/apps/gcm/prefs/debug", prefs->debug, &err)) return FALSE;
#endif
			if (!gconf_client_set_bool (client, "/apps/gcm/prefs/show_cliplist_titles", prefs->show_cliplist_titles, &err)) return FALSE;
			if (!gconf_client_set_bool (client, "/apps/gcm/prefs/show_statusbar", prefs->show_statusbar, &err)) return FALSE;
			if (!gconf_client_set_bool (client, "/apps/gcm/prefs/show_toolbar", prefs->show_toolbar, &err)) return FALSE;
			if (!gconf_client_set_bool (client, "/apps/gcm/prefs/show_menu", prefs->show_menu, &err)) return FALSE;
			if (!gconf_client_set_bool (client, "/apps/gcm/prefs/show_from", prefs->show_from, &err)) return FALSE;
			if (!gconf_client_set_bool (client, "/apps/gcm/prefs/show_type", prefs->show_type, &err)) return FALSE;
			if (!gconf_client_set_bool (client, "/apps/gcm/prefs/show_time", prefs->show_time, &err)) return FALSE;
				
			if (!gconf_client_set_bool (client, "/apps/gcm/prefs/manage_clipboard", prefs->manage_clipboard, &err)) return FALSE;
			if (!gconf_client_set_bool (client, "/apps/gcm/prefs/manage_primary", prefs->manage_primary, &err)) return FALSE;
			if (!gconf_client_set_bool (client, "/apps/gcm/prefs/manage_secondary", prefs->manage_secondary, &err)) return FALSE;

			if (!gconf_client_set_bool (client, "/apps/gcm/prefs/claim_clipboard", prefs->claim_clipboard, &err)) return FALSE;
			if (!gconf_client_set_bool (client, "/apps/gcm/prefs/claim_primary", prefs->claim_primary, &err)) return FALSE;
			if (!gconf_client_set_bool (client, "/apps/gcm/prefs/claim_secondary", prefs->claim_secondary, &err)) return FALSE;

			if (!gconf_client_set_bool (client, "/apps/gcm/prefs/useplugins", prefs->useplugins, &err)) return FALSE;
			if (!gconf_client_set_bool (client, "/apps/gcm/prefs/show_datap", prefs->show_datap, &err)) return FALSE;
			if (!gconf_client_set_bool (client, "/apps/gcm/prefs/multisel", prefs->multisel, &err)) return FALSE;

			if (!gconf_client_set_bool (client, "/apps/gcm/prefs/unselall", prefs->unselall, &err)) return FALSE;
			if (!gconf_client_set_bool (client, "/apps/gcm/prefs/followmode", prefs->followmode, &err)) return FALSE;
			if (!gconf_client_set_bool (client, "/apps/gcm/prefs/selectnew", prefs->selectnew, &err)) return FALSE;
			if (!gconf_client_set_int (client, "/apps/gcm/prefs/maxitems", prefs->maxitems, &err)) return FALSE;

		} else {
			gnome_error_dialog(_("Unable to initialize GConf. Check your GConf package.")); 
			return FALSE;
		}
	} else {
		return FALSE;
	}
	return TRUE;
}


Prefs* getprefs_fromfile (void)
{
	Prefs *prefs=NULL;
	GConfClient *client;
	GError *err=NULL;


	client = gconf_client_get_default();
	prefs = g_malloc0(sizeof(Prefs));

	
	if (gconf_client_get_bool (client, "/apps/gcm/prefs/notfirst_time", &err)) {

		prefs->selection_setting = gconf_client_get_int (client, "/apps/gcm/prefs/selection_setting", &err);
		prefs->beep_on_collect = gconf_client_get_bool (client, "/apps/gcm/prefs/beep_on_collect", &err);
		prefs->auto_collect = gconf_client_get_bool (client, "/apps/gcm/prefs/auto_collect", &err);
#ifdef DEVELOPMENT
		prefs->debug = gconf_client_get_bool (client, "/apps/gcm/prefs/debug", &err);
#endif
		prefs->show_toolbar = gconf_client_get_bool (client, "/apps/gcm/prefs/show_toolbar", &err);
		prefs->show_menu = gconf_client_get_bool (client, "/apps/gcm/prefs/show_menu", &err);
		prefs->show_cliplist_titles = gconf_client_get_bool (client, "/apps/gcm/prefs/show_cliplist_titles", &err);
		prefs->show_statusbar = gconf_client_get_bool (client, "/apps/gcm/prefs/show_statusbar", &err);
		prefs->show_from = gconf_client_get_bool (client, "/apps/gcm/prefs/show_from", &err);
		prefs->show_type = gconf_client_get_bool (client, "/apps/gcm/prefs/show_type", &err);
		prefs->show_time = gconf_client_get_bool (client, "/apps/gcm/prefs/show_time", &err);
		prefs->show_datap = gconf_client_get_bool (client, "/apps/gcm/prefs/show_datap", &err);
		prefs->multisel = gconf_client_get_bool (client, "/apps/gcm/prefs/multisel", &err);
		prefs->useplugins = gconf_client_get_bool (client, "/apps/gcm/prefs/useplugins", &err);
		
		prefs->manage_clipboard = gconf_client_get_bool (client, "/apps/gcm/prefs/manage_clipboard", &err);
		prefs->manage_primary = gconf_client_get_bool (client, "/apps/gcm/prefs/manage_primary", &err);
		prefs->manage_secondary = gconf_client_get_bool (client, "/apps/gcm/prefs/manage_secondary", &err);

		prefs->claim_clipboard = gconf_client_get_bool (client, "/apps/gcm/prefs/claim_clipboard", &err);
		prefs->claim_primary = gconf_client_get_bool (client, "/apps/gcm/prefs/claim_primary", &err);
		prefs->claim_secondary = gconf_client_get_bool (client, "/apps/gcm/prefs/claim_secondary", &err);

		prefs->unselall = gconf_client_get_bool (client, "/apps/gcm/prefs/unselall", &err);
		prefs->followmode = gconf_client_get_bool (client, "/apps/gcm/prefs/followmode", &err);
		prefs->selectnew = gconf_client_get_bool (client, "/apps/gcm/prefs/selectnew", &err);

		prefs->maxitems = gconf_client_get_int (client, "/apps/gcm/prefs/maxitems", &err);

	} else {
		prefs->selection_setting = SELECTION_SETTING_CLIPBOARD_ONLY;
		prefs->show_menu = TRUE;
		prefs->show_toolbar = TRUE;
#ifdef DEVELOPMENT
		prefs->debug = FALSE;
#endif
		prefs->auto_collect=TRUE;
		prefs->beep_on_collect=FALSE;
		prefs->show_toolbar=TRUE;
		prefs->show_menu=TRUE;
		prefs->show_cliplist_titles=TRUE;
		prefs->show_statusbar=TRUE;
		prefs->show_from=FALSE;
		prefs->show_type=TRUE;
		prefs->useplugins=TRUE;
		prefs->show_time=TRUE;
		prefs->show_datap=TRUE;
		prefs->multisel=TRUE;
		prefs->manage_clipboard=TRUE;
		prefs->manage_primary=FALSE;
		prefs->manage_secondary=FALSE;

		prefs->claim_clipboard=TRUE;
		prefs->claim_primary=FALSE;
		prefs->claim_secondary=FALSE;

		prefs->unselall=TRUE;
		prefs->followmode=TRUE;
		prefs->selectnew=TRUE;
		prefs->maxitems=25;

		plugins_on_firsttime((gpointer)client, (gpointer)prefs);
		saveprefs_tofile (prefs);
	
	}
	return prefs;
}

void reload_maxitems_prefs (MainWin *mwin, gboolean pluginnotify)
{
	app_remove_unwanted_items (mwin);
	if (pluginnotify) plugins_on_reload_prefs(GTK_WIDGET(mwin));

}

void reload_selection_prefs (MainWin *mwin, gboolean pluginnotify, gboolean getitem)
{
	PrefsWin *pwin = GNOME_PREFSWIN(mwin->prefswin);
		switch (mwin->prefs->selection_setting) {
		case SELECTION_SETTING_CLIPBOARD_ONLY :
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(pwin->clipboard_only_radio), TRUE);
			mwin->prefs->manage_primary = FALSE;
			mwin->prefs->claim_primary = FALSE;
			mwin->prefs->manage_clipboard = TRUE;
			mwin->prefs->claim_clipboard = TRUE;
			mwin->prefs->manage_secondary = FALSE;
			mwin->prefs->claim_secondary = FALSE;
		break;
		case SELECTION_SETTING_CLIPBOARD_AND_PRIMARY :
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(pwin->clipboard_and_primary_radio), TRUE);
			mwin->prefs->manage_primary = TRUE;
			mwin->prefs->claim_primary = FALSE;
			mwin->prefs->manage_clipboard = TRUE;
			mwin->prefs->claim_clipboard = TRUE;
			mwin->prefs->manage_secondary = FALSE;
			mwin->prefs->claim_secondary = FALSE;		
		break;
		case SELECTION_SETTING_PRIMARY_ONLY :
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(pwin->primary_only_radio), TRUE);
			mwin->prefs->manage_primary = TRUE;
			mwin->prefs->claim_primary = FALSE;
			mwin->prefs->manage_clipboard = FALSE;
			mwin->prefs->claim_clipboard = FALSE;
			mwin->prefs->manage_secondary = FALSE;
			mwin->prefs->claim_secondary = FALSE;
		break;
		default:
			/* Leave all settings as set by the professional user who knows about gconf-editor */
		break;
	}

	/* If we want to manage the primary */	
	if (mwin->prefs->manage_primary) {
		mwin->primary_poller = TRUE;
		app_start_primary_poller (mwin);
	} else {
		/* This will (also) stop the poller (if one is running) */
		mwin->primary_poller = FALSE;
	}
	/* If we dont claim ownership on the CLIPBOARD but still want to manage it */
	if ((mwin->prefs->claim_clipboard == FALSE) && (mwin->prefs->manage_clipboard)) {
		mwin->clipboard_poller = TRUE;
		app_start_clipboard_poller (mwin);	  
	} else {
		/* This will (also) stop the poller (if one is running) */
		mwin->clipboard_poller = FALSE;
	}

	if (getitem) app_get_new_item(mwin);
	if (pluginnotify) plugins_on_reload_prefs(GTK_WIDGET(mwin));
}

void reload_collect_prefs (MainWin *mwin, gboolean pluginnotify, gboolean getitem)
{
  if (mwin->prefs->auto_collect) {
	  gtk_widget_hide(mwin->toolbar_refresh);
  } else {
	  gtk_widget_show(mwin->toolbar_refresh);
  }
  if (getitem) app_get_new_item(mwin);
  if (pluginnotify) plugins_on_reload_prefs(GTK_WIDGET(mwin));
}

void reload_ui_prefs (MainWin *mwin, gboolean pluginnotify)
{
  if (mwin->prefs->show_toolbar) {
	  gtk_widget_show(mwin->toolbar);
	  gtk_widget_show(mwin->toolbarhandlebox);
  } else {
	  gtk_widget_hide(mwin->toolbar);
	  gtk_widget_hide(mwin->toolbarhandlebox);
  }

  if (mwin->prefs->show_menu) {
	  gtk_widget_show(mwin->mainwinmenu);
	  gtk_widget_show(mwin->menuhandlebox);
  } else {
	  gtk_widget_hide(mwin->mainwinmenu);
	  gtk_widget_hide(mwin->menuhandlebox);
  }

  if (mwin->prefs->show_statusbar) {
	  gtk_widget_show(mwin->appbar);
  } else {
	  gtk_widget_hide(mwin->appbar);
  }

  reload_cliplist_prefs(mwin, pluginnotify);

  if (mwin->prefs->multisel) {
	gtk_widget_show(mainwinmenu_edit_menu_uiinfo[MAINWINMENU_EDIT_SELECT_ALL].widget);
	gtk_widget_show(mainwinmenu_edit_menu_uiinfo[MAINWINMENU_EDIT_SELECT_NONE].widget);
	gtk_widget_show(popmainwinmenu_edit_menu_uiinfo[POPMAINWINMENU_EDIT_SELECT_ALL].widget);
	gtk_widget_show(popmainwinmenu_edit_menu_uiinfo[POPMAINWINMENU_EDIT_SELECT_NONE].widget);
	gtk_clist_set_selection_mode (GTK_CLIST(mwin->cliplist), GTK_SELECTION_MULTIPLE);  
  } else {
  	gtk_widget_hide(mainwinmenu_edit_menu_uiinfo[MAINWINMENU_EDIT_SELECT_ALL].widget);
	gtk_widget_hide(mainwinmenu_edit_menu_uiinfo[MAINWINMENU_EDIT_SELECT_NONE].widget);
  	gtk_widget_hide(popmainwinmenu_edit_menu_uiinfo[POPMAINWINMENU_EDIT_SELECT_ALL].widget);
	gtk_widget_hide(popmainwinmenu_edit_menu_uiinfo[POPMAINWINMENU_EDIT_SELECT_NONE].widget);
	gtk_clist_set_selection_mode (GTK_CLIST(mwin->cliplist), GTK_SELECTION_SINGLE);  
  }
  app_update_menus(mwin);
  if (pluginnotify) plugins_on_reload_prefs(GTK_WIDGET(mwin));
}

void reload_plugin_prefs (MainWin *mwin, gboolean pluginnotify)
{
	PrefsWin *pwin = GNOME_PREFSWIN(mwin->prefswin);

	if (mwin->prefs->useplugins) {
		init_plugins ((gpointer)mwin);
		gtk_widget_set_sensitive(pwin->plugscrolledwindow, TRUE);
		gtk_widget_set_sensitive(pwin->plugtreeview, TRUE);
	} else {
		cleanup_plugins();
		gtk_widget_set_sensitive(pwin->plugscrolledwindow, FALSE);
		gtk_widget_set_sensitive(pwin->plugtreeview, FALSE);
	}
	
	if (pluginnotify) plugins_on_reload_prefs(GTK_WIDGET(mwin));
}


void reload_cliplist_prefs(MainWin *mwin, gboolean pluginnotify)
{
  if (mwin->prefs->show_cliplist_titles) {
	  gtk_clist_column_titles_show (GTK_CLIST(mwin->cliplist));
  } else {
	  gtk_clist_column_titles_hide (GTK_CLIST(mwin->cliplist));
  }

  if (mwin->prefs->show_time) {
	  gtk_clist_set_column_visibility (GTK_CLIST(mwin->cliplist), CLIST_DATE_FIELD, TRUE);
  } else {
	  gtk_clist_set_column_visibility (GTK_CLIST(mwin->cliplist), CLIST_DATE_FIELD, FALSE);
  }

  if (mwin->prefs->show_datap) {
	  gtk_clist_set_column_visibility (GTK_CLIST(mwin->cliplist), CLIST_DATA_FIELD, TRUE);
  } else {
	  gtk_clist_set_column_visibility (GTK_CLIST(mwin->cliplist), CLIST_DATA_FIELD, FALSE);
  }

  if (mwin->prefs->show_from) {
	  gtk_clist_set_column_visibility (GTK_CLIST(mwin->cliplist), CLIST_FROM_FIELD, TRUE);
  } else {
	  gtk_clist_set_column_visibility (GTK_CLIST(mwin->cliplist), CLIST_FROM_FIELD, FALSE);
  }

  if (!mwin->prefs->show_from && !mwin->prefs->show_time && !mwin->prefs->show_datap && !mwin->prefs->show_type) {
	  gtk_clist_set_column_visibility (GTK_CLIST(mwin->cliplist), CLIST_DATA_FIELD, TRUE);
	  gtk_clist_set_column_visibility (GTK_CLIST(mwin->cliplist), CLIST_FROM_FIELD, FALSE);
  }
  if (pluginnotify) plugins_on_reload_prefs(GTK_WIDGET(mwin));
}

void reload_prefs (MainWin *mwin)
{

	if (mwin->prefs == NULL) mwin->prefs = getprefs_fromfile();

	reload_selection_prefs (mwin, FALSE, FALSE);
	reload_collect_prefs (mwin, FALSE, FALSE);
	reload_ui_prefs (mwin, FALSE);

	reload_plugin_prefs (mwin, FALSE);

	reload_maxitems_prefs(mwin, FALSE);

	plugins_on_reload_prefs(GTK_WIDGET(mwin));

}




gboolean app_save_items_to_session (MainWin *mwin)
{
	SaveAsData *sa;
	DIR *dir;
	gchar *str;
	gint row;

	if (mwin == NULL) return FALSE;
	str = g_strconcat(g_get_home_dir(), "/.gcm/", NULL);

	dir = opendir(str);
	if (!dir) {
		 if (mkdir (str, S_IXUSR|S_IRUSR|S_IWUSR|S_IXGRP|S_IRGRP) == -1) {
			 g_print("GCM: Error while trying to create directory (%s) for sessiondata\n", str);
			 g_free(str);
			 return FALSE;
		 }
	} else
		closedir(dir);
	g_free(str);

	sa = g_malloc0(sizeof(SaveAsData));
	sa->items = NULL;

	for (row=0; row< GTK_CLIST(mwin->cliplist)->rows; row++) {
		sa->items = g_list_append(sa->items, gtk_clist_get_row_data (GTK_CLIST(mwin->cliplist), row));
	}
	sa->path = g_strconcat(g_get_home_dir(), "/.gcm/saved_items_by_session.xml", NULL);

	if (gcm_save_items_to_disk(sa->path, sa)==-1) {
		g_print("GCM: IO error while trying to save session\n");
		saveasdata_free(sa);
		return FALSE;
	}
	saveasdata_free(sa);
	return TRUE;
}



void app_load_session_from_disk(MainWin *mwin)
{
	gchar *path = g_strconcat(g_get_home_dir(), "/.gcm/saved_items_by_session.xml", NULL);
	if (app_load_items_from_disk(mwin, path) != LOAD_ERROR_OK) {
		g_print("GCM: Error: Loading session from file\n");
	}
}

gint app_load_items_from_disk(MainWin *mwin, gchar *path)
{
	xmlDocPtr doc;
	xmlNodePtr cur;

#ifdef DEVELOPMENT
	if (mwin->prefs->debug) g_print("GCM: Loading items from disk..\n");
#endif		
	gtk_clist_freeze (GTK_CLIST(mwin->cliplist));
	doc = xmlParseFile(path);
	if (doc == NULL) {
		gtk_clist_thaw (GTK_CLIST(mwin->cliplist));
		return LOAD_ERROR_NOT_XML;
	}
	if (doc == NULL) return;
	cur = xmlDocGetRootElement(doc);
	if (cur == NULL) {
		gtk_clist_thaw (GTK_CLIST(mwin->cliplist));
		xmlFreeDoc(doc);
		return LOAD_ERROR_EMPTY;
	}
	if (xmlStrcmp(cur->name, (const xmlChar *) "gcm")) {
		gtk_clist_thaw (GTK_CLIST(mwin->cliplist));
		xmlFreeDoc(doc);
		return LOAD_ERROR_FORMAT;
	}
	cur = cur->xmlChildrenNode;

	while (cur)
	{
		Selection *item=NULL;
		
		item = (Selection*) malloc(sizeof(Selection));
		memset(item, 0, sizeof(Selection));
		item = gcm_parse_selection(doc, cur);
		if (item != NULL) app_add_selection(mwin, item);
			else g_print(_("GCM: Error: Invalid item in file."));
		cur = cur -> next;
	}
	xmlFreeDoc(doc);
	gtk_clist_thaw (GTK_CLIST(mwin->cliplist));
#ifdef DEVELOPMENT
	if (mwin->prefs->debug) g_print("GCM: Loading items from disk completed\n");
#endif
	return LOAD_ERROR_OK;
}


gint app_load_items_from_mem(MainWin *mwin, gchar *xml, gint size) 
{
	xmlDocPtr doc;
	xmlNodePtr cur;
#ifdef DEVELOPMENT
	if (mwin->prefs->debug) g_print ("GCM: Loading items from mem\n");
#endif	

	gtk_clist_freeze (GTK_CLIST(mwin->cliplist));
	doc = xmlParseMemory(xml, size);
	if (doc == NULL) {
		xmlFreeDoc(doc);
		gtk_clist_thaw (GTK_CLIST(mwin->cliplist));
		return STREAM_ERROR_NOT_XML;
	}
	if (doc == NULL) return;
	cur = xmlDocGetRootElement(doc);
	if (cur == NULL) {
		xmlFreeDoc(doc);
		gtk_clist_thaw (GTK_CLIST(mwin->cliplist));
		return STREAM_ERROR_EMPTY;
	}
	if (xmlStrcmp(cur->name, (const xmlChar *) "gcm")) {
		xmlFreeDoc(doc);
		gtk_clist_thaw (GTK_CLIST(mwin->cliplist));
		return STREAM_ERROR_FORMAT;
	}
	cur = cur->xmlChildrenNode;

	while (cur)
	{
		Selection *item=NULL;
		
		item = (Selection*) malloc(sizeof(Selection));
		memset(item, 0, sizeof(Selection));
#ifdef DEVELOPMENT
	if (mwin->prefs->debug) g_print("GCM: Parsing item..\n");
#endif		
		item = gcm_parse_selection(doc, cur);
#ifdef DEVELOPMENT
	if (mwin->prefs->debug) g_print("GCM: Parsing item completed\n");
#endif
		if (item != NULL) app_add_selection(mwin, item);
			else gnome_error_dialog(_("Invalid item in file."));
		cur = cur -> next;
	}
	xmlFreeDoc(doc);
	gtk_clist_thaw (GTK_CLIST(mwin->cliplist));
#ifdef DEVELOPMENT
	if (mwin->prefs->debug) g_print("Loading items from disk completed\n");
#endif		
	return STREAM_ERROR_OK;
}


void app_delete_selected_items(MainWin *mwin, GtkCList *cliplist)
{
	GList *selected;
	gint row;

	selected = cliplist->selection;
	
	if (!(selected)){
		return;
	} else {
		Selection *item;
		selected = g_list_first(selected);
		row = (gint) selected->data;
		item = gtk_clist_get_row_data (GTK_CLIST(mwin->cliplist), row);
		if (item != NULL)
			plugins_on_remove_item(GTK_WIDGET(mwin), (gpointer)item, row);
		gtk_clist_remove(cliplist, row);
		
		/*recursive function call*/
		app_delete_selected_items(mwin, cliplist);
		if (mwin->prefs->multisel==FALSE) gtk_clist_select_row(GTK_CLIST(mwin->cliplist), row, -1);
	}
}


gint app_add_text_selection (MainWin *mwin, gchar *type, gchar *from, gchar *data)
{
	Selection *item = (Selection*) g_malloc0(sizeof(Selection));
	GtkSelectionData *tdata = (GtkSelectionData*) g_malloc0(sizeof(GtkSelectionData));
	GtkSelectionData *tars = (GtkSelectionData*) g_malloc0(sizeof(GtkSelectionData));
	GdkAtom *atoms_targets;


	time_t timeval=time(0);
	char *tim = malloc (32);

	strftime(tim, 32, "%H:%M:%S", localtime (&timeval));
#ifdef DEVELOPMENT
	if (mwin->prefs->debug)
	g_print("GCM: Adding a new manually created textitem\n");
#endif

	item->time = tim;
	item->from = g_strdup(from);

	tdata->selection = GDK_NONE;
	tdata->target = gdk_atom_intern ("COMPOUND_TEXT", FALSE);
	tdata->type = gdk_atom_intern ("COMPOUND_TEXT", FALSE);
	tdata->format = 8;
	tdata->data = g_strdup(data);
	tdata->length =strlen(data);
	item->targets=NULL;

	tars->selection = GDK_NONE;
	tars->target = gdk_atom_intern("TARGETS", FALSE);
	tars->type = gdk_atom_intern("ATOM", FALSE);
	tars->format = 135697280;
	atoms_targets = (GdkAtom*)g_malloc0(sizeof(GdkAtom));
	atoms_targets[0] = gdk_atom_intern ("COMPOUND_TEXT", FALSE);
	tars->data = (guchar*) atoms_targets;
	tars->length = sizeof(atoms_targets);

	item->targets = g_list_append(item->targets,tdata);
	item->targets = g_list_append(item->targets,tars);
	return app_add_selection(mwin, item);
}


void app_update_text_selection (MainWin *mwin, gint row, gchar *type, gchar *from, gchar *data)
{
	gtk_clist_remove(GTK_CLIST(mwin->cliplist), row);
	app_add_text_selection(mwin, type, from, data);
}


void app_move_selected_up(MainWin *mwin, GList *remaining, gint startrow)
{
	if (mwin->prefs->multisel) {
		remaining = g_list_first(remaining);
			/* FIXME: move all up */
		gtk_clist_row_move (GTK_CLIST(mwin->cliplist), (gint) remaining->data, (gint) remaining->data-1);
	} else {
		remaining = g_list_first(remaining);
		gtk_clist_row_move (GTK_CLIST(mwin->cliplist), (gint) remaining->data, (gint) remaining->data-1);
	}
	
}


void app_move_selected_down(MainWin *mwin, GList *remaining, gint startrow)
{
	if (mwin->prefs->multisel) {
		remaining = g_list_first(remaining);
			/* FIXME: move all down */
		gtk_clist_row_move (GTK_CLIST(mwin->cliplist), (gint) remaining->data, (gint) remaining->data+1);
	} else {
		remaining = g_list_first(remaining);
		gtk_clist_row_move (GTK_CLIST(mwin->cliplist), (gint) remaining->data, (gint) remaining->data+1);
	}
	
}


gint app_merge_selectedrows (MainWin *mwin)
{
		GList *selected = GTK_CLIST(mwin->cliplist)->selection;
		Selection *newitem=NULL, *item=NULL;
		GtkSelectionData *newdata = (GtkSelectionData*) g_malloc0(sizeof(GtkSelectionData));
		GString *buffer = g_string_new("");
		gint row;
		time_t timeval=time(0);
		char *tim = malloc (32);
		strftime(tim, 32, "%H:%M:%S", localtime (&timeval));

		if (selected==NULL) return;
		while (selected) {
			item = gtk_clist_get_row_data(GTK_CLIST(mwin->cliplist),(gint) selected->data);
			if (item!=NULL) {
			  GcmPlugin *g;
			  GList *targets=NULL;
			  gboolean first=TRUE;
			  targets = g_list_copy(item->targets);
			  while (targets) {
				GtkSelectionData *data = (GtkSelectionData*)targets->data;
				gint targetenum = gcm_target_to_enum(data->target);
				if (targetenum==COMPOUND_TEXT_TARGET) {
					if (first) {
						newdata->target = data->target;
						newdata->selection = data->selection;
						newdata->type = data->type;
						newdata->format = data->format;
						first=FALSE;
					}
					buffer = g_string_append(buffer, (gchar*)data->data);
				}
				g = plugins_i_will_handle_that_target(data->target);
				if (g!=NULL) {
					newdata = g->handle_target_merge(newdata, data);
				}
				targets=g_list_next(targets);
			  }
			  g_list_free(targets);
			}
			selected = g_list_next(selected);
		}
		newdata->data = buffer->str;
		newdata->length = buffer->len;
		
		newitem=(Selection*) g_malloc0(sizeof(Selection));
		newitem->targets=NULL;
		newitem->targets = g_list_append(newitem->targets, newdata);
		newitem->time = (gchar*) tim;
		newitem->from = g_strdup("Unknown");
		
		
		g_string_free(buffer, FALSE);
		row = app_add_selection (mwin, newitem);
		return row;
}


void app_update_item (MainWin *mwin, gpointer *user_data) 
{
	gint row=gtk_clist_find_row_from_data (GTK_CLIST(mwin->cliplist), user_data);
	app_update_row (mwin, row);
	plugins_on_update_item(GTK_WIDGET(mwin), row, user_data);
	return;
}

void app_update_row (MainWin *mwin, gint row)
{
	GList *targets=NULL;
	Selection *item = gtk_clist_get_row_data (GTK_CLIST(mwin->cliplist), row);

	if (item != NULL) targets = g_list_copy(item->targets);
		else return;

#ifdef DEVELOPMENT
	if (mwin->prefs->debug) g_print("GCM: Updating listbox\n");
#endif

	if (targets==NULL) return;
	while (targets) {
		GtkSelectionData *data = (GtkSelectionData*)targets->data;
			if (data==NULL) {
				targets=g_list_next(targets);
				continue;
			}
			
				if (data->target == gdk_atom_intern("COMPOUND_TEXT", FALSE)) {

				gchar *preview=gcm_create_datapreview(gtk_clist_get_column_widget (GTK_CLIST(mwin->cliplist), CLIST_DATA_FIELD), data->data,CLIST_SPACE, CLIST_MAXLEN);
				if (preview==NULL) {
					targets=g_list_next(targets);
					continue;
				}
				/*Q: Free the old previewtext ?*/
				gtk_clist_set_text (GTK_CLIST(mwin->cliplist), row, CLIST_DATA_FIELD, preview);
				/*Q: Free this one?*/
				g_free(preview);
				break;
			}
		targets=g_list_next(targets);
	}
	g_list_free(targets);


#ifdef DEVELOPMENT
	if (mwin->prefs->debug) g_print("GCM: Updating listbox completed\n");
#endif			

}


void app_remove_unwanted_items (MainWin *mwin)
{
	while (GTK_CLIST(mwin->cliplist)->rows > mwin->prefs->maxitems)
			gtk_clist_remove(GTK_CLIST(mwin->cliplist), 0);
}

gint app_add_selection (MainWin *mwin, Selection *item)
{
	gchar *text[CLIST_FIELD_COUNT];
	gint row, i;
	GtkAdjustment *adj;
	GList *targets=NULL;

  if (item != NULL) {
    if (item->compressed == TRUE) 
		return ADD_ITEM_ERROR_SELECTION_COMPRESSED;

		adj = gtk_scrolled_window_get_vadjustment (GTK_SCROLLED_WINDOW
						     (mwin->scrolledwindow));

		mwin->lastnewitem = item;
		targets = g_list_copy(item->targets);
#ifdef DEVELOPMENT
	if (mwin->prefs->debug) g_print("GCM: Adding item\n");
#endif
		if (item->time != NULL) text[CLIST_DATE_FIELD] = g_strdup(item->time);
		text[CLIST_DATA_FIELD]=NULL;
		while (targets) {
			GtkSelectionData *data = (GtkSelectionData*)targets->data;
				if (data==NULL) {
					targets=g_list_next(targets);
					continue;
				}
				if (data->target == gdk_atom_intern("COMPOUND_TEXT", FALSE)) {
					
					gchar *preview=gcm_create_datapreview(gtk_clist_get_column_widget (GTK_CLIST(mwin->cliplist), CLIST_DATA_FIELD), data->data,CLIST_SPACE, CLIST_MAXLEN);
					if (preview==NULL) {
						targets=g_list_next(targets);
						continue;
					}
					text[CLIST_DATA_FIELD] = g_strdup(preview);
					g_free(preview);
					break;
				}
			targets=g_list_next(targets);
		}
		g_list_free(targets);
		if (text[CLIST_DATA_FIELD] == NULL) text[CLIST_DATA_FIELD] = g_strdup(_("Unable to create datapreview"));

		if (item->from != NULL) text[CLIST_FROM_FIELD] = g_strdup(item->from);
			else return -1;

		row = gtk_clist_append(GTK_CLIST (mwin->cliplist), text);
		if (mwin->prefs->beep_on_collect) gdk_beep();
		gtk_clist_set_row_data_full (GTK_CLIST(mwin->cliplist), row, item,
			(GtkDestroyNotify)cliplist_row_removed);

		if ((mwin->prefs->multisel)&&(mwin->prefs->unselall)) 
			gtk_clist_unselect_all (GTK_CLIST(mwin->cliplist));
		if (mwin->prefs->selectnew) 
			gtk_clist_select_row(GTK_CLIST(mwin->cliplist), row, 1);
		if (mwin->prefs->followmode) 
			gtk_adjustment_set_value (adj, adj->upper - adj->page_size);
		for (i=0; i<CLIST_FIELD_COUNT; i++) g_free(text[i]);

	app_remove_unwanted_items (mwin);

		plugins_on_add_item(GTK_WIDGET(mwin), (gpointer)item, row);
#ifdef DEVELOPMENT
	if (mwin->prefs->debug) g_print("GCM: Adding item completed\n");
#endif			
		return row;
	} else {
		g_print("GCM: ERROR: Item containted no data!\n");
		return ADD_ITEM_ERROR_SELECTION_CONTAINED_NO_DATA;
	}
}

void app_claim_selection(MainWin *mwin, Selection *item) 
{
	if (mwin->iammanager == FALSE) return;
	item = gcm_decompress_selection(item);

	mwin->currentitem = item;
	if (mwin->prefs->claim_clipboard) {
		GtkClipboard *clipboard = gtk_clipboard_get(GDK_NONE);
		app_claim_selection_on_clipboard (mwin, item, clipboard, GDK_NONE);
	}

	if (mwin->prefs->claim_primary) {
		GtkClipboard *primary = gtk_clipboard_get(GDK_SELECTION_PRIMARY);
		app_claim_selection_on_clipboard (mwin, item, primary, GDK_SELECTION_PRIMARY);
	}

	if (mwin->prefs->claim_secondary) {
		GtkClipboard *secondary = gtk_clipboard_get(GDK_SELECTION_SECONDARY);
		app_claim_selection_on_clipboard (mwin, item, secondary, GDK_SELECTION_PRIMARY);
	}
}

void app_claim_selection_on_clipboard (MainWin *mwin, Selection *item, GtkClipboard *clipboard, GdkAtom selection) 
{
	GtkTargetEntry ttargets[MAXTARGETS];
	GList *targets;
	gint cnt=0, cntd=0;
	MainWinAndItem *itemmwin = (MainWinAndItem*)g_malloc0(sizeof(MainWinAndItem));
	
	item = gcm_decompress_selection(item);
	
	if (mwin->iammanager == FALSE) return;

	itemmwin->mwin=mwin;
	itemmwin->item=item;
	itemmwin->selection=selection;
	itemmwin->clipboard=clipboard;
	
	if (item==NULL) return;
	targets = g_list_copy(item->targets);
	if (targets==NULL) return;

#ifdef DEVELOPMENT
	if (mwin->prefs->debug) g_print("GCM: Claiming selectionownership\n");
#endif

	cntd=g_list_length(targets);
#ifdef DEVELOPMENT
	if (mwin->prefs->debug)
	g_print("GCM: Claiming ownership for %d targets\n", cntd);
#endif
	while ((targets)&&(cnt<MAXTARGETS)) {
	//while ((targets)&&(cnt<mwin->prefs->maxtargets)) {
		GtkSelectionData *data = (GtkSelectionData*)targets->data;
		if (data==NULL) {
			cntd--;
			targets=g_list_next(targets);
			continue;
		}
		/*gtk_selection_add_target (GtkWidget *widget,
                                             GdkAtom selection,
                                             GdkAtom target,
                                             guint info);*/
#ifdef DEVELOPMENT
	if (mwin->prefs->debug)
	g_print("GCM: Target %s added\n", gdk_atom_name(data->type));
#endif
		ttargets[cnt].target = gdk_atom_name(data->type);
		ttargets[cnt].flags = 0;
		ttargets[cnt].info = (guint)data->type;

		cnt++;
		targets=g_list_next(targets);
	}

	if (!gtk_clipboard_set_with_data  (clipboard,
                                  ttargets,
                                  cntd,
                                  (GtkClipboardGetFunc)clipboard_get_selection_cb,
                                  (GtkClipboardClearFunc)clipboard_clear_selection_cb,
                                  itemmwin))
	{
#ifdef DEVELOPMENT
	if (mwin->prefs->debug)
	g_print("GCM: Claiming selectionownership failed, getting new item in a few seconds\n");
#endif
		/*usleep(10000);
		clipboard_clear_selection_cb (clipboard, itemmwin);*/
		gtk_timeout_add(GET_ITEM_WHEN_FAILED_TIMEOUT, get_new_item_from_clipboard_in_a_few_seconds_idle_func,(gpointer) itemmwin); 
	} else {
		plugins_on_selection_claimed(GTK_WIDGET(mwin), clipboard, ttargets, (gpointer)itemmwin->item);
	}
	g_list_free(targets);
	usleep(500); /*On some targetlists it otherwise crashes :-\ */
}




void process_received_selectiondata (MainWin *mwin, Selection *item, GtkSelectionData *data) 
{
	if (data==NULL) return;

	plugins_on_selectiondata_received(GTK_WIDGET(mwin),(gpointer) item, data);

	if (data->target == gdk_atom_intern("COMPOUND_TEXT", FALSE)) {
		time_t timeval=time(0);
		char *tim = malloc (32);
#ifdef DEVELOPMENT
	if (mwin->prefs->debug)
		g_print("GCM: Creating time for item\n");
#endif
		strftime(tim, 32, "%H:%M:%S", localtime (&timeval));

		item->time = (gchar*) tim;
		item->from = g_strdup(_("Unknown")); /*TODO: Not supported yet*/


	}
#ifdef DEVELOPMENT
	if (mwin->prefs->debug)
	g_print("GCM: Adding data (length=%d, format=%d) to list\n", data->length,data->format);
#endif

	item->targets=g_list_append(item->targets, data);
}




void app_try_to_get_text_target (MainWin *mwin)
{
	if ((mwin->prefs->manage_clipboard) && (mwin->prefs->claim_clipboard)) {
		GtkClipboard *clipboard = gtk_clipboard_get(GDK_NONE);
		app_try_to_get_text_target_from_clipboard (mwin, clipboard, GDK_NONE);
	}
	
	if (mwin->prefs->manage_secondary) {
		GtkClipboard *secondary = gtk_clipboard_get(GDK_SELECTION_SECONDARY);
		app_try_to_get_text_target_from_clipboard (mwin, secondary, GDK_SELECTION_SECONDARY);
	}

}

void app_try_to_get_text_target_from_clipboard (MainWin *mwin, GtkClipboard *clipboard, GdkAtom selection)
{
	GtkSelectionData *data=NULL;

#ifdef DEVELOPMENT
	if (mwin->prefs->debug) g_print("GCM: Trying to get the TEXT target from the selectionowner\n");
#endif

	if (clipboard == NULL) {
#ifdef DEVELOPMENT
		if (mwin->prefs->debug) g_print("GCM: No clipboard available\n");
#endif
		return;
	}
#ifdef DEVELOPMENT
	if (mwin->prefs->debug)
	g_print("GCM: Getting alternative clipboard\n");
#endif
	data = gtk_clipboard_wait_for_contents (clipboard , gdk_atom_intern ("COMPOUND_TEXT", FALSE));
	if (data) {
		Selection *item;
		item=(Selection*) g_malloc0(sizeof(Selection));
		item->compressed = FALSE;
		process_received_selectiondata (mwin, item, data);
		app_add_selection (mwin, item);
	} else {
#ifdef DEVELOPMENT
if (mwin->prefs->debug) 
		 g_print("GCM: Giving up, no clipboards found :-(\n");
#endif
		{
			MainWinAndItem *itemmwin = (MainWinAndItem*)g_malloc0(sizeof(MainWinAndItem));
			GtkTargetEntry ttargets[1];
			itemmwin->item = (Selection*)g_malloc0(sizeof(Selection));
			itemmwin->mwin = mwin;
			ttargets[0].target = g_strdup("COMPOUND_TEXT");
			ttargets[0].flags = 0;
			ttargets[0].info = (guint)gdk_atom_intern("COMPOUND_TEXT",FALSE);
#ifdef DEVELOPMENT
if (mwin->prefs->debug) 
		 g_print("GCM: Setting selectionownership\n");
#endif

			gtk_clipboard_set_with_data  (clipboard,
                                  ttargets,
                                  1,
                                  (GtkClipboardGetFunc)clipboard_get_selection_cb,
                                  (GtkClipboardClearFunc)clipboard_clear_selection_cb,
                                  itemmwin);
/*
 * Feature request by Havoc Pennington: Unselect all of Gnome Clipboard Manager is
 * is not giving a clipboard when an application asks for it...
 */
			gtk_clist_unselect_all (GTK_CLIST(mwin->cliplist));
		}
	}

}


void app_get_new_item (MainWin *mwin) 
{
	if (mwin->iammanager == FALSE) return;

	if (mwin->prefs->manage_clipboard) {
		GtkClipboard *clipboard = gtk_clipboard_get(GDK_NONE);
		app_get_new_item_from_clipboard (mwin,  clipboard, GDK_NONE);
	}
	
	if (mwin->prefs->manage_primary) {
		GtkClipboard *primary = gtk_clipboard_get(GDK_SELECTION_SECONDARY);
		app_get_new_item_from_clipboard (mwin,  primary, GDK_SELECTION_SECONDARY);
	}
	
	if (mwin->prefs->manage_secondary) {
		GtkClipboard *secondary = gtk_clipboard_get(GDK_SELECTION_SECONDARY);
		app_get_new_item_from_clipboard (mwin,  secondary, GDK_SELECTION_SECONDARY);
	}

}

void app_get_new_item_from_clipboard (MainWin *mwin, GtkClipboard *clipboard, GdkAtom selection) 
{
	GtkSelectionData *targets=NULL;
	if (mwin->iammanager == FALSE) return;
	/* g_print("Checking %s\n", gdk_atom_name(selection)); */
	
	if ((selection == GDK_SELECTION_PRIMARY) && (mwin->prefs->manage_primary==FALSE)) return;
	if ((selection == GDK_SELECTION_CLIPBOARD) && (mwin->prefs->manage_clipboard==FALSE)) return;
	if ((selection == GDK_SELECTION_SECONDARY) && (mwin->prefs->manage_secondary==FALSE)) return;	
		

#ifdef DEVELOPMENT
	if (mwin->prefs->debug) g_print("GCM: Requesting new item\n");
#endif
/*	g_signal_connect (G_OBJECT (clipboard), "selection_clear_event",
                      GTK_SIGNAL_FUNC (on_clipboard_lost),
                      mwin);*/
	if (clipboard == NULL) {
#ifdef DEVELOPMENT
		if (mwin->prefs->debug) g_print("GCM: No clipboard available\n");
#endif
		return;
	}
	targets = gtk_clipboard_wait_for_contents (clipboard , gdk_atom_intern ("TARGETS", FALSE));
	if (targets==NULL) {
#ifdef DEVELOPMENT
	if (mwin->prefs->debug)
		g_print("GCM: No targets available\n");
#endif
		/* The selectionowner cannot give a TARGETS selection so we will try to get the TEXT atom */
		app_try_to_get_text_target_from_clipboard (mwin, clipboard, selection);
		return;
	}

	if ((targets->type == gdk_atom_intern ("ATOM", FALSE)) || (targets->type == gdk_atom_intern ("TARGETS", FALSE))) {
		gint counter;
		GdkAtom *atoms_targets = NULL;
		Selection *item;
		item=(Selection*) g_malloc0(sizeof(Selection));
		item->compressed = FALSE;
		item->targets = NULL;
		atoms_targets = (GdkAtom *)targets->data;
		if (atoms_targets == NULL) return;

		for(counter = 0; counter < (targets->length / sizeof(GdkAtom)); counter++)
		{
			GtkSelectionData *data=NULL;
			if (atoms_targets[counter] == NULL) continue;
#ifdef DEVELOPMENT
	if (mwin->prefs->debug)
			g_print("GCM: processing target: %s\n", gdk_atom_name(atoms_targets[counter]));
#endif
			
				data = gtk_clipboard_wait_for_contents (clipboard , atoms_targets[counter]);
				if (data) {				
					process_received_selectiondata (mwin, item, data);
				} 
		}
		app_add_selection (mwin, item);
	} else {
		/* The selectionowner cannot give a TARGETS selection so we will try to get the TEXT atom */
		app_try_to_get_text_target_from_clipboard (mwin, clipboard, selection);
		return;
	}
}


void app_start_primary_poller (MainWin *mwin)
{
	
	if ((mwin->primary_poller)&&(mwin->primary_poller_is_running==FALSE)) {	
		ClipboardPollerData *cbpd = g_malloc0(sizeof(ClipboardPollerData));
		cbpd->mwin = mwin;
		cbpd->clipboard = gtk_clipboard_get(GDK_SELECTION_PRIMARY);
		cbpd->selection = XInternAtom(gdk_display, "PRIMARY", FALSE);

		mwin->primary_poller_id = gtk_timeout_add(PRIMARY_POLLER_TIMEOUT, primary_clipboard_idle_func,(gpointer) cbpd); 
	} else {
		mwin->primary_poller_is_running=FALSE;
	}
}


void app_start_clipboard_poller (MainWin *mwin)
{
	
	if ((mwin->clipboard_poller)&&(mwin->clipboard_poller_is_running==FALSE)) {	
		ClipboardPollerData *cbpd = g_malloc0(sizeof(ClipboardPollerData));
		cbpd->mwin = mwin;
		cbpd->clipboard = gtk_clipboard_get(GDK_SELECTION_CLIPBOARD);
		cbpd->selection = XInternAtom(gdk_display, "CLIPBOARD", FALSE);

		mwin->clipboard_poller_id = gtk_timeout_add(CLIPBOARD_POLLER_TIMEOUT, clipboard_clipboard_idle_func,(gpointer) cbpd); 
	} else {
		mwin->clipboard_poller_is_running=FALSE;
	}
}


