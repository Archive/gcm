/* * C o p y r i g h t *(also read COPYING)* * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *  Copyright (C) 2003  Philip Van Hoof <me@freax.org>                         *
 *                                                                             *
 *  This program is free software; you can redistribute it and/or modify       *
 *  it under the terms of the GNU General Public License as published by       *
 *  the Free Software Foundation; either version 2 of the License, or          *
 *  (at your option) any later version.                                        *
 *                                                                             *
 *  This program is distributed in the hope that it will be useful,            *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *  GNU General Public License for more details.                               *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License          *
 *  along with this program; if not, write to the Free Software                *
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA  *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifdef HAVE_CONFIG_H
#  include "../config.h"
#endif
/*#include "config.h"*/
#include <gnome.h>

#ifdef __cplusplus
extern "C" {
#endif

#define GNOME_TYPE_NEWITEMWIN (newitemwin_get_type())
#define GNOME_NEWITEMWIN(obj) (GTK_CHECK_CAST((obj), GNOME_TYPE_NEWITEMWIN, NewItemWin))
#define GNOME_NEWITEMWIN_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass), GNOME_TYPE_NEWITEMWIN, NewItemWinClass))
#define GNOME_IS_NEWITEMWIN(obj) (GTK_CHECK_TYPE((obj), GNOME_TYPE_NEWITEMWIN))
#define GNOME_IS_NEWITEMWIN_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GNOME_TYPE_NEWITEMWIN))

typedef struct _NewItemWin NewItemWin;
typedef struct _NewItemWinClass NewItemWinClass;

struct _NewItemWin {
  GtkWindow window;

  GtkWidget *menubarvbox;
  GtkWidget *menubardock;
  GtkWidget *newitemmenu;
  GtkWidget *scrolledwindow;
  GtkWidget *itemnew;

  GtkTextTagTable *tag;
  GtkTextBuffer *textbuffer;
  GtkWidget *textview;

  /* [2.0] GtkWidget *itemtext; */
  GtkWidget *hbtnbox;


  GtkWidget *newitemok;
  GtkWidget *newitemcancel;
  GtkTooltips *tooltips;

  GtkWidget *windowparent;
};

struct _NewItemWinClass {
	GtkWindowClass parent_class;
};

GtkType newitemwin_get_type(void);
GtkWidget* newitemwin_new (GtkWidget *windowparent);



#ifdef __cplusplus /* cpp compatibility */
}
#endif
