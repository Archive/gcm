/* * C o p y r i g h t *(also read COPYING)* * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *  Copyright (C) 2003  Philip Van Hoof <me@freax.org>                         *
 *                                                                             *
 *  This program is free software; you can redistribute it and/or modify       *
 *  it under the terms of the GNU General Public License as published by       *
 *  the Free Software Foundation; either version 2 of the License, or          *
 *  (at your option) any later version.                                        *
 *                                                                             *
 *  This program is distributed in the hope that it will be useful,            *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *  GNU General Public License for more details.                               *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License          *
 *  along with this program; if not, write to the Free Software                *
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA  *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */


#include "textitemwin.h"
#include "textitemwin_callbacks.h"
#include "mainwin.h"

/*txtwinmenu Callbacks */



void
on_mnuclose_activate                   (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
	TextItemWin *twin = GNOME_TEXTITEMWIN(user_data);
	textitemwin_cancel_and_close (twin, (gpointer*) menuitem);

}


void
on_mnucut_activate                     (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
	TextItemWin *twin = GNOME_TEXTITEMWIN(user_data);
	GtkTextBuffer *buffer;
	GtkClipboard *clipboard;
	if (twin->textview==NULL) return;
	clipboard = gtk_clipboard_get(GDK_NONE);
	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (twin->textview));
	gtk_text_buffer_cut_clipboard (buffer, clipboard, TRUE);
}


void
on_mnucopy_activate                    (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
	TextItemWin *twin = GNOME_TEXTITEMWIN(user_data);
	GtkTextBuffer *buffer;
	GtkClipboard *clipboard;
	if (twin->textview==NULL) return;
	clipboard = gtk_clipboard_get(GDK_NONE);
	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (twin->textview));
	gtk_text_buffer_copy_clipboard (buffer, clipboard);

}


void
on_mnupaste_activate                   (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
	TextItemWin *twin = GNOME_TEXTITEMWIN(user_data);
	GtkTextBuffer *buffer;
	GtkClipboard *clipboard;
	if (twin->textview==NULL) return;
	clipboard = gtk_clipboard_get(GDK_NONE);
	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (twin->textview));
	gtk_text_buffer_paste_clipboard (buffer, clipboard, NULL,TRUE);
}


void
on_mnuselectall_activate               (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
	TextItemWin *twin = GNOME_TEXTITEMWIN(user_data);
	GtkTextBuffer *buffer;
	GtkClipboard *clipboard;
	GtkTextIter start, end;

	if (twin->textview==NULL) return;
	clipboard = gtk_clipboard_get(GDK_NONE);
	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (twin->textview));
	gtk_text_buffer_get_bounds (buffer, &start, &end);
	/*TODO*/
}


void
on_mnuclear_activate                   (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
	TextItemWin *twin = GNOME_TEXTITEMWIN(user_data);
	GtkTextBuffer *buffer;
	GtkTextIter start, end;
	if (twin->textview==NULL) return;
	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (twin->textview));
	gtk_text_buffer_get_bounds (buffer, &start, &end);
	gtk_text_buffer_delete (buffer, &start, &end);
}




void
on_notebook_switch_page                (GtkNotebook *notebook,
                                        GtkNotebookPage *page,
                                        gint page_num,
                                        gpointer user_data)
{
	TextItemWin *twin = GNOME_TEXTITEMWIN(user_data);
	GList *tabs = g_list_copy(twin->tabs);

	twin->textview=NULL;
	while (tabs) {
		TextItemTab *tab = (TextItemTab*) tabs->data;
		if (tab!=NULL) {
			
			if (tab->tabnum == page_num) {
				if ((tab->textview != NULL) && (gtk_text_view_get_editable(GTK_TEXT_VIEW(tab->textview)))) {
					twin->textview = tab->textview;
					break;
				}
			}
		}
		tabs = g_list_next(tabs);
	}
}






void textitemwin_show(GtkWidget *widget, gpointer *user_data)
{ 
}
void textitemwin_hide(GtkWidget *widget, gpointer *user_data)
{
/*	TextItemWin *twin = GNOME_TEXTITEMWIN(user_data);
	textitemwin_cancel_and_close (twin, NULL);*/
	
}


void
on_textitemwin_ok_button_clicked
                                       (GtkButton *button,
                                        gpointer *user_data)
{
	TextItemWin *twin = GNOME_TEXTITEMWIN(user_data);
	texitemwin_ok (twin, (gpointer*) button);
	gtk_widget_destroy (GTK_WIDGET(twin));
}


void
on_textitemwin_cancel_button_clicked
                                       (GtkButton *button,
                                        gpointer *user_data)
{
	TextItemWin *twin = GNOME_TEXTITEMWIN(user_data);
	textitemwin_cancel_and_close (twin, (gpointer*) button);
}



void
on_search_replace_ok_button_clicked    (GtkButton       *button,
                                        gpointer         *user_data)
{
	TextItemTextTargetWidget *ttwin = (TextItemTextTargetWidget*) user_data;
	GtkTextIter start, end;
	GtkTextBuffer *buffer;
	gchar *text, *search, *replace, *textout;

	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (ttwin->textview));
	gtk_text_buffer_get_bounds (buffer, &start, &end);
	text = gtk_text_iter_get_text (&start, &end);

	search = g_strdup(gtk_editable_get_chars (GTK_EDITABLE(ttwin->txttosearchentry), 0, -1));
	replace = g_strdup(gtk_editable_get_chars (GTK_EDITABLE(ttwin->txttargetreplaceentry), 0, -1));
	if (!(strlen(search) <= 0)) {
		textout = g_string_replace(text, search, replace);
		gtk_text_buffer_set_text (buffer, textout, strlen(textout));
	}


}



void on_item_changed                   (GtkWidget        *widget,
                                        gpointer         *user_data)
{
	TextItemWin *twin = GNOME_TEXTITEMWIN(user_data);
	twin->changed = TRUE;
}

void
on_htmltextview_changed                (GtkTextBuffer    *buffer,
                                        gpointer         *user_data)
{
	TextItemHtmlTargetWidget *ttwin = (TextItemHtmlTargetWidget*)user_data;
	TextItemTab *tab;
	if (ttwin==NULL) return;
	tab = ttwin->tab;
	if (tab!=NULL) {
		gchar *text;
		GtkTextIter start, end;
		gtk_text_buffer_get_bounds (buffer, &start, &end);
		text = gtk_text_iter_get_text (&start, &end);
		tab->changed=TRUE;
		if (tab->newdata!=NULL) g_free(tab->newdata);
		tab->newdata = (guchar*) text;
	}
}

void
on_texttextview_changed                (GtkTextBuffer    *buffer,
                                        gpointer         *user_data)
{
	TextItemTextTargetWidget *ttwin = (TextItemTextTargetWidget*)user_data;
	TextItemTab *tab;
	if (ttwin==NULL) return;
	tab = ttwin->tab;
	if (tab!=NULL) {
		gchar *text;
		GtkTextIter start, end;
		gtk_text_buffer_get_bounds (buffer, &start, &end);
		text = gtk_text_iter_get_text (&start, &end);
		tab->changed=TRUE;
		if (tab->newdata!=NULL) g_free(tab->newdata);
		tab->newdata = (guchar*) text;
	}
}



void
on_htmlview_mustupdate                 (GtkTextBuffer    *buffer,
                                        gpointer         *user_data)
{
	TextItemHtmlTargetWidget *ttwin = (TextItemHtmlTargetWidget*)user_data;
	if (ttwin == NULL) return;
	/*if (ttwin->doc != NULL) {*/
		gchar *text;
		GtkTextIter start, end;
		gtk_text_buffer_get_bounds (buffer, &start, &end);
		text = gtk_text_iter_get_text (&start, &end);
	/*
		html_document_clear(ttwin->doc);
		html_document_open_stream (ttwin->doc, "text/html");
		if (strlen(text) < 300) { 
			html_document_write_stream (ttwin->doc, text, -1);
		} else {
			html_document_write_stream (ttwin->doc, "Html data to large", -1);
		}
		html_document_close_stream (ttwin->doc);
	*/
		g_free(text);
	/*}*/
}

void
on_btnRemLastChars_clicked             (GtkButton       *button,
                                        gpointer         *user_data)
{
	
	TextItemTextTargetWidget *ttwin = (TextItemTextTargetWidget*) user_data;
	/*TextItemWin *twin = GNOME_TEXTITEMWIN(ttwin->twin); unused*/

	gchar *text;
	gint r;
	GtkTextIter start, end;
	GtkTextBuffer *buffer;


	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (ttwin->textview));
	gtk_text_buffer_get_bounds (buffer, &start, &end);
	text = gtk_text_iter_get_text (&start, &end);

	
	r = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON(ttwin->sbtnLastChars));
	remove_last_x_chars_from_each_line(text, r);
	gtk_text_buffer_set_text (buffer, text, strlen(text));
	g_free(text);
}

void
on_btnRemFirstChars_clicked            (GtkButton       *button,
                                        gpointer         *user_data)
{
	TextItemTextTargetWidget *ttwin = (TextItemTextTargetWidget*) user_data;
	/*TextItemWin *twin = GNOME_TEXTITEMWIN(ttwin->twin); unused*/

	gchar *text;
	gint r;
	GtkTextIter start, end;
	GtkTextBuffer *buffer;

	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (ttwin->textview));
	gtk_text_buffer_get_bounds (buffer, &start, &end);
	text = gtk_text_iter_get_text (&start, &end);


	r = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON(ttwin->sbtnFirstChars));
	remove_first_x_chars_from_each_line(text, r);
	gtk_text_buffer_set_text (buffer, text, strlen(text));
	g_free(text);
}

void
on_btnRemFirstLines_clicked            (GtkButton       *button,
                                        gpointer         *user_data)
{
	TextItemTextTargetWidget *ttwin = (TextItemTextTargetWidget*) user_data;
	/*TextItemWin *twin = GNOME_TEXTITEMWIN(ttwin->twin); unused*/

	gchar *text;
	gint r;
	GtkTextIter start, end;
	GtkTextBuffer *buffer;


	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (ttwin->textview));
	gtk_text_buffer_get_bounds (buffer, &start, &end);
	text = gtk_text_iter_get_text (&start, &end);


	r = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON(ttwin->sbtnFirstLines));
	remove_first_x_lines(text, r);
	
	gtk_text_buffer_set_text (buffer, text, strlen(text));
	g_free(text);
}

void
on_btnRemLastLines_clicked             (GtkButton       *button,
                                        gpointer         *user_data)
{

	TextItemTextTargetWidget *ttwin = (TextItemTextTargetWidget*) user_data;
	/*TextItemWin *twin = GNOME_TEXTITEMWIN(ttwin->twin); unused*/

	gchar *text;
	gint r;
	GtkTextIter start, end;
	GtkTextBuffer *buffer;


	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (ttwin->textview));
	gtk_text_buffer_get_bounds (buffer, &start, &end);
	text = gtk_text_iter_get_text (&start, &end);


	r = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON(ttwin->sbtnLastLines));
	remove_last_x_lines(text, r);

	
	gtk_text_buffer_set_text (buffer, text, strlen(text));
	g_free(text);
}


void
on_btnInvert_clicked                   (GtkButton       *button,
                                        gpointer         *user_data)
{
	TextItemTextTargetWidget *ttwin = (TextItemTextTargetWidget*) user_data;
	/*TextItemWin *twin = GNOME_TEXTITEMWIN(ttwin->twin); unused*/

	gchar *text, *textout;
	gint len;
	GtkTextIter start, end;
	GtkTextBuffer *buffer;


	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (ttwin->textview));
	gtk_text_buffer_get_bounds (buffer, &start, &end);
	text = gtk_text_iter_get_text (&start, &end);

	len = strlen(text);
	if (len > 1) {
		textout = (gchar*) g_malloc0(sizeof(gchar) * len);
		revstr(text, len+1, textout);
		//textout = g_strreverse (text);
		gtk_text_buffer_set_text (buffer, textout, strlen(textout));
		g_free(textout);
	}
	g_free(text);
}
