/* * C o p y r i g h t *(also read COPYING)* * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *  Copyright (C) 2003  Philip Van Hoof <me@freax.org>                         *
 *                                                                             *
 *  This program is free software; you can redistribute it and/or modify       *
 *  it under the terms of the GNU General Public License as published by       *
 *  the Free Software Foundation; either version 2 of the License, or          *
 *  (at your option) any later version.                                        *
 *                                                                             *
 *  This program is distributed in the hope that it will be useful,            *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *  GNU General Public License for more details.                               *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License          *
 *  along with this program; if not, write to the Free Software                *
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA  *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */


#include <gconf/gconf.h>

#include <gconf/gconf-client.h>

#include "prefswin.h"
#include "prefswin_callbacks.h"
#include "mainwin.h"
#include "plugin.h"


void prefswin_show(GtkWidget *widget, gpointer user_data)
{

}

void prefswin_hide(GtkWidget *widget, gpointer user_data)
{
}

void
on_close_button_clicked
                                       (GtkButton    *button,
                                        gpointer     user_data)
{
	gtk_widget_destroy(GTK_WIDGET(user_data));
}


void
on_plugconfig_button_clicked
                                       (GtkButton    *button,
                                        gpointer     user_data)
{
	PrefsWin *pwin = GNOME_PREFSWIN(user_data);
	GtkTreeSelection *selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (pwin->plugtreeview));
	GtkTreeIter iter= pwin->plugiter;
    GtkTreeModel *model;

	gchar *filename;

	if (gtk_tree_selection_get_selected (selection, &model, &iter)) {
		GList *mylist = g_list_copy(GNOME_MAINWIN(pwin->windowparent)->plugin_list);
		gtk_tree_model_get (model, &iter, FILEN_COLUMN, &filename, -1);
		while (mylist) {
			GcmPlugin *p = (GcmPlugin*) mylist->data;
			if (!strcmp(p->filename, filename)) {
				p->configure(GTK_WIDGET(GNOME_MAINWIN(pwin->windowparent)));
			}
			mylist = g_list_next(mylist);
		}
		g_list_free(mylist);
		g_free (filename);
	}

}

void
on_plugabout_button_clicked
                                       (GtkButton    *button,
                                        gpointer     user_data)
{
	PrefsWin *pwin = GNOME_PREFSWIN(user_data);
	GtkTreeSelection *selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (pwin->plugtreeview));
	GtkTreeIter iter= pwin->plugiter;
    GtkTreeModel *model;

	gchar *filename;

	if (gtk_tree_selection_get_selected (selection, &model, &iter)) {
		GList *mylist = g_list_copy(GNOME_MAINWIN(pwin->windowparent)->plugin_list);
		gtk_tree_model_get (model, &iter, FILEN_COLUMN, &filename, -1);
		while (mylist) {
			GcmPlugin *p = (GcmPlugin*) mylist->data;
			if (!strcmp(p->filename, filename)) {
				p->about(GTK_WIDGET(GNOME_MAINWIN(pwin->windowparent)));
			}
			mylist = g_list_next(mylist);
		}
		g_list_free(mylist);
		g_free (filename);
	}

}





void
on_collect_changed  (GtkWidget    *toggle,
                     gpointer     user_data)
{
	PrefsWin *pwin = GNOME_PREFSWIN(user_data);
	GConfClient* client = GCONF_CLIENT(pwin->client);

	gconf_client_set_bool (client, "/apps/gcm/prefs/auto_collect",
                          gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON(pwin->colauto)), NULL);

}



void
on_maxitems_changed (GtkWidget *spin,
					 gpointer user_data)
{
	PrefsWin *pwin = GNOME_PREFSWIN(user_data);
	GConfClient* client = GCONF_CLIENT(pwin->client);

	gconf_client_set_int (client, "/apps/gcm/prefs/maxitems",
                          gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON(pwin->colmaxspin)), NULL);

}


void
on_beep_changed  (GtkWidget    *toggle,
				gpointer     user_data)
{
	PrefsWin *pwin = GNOME_PREFSWIN(user_data);
	GConfClient* client = GCONF_CLIENT(pwin->client);

	gconf_client_set_bool (client, "/apps/gcm/prefs/beep_on_collect",
                          gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON(pwin->colbeep)), NULL);
}




void on_clipboard_only_toggled (GtkToggleButton *togglebutton,
                                            gpointer user_data)
{
	PrefsWin *pwin = GNOME_PREFSWIN(user_data);
	GConfClient* client = GCONF_CLIENT(pwin->client);

	gconf_client_set_int (client, "/apps/gcm/prefs/selection_setting",
					SELECTION_SETTING_CLIPBOARD_ONLY, NULL);
}

void on_clipboard_and_primary_toggled (GtkToggleButton *togglebutton,
                                            gpointer user_data)
{
	PrefsWin *pwin = GNOME_PREFSWIN(user_data);
	GConfClient* client = GCONF_CLIENT(pwin->client);

	gconf_client_set_int (client, "/apps/gcm/prefs/selection_setting",
					SELECTION_SETTING_CLIPBOARD_AND_PRIMARY, NULL);

}

void on_primary_only_toggled (GtkToggleButton *togglebutton,
                                            gpointer user_data)
{
	PrefsWin *pwin = GNOME_PREFSWIN(user_data);
	GConfClient* client = GCONF_CLIENT(pwin->client);

	gconf_client_set_int (client, "/apps/gcm/prefs/selection_setting",
					SELECTION_SETTING_PRIMARY_ONLY, NULL);

}



void
plugin_enabled_item_toggled (GtkCellRendererToggle *cell,
              gchar                 *path_str,
              gpointer               data)
{
  PrefsWin *pwin =  GNOME_PREFSWIN(data);
  MainWin *mwin = GNOME_MAINWIN(pwin->windowparent);
  GcmPlugin *plugin;
  GtkTreeModel *model = gtk_tree_view_get_model (GTK_TREE_VIEW(pwin->plugtreeview));
  GtkTreePath *path = gtk_tree_path_new_from_string (path_str);
  GtkTreeIter iter;
  gboolean enabled_data;
  gchar *filename_data, *des_data;

  gtk_tree_model_get_iter (model, &iter, path);
  gtk_tree_model_get (model, &iter, 
			DES_COLUMN, &des_data,
			FILEN_COLUMN, &filename_data, 
			ENABLED_COLUMN, &enabled_data, -1);

  plugin = get_plugin_pointer_by_filename (GTK_WIDGET(mwin), filename_data);


  if (plugin != NULL) {
	GConfClient* client = GCONF_CLIENT(pwin->client);
	gchar *confkey = PLUGIN_ENABLED_CONFIG_KEY (*plugin);
	if (enabled_data) {
		gconf_client_set_bool (client, confkey, FALSE, NULL);
	  } else {
		gconf_client_set_bool (client, confkey, TRUE, NULL);
	  }
	g_free(confkey);
  }
  gtk_tree_path_free (path);
}
