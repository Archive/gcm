/* * C o p y r i g h t *(also read COPYING)* * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *  Copyright (C) 2003  Philip Van Hoof <me@freax.org>                         *
 *                                                                             *
 *  This program is free software; you can redistribute it and/or modify       *
 *  it under the terms of the GNU General Public License as published by       *
 *  the Free Software Foundation; either version 2 of the License, or          *
 *  (at your option) any later version.                                        *
 *                                                                             *
 *  This program is distributed in the hope that it will be useful,            *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *  GNU General Public License for more details.                               *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License          *
 *  along with this program; if not, write to the Free Software                *
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA  *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include <unistd.h>
#include <stdio.h>
#include "textitemwin.h"
#include "mainwin.h"
/*#include "config.h"*/
#include "textitemwin_callbacks.h"
#include "plugin.h"
#include "../libgcm/libgcm.h"
/*#include <gtkmozembed.h>*/

static void textitemwin_class_init(TextItemWinClass *klass);
static void textitemwin_init(TextItemWin *twin);
static void textitemwin_destroy(GtkObject *object);

static GtkWindowClass *parent_class = NULL;

Selection *itemp=NULL;

static gulong XA_COMPOUND_TEXT = 0;

static const struct {
  gulong      *atom;
  const gchar *atom_name;
} atoms[] = {
  { &XA_COMPOUND_TEXT,                  "COMPOUND_TEXT", },
};




static GnomeUIInfo mnufilemenu_uiinfo[] =
{
#define TEXTITEMWIN_FILEMENU_CLOSE 0
  GNOMEUIINFO_MENU_EXIT_ITEM (on_mnuclose_activate, NULL),
#define TEXTITEMWIN_FILEMENU_EMD 1
  GNOMEUIINFO_END
};

static GnomeUIInfo mnueditmenu_uiinfo[] =
{
#define TEXTITEMWIN_EDITMENU_CUT 0
  GNOMEUIINFO_MENU_CUT_ITEM (on_mnucut_activate, NULL),
#define TEXTITEMWIN_EDITMENU_COPY 1
  GNOMEUIINFO_MENU_COPY_ITEM (on_mnucopy_activate, NULL),
#define TEXTITEMWIN_EDITMENU_PASTE 2
  GNOMEUIINFO_MENU_PASTE_ITEM (on_mnupaste_activate, NULL),
#define TEXTITEMWIN_EDITMENU_SELECTALL 3
  GNOMEUIINFO_MENU_SELECT_ALL_ITEM (on_mnuselectall_activate, NULL),
#define TEXTITEMWIN_EDITMENU_CLEAR 4
  GNOMEUIINFO_MENU_CLEAR_ITEM (on_mnuclear_activate, NULL),
#define TEXTITEMWIN_EDITMENU_END 5
  GNOMEUIINFO_END
};

static GnomeUIInfo menubar_uiinfo[] =
{
#define TEXTITEMWIN_MENU_FILE 0
  GNOMEUIINFO_MENU_FILE_TREE (mnufilemenu_uiinfo),
#define TEXTITEMWIN_MENU_EDIT 1
  GNOMEUIINFO_MENU_EDIT_TREE (mnueditmenu_uiinfo),
#define TEXTITEMWIN_MENU_END 2
  GNOMEUIINFO_END
};


GtkType textitemwin_get_type(void)
{ /* textitemwin gettype */
   static GtkType textitemwin_type = 0;
   
   if (!textitemwin_type)
   {
      static const GtkTypeInfo textitemwin_info =
      {	
         "TextItemWin",
         sizeof (TextItemWin),
         sizeof (TextItemWinClass),
         (GtkClassInitFunc) textitemwin_class_init,
         (GtkObjectInitFunc) textitemwin_init,
         NULL,
         NULL,
         (GtkClassInitFunc) NULL,
      };
      
      textitemwin_type = gtk_type_unique (GTK_TYPE_WINDOW, &textitemwin_info);
   }

   return (textitemwin_type);
}

static void textitemwin_class_init(TextItemWinClass *klass)
{ /* textitemwin classinit */
   GtkObjectClass *object_class;
   object_class = (GtkObjectClass*)klass;
   object_class->destroy = textitemwin_destroy;
   parent_class = gtk_type_class(GTK_TYPE_WINDOW);
}

static void textitemwin_init(TextItemWin *twin)
{ /* textitemwin init */

}


static void textitemwin_destroy(GtkObject *object)
{ /* ontextitemwin destroy */
   GtkWidget *twin;
  
   g_return_if_fail(object != NULL);
   g_return_if_fail(GNOME_IS_TEXTITEMWIN(object));
   
   twin = GTK_WIDGET(object);
      
   gtk_widget_destroy(twin);
}



void textitemwin_createhintsfor_menus(TextItemWin *twin)
{
	gint i;
	for (i=0; i<TEXTITEMWIN_FILEMENU_EMD; i++) {
		if (mnufilemenu_uiinfo[i].widget) {
			gtk_widget_ref (mnufilemenu_uiinfo[i].widget);
			/* [2.0] gtk_accel_group_attach(mwin->accel_group, GTK_OBJECT(mainwinmenu_file_menu_uiinfo[i].widget));*/
		}
	}

	for (i=0; i<TEXTITEMWIN_EDITMENU_END; i++) {
		if (mnueditmenu_uiinfo[i].widget) {
			gtk_widget_ref (mnueditmenu_uiinfo[i].widget);
			/* [2.0] gtk_accel_group_attach(mwin->accel_group, GTK_OBJECT(mainwinmenu_file_menu_uiinfo[i].widget));*/
		}
	}

	for (i=0; i<TEXTITEMWIN_MENU_END; i++) {
		if (menubar_uiinfo[i].widget) {
			gtk_widget_ref (menubar_uiinfo[i].widget);
		}
	}

}

void textitemwin_prepare_menus(TextItemWin *twin)
{ /* Creates all menu's */
	gint i;

	for (i=0; i<TEXTITEMWIN_FILEMENU_EMD; i++) {
		mnufilemenu_uiinfo[i].user_data = twin;
	}

	for (i=0; i<TEXTITEMWIN_EDITMENU_END; i++) {
		mnueditmenu_uiinfo[i].user_data = twin;
	}

	for (i=0; i<TEXTITEMWIN_MENU_END; i++) {
		menubar_uiinfo[i].user_data = twin;
	}
}



/*
static void
mozilla_init_home (void)
{
        char *mozilla_five_home;
        mozilla_five_home = g_strdup (g_getenv ("MOZILLA_FIVE_HOME"));
        gtk_moz_embed_set_comp_path (mozilla_five_home);
        g_free (mozilla_five_home);
}

void
mozilla_init_profile (void)
{
        char *profile_path;
        profile_path = g_build_filename (g_get_home_dir (),
                                         ".gcm",
                                         NULL);
        gtk_moz_embed_set_profile_path (profile_path, "htmlwidget");
        g_free (profile_path);
}
*/
GtkWidget* textitemwin_new (GtkWidget *windowparent, gpointer *user_data)
{ /* Creates a new textitemwin */
  TextItemWin *twin;
  gint x=0;
  GList *tabs=NULL, *targets=NULL;
  Selection *item = (Selection*)user_data;
  itemp=item;
  twin->item = (gpointer)item;
	
	if (item->compressed==TRUE) g_print(_("GCM: Warning! this item is still compressed\n"));
/*
  twin->plugin_create_texttargetwidget = create_texttargetwidget;
  twin->plugin_create_htmltargetwidget = create_htmltargetwidget;
  twin->plugin_create_tarstargetwidget = create_tarstargetwidget;
  twin->plugin_create_timetargetwidget = create_timetargetwidget;

  twin->plugin_prepare_menus = textitemwin_prepare_menus;
  twin->plugin_createhintsfor_menus = textitemwin_createhintsfor_menus;

  twin->plugin_revstr = revstr;
  twin->plugin_remove_first_x_chars_from_each_line = remove_first_x_chars_from_each_line;
  twin->plugin_remove_last_x_chars_from_each_line = remove_last_x_chars_from_each_line;
  twin->plugin_remove_first_x_lines = remove_first_x_lines;
  twin->plugin_remove_last_x_lines = remove_last_x_lines;

  twin->plugin_search_replace_using_sed = search_replace_using_sed;
  twin->plugin_string_replace = g_string_replace;
*/
  twin->tabs=NULL;
  twin = gtk_type_new(GNOME_TYPE_TEXTITEMWIN);
  g_signal_connect(G_OBJECT(twin), "show", G_CALLBACK(textitemwin_show), twin);
  g_signal_connect(G_OBJECT(twin), "hide", G_CALLBACK(textitemwin_hide), twin);
  twin->windowparent = windowparent;
  gtk_window_set_transient_for (GTK_WINDOW(twin), GTK_WINDOW(twin->windowparent));
  textitemwin_prepare_menus(twin);

  gtk_widget_set_size_request (GTK_WIDGET (twin), 450, 550);
  gtk_window_set_title (GTK_WINDOW (twin), _("Editing item"));
  gtk_window_set_policy (GTK_WINDOW (twin), TRUE, TRUE, FALSE);


  twin->toplevelvbox = gtk_vbox_new (FALSE, 0);
  gtk_widget_show (twin->toplevelvbox);
  gtk_container_add (GTK_CONTAINER (twin), twin->toplevelvbox);

  twin->menubar = gtk_menu_bar_new ();
  gtk_widget_show (twin->menubar);
  gtk_box_pack_start (GTK_BOX (twin->toplevelvbox), twin->menubar, FALSE, FALSE, 0);
  gnome_app_fill_menu (GTK_MENU_SHELL (twin->menubar), menubar_uiinfo,
                       NULL, FALSE, 0);

/* The toplevel notebook */
  twin->notebook = gtk_notebook_new ();
  gtk_widget_show (twin->notebook);
  gtk_box_pack_start (GTK_BOX (twin->toplevelvbox), twin->notebook, TRUE, TRUE, 0);
  gtk_notebook_set_scrollable (GTK_NOTEBOOK(twin->notebook), TRUE);


	targets = g_list_copy(item->targets);
	while (targets) {
		GtkSelectionData *data = (GtkSelectionData*)targets->data;
		if (data==NULL) {
			targets=g_list_next(targets);
			continue;
		} else {
			gboolean h=FALSE;
			gint targetenum;
			GcmPlugin *g;
			TextItemTab *mytab = (TextItemTab*)g_malloc0(sizeof(TextItemTab));
			mytab->data = data;
			mytab->codeset=NULL;
			mytab->textview=NULL;
			mytab->changed=FALSE;
			
		g = plugins_i_will_handle_that_target(data->target);
		if (g!=NULL)
				h = g->handle_target_tab((gpointer)mytab, data);

		if (!(h)&&(data->data != NULL)) {
			targetenum = gcm_target_to_enum(data->target);
			/* Feature request: add extra types :-) or write plugins for them */
			
		  switch (targetenum) {
			case COMPOUND_TEXT_TARGET :
			case TEXT_TARGET :
			case UTF8_STRING_TARGET :
			case STRING_TARGET :
			case UTF_8_TARGET :
			case TEXTPLAIN_CHARSET_UTF_8_TARGET :
				{TextItemTextTargetWidget *ttwin;
				ttwin = create_texttargetwidget(twin, mytab, data);
				mytab->tabwidget = GTK_WIDGET(ttwin->txttargetvbox);
				mytab->textview = ttwin->textview;
				ttwin->tab = mytab;
				}break;
			case TEXTHTML_TARGET :
				{TextItemHtmlTargetWidget *ttwin;
				ttwin = create_htmltargetwidget(twin, mytab, data);
				mytab->tabwidget = GTK_WIDGET(ttwin->htmltargetvbox);
				mytab->textview = ttwin->textview;
				ttwin->tab = mytab;
				}break; 
			
				case TEXTUNICODE_TARGET:
				{TextItemTextTargetWidget *ttwin;
				gint i;
				guchar *mydata;

				mydata = (guchar*) g_convert (data->data, data->length,"UTF-8", "UCS-2" ,NULL,&i, NULL);

				if ((mydata == NULL)||(!(g_utf8_validate (mydata, i, NULL)))) {
					GtkWidget *label = gtk_label_new("Converting to UTF-8 failed");
					ttwin->txttargetvbox = gtk_vbox_new (FALSE, 0);
					gtk_widget_show (ttwin->txttargetvbox);
					gtk_box_pack_start (GTK_BOX (ttwin->txttargetvbox), label, TRUE, TRUE, 0);
					gtk_widget_show(label);
					mytab->tabwidget = GTK_WIDGET(ttwin->txttargetvbox);
					ttwin->tab = mytab;
				} else {
					
					GtkSelectionData *newdata = gtk_selection_data_copy(data);

					g_free(newdata->data);
					newdata->data = mydata;
					newdata->length = i;
					ttwin = create_texttargetwidget(twin, mytab, newdata);
					if (mytab != NULL) mytab->codeset=g_strdup("UCS-2");
					mytab->tabwidget = GTK_WIDGET(ttwin->txttargetvbox);
					mytab->textview = ttwin->textview;
					ttwin->tab = mytab;

				}
				}break;
			case TARGETS_TARGET :
				{TextItemTargetsTargetWidget *ttwin;
				ttwin = create_tarstargetwidget(twin, mytab, data);
				mytab->tabwidget = GTK_WIDGET(ttwin->tarstargetvbox);
				mytab->textview = ttwin->textview;
				ttwin->tab = mytab;
				}break;
			case TIMESTAMP_TARGET :
				{TextItemTimeStampTargetWidget *ttwin;
				ttwin = create_timetargetwidget(twin, mytab, data);
				mytab->tabwidget = GTK_WIDGET(ttwin->tarstargetvbox);
				mytab->textview = ttwin->textview;
				ttwin->tab = mytab;
				}break;
			default:
				{
					GtkWidget *unsupportedlabel1;
					mytab->textview=NULL;
					mytab->tabwidget = gtk_vbox_new (FALSE, 0);
					unsupportedlabel1 = gtk_label_new(
					  _("This is an unsupported target type in the editwindow. If\n"
						"you know how, feel free to contribute support for it.\n\n"
			  
						"You probably want to checkout the COMPOUND_TEXT and TEXT\n"
						"tabs or the 'text/html' tabs. These tabs\n"
						"will give you the ability to modify the actual data of\n"
						"the selection\n\n"

						"Note that it might be necessary to update all targets with\n"
						"the same name in order to modify a specific target type\n"
						"successfully. This is known to be necessary for the 'text/html'\n"
						"target when copypasting using Mozilla composer.\n\n"
					  ));
					gtk_widget_show(unsupportedlabel1);
					gtk_box_pack_start (GTK_BOX (mytab->tabwidget), unsupportedlabel1, TRUE, FALSE, 0);
				}break;
		  }
		}
		  mytab->tablabel = gtk_label_new (gdk_atom_name(data->target));
		  mytab->tabparent = twin->notebook;
		  twin->tabs = g_list_append(twin->tabs, mytab);
	  }
		targets=g_list_next(targets);
  }
  g_list_free(targets);


	x=0; tabs = g_list_copy(twin->tabs);
	while (tabs) {
		TextItemTab *tab = (TextItemTab*)tabs->data;
		tab->tabnum = x;
		gtk_widget_show (tab->tablabel);
		gtk_label_set_justify (GTK_LABEL (tab->tablabel), GTK_JUSTIFY_LEFT);
		gtk_widget_show (tab->tabwidget);
		gtk_container_add (GTK_CONTAINER (twin->notebook), tab->tabwidget);
		gtk_notebook_set_tab_label (GTK_NOTEBOOK (twin->notebook), gtk_notebook_get_nth_page (GTK_NOTEBOOK (twin->notebook), tab->tabnum), tab->tablabel);

		x++;
		tabs = g_list_next(tabs);
	}
	g_list_free(tabs);

  twin->hbuttonbox = gtk_hbutton_box_new ();
  gtk_widget_show (twin->hbuttonbox);
  gtk_box_pack_start (GTK_BOX (twin->toplevelvbox), twin->hbuttonbox, FALSE, FALSE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (twin->hbuttonbox), 6);
  gtk_button_box_set_layout (GTK_BUTTON_BOX (twin->hbuttonbox), GTK_BUTTONBOX_END);
  gtk_button_box_set_spacing (GTK_BUTTON_BOX (twin->hbuttonbox), 6);

  twin->cancel_button = gtk_button_new_from_stock ("gtk-cancel");
  gtk_widget_show (twin->cancel_button);
  gtk_container_add (GTK_CONTAINER (twin->hbuttonbox), twin->cancel_button);
  GTK_WIDGET_SET_FLAGS (twin->cancel_button, GTK_CAN_DEFAULT);

  twin->ok_button = gtk_button_new_from_stock ("gtk-ok");
  gtk_widget_show (twin->ok_button);
  gtk_container_add (GTK_CONTAINER (twin->hbuttonbox), twin->ok_button);
  GTK_WIDGET_SET_FLAGS (twin->ok_button, GTK_CAN_DEFAULT);


	g_signal_connect (G_OBJECT (twin->notebook), "switch_page",
                      G_CALLBACK (on_notebook_switch_page),
                      twin);
	g_signal_connect (G_OBJECT (twin->cancel_button), "clicked",
                      G_CALLBACK (on_textitemwin_cancel_button_clicked),
                      twin);

	g_signal_connect (G_OBJECT (twin->ok_button), "clicked",
                      G_CALLBACK (on_textitemwin_ok_button_clicked),
                      twin);


/* Signals */
  textitemwin_createhintsfor_menus(twin);

  gtk_window_set_wmclass (GTK_WINDOW (twin), "textitemwin", "gcm");
  twin->changed=FALSE;
  
  return GTK_WIDGET(twin);

}


TextItemHtmlTargetWidget * create_htmltargetwidget (TextItemWin *twin,TextItemTab *tab, GtkSelectionData *data)
{
	TextItemHtmlTargetWidget *ttwin = (TextItemHtmlTargetWidget*)g_malloc0(sizeof(TextItemHtmlTargetWidget));
	GtkSelectionData *newdata;
	if ((data->data != NULL)&&(g_utf8_validate (data->data, data->length, NULL))) {
		/* The format of the text/html data is UTF-8 */
		newdata = data;
	} else {
		/* The format is not UTF-8, assuming Mozilla so UCS-2 */
		
		gint i;
		guchar *mydata = (guchar*) g_convert (data->data, data->length,"UTF-8", "UCS-2" ,NULL,&i, NULL);

		if ((mydata == NULL)||(!(g_utf8_validate (mydata, i, NULL)))) {
			GtkWidget *label = gtk_label_new(_("Converting to UTF-8 failed"));
			ttwin->htmltargetvbox = gtk_vbox_new (FALSE, 0);
			gtk_widget_show (ttwin->htmltargetvbox);
			gtk_box_pack_start (GTK_BOX (ttwin->htmltargetvbox), label, TRUE, TRUE, 0);
			gtk_widget_show(label);
			return ttwin;
		} else {
			newdata = gtk_selection_data_copy (data);
			g_free(newdata->data);
			newdata->data = mydata;
			newdata->length = i;
			if (tab != NULL)
				tab->codeset=g_strdup("UCS-2");
		}

	}


	ttwin->twin = twin;

	ttwin->htmltargetvbox = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (ttwin->htmltargetvbox);

	ttwin->notebook = gtk_notebook_new ();
	gtk_widget_show (ttwin->notebook);
	gtk_notebook_set_tab_pos (GTK_NOTEBOOK (ttwin->notebook), GTK_POS_BOTTOM);
	gtk_box_pack_start (GTK_BOX (ttwin->htmltargetvbox), ttwin->notebook, TRUE, TRUE, 0);


	ttwin->htmlsrclabel = gtk_label_new (_("Html Source"));
	gtk_widget_show (ttwin->htmlsrclabel);
	gtk_label_set_justify (GTK_LABEL (ttwin->htmlsrclabel), GTK_JUSTIFY_LEFT);


	ttwin->htmlsrcvbox = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (ttwin->htmlsrcvbox);
	gtk_container_add (GTK_CONTAINER (ttwin->notebook), ttwin->htmlsrcvbox);
	gtk_notebook_set_tab_label (GTK_NOTEBOOK (ttwin->notebook), gtk_notebook_get_nth_page (GTK_NOTEBOOK (ttwin->notebook), 0), ttwin->htmlsrclabel);


	{
		TextItemTextTargetWidget *texttwin;

		texttwin = create_texttargetwidget (twin, tab, newdata);
		gtk_box_pack_start (GTK_BOX (ttwin->htmlsrcvbox), texttwin->txttargetvbox, TRUE, TRUE, 0);
		ttwin->textview = texttwin->textview;
		ttwin->buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (ttwin->textview));
		g_signal_connect (G_OBJECT (ttwin->buffer), "changed",
                      G_CALLBACK (on_item_changed),
                      twin);
		g_signal_connect (G_OBJECT (ttwin->buffer), "changed",
                      G_CALLBACK (on_htmltextview_changed),
                      ttwin);
		g_signal_connect (G_OBJECT (ttwin->buffer), "changed",
                      G_CALLBACK (on_htmlview_mustupdate),
                      ttwin);
	}
/*	mozilla_init_home();
	mozilla_init_profile();
	ttwin->htmlprevvbox = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (ttwin->htmlprevvbox);
	gtk_container_add (GTK_CONTAINER (ttwin->notebook), ttwin->htmlprevvbox);

	ttwin->htmlpreviewlabel = gtk_label_new (_("Html Preview"));
	gtk_widget_show (ttwin->htmlpreviewlabel);
	gtk_label_set_justify (GTK_LABEL (ttwin->htmlpreviewlabel), GTK_JUSTIFY_LEFT);

	gtk_notebook_set_tab_label (GTK_NOTEBOOK (ttwin->notebook), gtk_notebook_get_nth_page 
			(GTK_NOTEBOOK (ttwin->notebook), 1), ttwin->htmlpreviewlabel);
*/
	/* #ifdef MOZILLAHTML */
	/* http://www.mozilla.org/unix/gtk-embedding.html */
	/*gtk_moz_embed_set_comp_path (MOZILLA_HOME);*/
/*	ttwin->htmlviewer = gtk_moz_embed_new ();
	gtk_widget_show (ttwin->htmlviewer);
	gtk_moz_embed_open_stream (GTK_MOZ_EMBED(ttwin->htmlviewer), "file://", "text/html");
	gtk_moz_embed_append_data (GTK_MOZ_EMBED(ttwin->htmlviewer), newdata->data, newdata->length);
	gtk_moz_embed_close_stream (GTK_MOZ_EMBED(ttwin->htmlviewer));
*/
	/* #endif */
		
	/*
	#ifdef GTKHTML
	ttwin->scrolled_window = gtk_scrolled_window_new (NULL, NULL);
	ttwin->htmlviewer = html_view_new();
	ttwin->doc = html_document_new();
	html_document_open_stream (ttwin->doc, "text/html");
	html_document_write_stream (ttwin->doc, newdata->data, -1);
	html_view_set_document (HTML_VIEW(ttwin->htmlviewer), ttwin->doc);
	html_document_close_stream (ttwin->doc);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (ttwin->scrolled_window),
                                        GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_container_add (GTK_CONTAINER (ttwin->scrolled_window), ttwin->htmlviewer);
	gtk_box_pack_start (GTK_BOX (ttwin->htmlprevvbox), ttwin->scrolled_window, TRUE, TRUE, 0);
	gtk_widget_show(ttwin->scrolled_window);
	#endif
	
	

	gtk_box_pack_start (GTK_BOX (ttwin->htmlprevvbox), ttwin->htmlviewer, TRUE, TRUE, 0);
	gtk_widget_show(ttwin->htmlviewer);
*/
	return ttwin;

}

TextItemTargetsTargetWidget * create_tarstargetwidget (TextItemWin *twin,TextItemTab *tab, GtkSelectionData *data)
{
	TextItemTargetsTargetWidget *ttwin = (TextItemTargetsTargetWidget*)g_malloc0(sizeof(TextItemTargetsTargetWidget));
	GdkAtom *atoms_targets = NULL;
	GString *str = g_string_new("");
	GtkTextBuffer *buffer;
	gint counter;

	ttwin->twin = twin;
	ttwin->tarstargetvbox = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (ttwin->tarstargetvbox);
	ttwin->scrolled_window = gtk_scrolled_window_new (NULL, NULL);

	atoms_targets = (GdkAtom *)data->data;
	if (atoms_targets != NULL) {
		for(counter = 0; counter < (data->length / sizeof(GdkAtom)); counter++)
		{
			if (atoms_targets[counter] == NULL) continue;
			str = g_string_append(str, gdk_atom_name(atoms_targets[counter]));
			str = g_string_append_c(str, '\n');
		}
	}
	ttwin->textview = gtk_text_view_new ();
	gtk_text_view_set_editable (GTK_TEXT_VIEW(ttwin->textview), FALSE);
	gtk_widget_show (ttwin->textview);

	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (ttwin->textview));
	gtk_text_buffer_set_text (buffer, str->str, str->len);
	g_string_free(str, TRUE);

	gtk_widget_show(ttwin->textview);

	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (ttwin->scrolled_window),
                                        GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_container_add (GTK_CONTAINER (ttwin->scrolled_window), ttwin->textview);
	gtk_box_pack_start (GTK_BOX (ttwin->tarstargetvbox), ttwin->scrolled_window, TRUE, TRUE, 0);
	gtk_widget_show(ttwin->scrolled_window);


	return ttwin;
}


TextItemTimeStampTargetWidget * create_timetargetwidget (TextItemWin *twin,TextItemTab *tab, GtkSelectionData *data)
{
	TextItemTimeStampTargetWidget *ttwin = (TextItemTimeStampTargetWidget*)g_malloc0(sizeof(TextItemTimeStampTargetWidget));
	GString *str = g_string_new("");
	gchar *tstr = g_strdup_printf("%d", (guint32) data->data);
	GtkTextBuffer *buffer;

	ttwin->twin = twin;
	ttwin->tarstargetvbox = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (ttwin->tarstargetvbox);
	ttwin->scrolled_window = gtk_scrolled_window_new (NULL, NULL);
	str = g_string_append(str, tstr);
	g_free(tstr);
	ttwin->textview = gtk_text_view_new ();
	gtk_text_view_set_editable (GTK_TEXT_VIEW(ttwin->textview), FALSE);
	gtk_widget_show (ttwin->textview);

	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (ttwin->textview));
	gtk_text_buffer_set_text (buffer, str->str, str->len);
	g_string_free(str, TRUE);

	gtk_widget_show(ttwin->textview);

	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (ttwin->scrolled_window),
                                        GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_container_add (GTK_CONTAINER (ttwin->scrolled_window), ttwin->textview);
	gtk_box_pack_start (GTK_BOX (ttwin->tarstargetvbox), ttwin->scrolled_window, TRUE, TRUE, 0);
	gtk_widget_show(ttwin->scrolled_window);


	return ttwin;
}



TextItemTextTargetWidget * create_texttargetwidget (TextItemWin *twin,TextItemTab *tab, GtkSelectionData *data)
{
 TextItemTextTargetWidget *ttwin = (TextItemTextTargetWidget*)g_malloc0(sizeof(TextItemTextTargetWidget));
  gchar *text;
  if ((data->data != NULL)&&(g_utf8_validate (data->data, data->length, NULL))) {
    text = g_strdup(data->data);
  } else {
    text = g_strdup(_("This is not a valid UTF-8 target"));
  }
  ttwin->twin = twin;
  ttwin->txttargetvbox = gtk_vbox_new (FALSE, 0);
  gtk_widget_show (ttwin->txttargetvbox);

  ttwin->txttargetscrolledwindow = gtk_scrolled_window_new (NULL, NULL);
  gtk_widget_show (ttwin->txttargetscrolledwindow);
  gtk_box_pack_start (GTK_BOX (ttwin->txttargetvbox), ttwin->txttargetscrolledwindow, TRUE, TRUE, 0);

  ttwin->textview = gtk_text_view_new ();
  gtk_widget_show (ttwin->textview);
  gtk_container_add (GTK_CONTAINER (ttwin->txttargetscrolledwindow), ttwin->textview);

  ttwin->buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (ttwin->textview));
  gtk_text_buffer_set_text (ttwin->buffer, text, strlen(text));
  g_free(text);

  g_signal_connect (G_OBJECT (ttwin->buffer), "changed",
                      G_CALLBACK (on_item_changed),
                      twin);
  g_signal_connect (G_OBJECT (ttwin->buffer), "changed",
                      G_CALLBACK (on_texttextview_changed),
                      ttwin);

  ttwin->notebook = gtk_notebook_new ();
  gtk_widget_show (ttwin->notebook);
  gtk_box_pack_start (GTK_BOX (ttwin->txttargetvbox), ttwin->notebook, TRUE, TRUE, 0);

  ttwin->txttargettopvbox = gtk_vbox_new (FALSE, 0);
  gtk_widget_show (ttwin->txttargettopvbox);
  gtk_container_add (GTK_CONTAINER (ttwin->notebook), ttwin->txttargettopvbox);

  ttwin->txttargettosearchvbox1 = gtk_vbox_new (FALSE, 0);
  gtk_widget_show (ttwin->txttargettosearchvbox1);
  gtk_box_pack_start (GTK_BOX (ttwin->txttargettopvbox), ttwin->txttargettosearchvbox1, TRUE, TRUE, 0);

  ttwin->txttargettosearchhbox1 = gtk_hbox_new (FALSE, 0);
  gtk_widget_show (ttwin->txttargettosearchhbox1);
  gtk_box_pack_start (GTK_BOX (ttwin->txttargettosearchvbox1), ttwin->txttargettosearchhbox1, TRUE, TRUE, 0);

  ttwin->txttargettosearchhbox2 = gtk_hbox_new (FALSE, 0);
  gtk_widget_show (ttwin->txttargettosearchhbox2);
  gtk_box_pack_start (GTK_BOX (ttwin->txttargettosearchhbox1), ttwin->txttargettosearchhbox2, TRUE, TRUE, 0);

  ttwin->txttargettosearchvbox2 = gtk_vbox_new (FALSE, 0);
  gtk_widget_show (ttwin->txttargettosearchvbox2);
  gtk_box_pack_start (GTK_BOX (ttwin->txttargettosearchhbox2), ttwin->txttargettosearchvbox2, TRUE, TRUE, 0);

  ttwin->txttargettosearchhbox3 = gtk_hbox_new (FALSE, 0);
  gtk_widget_show (ttwin->txttargettosearchhbox3);
  gtk_box_pack_start (GTK_BOX (ttwin->txttargettosearchvbox2), ttwin->txttargettosearchhbox3, TRUE, TRUE, 0);
/* Pango here !*/
  ttwin->txttargetsearchlabel = gtk_label_new (_("String to search"));
  {
    gchar *trans = g_strdup_printf ("<span weight=\"bold\">%s</span>",_("String to search"));
    gtk_label_set_markup (GTK_LABEL(ttwin->txttargetsearchlabel), trans);
    g_free(trans);
  }
  gtk_widget_show (ttwin->txttargetsearchlabel);
  gtk_box_pack_start (GTK_BOX (ttwin->txttargettosearchhbox3), ttwin->txttargetsearchlabel, FALSE, FALSE, 0);

  gtk_label_set_justify (GTK_LABEL (ttwin->txttargetsearchlabel), GTK_JUSTIFY_LEFT);

  ttwin->txttargetfillerlabel1 = gtk_label_new ("");
  gtk_widget_show (ttwin->txttargetfillerlabel1);
  gtk_box_pack_start (GTK_BOX (ttwin->txttargettosearchhbox3), ttwin->txttargetfillerlabel1, FALSE, FALSE, 0);
  gtk_label_set_justify (GTK_LABEL (ttwin->txttargetfillerlabel1), GTK_JUSTIFY_LEFT);

  ttwin->txttargettosearchhbox4 = gtk_hbox_new (FALSE, 0);
  gtk_widget_show (ttwin->txttargettosearchhbox4);
  gtk_box_pack_start (GTK_BOX (ttwin->txttargettosearchvbox2), ttwin->txttargettosearchhbox4, TRUE, TRUE, 0);

  ttwin->txttargetfillerlabel2 = gtk_label_new (_("    "));
  gtk_widget_show (ttwin->txttargetfillerlabel2);
  gtk_box_pack_start (GTK_BOX (ttwin->txttargettosearchhbox4), ttwin->txttargetfillerlabel2, FALSE, FALSE, 0);
  gtk_label_set_justify (GTK_LABEL (ttwin->txttargetfillerlabel2), GTK_JUSTIFY_LEFT);

  ttwin->txttosearchentry = gtk_entry_new ();
  gtk_widget_show (ttwin->txttosearchentry);
  gtk_box_pack_start (GTK_BOX (ttwin->txttargettosearchhbox4), ttwin->txttosearchentry, TRUE, TRUE, 6);

  ttwin->txttargettoreplacevbox1 = gtk_vbox_new (FALSE, 0);
  gtk_widget_show (ttwin->txttargettoreplacevbox1);
  gtk_box_pack_start (GTK_BOX (ttwin->txttargettopvbox), ttwin->txttargettoreplacevbox1, TRUE, TRUE, 0);

  ttwin->txttargettoreplacehbox1 = gtk_hbox_new (FALSE, 0);
  gtk_widget_show (ttwin->txttargettoreplacehbox1);
  gtk_box_pack_start (GTK_BOX (ttwin->txttargettoreplacevbox1), ttwin->txttargettoreplacehbox1, TRUE, TRUE, 6);

  ttwin->txttargettoreplacelabel = gtk_label_new (_("String to replace"));
  {
    gchar *trans = g_strdup_printf ("<span weight=\"bold\">%s</span>",_("String to replace"));
    gtk_label_set_markup (GTK_LABEL(ttwin->txttargettoreplacelabel), trans);
    g_free(trans);
  }
  gtk_widget_show (ttwin->txttargettoreplacelabel);
  gtk_box_pack_start (GTK_BOX (ttwin->txttargettoreplacehbox1), ttwin->txttargettoreplacelabel, FALSE, FALSE, 0);
  gtk_label_set_justify (GTK_LABEL (ttwin->txttargettoreplacelabel), GTK_JUSTIFY_LEFT);

  ttwin->txttargetfillerlabel3 = gtk_label_new ("");
  gtk_widget_show (ttwin->txttargetfillerlabel3);
  gtk_box_pack_start (GTK_BOX (ttwin->txttargettoreplacehbox1), ttwin->txttargetfillerlabel3, FALSE, FALSE, 0);
  gtk_label_set_justify (GTK_LABEL (ttwin->txttargetfillerlabel3), GTK_JUSTIFY_LEFT);

  ttwin->txttargettoreplacehbox2 = gtk_hbox_new (FALSE, 0);
  gtk_widget_show (ttwin->txttargettoreplacehbox2);
  gtk_box_pack_start (GTK_BOX (ttwin->txttargettoreplacevbox1), ttwin->txttargettoreplacehbox2, TRUE, TRUE, 0);

  ttwin->txttargetfillerlabel4 = gtk_label_new (_("    "));
  gtk_widget_show (ttwin->txttargetfillerlabel4);
  gtk_box_pack_start (GTK_BOX (ttwin->txttargettoreplacehbox2), ttwin->txttargetfillerlabel4, FALSE, FALSE, 0);
  gtk_label_set_justify (GTK_LABEL (ttwin->txttargetfillerlabel4), GTK_JUSTIFY_LEFT);

  ttwin->txttargetreplaceentry = gtk_entry_new ();
  gtk_widget_show (ttwin->txttargetreplaceentry);
  gtk_box_pack_start (GTK_BOX (ttwin->txttargettoreplacehbox2), ttwin->txttargetreplaceentry, TRUE, TRUE, 6);

  ttwin->txttargethseparator = gtk_hseparator_new ();
  gtk_widget_show (ttwin->txttargethseparator);
  gtk_box_pack_start (GTK_BOX (ttwin->txttargettopvbox), ttwin->txttargethseparator, TRUE, TRUE, 0);

  ttwin->txttargethbuttonbox = gtk_hbutton_box_new ();
  gtk_widget_show (ttwin->txttargethbuttonbox);
  gtk_box_pack_start (GTK_BOX (ttwin->txttargettopvbox), ttwin->txttargethbuttonbox, TRUE, TRUE, 6);
  gtk_button_box_set_layout (GTK_BUTTON_BOX (ttwin->txttargethbuttonbox), GTK_BUTTONBOX_SPREAD);
  gtk_button_box_set_spacing (GTK_BUTTON_BOX (ttwin->txttargethbuttonbox), 6);

  ttwin->txttargetreplacebutton = gtk_button_new_from_stock ("gtk-find-and-replace");
  gtk_widget_show (ttwin->txttargetreplacebutton);
  gtk_container_add (GTK_CONTAINER (ttwin->txttargethbuttonbox), ttwin->txttargetreplacebutton);
  GTK_WIDGET_SET_FLAGS (ttwin->txttargetreplacebutton, GTK_CAN_DEFAULT);

  ttwin->txttargetsearcandreplace = gtk_label_new (_("Search And Replace"));
  gtk_widget_show (ttwin->txttargetsearcandreplace);
  gtk_notebook_set_tab_label (GTK_NOTEBOOK (ttwin->notebook), gtk_notebook_get_nth_page (GTK_NOTEBOOK (ttwin->notebook), 0), ttwin->txttargetsearcandreplace);
  gtk_label_set_justify (GTK_LABEL (ttwin->txttargetsearcandreplace), GTK_JUSTIFY_LEFT);


/*tab2*/

  ttwin->misc_vbox = gtk_vbox_new (FALSE, 0);
  gtk_widget_show (ttwin->misc_vbox);
  gtk_container_add (GTK_CONTAINER (ttwin->notebook), ttwin->misc_vbox);
  
  ttwin->tblMisc01 = gtk_table_new (5, 4, FALSE);
  gtk_widget_show (ttwin->tblMisc01);
  
  gtk_box_pack_start (GTK_BOX (ttwin->misc_vbox), ttwin->tblMisc01, TRUE, TRUE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (ttwin->tblMisc01), 4);
  gtk_table_set_row_spacings (GTK_TABLE (ttwin->tblMisc01), 4);
  gtk_table_set_col_spacings (GTK_TABLE (ttwin->tblMisc01), 4);  

  ttwin->btnRemLastChars = gtk_button_new_with_label (_("go"));
  gtk_widget_show (ttwin->btnRemLastChars);
  gtk_table_attach (GTK_TABLE (ttwin->tblMisc01), ttwin->btnRemLastChars, 3, 4, 1, 2,
                    (GtkAttachOptions) (GTK_EXPAND),
                    (GtkAttachOptions) (0), 0, 0);

  ttwin->btnRemFirstChars = gtk_button_new_with_label (_("go"));
  gtk_widget_show (ttwin->btnRemFirstChars);
  gtk_table_attach (GTK_TABLE (ttwin->tblMisc01), ttwin->btnRemFirstChars, 3, 4, 0, 1,
                    (GtkAttachOptions) (GTK_EXPAND),
                    (GtkAttachOptions) (0), 0, 0);

  ttwin->lblTlastChars = gtk_label_new (_("characters from each line "));
  gtk_widget_show (ttwin->lblTlastChars);
  gtk_table_attach (GTK_TABLE (ttwin->tblMisc01), ttwin->lblTlastChars, 2, 3, 1, 2,
                    (GtkAttachOptions) (GTK_EXPAND),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_misc_set_alignment (GTK_MISC (ttwin->lblTlastChars), 0, 0.5);

  ttwin->lblTfirstChars = gtk_label_new (_("characters from each line "));
  gtk_widget_show (ttwin->lblTfirstChars);
  gtk_table_attach (GTK_TABLE (ttwin->tblMisc01), ttwin->lblTfirstChars, 2, 3, 0, 1,
                    (GtkAttachOptions) (GTK_EXPAND),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_misc_set_alignment (GTK_MISC (ttwin->lblTfirstChars), 0, 0.5);

  ttwin->lblRFirstChars = gtk_label_new (_("Remove first "));     
  gtk_widget_show (ttwin->lblRFirstChars); 
  gtk_table_attach (GTK_TABLE (ttwin->tblMisc01), ttwin->lblRFirstChars, 0, 1, 0, 1, 
                    (GtkAttachOptions) (GTK_EXPAND),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_misc_set_alignment (GTK_MISC (ttwin->lblRFirstChars), 0, 0.5);

  ttwin->lblRLastChars = gtk_label_new (_("Remove last "));
  gtk_widget_show (ttwin->lblRLastChars);
  gtk_table_attach (GTK_TABLE (ttwin->tblMisc01), ttwin->lblRLastChars, 0, 1, 1, 2,
                    (GtkAttachOptions) (GTK_EXPAND),
                    (GtkAttachOptions) (0), 0, 0);  
  gtk_misc_set_alignment (GTK_MISC (ttwin->lblRLastChars), 0, 0.5);

  ttwin->sbtnLastChars_adj = gtk_adjustment_new (1, 0, 99999, 1, 10, 10);
  ttwin->sbtnLastChars = gtk_spin_button_new (GTK_ADJUSTMENT (ttwin->sbtnLastChars_adj), 1, 0);
  
  gtk_widget_show (ttwin->sbtnLastChars);
  gtk_table_attach (GTK_TABLE (ttwin->tblMisc01), ttwin->sbtnLastChars, 1, 2, 1, 2,
                    (GtkAttachOptions) (GTK_EXPAND),
                    (GtkAttachOptions) (0), 0, 0);

  ttwin->sbtnFirstChars_adj = gtk_adjustment_new (1, 0, 99999, 1, 10, 10);
  ttwin->sbtnFirstChars = gtk_spin_button_new (GTK_ADJUSTMENT (ttwin->sbtnFirstChars_adj), 1, 0);
  gtk_widget_show (ttwin->sbtnFirstChars);
  gtk_table_attach (GTK_TABLE (ttwin->tblMisc01), ttwin->sbtnFirstChars, 1, 2, 0, 1,
                    (GtkAttachOptions) (GTK_EXPAND),
                    (GtkAttachOptions) (0), 0, 0);

  ttwin->btnRemFirstLines = gtk_button_new_with_label (_("go"));
  gtk_widget_show (ttwin->btnRemFirstLines);
  gtk_table_attach (GTK_TABLE (ttwin->tblMisc01), ttwin->btnRemFirstLines, 3, 4, 2, 3,
                    (GtkAttachOptions) (GTK_EXPAND),
                    (GtkAttachOptions) (0), 0, 0);

  ttwin->btnRemLastLines = gtk_button_new_with_label (_("go"));

  gtk_widget_show (ttwin->btnRemLastLines);
  gtk_table_attach (GTK_TABLE (ttwin->tblMisc01), ttwin->btnRemLastLines, 3, 4, 3, 4,
                    (GtkAttachOptions) (GTK_EXPAND),
                    (GtkAttachOptions) (0), 0, 0);

  ttwin->lblTFirstLines = gtk_label_new (_("lines"));
  gtk_widget_show (ttwin->lblTFirstLines);
  gtk_table_attach (GTK_TABLE (ttwin->tblMisc01), ttwin->lblTFirstLines, 2, 3, 2, 3,
                    (GtkAttachOptions) (GTK_EXPAND),
                    (GtkAttachOptions) (0), 0, 0);  
  gtk_misc_set_alignment (GTK_MISC (ttwin->lblTFirstLines), 0, 0.5);

  ttwin->lblTLastLines = gtk_label_new (_("lines"));
  gtk_widget_show (ttwin->lblTLastLines);
  gtk_table_attach (GTK_TABLE (ttwin->tblMisc01), ttwin->lblTLastLines, 2, 3, 3, 4,
                    (GtkAttachOptions) (GTK_EXPAND),
                    (GtkAttachOptions) (0), 0, 0);  
  gtk_misc_set_alignment (GTK_MISC (ttwin->lblTLastLines), 0, 0.5);

  ttwin->sbtnFirstLines_adj = gtk_adjustment_new (1, 0, 99999, 1, 10, 10);
  ttwin->sbtnFirstLines = gtk_spin_button_new (GTK_ADJUSTMENT (ttwin->sbtnFirstLines_adj), 1, 0);
  gtk_widget_show (ttwin->sbtnFirstLines);
  gtk_table_attach (GTK_TABLE (ttwin->tblMisc01), ttwin->sbtnFirstLines, 1, 2, 2, 3,
                    (GtkAttachOptions) (0), 
                    (GtkAttachOptions) (0), 0, 0);

  ttwin->sbtnLastLines_adj = gtk_adjustment_new (1, 0, 99999, 1, 10, 10);
  ttwin->sbtnLastLines = gtk_spin_button_new (GTK_ADJUSTMENT (ttwin->sbtnLastLines_adj), 1, 0);

  gtk_widget_show (ttwin->sbtnLastLines);
  gtk_table_attach (GTK_TABLE (ttwin->tblMisc01), ttwin->sbtnLastLines, 1, 2, 3, 4,
                    (GtkAttachOptions) (0),
                    (GtkAttachOptions) (0), 0, 0);

  ttwin->lblRFirstLines = gtk_label_new (_("Remove first "));
  gtk_widget_show (ttwin->lblRFirstLines);
  gtk_table_attach (GTK_TABLE (ttwin->tblMisc01), ttwin->lblRFirstLines, 0, 1, 2, 3,
                    (GtkAttachOptions) (GTK_EXPAND),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_misc_set_alignment (GTK_MISC (ttwin->lblRFirstLines), 0, 0.5);

  ttwin->lblRLastLines = gtk_label_new (_("Remove last "));
  gtk_widget_show (ttwin->lblRLastLines);
  gtk_table_attach (GTK_TABLE (ttwin->tblMisc01), ttwin->lblRLastLines, 0, 1, 3, 4,
                    (GtkAttachOptions) (GTK_EXPAND),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_misc_set_alignment (GTK_MISC (ttwin->lblRLastLines), 0, 0.5);

  ttwin->btnInvert = gtk_button_new_with_label (_("go"));
  gtk_widget_show (ttwin->btnInvert);
  gtk_table_attach (GTK_TABLE (ttwin->tblMisc01), ttwin->btnInvert, 3, 4, 4, 5,
                    (GtkAttachOptions) (GTK_EXPAND),
                    (GtkAttachOptions) (0), 0, 0);

  ttwin->lblRev = gtk_label_new (_("Invert text"));

  gtk_widget_show (ttwin->lblRev);
  gtk_table_attach (GTK_TABLE (ttwin->tblMisc01), ttwin->lblRev, 0, 1, 4, 5,
                    (GtkAttachOptions) (GTK_EXPAND),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_misc_set_alignment (GTK_MISC (ttwin->lblRev), 0, 0.5);

  ttwin->txttargetother = gtk_label_new (_("Other Functions"));
  gtk_widget_show (ttwin->txttargetother);

  gtk_notebook_set_tab_label (GTK_NOTEBOOK (ttwin->notebook), gtk_notebook_get_nth_page (GTK_NOTEBOOK (ttwin->notebook), 1), ttwin->txttargetother);
  gtk_label_set_justify (GTK_LABEL (ttwin->txttargetother), GTK_JUSTIFY_LEFT);

 
  g_signal_connect (G_OBJECT (ttwin->txttargetreplacebutton), "clicked",
                      G_CALLBACK (on_search_replace_ok_button_clicked),
                      ttwin);

  g_signal_connect (G_OBJECT (ttwin->btnRemLastChars), "clicked",
                      G_CALLBACK (on_btnRemLastChars_clicked),
                      ttwin);
 
  g_signal_connect (G_OBJECT (ttwin->btnRemLastChars), "clicked",
                      G_CALLBACK (on_btnRemLastChars_clicked),
                      ttwin);
  g_signal_connect (G_OBJECT (ttwin->btnRemFirstChars), "clicked",
                      G_CALLBACK (on_btnRemFirstChars_clicked),
                      ttwin);
  g_signal_connect (G_OBJECT (ttwin->btnRemFirstLines), "clicked",
                      G_CALLBACK (on_btnRemFirstLines_clicked),
                      ttwin);
  g_signal_connect (G_OBJECT (ttwin->btnRemLastLines), "clicked",
                      G_CALLBACK (on_btnRemLastLines_clicked),
                      ttwin);
  g_signal_connect (G_OBJECT (ttwin->btnInvert), "clicked",
                      G_CALLBACK (on_btnInvert_clicked),
                      ttwin);
  return ttwin;
}

gchar* string_replace(gchar *string, gchar *oldpiece, gchar *newpiece)
{
   gint str_index, newstr_index,oldpiece_index, end, new_len,old_len, cpy_len;
   gchar *c;
   gint cnt=0;
   static gchar newstring[MAXLENSEARCHREPLACE];


   if ((c = (gchar *) strstr(string, oldpiece)) == NULL)
      return string;

   new_len = strlen(newpiece);
   old_len = strlen(oldpiece);
   end = strlen(string) - old_len;
   oldpiece_index = c - string;

   newstr_index = 0;
   str_index = 0;
   while ((str_index <= end && c != NULL)&&(cnt < MAXLENSEARCHREPLACE))
   {
      cnt++;
      /* Copy characters from the left of matched pattern occurence */
      cpy_len = oldpiece_index-str_index;
      strncpy(newstring+newstr_index, string+str_index, cpy_len);
      newstr_index += cpy_len;
      str_index += cpy_len;

      /* Copy replacement characters instead of matched pattern */
      strcpy(newstring+newstr_index, newpiece);
      newstr_index += new_len;
      str_index += old_len;

      /* Check for another pattern match */
      if((c = (gchar *)strstr(string+str_index, oldpiece)) != NULL)
         oldpiece_index = c - string;
   }
   /* Copy remaining characters from the right of last matched pattern */
   strcpy(newstring+newstr_index, string+str_index);

   return newstring;
} 


gchar* search_replace_using_sed (gchar *buffer, gchar *search, gchar *replace)
{ 
	if ((strlen(buffer) > 0) && (strlen(search) > 0)) {
		gint wp[2], rp[2], status;
		gchar regexp_str[256];	
		gchar *bufout;
		unsigned int i, subst_size = strlen(buffer) * 2;
		gint growth_step = 64;
	
		bufout = (gchar*) g_malloc0(subst_size);
		if (pipe(wp) == -1)
			return (gchar *) -1;
		if (pipe(rp) == -1)
			return (gchar *) -1;
	
		if (write(wp[1], buffer, strlen(buffer)) == -1)
			return (gchar *) -1;
		close(wp[1]);	
	
		if (fork() == 0) {
			if (dup2(wp[0], 0) == -1)
				exit(1);
			if (dup2(rp[1], 1) == -1)
				exit(1);
			sprintf(regexp_str, "s/%s/%s/g", search, replace);
			execl(SEDPATH, "sed", regexp_str, (char *)0);
		}
		wait(&status);
		close(rp[1]);
	
		for(i = 0;read(rp[0], &bufout[i], sizeof(char)) > 0;) {
			
			if (++i >= subst_size) {
				subst_size += growth_step;
				realloc(bufout, subst_size);
			}
		}
		bufout[i] = '\0';
	
		return bufout;
	} else return buffer;
}



void textitemwin_close_question (gint reply, TextItemWin *twin) 
{
	if (reply == 0) {
		/* TODO: Free all memory */
		gtk_widget_destroy(GTK_WIDGET(twin));
	}
}

void textitemwin_cancel_and_close (TextItemWin *twin, gpointer *user_data)
{
	if (twin->changed) {
		gnome_question_dialog (_("The item was modified, are you sure you want to close this window?"),
			(GnomeReplyCallback) (textitemwin_close_question), twin);
	} else {
		/* TODO: Free all memory */
		gtk_widget_destroy(GTK_WIDGET(twin));
	}
}

void texitemwin_ok (TextItemWin *twin, gpointer *user_data)
{
	Selection *item = itemp;
	GList *tabs=NULL, *targets=NULL;

	if (item==NULL) {
		g_print("GCM: The item dissapeared :-(\n");
		return;
	}

	targets = g_list_copy(item->targets);
	tabs = g_list_copy(twin->tabs);
	while ((tabs) && (targets)) {
		/*Lets hope that the amount of tabs and the amount of targets has not changed yet*/
		TextItemTab *tab = (TextItemTab*)tabs->data;
		GtkSelectionData *data = (GtkSelectionData*)targets->data;
		if (tab->changed) {
			if (tab->codeset != NULL) {
				gsize size;
				data->data = (guchar*) g_convert (tab->newdata, strlen(tab->newdata), tab->codeset, "UTF-8",NULL, &size, NULL);
				data->length = (gint) size;
			} else {
				data->data = tab->newdata;
				data->length = strlen(tab->newdata);
			}
		}
		targets = g_list_next(targets);
		tabs = g_list_next(tabs);
	}
	g_list_free(tabs);
	g_list_free(targets);
	
	app_update_item(GNOME_MAINWIN(twin->windowparent), (gpointer) item);
}


void revstr(gchar *input_buf, gint buflen, gchar *output_buf) 
{
	register gint i;

	for(i = 0; i < buflen-1; i++)
		*(output_buf+i) = *(input_buf+buflen-i-2);
}


void remove_first_x_lines(gchar *s1, gint x)
{
	gint i = 0, j, len = strlen(s1);
	
	while (x-- && i < len) {
		while (s1[i] != '\n' && i < len)
			i++;
		i++;
	}
	
	for (j = 0; i < len; i++, j++)
		s1[j] = s1[i];
	
	s1[j] = '\0';
}

void remove_last_x_lines(gchar *s1, gint x)
{ 
	gint i, len = strlen(s1);
	
	i = len - 1;
	while (x-- && i >= 0) {
		while (s1[i] != '\n' && i > 0)
			i--;
		i--;
	}
	
	s1[i + 1] = '\0';
	
}

void remove_first_x_chars_from_each_line(gchar *s1, gint x) 
{
	gint i, j, trigger;
	
	for (i = 0, j = 0, trigger = 0; i < strlen(s1); i++, trigger++) {
	
		if (s1[i] == '\n') {
			s1[j++] = s1[i];
			trigger = 0;
		}
		
		if (trigger > x) {
			s1[j++] = s1[i];
		}
			
	}
	
	s1[j] = '\0';
}

void remove_last_x_chars_from_each_line(gchar *s1, gint x) 
{ 
	gint i, j, back, erase, len = strlen(s1);
	
	for (i = 0, j = 0, back = 0; i < len; i++, j++) {
		
		erase = back > x ? x : back;
		if (s1[i] == '\n' || i == len -1) {
			if (i == len - 1 && s1[i] != '\n')
				s1[i] = '\0';
			else
				j -= erase;
			back = 0;
		}
		
		s1[j] = s1[i];
		
		if (s1[i] != '\n')
			back++;
		
	}
	
	s1[j] = '\0';
}



/* These functions/routines came from hypermail http://freshmeat.net/search?q=hypermail */
#define INIT_PUSH(x) memset(&(x), 0, sizeof(struct Push))
#define RETURN_PUSH(x) return (x).string

/*
** Push byte onto a buffer realloc the buffer if needed.
**   
** Returns the (new) buffer pointer.
*/
char *PushByte(struct Push *push, char byte)
{                               /* byte to append */
#define PUSH_DEFAULT 32         /* default strings are this big */
    if (!push)
        return NULL;            /* this is a sever error */
    if (!push->string) {
        push->string = (char *)malloc(PUSH_DEFAULT);
        if (!push->string)
            return NULL;        /* error again */
        push->alloc = PUSH_DEFAULT;
        push->len = 0;
    }
    else if ((push->len + 2) >= push->alloc) {
        char *newptr;
        newptr = (char *)realloc(push->string, push->alloc * 2);        /* double the size */
        if (!newptr) {
            return push->string;        /* live on the old one! */
        }
        push->alloc *= 2;       /* enlarge the alloc size */
        push->string = newptr;  /* use the new buffer */
    }
    push->string[push->len++] = byte;
    push->string[push->len] = 0;        /* zero terminate */
    
    return push->string;        /* current buffer */
}

/*
** Push a string onto a buffer, and realloc the buffer if needed.
**
** Returns the (new) buffer pointer.
*/
char *PushString(struct Push *push, const char *append)
{                               /* string to append */
    char *string = NULL;
        
#if 1
    return PushNString(push, append, strlen(append));
#else 
    while (*append) {           /* continue until zero termination */
        string = PushByte(push, *append);
        append++;               /* get next character */
    }
#endif
    return string;              /* this is the new buffer */
}
    
/*
** Push a limited string onto a buffer, and realloc the buffer if needed.
**
** Returns the (new) buffer pointer.
*/
char *PushNString(struct Push *push, const char *append,/* string to append */
                  int size)
{                               /* maximum number of bytes to copy */
    char *string = NULL;
#if 1 
    if (!push)
        return NULL;            /* this is a sever error */ 
    if (!push->string) {
        push->string = (char *)malloc(PUSH_DEFAULT + size);
        if (!push->string)
            return NULL;        /* error again */
        push->alloc = PUSH_DEFAULT + size;
        push->len = 0;
    }
    else if ((push->len + size + 1) >= push->alloc) { 
        char *newptr;   
        push->alloc = 2*push->alloc + size;     /* enlarge the alloc size */
        newptr = (char *)realloc(push->string, push->alloc);    /* double the size */
        if (!newptr) {
            return push->string;        /* live on the old one! */
        }
        push->string = newptr;  /* use the new buffer */

    }   

    strncpy(push->string + push->len, append, size);
    push->len += size;
    push->string[push->len] = 0;
                  
    string = push->string;      /* current buffer */
  
#else 
    while (*append && size--) { /* continue until zero termination */
        string = PushByte(push, *append);
        append++;               /* get next character */
    }
#endif
 return string;              /* this is the new buffer */
}

/* Given a string, replaces all instances of "oldpiece" with "newpiece".
**   
** Modified this routine to eliminate recursion and to avoid infinite
** expansion of string when newpiece contains oldpiece.  --Byron Darrah
**
** 1998-11-17 (Daniel) modified to deal with any length strings and dynamic
** buffers.
*/

gchar * g_string_replace (gchar *gstring, gchar *goldpiece, gchar *gnewpiece)
{
	char *string = (char*) gstring;
	char *oldpiece = (char*) goldpiece;
	char *newpiece = (char*) gnewpiece;

	return (gchar*) myreplace(string, oldpiece, newpiece);
}

char * myreplace(char *string, char *oldpiece, char *newpiece)
{ 
    int str_index, newstr_index, oldpiece_index, end,
        new_len, old_len, cpy_len;
    char *c;
    struct Push buff;
    
    INIT_PUSH(buff);
    
    if ((c = (char *)strstr(string, oldpiece)) == NULL) {
        /* push the whole thing */
        PushString(&buff, string);
        RETURN_PUSH(buff);
    }
    
    new_len = strlen(newpiece);
    old_len = strlen(oldpiece);
    end = strlen(string) - old_len;
    oldpiece_index = c - string;
    
    newstr_index = 0;
    str_index = 0;
    while (str_index <= end && c != NULL) {
        /* Copy characters from the left of matched pattern occurence */
        cpy_len = oldpiece_index - str_index;
        PushNString(&buff, string + str_index, cpy_len);
                        
        newstr_index += cpy_len;
        str_index += cpy_len;
    
        /* Copy replacement characters instead of matched pattern */
        
        PushString(&buff, newpiece);
    
        newstr_index += new_len;
        str_index += old_len;
    
        /* Check for another pattern match */
        if ((c = (char *)strstr(string + str_index, oldpiece)) != NULL)
            oldpiece_index = c - string;
    }
    /* Copy remaining characters from the right of last matched pattern */
    PushString(&buff, string + str_index);
    
    RETURN_PUSH(buff);
}



