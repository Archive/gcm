#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include "libgcm.h"
#include "controlsocket.h"

#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif


static gpointer remote_read_packet(gint fd, ServerPktHeader * pkt_hdr)
{
	gpointer data = NULL;

	if (read(fd, pkt_hdr, sizeof (ServerPktHeader)) == sizeof (ServerPktHeader))
	{
		if (pkt_hdr->data_length)
		{
			data = g_malloc0(pkt_hdr->data_length);
			read(fd, data, pkt_hdr->data_length);
		}
	}
	return data;
}

static void remote_read_ack(gint fd)
{
	gpointer data;
	ServerPktHeader pkt_hdr;

	data = remote_read_packet(fd, &pkt_hdr);
	if (data)
		g_free(data);

}

static void remote_send_packet(gint fd, guint32 command, gpointer data, guint32 data_length)
{
	ClientPktHeader pkt_hdr;

	pkt_hdr.version = GCM_PROTOCOL_VERSION;
	pkt_hdr.command = command;
	pkt_hdr.data_length = data_length;
	write(fd, &pkt_hdr, sizeof (ClientPktHeader));
	if (data_length && data)
		write(fd, data, data_length);
}

static void remote_send_guint32(gint session, guint32 cmd, guint32 val)
{
	gint fd;

	if ((fd = gcm_connect_to_session(session)) == -1)
		return;
	remote_send_packet(fd, cmd, &val, sizeof (guint32));
	remote_read_ack(fd);
	close(fd);
}

static void remote_send_boolean(gint session, guint32 cmd, gboolean val)
{
	gint fd;

	if ((fd = gcm_connect_to_session(session)) == -1)
		return;
	remote_send_packet(fd, cmd, &val, sizeof (gboolean));
	remote_read_ack(fd);
	close(fd);
}

static void remote_send_gfloat(gint session, guint32 cmd, gfloat value)
{
	gint fd;

	if ((fd = gcm_connect_to_session(session)) == -1)
		return;
	remote_send_packet(fd, cmd, &value, sizeof (gfloat));
	remote_read_ack(fd);
	close(fd);
}

static void remote_send_string(gint session, guint32 cmd, gchar * string)
{
	gint fd;

	if ((fd = gcm_connect_to_session(session)) == -1)
		return;
	remote_send_packet(fd, cmd, string, string ? strlen(string) + 1 : 0);
	remote_read_ack(fd);
	close(fd);
}

static gboolean remote_cmd(gint session, guint32 cmd)
{
	gint fd;

	if ((fd = gcm_connect_to_session(session)) == -1)
		return FALSE;
	remote_send_packet(fd, cmd, NULL, 0);
	remote_read_ack(fd);
	close(fd);

	return TRUE;
}

static gboolean remote_get_gboolean(gint session, gint cmd)
{
	ServerPktHeader pkt_hdr;
	gboolean ret = FALSE;
	gpointer data;
	gint fd;

	if ((fd = gcm_connect_to_session(session)) == -1)
		return ret;
	remote_send_packet(fd, cmd, NULL, 0);
	data = remote_read_packet(fd, &pkt_hdr);
	if (data)
	{
		ret = *((gboolean *) data);
		g_free(data);
	}
	remote_read_ack(fd);
	close(fd);

	return ret;
}

static guint32 remote_get_gint(gint session, gint cmd)
{
	ServerPktHeader pkt_hdr;
	gpointer data;
	gint fd, ret = 0;

	if ((fd = gcm_connect_to_session(session)) == -1)
		return ret;
	remote_send_packet(fd, cmd, NULL, 0);
	data = remote_read_packet(fd, &pkt_hdr);
	if (data)
	{
		ret = *((gint *) data);
		g_free(data);
	}
	remote_read_ack(fd);
	close(fd);
	return ret;
}

static gfloat remote_get_gfloat(gint session, gint cmd)
{
	ServerPktHeader pkt_hdr;
	gpointer data;
	gint fd;
	gfloat ret = 0.0;

	if ((fd = gcm_connect_to_session(session)) == -1)
		return ret;
	remote_send_packet(fd, cmd, NULL, 0);
	data = remote_read_packet(fd, &pkt_hdr);
	if (data)
	{
		ret = *((gfloat *) data);
		g_free(data);
	}
	remote_read_ack(fd);
	close(fd);
	return ret;
}

gchar *remote_get_string(gint session, gint cmd)
{
	ServerPktHeader pkt_hdr;
	gpointer data;
	gint fd;

	if ((fd = gcm_connect_to_session(session)) == -1)
		return NULL;
	remote_send_packet(fd, cmd, NULL, 0);
	data = remote_read_packet(fd, &pkt_hdr);
	remote_read_ack(fd);
	close(fd);
	return data;
}

gchar *remote_get_string_pos(gint session, gint cmd, guint32 pos)
{
	ServerPktHeader pkt_hdr;
	gpointer data;
	gint fd;

	if ((fd = gcm_connect_to_session(session)) == -1)
		return NULL;
	remote_send_packet(fd, cmd, &pos, sizeof (guint32));
	data = remote_read_packet(fd, &pkt_hdr);
	remote_read_ack(fd);
	close(fd);
	return data;
}

gint gcm_connect_to_session(gint session)
{
	gint fd;
	uid_t stored_uid, euid;
	struct sockaddr_un saddr;

	if ((fd = socket(AF_UNIX, SOCK_STREAM, 0)) != -1)
	{
		saddr.sun_family = AF_UNIX;
		stored_uid = getuid();
		euid = geteuid();
		setuid(euid);
		sprintf(saddr.sun_path, "%s/gcm_%s.%d", g_get_tmp_dir(), g_get_user_name(), session);
		setreuid(stored_uid, euid);
		if (connect(fd, (struct sockaddr *) &saddr, sizeof (saddr)) != -1)
			return fd;
	}
	close(fd);
	return -1;
}

gint gcm_remote_get_version(gint session)
{
	return remote_get_gint(session, CMD_GET_VERSION);
}

gboolean gcm_remote_is_running(gint session)
{
	return remote_cmd(session, CMD_PING);
}

