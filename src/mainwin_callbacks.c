/* * C o p y r i g h t *(also read COPYING)* * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *  Copyright (C) 2003  Philip Van Hoof <me@freax.org>                         *
 *                                                                             *
 *  This program is free software; you can redistribute it and/or modify       *
 *  it under the terms of the GNU General Public License as published by       *
 *  the Free Software Foundation; either version 2 of the License, or          *
 *  (at your option) any later version.                                        *
 *                                                                             *
 *  This program is distributed in the hope that it will be useful,            *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *  GNU General Public License for more details.                               *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License          *
 *  along with this program; if not, write to the Free Software                *
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA  *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */


#include "mainwin.h"
#include "mainwin_callbacks.h"
#include "plugin.h"
#include "../libgcm/libgcm.h"

static gulong XA_COMPOUND_TEXT = 0;
static const struct {
  gulong      *atom;
  const gchar *atom_name;
} atoms[] = {
  { &XA_COMPOUND_TEXT,                  "COMPOUND_TEXT", },
};



void mainwin_show(GtkWidget *widget, gpointer *user_data)
{
	plugins_on_mainwin_show(widget, user_data);
}
void mainwin_hide(GtkWidget *widget, gpointer *user_data)
{
	plugins_on_mainwin_hide(widget, user_data);
}



int
on_mainwin_event                       (GtkWidget       *widget,
										GdkEvent        *event,
										gpointer         *user_data)
{
	MainWin *mwin = GNOME_MAINWIN(user_data);
	GdkEventButton *button_event = (GdkEventButton *) event;

	app_update_menus(mwin);
	if (event->type == GDK_BUTTON_PRESS && button_event->button == 3) {
		gtk_menu_popup (GTK_MENU(mwin->cliplistpopupmenu), NULL, NULL, NULL, NULL, 3, 
						button_event->time);
		return TRUE;
	}
	return FALSE;
}



void
on_mainwinmenu_newitem_activate        (GtkMenuItem     *menuitem,
                                        gpointer         *user_data)
{
	MainWin *mwin = GNOME_MAINWIN(user_data);
	app_create_and_show_newitemwin(mwin);
}

void
on_mainwinmenu_deleteitem_activate     (GtkMenuItem     *menuitem,
                                        gpointer         *user_data)
{
	MainWin *mwin = GNOME_MAINWIN(user_data);
	gtk_clist_freeze (GTK_CLIST(mwin->cliplist));
	app_delete_selected_items (mwin, GTK_CLIST(mwin->cliplist));
	gtk_clist_thaw (GTK_CLIST(mwin->cliplist));
}


void
on_mainwinmenu_moveup_activate         (GtkMenuItem     *menuitem,
                                        gpointer         *user_data)
{
	MainWin *mwin = GNOME_MAINWIN(user_data);
	GList *remaining;
	remaining = g_list_copy(GTK_CLIST(mwin->cliplist)->selection);
	if (remaining!=NULL) {
		gtk_clist_freeze (GTK_CLIST(mwin->cliplist));
		app_move_selected_up (mwin, remaining, -1);
		g_list_free(remaining);
		gtk_clist_thaw (GTK_CLIST(mwin->cliplist));
	}
	
}



void
on_mainwinmenu_movedown_activate         (GtkMenuItem     *menuitem,
                                        gpointer         *user_data)
{
	MainWin *mwin = GNOME_MAINWIN(user_data);
	GList *remaining;
	remaining = g_list_copy(GTK_CLIST(mwin->cliplist)->selection);
	if (remaining!=NULL) {
		gtk_clist_freeze (GTK_CLIST(mwin->cliplist));
		app_move_selected_down (mwin, remaining, -1);
		gtk_clist_thaw (GTK_CLIST(mwin->cliplist));
		g_list_free(remaining);
	}
}

 /* Callback from the overwrite questiondialog */
void
on_mainwin_overwrite_yes_no             (gint answer, gpointer *user_data)
{
	SaveAsData *sa = (SaveAsData*)user_data;
	if (answer == 0)
		if (!gcm_save_items_to_disk(sa->path, sa)) {
			gnome_error_dialog(_("IO error while trying to save file"));
		}
	saveasdata_free(sa);
}


 /* OK callback */
void
on_mainwin_saveallfilelist_ok_button_clicked
                                       (GtkButton *button,
                                        gpointer *user_data)
{
	MainWin *mwin = GNOME_MAINWIN(user_data);
	gchar *msg;
	gint row;
	GtkWidget *question;
	FILE *f;
	SaveAsData *sa;

	sa = g_malloc0(sizeof(SaveAsData));
	sa->items = NULL;

	for (row=0; row< GTK_CLIST(mwin->cliplist)->rows; row++) {
		sa->items = g_list_append(sa->items, gtk_clist_get_row_data (GTK_CLIST(mwin->cliplist), row));
	}
	sa->path = g_strdup(gtk_file_selection_get_filename(GTK_FILE_SELECTION(mwin->savefilelist)));
	
	gtk_widget_destroy(mwin->savefilelist);
	if (f=fopen(sa->path,"r")) {
		fclose(f);
		msg = g_strdup_printf("A file named ''%s'' already exists.\n"
							  "Do you want to replace it with the "
							  "one you are saving?", sa->path);
		question = gnome_question_dialog_modal
				(_(msg), (GnomeReplyCallback) (on_mainwin_overwrite_yes_no), sa);
		gtk_widget_show(question);
	} else {
		if (gcm_save_items_to_disk(sa->path, sa)==-1) {
			gnome_error_dialog(_("IO error while trying to save file"));
		}
		saveasdata_free(sa);
	}
	mwin->savefilelist_inuse=FALSE;
	app_update_menus(mwin);
}


void *savefilestodisk_thread (gpointer *user_data) 
{
	MainWin *mwin = GNOME_MAINWIN(user_data);
	gchar *msg;
	gint row;
	GList *selected;
	GtkWidget *question;
	FILE *f;
	SaveAsData *sa;

	sa = g_malloc0(sizeof(SaveAsData));
	sa->items = NULL;

	selected = GTK_CLIST(mwin->cliplist)->selection;
	if (selected) {
		while (selected) {
			row = (gint) selected->data;
			sa->items = g_list_append(sa->items, gtk_clist_get_row_data (GTK_CLIST(mwin->cliplist), row));
			selected = g_list_next(selected);
		}
	} else { 
		gnome_error_dialog(_("No item selected"));
		mwin->savefilelist_inuse=FALSE;
		app_update_menus(mwin);
		return;
	}
	
	sa->path = g_strdup(gtk_file_selection_get_filename(GTK_FILE_SELECTION(mwin->savefilelist)));

	gtk_widget_destroy(mwin->savefilelist);
	if (f=fopen(sa->path,"r")) {
		fclose(f);
		msg = g_strdup_printf("A file named ''%s'' already exists.\n"
							  "Do you want to replace it with the "
							  "one you are saving?", sa->path);
		question = gnome_question_dialog_modal
				(_(msg), (GnomeReplyCallback) (on_mainwin_overwrite_yes_no), sa);
		gtk_widget_show(question);
	} else {
		if (gcm_save_items_to_disk(sa->path, sa)==-1) {
			gnome_error_dialog(_("IO error while trying to save file"));
		}
		saveasdata_free(sa);
	}
	mwin->savefilelist_inuse=FALSE;
	app_update_menus(mwin);
}

 /* OK Callback from the gnomefilelist (save) */
void
on_mainwin_savefilelist_ok_button_clicked
                                       (GtkButton *button,
                                        gpointer *user_data)
{
	/*TODO: MainWin *mwin = GNOME_MAINWIN(user_data);*/
	/*GThread *savet; BUGGY
	savet = g_thread_create ((GThreadFunc)savefilestodisk_thread, mwin, FALSE,NULL);
	gdk_threads_enter();*/
	savefilestodisk_thread(user_data);

}
/* CANCEL Callback from the gnomefilelist (save) */
void 
on_mainwin_savefilelist_cancel_button_clicked
                                       (GtkButton *button,
                                        gpointer *user_data)
{
	MainWin *mwin = GNOME_MAINWIN(user_data);
	mwin->savefilelist_inuse=FALSE;
	app_update_menus(mwin);
	gtk_widget_destroy(mwin->savefilelist);
}


void 
on_mainwinmenu_file_savetodisk_activate
                                       (GtkMenuItem     *menuitem,
                                        gpointer         *user_data)
{
	MainWin *mwin = GNOME_MAINWIN(user_data);

	GList *selected;

	selected = GTK_CLIST(mwin->cliplist)->selection;
	if (selected) {
		selected = g_list_first (selected);
		if (selected == NULL) {
			mwin->savefilelist_inuse=FALSE;
			app_update_menus(mwin);			
			gnome_error_dialog(_("No item selected"));
			return;
		}
		mwin->savefilelist_inuse=TRUE;
		app_update_menus(mwin);
		
		mwin->savefilelist = gtk_file_selection_new ("Save to XML file");

		gtk_widget_show(mwin->savefilelist);

		g_signal_connect (G_OBJECT (mwin->savefilelist), "destroy",
				  G_CALLBACK (on_mainwin_savefilelist_cancel_button_clicked),
				  mwin);
		
		g_signal_connect (G_OBJECT (GTK_FILE_SELECTION(mwin->savefilelist)->ok_button),
   					  "clicked", G_CALLBACK (on_mainwin_savefilelist_ok_button_clicked),
   					  mwin);

		g_signal_connect (G_OBJECT (GTK_FILE_SELECTION(mwin->savefilelist)->cancel_button),
   					  "clicked", G_CALLBACK (on_mainwin_savefilelist_cancel_button_clicked),
   					  mwin);


	} else {
		gnome_error_dialog(_("No item selected"));
	}
}

void 
on_mainwinmenu_file_savealltodisk_activate
                                        (GtkMenuItem    *menuitem,
                                        gpointer         *user_data)
{
	MainWin *mwin = GNOME_MAINWIN(user_data);
	mwin->savefilelist_inuse=TRUE;
	app_update_menus(mwin);

	mwin->savefilelist = gtk_file_selection_new ("Save to XML file");

	gtk_widget_show(mwin->savefilelist);

	g_signal_connect (G_OBJECT (mwin->savefilelist), "destroy",
			  G_CALLBACK (on_mainwin_savefilelist_cancel_button_clicked),
			  mwin);
		
	g_signal_connect (G_OBJECT (GTK_FILE_SELECTION(mwin->savefilelist)->ok_button),
   				  "clicked", G_CALLBACK (on_mainwin_saveallfilelist_ok_button_clicked),
   				  mwin);

	g_signal_connect (G_OBJECT (GTK_FILE_SELECTION(mwin->savefilelist)->cancel_button),
   				  "clicked", G_CALLBACK (on_mainwin_savefilelist_cancel_button_clicked),
   				  mwin);

}



/* OK callback from the gnomefilelist (open) Loads items from disk */
void 
on_mainwin_openfilelist_ok_button_clicked
                                       (GtkButton *button,
                                        gpointer *user_data)
{
	MainWin *mwin = GNOME_MAINWIN(user_data);
	gchar *path;
	gint i;

	path = g_strdup(gtk_file_selection_get_filename(GTK_FILE_SELECTION(mwin->openfilelist)));
	mwin->openfilelist_inuse=FALSE;
	app_update_menus(mwin);
	gtk_widget_destroy(mwin->openfilelist);
	if (i = app_load_items_from_disk(mwin, path) != LOAD_ERROR_OK) {
		switch (i) {
			case LOAD_ERROR_NOT_XML:
				gnome_error_dialog(_("Not an XML file!"));
			break;
			case LOAD_ERROR_EMPTY:
				gnome_error_dialog(_("Empty document!"));
			break;
			case LOAD_ERROR_FORMAT:
				gnome_error_dialog(_("This is not a GCM XML file!"));
			break;
			default:
				gnome_error_dialog(_("Unknown error while loading file!"));
			break;
		}
	}
	g_free(path);
}

/* CANCEL callback from the gnomefilelist (open) */
void 
on_mainwin_openfilelist_cancel_button_clicked
                                       (GtkButton *button,
                                        gpointer *user_data)
{
	MainWin *mwin = GNOME_MAINWIN(user_data);
	mwin->openfilelist_inuse=FALSE;
	app_update_menus(mwin);
	gtk_widget_destroy(mwin->openfilelist);
}


void 
on_mainwinmenu_file_openfromdisk_activate
                                       (GtkMenuItem     *menuitem,
                                        gpointer         *user_data)
{
	MainWin *mwin = GNOME_MAINWIN(user_data);
	mwin->openfilelist_inuse=TRUE;
	app_update_menus(mwin);
	mwin->openfilelist = gtk_file_selection_new ("Load from XML file");
	gtk_widget_show(mwin->openfilelist);
	
	g_signal_connect (G_OBJECT (mwin->openfilelist), "destroy",
				  G_CALLBACK (on_mainwin_openfilelist_cancel_button_clicked),
				  mwin);
	
	g_signal_connect (G_OBJECT (GTK_FILE_SELECTION(mwin->openfilelist)->cancel_button),
   					  "clicked", G_CALLBACK (on_mainwin_openfilelist_cancel_button_clicked),
   					  mwin);
	
	g_signal_connect (G_OBJECT (GTK_FILE_SELECTION(mwin->openfilelist)->ok_button),
   					  "clicked", G_CALLBACK (on_mainwin_openfilelist_ok_button_clicked),
   					  mwin);
}


void 
on_mainwinmenu_exit_activate           (GtkMenuItem     *menuitem,
                                        gpointer         *user_data)
{
	if (user_data != NULL) {
		MainWin *mwin = GNOME_MAINWIN(user_data);
		gtk_widget_hide(GTK_WIDGET(mwin));
	}
}


/* Cut selected items (copy and delete) */
void 
on_mainwinmenu_cut_activate            (GtkMenuItem     *menuitem,
                                        gpointer         *user_data)
{
	MainWin *mwin = GNOME_MAINWIN(user_data);
	on_mainwinmenu_copy_activate (menuitem, user_data);
	gtk_clist_freeze (GTK_CLIST(mwin->cliplist));
	app_delete_selected_items(mwin, GTK_CLIST(mwin->cliplist));
	gtk_clist_thaw (GTK_CLIST(mwin->cliplist));
}


/* Copy selected items (copy to gcm's copypaste memory) */
void 
on_mainwinmenu_copy_activate           (GtkMenuItem     *menuitem,
                                        gpointer         *user_data)
{
	MainWin *mwin = GNOME_MAINWIN(user_data);
	GList *selected = GTK_CLIST(mwin->cliplist)->selection;

	/* Free all stuff that is in the mwin->copypaste */
	if (mwin->copypaste!=NULL) {
		GList *tselected = g_list_copy(mwin->copypaste);
		while (tselected) {
			Selection *item = (Selection*) tselected->data;
			selection_free(item);
			tselected = g_list_next(tselected);
		}
		g_list_free(tselected);
		g_list_free(mwin->copypaste);
	}
	mwin->copypaste = NULL;
	/* copy all selected items into mwin->copypaste */
	if (selected) {
		gtk_clist_freeze (GTK_CLIST(mwin->cliplist));
		while (selected) {
			GList *targets=NULL, *newtargets=NULL;
			Selection *nitem, *mitem;
			/*gchar *text;*/
			nitem=(Selection*) g_malloc0(sizeof(Selection));
			mitem = gtk_clist_get_row_data(GTK_CLIST(mwin->cliplist),(gint) selected->data);
			if (mitem == NULL) continue;

			nitem->time = g_strdup(mitem->time);
			nitem->from = g_strdup(mitem->from);
			targets = g_list_copy(mitem->targets);
			while (targets) {
				GtkSelectionData *data = (GtkSelectionData*)targets->data;
				GtkSelectionData *newdata;/* = (GtkSelectionData*)g_malloc0(sizeof(GtkSelectionData));*/
				newdata = gtk_selection_data_copy (data);
				newtargets = g_list_append(newtargets, newdata);
				targets=g_list_next(targets);
			}
			nitem->targets = newtargets;/*g_list_copy(newtargets);*/
			mwin->copypaste = g_list_append(mwin->copypaste, nitem);
			selected = g_list_next(selected);
		}
	gtk_clist_thaw (GTK_CLIST(mwin->cliplist));
	g_list_free(selected);
	}
}


/* Get items from gcm's copypaste memory */
void 
on_mainwinmenu_paste_activate          (GtkMenuItem     *menuitem,
                                        gpointer         *user_data)
{
	MainWin *mwin = GNOME_MAINWIN(user_data);
	GList *selected = g_list_copy(mwin->copypaste);

	if (selected) {
		gtk_clist_freeze (GTK_CLIST(mwin->cliplist));
		while (selected) {
			Selection *nitem, *item;
			GList *targets=NULL, *newtargets=NULL;
			nitem=(Selection*) g_malloc0(sizeof(Selection));
			item = (Selection*) selected->data;

			nitem->time = g_strdup(item->time);
			nitem->from = g_strdup(item->from);

			targets = g_list_copy(item->targets);
			
			while (targets) {
				GtkSelectionData *data = (GtkSelectionData*)targets->data;
				GtkSelectionData *newdata;/* = (GtkSelectionData*)g_malloc0(sizeof(GtkSelectionData));*/
				newdata = gtk_selection_data_copy (data);
				newtargets = g_list_append(newtargets, newdata);
				targets=g_list_next(targets);
			}
			/*nitem->targets = g_list_copy(newtargets);*/
			nitem->targets = newtargets;
			app_add_selection(mwin, nitem);
			selected = g_list_next(selected);
		}
		gtk_clist_thaw (GTK_CLIST(mwin->cliplist));
		g_list_free(selected);
	}
	/*app_get_new_item (mwin);*/
}

void 
on_mainwinmenu_clear_activate          (GtkMenuItem     *menuitem,
                                        gpointer         *user_data)
{
	MainWin *mwin = GNOME_MAINWIN(user_data);
	gtk_clist_freeze (GTK_CLIST(mwin->cliplist));
	gtk_clist_select_all (GTK_CLIST(mwin->cliplist));
	app_delete_selected_items(mwin, GTK_CLIST(mwin->cliplist));
	/*gtk_clist_clear (GTK_CLIST(mwin->cliplist)); Does not free the items !*/
	gtk_clist_thaw (GTK_CLIST(mwin->cliplist));
}


void 
on_mainwinmenu_selectall_activate     (GtkMenuItem     *menuitem,
                                        gpointer         *user_data)
{
	MainWin *mwin = GNOME_MAINWIN(user_data);
	gtk_clist_freeze (GTK_CLIST(mwin->cliplist));
	gtk_clist_select_all (GTK_CLIST(mwin->cliplist));
	gtk_clist_thaw (GTK_CLIST(mwin->cliplist));
}

void 
on_mainwinmenu_selectnone_activate     (GtkMenuItem     *menuitem,
                                        gpointer         *user_data)
{
	MainWin *mwin = GNOME_MAINWIN(user_data);
	gtk_clist_freeze (GTK_CLIST(mwin->cliplist));
	gtk_clist_unselect_all (GTK_CLIST(mwin->cliplist));
	gtk_clist_thaw (GTK_CLIST(mwin->cliplist));
}

void 
on_mainwinmenu_preferences_activate    (GtkMenuItem     *menuitem,
                                        gpointer         *user_data)
{
	MainWin *mwin = GNOME_MAINWIN(user_data);
	app_show_prefswin (mwin);
}


void 
on_mainwinmenu_about_activate          (GtkMenuItem     *menuitem,
                                        gpointer         *user_data)
{
	MainWin *mwin = GNOME_MAINWIN(user_data);
	app_create_and_show_aboutwin (mwin);
}



void 
on_cliplistpopupmenu_new_activate      (GtkMenuItem     *menuitem,
                                        gpointer         *user_data)
{
	MainWin *mwin = GNOME_MAINWIN(user_data);
	app_create_and_show_newitemwin (mwin);
}


void 
on_cliplistpopupmenu_cut_activate      (GtkMenuItem     *menuitem,
                                        gpointer         *user_data)
{
	
	on_mainwinmenu_cut_activate (NULL, user_data);
}

void 
on_cliplistpopupmenu_copy_activate     (GtkMenuItem     *menuitem,
                                        gpointer         *user_data)
{
	
	on_mainwinmenu_copy_activate (NULL, user_data);
}

void 
on_cliplistpopupmenu_paste_activate    (GtkMenuItem     *menuitem,
                                        gpointer         *user_data)
{
	
	on_mainwinmenu_paste_activate (NULL, user_data);
}

void 
on_cliplistpopupmenu_edit_activate     (GtkMenuItem     *menuitem,
                                        gpointer         *user_data)
{
	MainWin *mwin = GNOME_MAINWIN(user_data);
/* Checkout toolbarmenu's callback too */
	GList *selected;
	if (GTK_CLIST(mwin->cliplist)->selection == NULL) {
		gnome_error_dialog(_("No item selected"));
		return;
	}
	selected = g_list_copy(GTK_CLIST(mwin->cliplist)->selection);

	while (selected) {
		app_create_and_show_textitemwin (mwin, (gint) selected->data);
		selected = g_list_next(selected);
	}
	g_list_free(selected);

}



/* Convert the item to collection of items by parsing for xml */
void 
on_cliplistpopupmenu_convert_activate  (GtkMenuItem     *menuitem,
                                        gpointer         *user_data)
{
	MainWin *mwin = GNOME_MAINWIN(user_data);
	GList *selected;

	selected = GTK_CLIST(mwin->cliplist)->selection;
	if (selected) {
		while (selected) {
			gchar *xml=NULL;
			Selection *item = gtk_clist_get_row_data(GTK_CLIST(mwin->cliplist),(gint) selected->data);
			GList *targets;
			if (item==NULL) continue;
			targets = g_list_copy(item->targets);
			if (targets==NULL) continue;
			while (targets) {
				GtkSelectionData *data = (GtkSelectionData*)targets->data;
				if (data==NULL)
					break;
				if (data->type == (gpointer)XA_COMPOUND_TEXT) {
					xml = g_strdup(data->data);
					break;
				}
				targets=g_list_next(targets);
			}
			if (xml != NULL) {
				gint i;
				if (i = app_load_items_from_mem(mwin, xml, strlen(xml)) != STREAM_ERROR_OK) {
					switch (i) {
						case STREAM_ERROR_NOT_XML:
						gnome_error_dialog(_("Not an XML stream!"));
						break;
						case STREAM_ERROR_EMPTY:
						gnome_error_dialog(_("Empty stream!"));
						break;
						case STREAM_ERROR_FORMAT:
						gnome_error_dialog(_("This is not a GCM XML stream!"));
						break;
						default:
						gnome_error_dialog(_("Unknown error while processing stream!"));
						break;
					}
				}
				g_free(xml);
			}
			selected = g_list_next(selected);
		}
		g_list_free(selected);
	}
}
void
on_cliplistpopupmenu_merge_activate  (GtkMenuItem     *menuitem,
                                        gpointer         *user_data)
{
	MainWin *mwin = GNOME_MAINWIN(user_data);
	app_merge_selectedrows(mwin);
}



void 
on_toolbar_newitem_clicked             (GtkButton       *button,
                                        gpointer         *user_data)
{
	MainWin *mwin = GNOME_MAINWIN(user_data);
	app_create_and_show_newitemwin (mwin);
}


void 
on_toolbar_clear_clicked              (GtkButton       *button,
                                        gpointer         *user_data)
{
	on_mainwinmenu_clear_activate(NULL, user_data);
}

void 
on_toolbar_deleteitem_clicked          (GtkButton       *button,
                                        gpointer         *user_data)
{
	MainWin *mwin = GNOME_MAINWIN(user_data);
	gtk_clist_freeze (GTK_CLIST(mwin->cliplist));
	app_delete_selected_items(mwin, GTK_CLIST(mwin->cliplist));
	gtk_clist_thaw (GTK_CLIST(mwin->cliplist));
}

void 
on_toolbar_edititem_clicked            (GtkButton       *button,
                                        gpointer         *user_data)
{
	MainWin *mwin = GNOME_MAINWIN(user_data);
/* Checkout cliplistpopupmenu's callback too */
	GList *selected;
	if (GTK_CLIST(mwin->cliplist)->selection == NULL) {
		gnome_error_dialog(_("No item selected"));
		return;
	}
	selected = g_list_copy(GTK_CLIST(mwin->cliplist)->selection);

	while (selected) {
		app_create_and_show_textitemwin (mwin, (gint) selected->data);
		selected = g_list_next(selected);
	}
	g_list_free(selected);/*
	on_cliplistpopupmenu_edit_activate (NULL, user_data);*/
}


void 
on_toolbar_preferences_clicked         (GtkButton       *button,
                                        gpointer         *user_data)
{
	MainWin *mwin = GNOME_MAINWIN(user_data);
	app_show_prefswin (mwin);
}


/* Refresh = get current = get new item (bad naming) (toobar) */
void 
on_toolbar_refresh_clicked             (GtkButton       *button,
                                        gpointer         *user_data)
{
	MainWin *mwin = GNOME_MAINWIN(user_data);
	app_get_new_item (mwin);
}


void 
on_toolbar_quit_clicked                (GtkButton       *button,
                                        gpointer         *user_data)
{
	on_mainwinmenu_exit_activate(NULL, user_data);
}





/* Select a row (return on doubleclick!) */
void 
on_cliplist_select_row                 (GtkCList        *clist,
                                        gint             row,
                                        gint             column,
                                        GdkEvent        *event,
                                        gpointer         *user_data)

{
	MainWin *mwin = GNOME_MAINWIN(user_data);
	Selection *item = gtk_clist_get_row_data (clist, row);
	
	app_update_menus(mwin);
	if (item!=NULL) {
		if ((event)&&(event->button.type == GDK_2BUTTON_PRESS)) {
			app_create_and_show_textitemwin (mwin, row);
			return;
		} else {
				app_claim_selection(mwin, item);
			return;
		}
	}
}


/* unselect row (update menu's) */
void 
on_cliplist_unselect_row               (GtkCList        *clist,
                                        gint             row,
                                        gint             column,
                                        GdkEvent        *event,
                                        gpointer         *user_data)
{
	MainWin *mwin = GNOME_MAINWIN(user_data);
	Selection *item = gtk_clist_get_row_data (clist, row);
	item = gcm_compress_selection(item);
	app_update_menus(mwin);
}

/* on any event (calling popupmenu on rightclick) */
int 
on_cliplist_event                      (GtkCList        *clist,
                                        GdkEvent        *event,
                                        gpointer         *user_data)
{
	MainWin *mwin = GNOME_MAINWIN(user_data);
	GdkEventButton *button_event = (GdkEventButton *) event;
	
	if (event->type == GDK_BUTTON_PRESS && button_event->button == 3) {
		app_update_menus(mwin);
		gtk_menu_popup (GTK_MENU(mwin->cliplistpopupmenu), NULL, NULL, NULL, NULL, 3, 
						button_event->time);
		return TRUE;
	}
	return FALSE;
}



/* If a row got removed (free the attached Selection) */
void cliplist_row_removed (gpointer *user_data)
{ 
	Selection *item = (Selection*)user_data;
	if (item!=NULL) selection_free(item);
}



void clipboard_get_selection_cb (GtkClipboard *clipboard,
                                 GtkSelectionData *selection_data,
                                 guint info,
                                 gpointer user_data_or_owner) 
{
	MainWinAndItem *itemmwin = (MainWinAndItem*)user_data_or_owner;
#ifdef DEVELOPMENT
	MainWin *mwin = itemmwin->mwin;
#endif
	Selection *item = itemmwin->item;
	item = gcm_decompress_selection (item);
	GList *targets=NULL;
	if (item == NULL) return;
	targets = g_list_copy(item->targets);
	if (targets == NULL) return;
targets = g_list_first(targets);
#ifdef DEVELOPMENT
	if (mwin->prefs->debug)
	g_print("GCM: I received a request for the selection target=%s\n", gdk_atom_name(selection_data->target));
#endif

	while (targets) {
		GtkSelectionData *data = (GtkSelectionData*)targets->data;
		if (data==NULL) {
			targets=g_list_next(targets);
			continue;
		}
		if (selection_data->target == data->type) {
#ifdef DEVELOPMENT
	if (mwin->prefs->debug)
			g_print("GCM: I can answer the request\n");
#endif

			gtk_selection_data_set(selection_data,
                                   data->type,
                                   data->format,
                                   data->data,
                                   data->length);
	
			/*question: should we break here? */
			/*break;*/
		}
		targets=g_list_next(targets);
	}
	g_list_free(targets);
}

void clipboard_clear_selection_cb(GtkClipboard *clipboard,
                                  gpointer user_data_or_owner)
{

	MainWinAndItem *itemmwin = (MainWinAndItem*)user_data_or_owner;
	MainWin *mwin = itemmwin->mwin;

   /*Unused	Selection *item = itemmwin->item;*/
	if (mwin==NULL) return;
#ifdef DEVELOPMENT
	if (mwin->prefs->debug)
	g_print("GCM: I lost the selectionownership\n");
#endif
	if (mwin->prefs->auto_collect==TRUE ) {
#ifdef DEVELOPMENT
	if (mwin->prefs->debug)
	g_print("GCM: I am now going to get the new selection from the new owner\n");
#endif
		app_get_new_item_from_clipboard (mwin, clipboard, itemmwin->selection);
	}

	return;
}




gint primary_clipboard_idle_func(gpointer data)
{
	if (data != NULL) {
		ClipboardPollerData *cbpd = (ClipboardPollerData*) data;
		Window new_owner;
		
		if (!cbpd->mwin->primary_poller) {
			cbpd->mwin->primary_poller_is_running=FALSE;
			return FALSE;
		} else cbpd->mwin->primary_poller_is_running=TRUE;

		new_owner = XGetSelectionOwner(gdk_display, cbpd->selection);
		if (new_owner != cbpd->mwin->last_primary_owner) {
			cbpd->mwin->last_primary_owner = new_owner;
			if ((new_owner != BadWindow) && (new_owner!=None))
				app_get_new_item_from_clipboard (cbpd->mwin, cbpd->clipboard, GDK_SELECTION_PRIMARY);

			return TRUE;
		}

		return TRUE;
	} else return FALSE;
}

gint clipboard_clipboard_idle_func(gpointer data)
{
	if (data != NULL) {
		ClipboardPollerData *cbpd = (ClipboardPollerData*) data;
		Window new_owner;
		
		if (!cbpd->mwin->clipboard_poller) {
			cbpd->mwin->clipboard_poller_is_running=FALSE;
			return FALSE;
		} else cbpd->mwin->clipboard_poller_is_running=TRUE;


		new_owner = XGetSelectionOwner(gdk_display, cbpd->selection);
		if (new_owner != cbpd->mwin->last_clipboard_owner) {
			cbpd->mwin->last_clipboard_owner = new_owner;
			if ((new_owner != BadWindow) && (new_owner!=None))
				app_get_new_item_from_clipboard (cbpd->mwin, cbpd->clipboard, GDK_SELECTION_CLIPBOARD);

			return TRUE;
		}

		return TRUE;
	} else return FALSE;
}

gint get_new_item_from_clipboard_in_a_few_seconds_idle_func (gpointer user_data_or_owner)
{
	MainWinAndItem *itemmwin = (MainWinAndItem*)user_data_or_owner;
	MainWin *mwin = itemmwin->mwin;
	GtkClipboard *clipboard = itemmwin->clipboard;
	GdkAtom selection = itemmwin->selection;
	
	app_get_new_item_from_clipboard (mwin, clipboard, selection);
	
	return FALSE;
}


