/* * C o p y r i g h t *(also read COPYING)* * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *  Copyright (C) 2003  Philip Van Hoof <me@freax.org>                         *
 *                                                                             *
 *  This program is free software; you can redistribute it and/or modify       *
 *  it under the terms of the GNU General Public License as published by       *
 *  the Free Software Foundation; either version 2 of the License, or          *
 *  (at your option) any later version.                                        *
 *                                                                             *
 *  This program is distributed in the hope that it will be useful,            *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *  GNU General Public License for more details.                               *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License          *
 *  along with this program; if not, write to the Free Software                *
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA  *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
 
/*  A lot pieces of this code come from the XMMS sources (http://www.xmms.org)
 * 
 *  XMMS - Cross-platform multimedia player
 *  Copyright (C) 1998-2002  Peter Alm, Mikael Alm, Olle Hallnas,
 *                           Thomas Nilsson and 4Front Technologies
 *  Copyright (C) 1999-2002  Haavard Kvaalen
 */
#include <gnome.h>

#ifndef CONTROLSOCKET_H
#define CONTROLSOCKET_H

#define GCM_PROTOCOL_VERSION	1

gboolean setup_ctrlsocket(gpointer *mwinp);
void cleanup_ctrlsocket(void);
void start_ctrlsocket(void);
void check_ctrlsocket(void);
gint ctrlsocket_get_session_id(void);

enum
{
	CMD_GET_VERSION,CMD_MWIN_HIDE,CMD_MWIN_SHOW,CMD_PING,CMD_SELECT_ROW,
	CMD_PREFS_SHOW,CMD_GET_ROWCNT,CMD_GET_ROWPREVIEW,CMD_GET_ROWDATE,
	CMD_GET_ROWFROM, CMD_SHOW_TEXTITEMWIN, CMD_NEWWIN_SHOW, CMD_ABOUT_SHOW,
	CMD_SAVE_ALL, CMD_SAVE, CMD_DELETE_SELECTED,CMD_GET_NEW, CMD_MERGE_SELECTED,
	CMD_SELECT_NONE, CMD_SELECT_ALL, CMD_CLEAR, CMD_EXIT_GCM, CMD_ADD_TEXTSEL,
	CMD_PREFS_HIDE, CMD_NEWWIN_HIDE, CMD_MOVE_SELECTED_UP, CMD_MOVE_SELECTED_DOWN
};

typedef struct
{
	guint16 version;
	guint16 command;
	guint32 data_length;
}
ClientPktHeader;

typedef struct
{
	guint16 version;
	guint32 data_length;
}
ServerPktHeader;

#endif
