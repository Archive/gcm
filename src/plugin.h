/* * C o p y r i g h t *(also read COPYING)* * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *  Copyright (C) 2003  Philip Van Hoof <me@freax.org>                         *
 *                                                                             *
 *  This program is free software; you can redistribute it and/or modify       *
 *  it under the terms of the GNU General Public License as published by       *
 *  the Free Software Foundation; either version 2 of the License, or          *
 *  (at your option) any later version.                                        *
 *                                                                             *
 *  This program is distributed in the hope that it will be useful,            *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *  GNU General Public License for more details.                               *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License          *
 *  along with this program; if not, write to the Free Software                *
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA  *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifdef HAVE_CONFIG_H
#  include "../config.h"
#endif

#include <gnome.h>

#define PLUGIN_ENABLED_CONFIG_KEY(obj)		g_strdup_printf ("/apps/gcm/plugins/%s/enabled", (obj).name)

#ifdef __cplusplus
extern "C" {
#endif
/* This plugin code is like the plugin code of xmms */
typedef struct _GcmPlugin GcmPlugin;

struct _GcmPlugin {
	void *handle;		/* Filled in by gcm */
	char *filename;		/* Filled in by gcm */
	char *description;	/* The description that is shown in the preferences box */
	char *name;
	gboolean enabled;
	/* Standard functions (must exist) */
	void (*init) (GtkWidget *mainwin); /* Happens when the plugin gets added */
	void (*about) (GtkWidget *mainwin); /* To show an aboutbox */
	void (*configure) (GtkWidget *mainwin); /* To show the configure-box of the plugin */
	void (*cleanup) (void); /* To cleanup the plugin (free stuff) */

	/* Events that happen in Gnome Clipboard Manager */
	void (*on_mainwin_show) (GtkWidget *mainwin, gpointer *user_data);
	void (*on_mainwin_hide) (GtkWidget *mainwin, gpointer *user_data);
	void (*on_plugin_added) (GtkWidget *mainwin, GcmPlugin *p);
	void (*on_firsttime) (gpointer *client, gpointer *prefs);
	void (*on_reload_prefs) (GtkWidget *mainwin);
	void (*on_update_item) (GtkWidget *mainwin, gint row, gpointer *user_data);
	void (*on_add_item) (GtkWidget *mainwin, gpointer *item, gint row);
	void (*on_remove_item) (GtkWidget *mainwin, gpointer *item, gint row);
	
	void (*on_selection_claimed) (GtkWidget *mainwin, GtkClipboard *clipboard,
								  GtkTargetEntry *ttargets, gpointer *item);

	void (*on_selectiondata_received) (GtkWidget *mainwin, gpointer *item, GtkSelectionData *data);
	GcmPlugin * (*i_will_handle_that_target) (GdkAtom target);
	gboolean (*handle_target_tab) (gpointer *mytab, GtkSelectionData *data);
	GtkSelectionData * (*handle_target_merge) (GtkSelectionData *old, GtkSelectionData *in);

};

void* find_dynamic_symbol(void *handle, char *symbol);
void dynamic_lib_error(void);
void close_dynamic_lib(void* handle);
void* open_dynamic_lib(char *filename);

gboolean pluginsystem_ready (void);
void add_plugin(gchar * filename);
void init_plugins(gpointer *mwinp);
void cleanup_plugins(void);
void scan_plugins(char *dirname);
void cleanup_plugins(void);
int plugin_check_duplicate(char *filename);

void plugin_enable(GcmPlugin *pe);
void plugin_disable(GcmPlugin *pe);
gboolean plugin_enabled (GcmPlugin *pe);


void plugins_on_mainwin_show			(GtkWidget *mainwin, gpointer *user_data);
void plugins_on_mainwin_hide			(GtkWidget *mainwin, gpointer *user_data);
void plugins_on_plugin_added			(GtkWidget *mainwin, GcmPlugin *pe);
void plugins_on_firsttime				(gpointer *client, gpointer *prefs);
void plugins_on_reload_prefs			(GtkWidget *mainwin);
void plugins_on_update_item				(GtkWidget *mainwin, gint row, gpointer *user_data);
void plugins_on_add_item				(GtkWidget *mainwin, gpointer *item, gint row);
void plugins_on_remove_item				(GtkWidget *mainwin, gpointer *item, gint row);
void plugins_on_selection_claimed		(GtkWidget *mainwin, GtkClipboard *clipboard,
										 GtkTargetEntry *ttargets, gpointer *item);
void plugins_on_selectiondata_received	(GtkWidget *mainwin,gpointer *item, GtkSelectionData *data);

GcmPlugin *plugins_i_will_handle_that_target (GdkAtom target);


GcmPlugin * get_plugin_pointer_by_filename (GtkWidget *mainwin, gchar *filename);
GcmPlugin * get_plugin_pointer_by_description (GtkWidget *mainwin, gchar *des);

#ifdef __cplusplus /* cpp compatibility */
}
#endif
