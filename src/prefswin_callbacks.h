/* * C o p y r i g h t *(also read COPYING)* * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *  Copyright (C) 2003  Philip Van Hoof <me@freax.org>                         *
 *                                                                             *
 *  This program is free software; you can redistribute it and/or modify       *
 *  it under the terms of the GNU General Public License as published by       *
 *  the Free Software Foundation; either version 2 of the License, or          *
 *  (at your option) any later version.                                        *
 *                                                                             *
 *  This program is distributed in the hope that it will be useful,            *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *  GNU General Public License for more details.                               *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License          *
 *  along with this program; if not, write to the Free Software                *
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA  *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifdef HAVE_CONFIG_H
#  include "../config.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif
/* prefswinmenu Callbacks */

void prefswin_show(GtkWidget *widget, gpointer user_data);
void prefswin_hide(GtkWidget *widget, gpointer user_data);
	
void
on_close_button_clicked
                                       (GtkButton    *button,
                                        gpointer     user_data);
	
void
on_plugconfig_button_clicked
                                       (GtkButton    *button,
                                        gpointer     user_data);

void
on_plugabout_button_clicked
                                       (GtkButton    *button,
										gpointer     user_data);


void
on_beep_changed  (GtkWidget    *toggle,
				gpointer     user_data);

void
on_collect_changed  (GtkWidget    *toggle,
                     gpointer     user_data);


void
on_maxitems_changed (GtkWidget *spin,
					 gpointer user_data);

void on_clipboard_only_toggled (GtkToggleButton *togglebutton,
                                            gpointer user_data);
void on_clipboard_and_primary_toggled (GtkToggleButton *togglebutton,
                                            gpointer user_data);
void on_primary_only_toggled (GtkToggleButton *togglebutton,
                                            gpointer user_data);

void
plugin_enabled_item_toggled (GtkCellRendererToggle *cell,
              gchar                 *path_str,
              gpointer               data);


#ifdef __cplusplus /* cpp compatibility */
}
#endif
