/* * C o p y r i g h t *(also read COPYING)* * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *  Copyright (C) 2003  Philip Van Hoof <me@freax.org>                         *
 *                                                                             *
 *  This program is free software; you can redistribute it and/or modify       *
 *  it under the terms of the GNU General Public License as published by       *
 *  the Free Software Foundation; either version 2 of the License, or          *
 *  (at your option) any later version.                                        *
 *                                                                             *
 *  This program is distributed in the hope that it will be useful,            *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *  GNU General Public License for more details.                               *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License          *
 *  along with this program; if not, write to the Free Software                *
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA  *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */


#ifdef HAVE_CONFIG_H
#  include "../config.h"
#endif





#include <gnome.h>
#include <X11/Xlib.h> 
#include <X11/Xatom.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h> 
#include <gdk/gdk.h>
#include <gdk/gdkprivate.h>
#include <gdk/gdkx.h>
#include <gtk/gtk.h>

#ifdef __cplusplus
extern "C" {
#endif
#define MAXTARGETS 150
#define GNOME_TYPE_MAINWIN (mainwin_get_type())
#define GNOME_MAINWIN(obj) (GTK_CHECK_CAST((obj), GNOME_TYPE_MAINWIN, MainWin))
#define GNOME_MAINWIN_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass), GNOME_TYPE_MAINWIN, MainWinClass))
#define GNOME_IS_MAINWIN(obj) (GTK_CHECK_TYPE((obj), GNOME_TYPE_MAINWIN))
#define GNOME_IS_MAINWIN_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GNOME_TYPE_MAINWIN))

#define CLIST_DATE_FIELD 0
#define CLIST_FROM_FIELD 1
#define CLIST_DATA_FIELD 2
#define CLIST_FIELD_COUNT 3


#define CLIST_MAXLEN 200
#define CLIST_SPACE 120


#define PRIMARY_POLLER_TIMEOUT 1000
#define CLIPBOARD_POLLER_TIMEOUT 1000
#define GET_ITEM_WHEN_FAILED_TIMEOUT 1500

#define ADD_ITEM_ERROR_SELECTION_CONTAINED_NO_DATA 0
#define ADD_ITEM_ERROR_SELECTION_COMPRESSED -1

#define SELECTION_SETTING_CLIPBOARD_ONLY 0
#define SELECTION_SETTING_CLIPBOARD_AND_PRIMARY 1
#define SELECTION_SETTING_PRIMARY_ONLY 2
#define SELECTION_SETTING_ADVANCED 3

typedef struct _MainWin MainWin;
typedef struct _MainWinClass MainWinClass;
typedef struct _MainWinAndItem MainWinAndItem;
typedef struct _Selection Selection;
typedef struct _Prefs Prefs;
typedef struct _TargetManip TargetManip;
typedef struct _SaveAsData SaveAsData;
typedef struct _ClipboardPollerData ClipboardPollerData;
	
struct _ClipboardPollerData {
	GtkClipboard *clipboard;
	MainWin *mwin;
	Atom selection;
};

struct _MainWinAndItem {
	Selection *item;
	MainWin *mwin;
	GdkAtom selection;
	GtkClipboard *clipboard;
};


struct _Prefs {
	gint selection_setting;
	
	gboolean manage_clipboard;
	gboolean manage_primary;
	gboolean manage_secondary;

	gboolean claim_clipboard;
	gboolean claim_primary;
	gboolean claim_secondary;

#ifdef DEVELOPMENT
	gboolean debug;
#endif
	gboolean beep_on_collect;
	gboolean auto_collect;
	gboolean show_toolbar;
	gboolean show_menu;
	gboolean show_cliplist_titles;
	gboolean show_statusbar;
	gboolean show_from;
	gboolean show_time;
	gboolean show_type;
	gboolean show_datap;
	gboolean multisel;

	gboolean unselall;
	gboolean followmode;
	gboolean selectnew;

	gint maxitems;
	gboolean useplugins;

};


struct _Selection {
  gchar *time;
  gchar *from;

  gboolean compressed;

  Window owning_app;
  Window owning_window;

  GList *csizes;
  GList *targets;
};


struct _MainWin {
  GtkWindow window;
  GtkWidget *topvbox;
  GtkWidget *menuhandlebox;
  GtkWidget *toolbarhandlebox;
  GtkWidget *vbox;
  GtkWidget *mainwinmenu;
  GtkWidget *toolbar;
  GtkWidget *tmp_toolbar_icon;
  GtkWidget *toolbar_newitem;
  GtkWidget *toolbar_deleteitem;
  GtkWidget *toolbar_edititem;
  GtkWidget *toolbar_clear;
  GtkWidget *toolbar_preferences;
  GtkWidget *toolbar_refresh;
  GtkWidget *toolbar_quit;
  GtkWidget *scrolledwindow;
  GtkWidget *cliplist;
  GtkWidget *cliplistlabel_time;
  GtkWidget *cliplistlabel_type;
  GtkWidget *cliplistlabel_from;
  GtkWidget *cliplistlabel_data;
  GtkWidget *appbar;
  GtkWidget *cliplistpopupmenu;

  GtkWidget *prefswin;

  GtkWidget *newitemwin;

  GtkWidget *savefilelist;
  gboolean savefilelist_inuse;
 
  GtkWidget *openfilelist;
  gboolean openfilelist_inuse;
 
  GtkWidget *windowparent;
  gint lastnewrow;
  Selection *lastnewitem;
  Selection *currentitem;

  GdkAtom target_atom;
  Prefs *prefs;
  GList *copypaste;
  gchar *clipboard;
  GdkAtom clipboardtarget;
  GtkAccelGroup *accel_group;
#ifdef APPLET  
  GtkWidget *applet;
  GtkWidget *appletscroll;
  gint appletheight;
#endif

  Window new_owner;
  Window last_owning_window;
  
  Window last_primary_owner;
  gboolean primary_poller;
  gint primary_poller_id;
  gboolean primary_poller_is_running;

  Window last_clipboard_owner;
  gboolean clipboard_poller;
  gint clipboard_poller_id;
  gboolean clipboard_poller_is_running;

  gboolean iammanager;
 
  gchar *new_owner_title;
  gint argc;
  gchar **argv;
  GList *plugin_list;
  gint timeout;
  
  void (*plugin_update_item) (MainWin *mwin, gpointer *user_data);
  void (*plugin_update_row) (MainWin *mwin, gint row);
  void (*plugin_get_new_item) (MainWin *mwin) ;
  void (*plugin_delete_selected_items) (MainWin *mwin, GtkCList *cliplist);
  gint (*plugin_add_selection) (MainWin *mwin, Selection *item);
  gint (*plugin_add_text_selection) (MainWin *mwin, gchar *type, gchar *from, gchar *data);
  void (*plugin_update_menus) (MainWin *mwin);
  void (*plugin_reload_prefs) (MainWin *mwin);
  void (*plugin_claim_selection) (MainWin *mwin, Selection *item);

  void (*plugin_update_text_selection) (MainWin *mwin, gint row, gchar *type, gchar *from, gchar *data);
  void (*plugin_move_selected_up) (MainWin *mwin, GList *remaining, gint startrow);
  void (*plugin_move_selected_down) (MainWin *mwin, GList *remaining, gint startrow);
  void (*plugin_selection_free) (Selection *item);
  void (*plugin_prefs_free) (Prefs *p);
  void (*plugin_try_to_get_text_target) (MainWin *mwin);
  void (*plugin_saveasdata_free) (SaveAsData *sa);
  void (*plugin_show_prefswin) (MainWin *mwin);

  void (*plugin_enable_plugin) (MainWin *mwin, gpointer pe);
  void (*plugin_disable_plugin) (MainWin *mwin, gpointer pe);

};

struct _MainWinClass {
   GtkWindowClass parent_class;
};

struct _SaveAsData {
	GList *items;
	gchar *path;
};



GtkType mainwin_get_type(void);
GtkWidget* mainwin_new (GtkWidget *windowpavoid);

gint idle_func(gpointer data);
void app_update_item (MainWin *mwin, gpointer *user_data);
void app_update_row (MainWin *mwin, gint row);
gint app_load_items_from_disk(MainWin *mwin, gchar *path);
gint app_load_items_from_mem(MainWin *mwin, gchar *xml, gint size);
void app_get_new_item (MainWin *mwin) ;
void app_delete_selected_items(MainWin *mwin, GtkCList *cliplist);
gint app_add_selection (MainWin *mwin, Selection *item);
gint app_add_text_selection (MainWin *mwin, gchar *type, gchar *from, gchar *data);
void app_update_menus(MainWin *mwin);
void app_claim_selection(MainWin *mwin, Selection *item);


gboolean saveprefs_tofile (Prefs *prefs);
Prefs* getprefs_fromfile (void);
void app_update_text_selection (MainWin *mwin, gint row, gchar *type, gchar *from, gchar *data);
void app_move_selected_up(MainWin *mwin, GList *remaining, gint startrow);
void app_move_selected_down(MainWin *mwin, GList *remaining, gint startrow);

void app_load_session_from_disk(MainWin *mwin);
void app_remove_plugin (MainWin *mwin, gpointer pe);

void app_set_plugin_status (MainWin *mwin, gpointer pe, gboolean status);
void app_disable_plugin(MainWin *mwin, gpointer pe);
void app_enable_plugin(MainWin *mwin, gpointer pe);
void app_add_plugin (MainWin *mwin, gpointer pe);

void selection_free (Selection *item);
void prefs_free (Prefs *p);
void app_try_to_get_text_target (MainWin *mwin);
void saveasdata_free (SaveAsData *sa);

void app_show_prefswin (MainWin *mwin);
GtkWidget* app_create_and_show_newitemwin (MainWin *mwin);
GtkWidget* app_create_and_show_textitemwin (MainWin *mwin, gint row);
GtkWidget* app_create_and_show_aboutwin (MainWin *mwin);
gboolean app_save_items_to_session (MainWin *mwin);
gboolean app_save_items_to_disk(gchar *path, SaveAsData *sa);
gint app_merge_selectedrows (MainWin *mwin);
void app_create_cliplist(MainWin *mwin);
void app_remove_unwanted_items (MainWin *mwin);
void reload_prefs (MainWin *mwin);
void reload_maxitems_prefs (MainWin *mwin, gboolean pluginnotify);
void reload_cliplist_prefs(MainWin *mwin, gboolean pluginnotify);
void reload_plugin_prefs (MainWin *mwin, gboolean pluginnotify);
void reload_ui_prefs (MainWin *mwin, gboolean pluginnotify);
void reload_collect_prefs (MainWin *mwin, gboolean pluginnotify, gboolean getitem);
void reload_selection_prefs (MainWin *mwin, gboolean pluginnotify, gboolean getitem);

void app_prepare_menus(MainWin *mwin);
void app_createhintsfor_menus(MainWin *mwin);
void process_received_selectiondata (MainWin *mwin, Selection *item, GtkSelectionData *data);

void app_claim_selection_on_clipboard (MainWin *mwin, Selection *item, GtkClipboard *clipboard, GdkAtom selection);
void app_try_to_get_text_target_from_clipboard (MainWin *mwin, GtkClipboard *clipboard, GdkAtom selection);
void app_get_new_item_from_clipboard (MainWin *mwin, GtkClipboard *clipboard, GdkAtom selection);
void app_start_primary_poller (MainWin *mwin);
void app_start_clipboard_poller (MainWin *mwin);


#ifdef __cplusplus /* cpp compatibility */
}
#endif

