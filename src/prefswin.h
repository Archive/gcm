/* * C o p y r i g h t *(also read COPYING)* * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *  Copyright (C) 2003  Philip Van Hoof <me@freax.org>                         *
 *                                                                             *
 *  This program is free software; you can redistribute it and/or modify       *
 *  it under the terms of the GNU General Public License as published by       *
 *  the Free Software Foundation; either version 2 of the License, or          *
 *  (at your option) any later version.                                        *
 *                                                                             *
 *  This program is distributed in the hope that it will be useful,            *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *  GNU General Public License for more details.                               *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License          *
 *  along with this program; if not, write to the Free Software                *
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA  *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
#ifdef HAVE_CONFIG_H
#  include "../config.h"
#endif

#include <gnome.h>

#ifdef __cplusplus
extern "C" {
#endif
#define GNOME_TYPE_PREFSWIN (prefswin_get_type())
#define GNOME_PREFSWIN(obj) (GTK_CHECK_CAST((obj), GNOME_TYPE_PREFSWIN, PrefsWin))
#define GNOME_PREFSWIN_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass), GNOME_TYPE_PREFSWIN, PrefsWinClass))
#define GNOME_IS_PREFSWIN(obj) (GTK_CHECK_TYPE((obj), GNOME_TYPE_PREFSWIN))
#define GNOME_IS_PREFSWIN_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GNOME_TYPE_PREFSWIN))



typedef struct _PrefsWin PrefsWin;
typedef struct _PrefsWinClass PrefsWinClass;

enum
{
   ENABLED_COLUMN,
   DES_COLUMN,
   FILEN_COLUMN,
 VISIBLE_COLUMN,
  WORLD_COLUMN,

   PLUGN_COLUMNS
};

struct _PrefsWin {
  GtkWindow window;
  gpointer client;
	
  GtkWidget *prefvbox;
  GtkWidget *prefnbk;

  GtkWidget *genvbox;

  GtkSizeGroup *sizegroup;
  GtkSizeGroup *sizegroup2;

  GtkWidget *t1hbox, *t1spacer;
  GtkWidget *t6hbox, *t6spacer;

  GtkWidget *seltypetoplabelh;
  GtkWidget *seltypetoplabel;
  GtkWidget *seltypevbox;
  GtkWidget *seltypehbox1;
  GtkWidget *seltypehbox2;
  GtkWidget *seltypehbox3;
  GtkWidget *seltypehbox4;
  GtkWidget *seltypetable;
  GtkWidget *seltypelabel;

  
  GtkWidget *clipboard_only_radio,
  			 *clipboard_and_primary_radio,
			 *primary_only_radio;
  
  GtkWidget *coltoplabelh;
  GtkWidget *coltoplabel;
  GtkWidget *colvbox;
  GtkWidget *colauto;
  GtkWidget *colbeep;
  GtkWidget *colmaxhbox;
  GtkWidget *colmaxlbl;
  GtkObject *colmaxspinadj;
  GtkWidget *colmaxspin;
  GtkWidget *colmaxlabel;
  GtkWidget *prefnbkgeneral;

  GtkWidget *prefnbkplug;
  GtkWidget *plugtvbox;
  GtkWidget *plugtoplabelh;
  GtkWidget *plugtoplabel;
  GtkWidget *plughbox;
  GtkWidget *plugbtnbox;
  GtkWidget *plugconfig;
  GtkWidget *plugabout;

  GtkListStore *plugstore;
  GtkTreeIter plugiter;
  GtkWidget *plugtreeview;
  GtkWidget *plugscrolledwindow;
  
  GtkTreeViewColumn *plugdescolumn,*plugfilencolumn,*plugenabledcolumn;
  GtkCellRenderer *plugtext1cell,*plugtext2cell,*plugenabledcell;


  GtkWidget *buthbox;
  GtkWidget *newrowbut;
  GtkWidget *remrowbut;
  


  GtkWidget *prefnbkmisc;
  GtkWidget *prefhsep;
  GtkWidget *prefhbtnbox;
  GtkWidget *close_button;
  GtkTooltips *tooltips;

  GtkWidget *windowparent;
};

struct _PrefsWinClass {
   GtkWindowClass parent_class;
};


GtkType prefswin_get_type(void);
GtkWidget* prefswin_new (GtkWidget *windowparent);

void prefswin_fill_in_prefs (PrefsWin *pwin, gpointer p);


#ifdef __cplusplus /* cpp compatibility */
}
#endif
