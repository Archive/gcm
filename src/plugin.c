/* * C o p y r i g h t *(also read COPYING)* * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *  Copyright (C) 2003  Philip Van Hoof <me@freax.org>                         *
 *                                                                             *
 *  This program is free software; you can redistribute it and/or modify       *
 *  it under the terms of the GNU General Public License as published by       *
 *  the Free Software Foundation; either version 2 of the License, or          *
 *  (at your option) any later version.                                        *
 *                                                                             *
 *  This program is distributed in the hope that it will be useful,            *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *  GNU General Public License for more details.                               *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License          *
 *  along with this program; if not, write to the Free Software                *
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA  *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "plugin.h"

#include "mainwin.h"
#include "prefswin.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <dirent.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#ifdef HPUX
# include <dl.h>
#else
# include <dlfcn.h>
#endif

#ifdef HPUX
# define SHARED_LIB_EXT ".sl"
#else
# define SHARED_LIB_EXT ".so"
#endif

#ifndef RTLD_NOW
# define RTLD_NOW 0
#endif

MainWin *mwin;
gboolean init=FALSE;
/* This plugin code is like the plugin code of xmms */


int plugin_check_duplicate(char *filename)
{
if (init==FALSE) return;
else {

	GList *l;
	gchar *base_filename = g_path_get_basename(filename);

	/* We just loop all plugins for the same filename */
	for (l = mwin->plugin_list; l; l = l->next)
		if (!strcmp(base_filename,
			    g_basename(((GcmPlugin*)l->data)->filename)))
			return 1;

	g_free(base_filename);
	return 0;
}
}

void init_plugins(gpointer *mwinp)
{
	cleanup_plugins ();
	gchar *dir;
	if (mwinp!=NULL) {
		mwin = (MainWin*) mwinp;
		init=TRUE;
	} else return;
	mwin->plugin_list=NULL;

	/* Scan for plugins in the homedir/.gcm/Plugins/ of the user */
	dir = g_strconcat(g_get_home_dir(), "/.gcm/Plugins", NULL);
	scan_plugins(dir);
	g_free(dir);

	/* Scan for plugins in the PLUGIN_DIR which is set the the automake scripts */
	scan_plugins(PLUGIN_DIR);

}

void close_dynamic_lib(void* handle)
{
#ifdef HPUX
	shl_t h = handle;
	shl_unload(h);
#else
	dlclose(handle);
#endif
}

void* open_dynamic_lib(char *filename)
{
#ifdef HPUX
	/*
	 * Use shl_load family of functions on HP-UX. HP-UX does not
	 * support dlopen on 32-bit PA-RISC executables
	 */
	return shl_load(filename, BIND_DEFERRED, 0); 
#else
	//return dlopen(filename, RTLD_GLOBAL);
	return dlopen(filename, RTLD_NOW);
#endif
}
void* find_dynamic_symbol(void *handle, char *symbol)
{
#ifdef HPUX
	void *gpi;
	shl_t h = handle;
	if ((shl_findsym(&h, symbol, TYPE_PROCEDURE, &gpi)) != 0)
		gpi = NULL;
	return gpi;
#else
#ifdef SYMBOL_PREFIX
	char *sym = g_strconcat(SYMBOL_PREFIX, symbol, NULL);
	void *symh = dlsym(handle, sym);
	g_free(sym);
	return symh;
#else
	return dlsym(handle, symbol);
#endif /* SYMBOL_PREFIX */
#endif /* HPUX */
}

void dynamic_lib_error(void)
{
#ifdef HPUX
	perror("Error loading plugin!"); 
#else
	fprintf(stderr, "%s\n", dlerror());
#endif
}

void add_plugin(gchar * filename)
{
if (init==FALSE) return;
else {
	void *h;
	void *(*gpi) (void);

	if (plugin_check_duplicate(filename))
		return;
	
	if ((h = open_dynamic_lib(filename)) == NULL) {
		dynamic_lib_error();
		return;
	}

	/*
	 * If the plugin does not have a get_gcmplugin_info which returns
	 * a struct to a GcmPlugin (which holds the pointers to the functions
	 * and data) then it is not a Gnome Clipboard Manager plugin ;) else
	 * we add it's handle and filename to it's struct and we start it's
	 * init function (whooo, OO programming in C ! ;)).
	 */
	if ((gpi = find_dynamic_symbol(h, "get_gcmplugin_info")) != NULL) {
		GcmPlugin *p = gpi();
		p->handle = h;
		p->filename = g_strdup(filename);
		/*p->init();*/
		app_add_plugin(GNOME_MAINWIN(mwin), (gpointer)p);

	} else {
#ifdef DEVELOPMENT
		g_print("GCM PLUGIN LOADER: This is not a Gnome Clipboard Manager plugin\n");
#endif
		close_dynamic_lib(h);
	}
	
}
}

void scan_plugins(char *dirname)
{
if (init==FALSE) return;
else {
	gchar *filename, *ext;
	DIR *dir;

	struct dirent *ent;
	struct stat statbuf;

	dir = opendir(dirname);
	if (!dir)
		return;

	/* Scan the directory for files */
	while ((ent = readdir(dir)) != NULL)
	{
		filename = g_strdup_printf("%s/%s", dirname, ent->d_name);
		/* If we found a file with the so or sl extention */
		if (!stat(filename, &statbuf) && S_ISREG(statbuf.st_mode) &&
		    (ext = strrchr(ent->d_name, '.')) != NULL)
			if (!strcmp(ext, SHARED_LIB_EXT)) {
#ifdef DEVELOPMENT
				g_print("GCM PLUGIN LOADER: Scanning %s\n", filename);
#endif
				add_plugin(filename); /* Add the file */
			}
		g_free(filename);
	}
	closedir(dir);
}
}

gboolean pluginsystem_ready (void)
{
	if ((mwin != NULL) && (init==TRUE)) return TRUE;
		else return FALSE;
}

void cleanup_plugins(void)
{
	if (init==FALSE) {
		return;
	} else {
		GList *node = g_list_copy(mwin->plugin_list);

		while (node)
		{
			GcmPlugin *ip = (GcmPlugin *) node->data;
			app_remove_plugin (mwin, ip);
			node = node->next;
		}

		g_list_free(node);
		if (mwin->plugin_list)
			g_list_free(mwin->plugin_list);
		mwin->plugin_list = NULL;
		init=FALSE;
	}
}



void plugins_on_mainwin_show (GtkWidget       *mainwin,
   						       gpointer         *user_data)
{
if (init==FALSE) return;
else {
	GList *mylist = g_list_copy(mwin->plugin_list);
	while (mylist) {
		GcmPlugin *p = (GcmPlugin*) mylist->data;
		p->on_mainwin_show(mainwin, user_data);
		mylist = g_list_next(mylist);
	}
	g_list_free(mylist);	
}
}

void plugins_on_mainwin_hide (GtkWidget       *mainwin,
   						       gpointer         *user_data)
{
 if (init == FALSE) return;
 else {
	GList *mylist = g_list_copy(mwin->plugin_list);
	while (mylist) {
		GcmPlugin *p = (GcmPlugin*) mylist->data;
		p->on_mainwin_hide(mainwin, user_data);
		mylist = g_list_next(mylist);
	}
	g_list_free(mylist);	
 }
}

void plugins_on_plugin_added  (GtkWidget *mainwin, GcmPlugin *pe)
{
 if (init == FALSE) return;
 else {
	GList *mylist = g_list_copy(mwin->plugin_list);
	while (mylist) {
		GcmPlugin *p = (GcmPlugin*) mylist->data;
		p->on_plugin_added(mainwin, pe);
		mylist = g_list_next(mylist);
	}
	g_list_free(mylist);
 }
}


void plugins_on_firsttime (gpointer *client, gpointer *prefs)
{
  if (init == FALSE) return;
  else {
	GList *mylist = g_list_copy(mwin->plugin_list);
	while (mylist) {
		GcmPlugin *p = (GcmPlugin*) mylist->data;
		p->on_firsttime(client, prefs);
		mylist = g_list_next(mylist);
	}
	g_list_free(mylist);
  }
}

void plugins_on_reload_prefs (GtkWidget *mainwin)
{
  if (init == FALSE) return;
  else {
	GList *mylist = g_list_copy(mwin->plugin_list);
	while (mylist) {
		GcmPlugin *p = (GcmPlugin*) mylist->data;
		p->on_reload_prefs(mainwin);
		mylist = g_list_next(mylist);
	}
	g_list_free(mylist);
  }
}

void plugins_on_update_item	(GtkWidget *mainwin, gint row, gpointer *user_data)
{
  if (init == FALSE) return;
  else {
	GList *mylist = g_list_copy(mwin->plugin_list);
	while (mylist) {
		GcmPlugin *p = (GcmPlugin*) mylist->data;
		p->on_update_item(mainwin, row, user_data);
		mylist = g_list_next(mylist);
	}
	g_list_free(mylist);
  }

}

void plugins_on_add_item (GtkWidget *mainwin, gpointer *item, gint row)
{
  if (init == FALSE) return;
  else {
	GList *mylist = g_list_copy(mwin->plugin_list);
	while (mylist) {
		GcmPlugin *p = (GcmPlugin*) mylist->data;
		p->on_add_item(mainwin, item, row);
		mylist = g_list_next(mylist);
	}
	g_list_free(mylist);

}
}

void plugins_on_remove_item	(GtkWidget *mainwin, gpointer *item, gint row)
{
  if (init == FALSE) return;
  else {
	GList *mylist = g_list_copy(mwin->plugin_list);
	while (mylist) {
		GcmPlugin *p = (GcmPlugin*) mylist->data;
		p->on_remove_item(mainwin, item, row);
		mylist = g_list_next(mylist);
	}
	g_list_free(mylist);

}
}
void plugins_on_selection_claimed(GtkWidget *mainwin, GtkClipboard *clipboard,
								  GtkTargetEntry *ttargets, gpointer *item)
{
  if (init == FALSE) return;
  else {
	GList *mylist = g_list_copy(mwin->plugin_list);
	while (mylist) {
		GcmPlugin *p = (GcmPlugin*) mylist->data;
		p->on_selection_claimed(mainwin, clipboard, ttargets, item);
		mylist = g_list_next(mylist);
	}
	g_list_free(mylist);

  }
}

void plugins_on_selectiondata_received(GtkWidget *mainwin,gpointer *item, GtkSelectionData *data)
{
	  if (init == FALSE) return;
  else {
	GList *mylist = g_list_copy(mwin->plugin_list);
	while (mylist) {
		GcmPlugin *p = (GcmPlugin*) mylist->data;
		p->on_selectiondata_received(mainwin, item, data);
		mylist = g_list_next(mylist);
	}
	g_list_free(mylist);

  }

}


GcmPlugin *plugins_i_will_handle_that_target(GdkAtom target)
{
  if (init == FALSE) return NULL;
  else {
	GList *mylist = g_list_copy(mwin->plugin_list);
	while (mylist) {
		GcmPlugin *p = (GcmPlugin*) mylist->data;

		if (p->i_will_handle_that_target(target) != NULL) {
			g_list_free(mylist);
			return p;
		}
		mylist = g_list_next(mylist);
	}
	g_list_free(mylist);
	return NULL;
  }
}


GcmPlugin * get_plugin_pointer_by_description (GtkWidget *mainwin, gchar *des)
{
	MainWin *mwin = GNOME_MAINWIN(mainwin);
	GList *mylist = g_list_copy(mwin->plugin_list);
	while (mylist) {
		GcmPlugin *p = (GcmPlugin*) mylist->data;
		if (!(strcmp(p->description, des))) {
			g_list_free(mylist);
			return p;
		}
		mylist = g_list_next(mylist);
	}
	g_list_free(mylist);
	return NULL;
}

GcmPlugin * get_plugin_pointer_by_filename (GtkWidget *mainwin, gchar *filename)
{
	MainWin *mwin = GNOME_MAINWIN(mainwin);
	GList *mylist = g_list_copy(mwin->plugin_list);
	while (mylist) {
		GcmPlugin *p = (GcmPlugin*) mylist->data;
		if (!(strcmp(p->filename, filename))) {
			g_list_free(mylist);
			return p;
		}
		mylist = g_list_next(mylist);
	}
	g_list_free(mylist);
	return NULL;
}
