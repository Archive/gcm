/* * C o p y r i g h t *(also read COPYING)* * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *  Copyright (C) 2003  Philip Van Hoof <me@freax.org>                         *
 *                                                                             *
 *  This program is free software; you can redistribute it and/or modify       *
 *  it under the terms of the GNU General Public License as published by       *
 *  the Free Software Foundation; either version 2 of the License, or          *
 *  (at your option) any later version.                                        *
 *                                                                             *
 *  This program is distributed in the hope that it will be useful,            *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *  GNU General Public License for more details.                               *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License          *
 *  along with this program; if not, write to the Free Software                *
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA  *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifdef HAVE_CONFIG_H
#  include "../config.h"
#endif


#ifdef __cplusplus
extern "C" {
#endif

void
on_mnuclose_activate                   (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_mnucut_activate                     (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_mnucopy_activate                    (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_mnupaste_activate                   (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_mnuselectall_activate               (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_mnuclear_activate                   (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_notebook_switch_page                (GtkNotebook *notebook,
                                        GtkNotebookPage *page,
                                        gint page_num,
                                        gpointer user_data);


/* txtwinmenu Callbacks */

void textitemwin_show(GtkWidget *widget, gpointer *user_data);

void textitemwin_hide(GtkWidget *widget, gpointer *user_data);


void
on_textitemwin_ok_button_clicked
                                       (GtkButton *button,
                                        gpointer *user_data);
void
on_textitemwin_cancel_button_clicked
                                       (GtkButton *button,
                                        gpointer *user_data);

void
on_textitemwin_overwrite_yes_no        (gint answer,
                                        gpointer *user_data);

void
on_txtwinmenu_file_close_activate      (GtkMenuItem     *menuitem,
                                        gpointer         *user_data);

void
on_txtwinmenu_edit_cut_activate        (GtkMenuItem     *menuitem,
                                        gpointer         *user_data);

void
on_txtwinmenu_edit_copy_activate       (GtkMenuItem     *menuitem,
                                        gpointer         *user_data);

void
on_txtwinmenu_edit_paste_activate      (GtkMenuItem     *menuitem,
                                        gpointer         *user_data);

void
on_txtwinmenu_edit_selectall_activate  (GtkMenuItem     *menuitem,
                                        gpointer         *user_data);

void
on_txtwinmenu_edit_clear_activate      (GtkMenuItem     *menuitem,
                                        gpointer         *user_data);
void
on_item_changed                        (GtkWidget         *widget,
                                        gpointer         *user_data);

void
on_htmltextview_changed                (GtkTextBuffer    *buffer,
                                        gpointer         *user_data);

void
on_texttextview_changed                (GtkTextBuffer    *buffer,
                                        gpointer         *user_data);
void
on_htmlview_mustupdate                 (GtkTextBuffer    *buffer,
                                        gpointer         *user_data);

void
on_btnRemFirstChars_clicked            (GtkButton        *button,
                                        gpointer         *user_data);

void
on_btnRemLastChars_clicked             (GtkButton        *button,
                                        gpointer         *user_data);

void
on_btnRemFirstLines_clicked            (GtkButton       *button,
                                        gpointer         *user_data);

void
on_btnRemLastLines_clicked             (GtkButton       *button,
                                        gpointer         *user_data);

void
on_btnInvert_clicked                   (GtkButton       *button,
                                        gpointer         *user_data);
void
on_search_replace_ok_button_clicked    (GtkButton       *button,
                                        gpointer         *user_data);



#ifdef __cplusplus /* cpp compatibility */
}
#endif
