/* * C o p y r i g h t *(also read COPYING)* * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *  Copyright (C) 2003  Philip Van Hoof <me@freax.org>                         *
 *                                                                             *
 *  This program is free software; you can redistribute it and/or modify       *
 *  it under the terms of the GNU General Public License as published by       *
 *  the Free Software Foundation; either version 2 of the License, or          *
 *  (at your option) any later version.                                        *
 *                                                                             *
 *  This program is distributed in the hope that it will be useful,            *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *  GNU General Public License for more details.                               *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License          *
 *  along with this program; if not, write to the Free Software                *
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA  *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifdef HAVE_CONFIG_H
# include "../config.h"
#endif


#ifdef __cplusplus
extern "C" {
#endif

void mainwin_show(GtkWidget *widget, gpointer *user_data);
void mainwin_hide(GtkWidget *widget, gpointer *user_data);

int
on_mainwin_event  (GtkWidget *widget,
                   GdkEvent *event,
                   gpointer *user_data);


gboolean
on_mainwin_selection_lost (GtkWidget *widget,
                   GdkEventSelection *event,
                   gpointer *user_data);

void
on_mainwinmenu_newitem_activate (GtkMenuItem *menuitem,
                   gpointer *user_data);

void
on_mainwinmenu_deleteitem_activate (GtkMenuItem *menuitem,
                   gpointer *user_data);
void
on_mainwinmenu_moveup_activate (GtkMenuItem *menuitem,
                   gpointer *user_data);

void
on_mainwinmenu_movedown_activate (GtkMenuItem *menuitem,
                   gpointer *user_data);
void
on_mainwin_savefilelist_ok_button_clicked
                   (GtkButton *button,
                   gpointer *user_data);

void
on_mainwin_savefilelist_cancel_button_clicked
                   (GtkButton *button,
                   gpointer *user_data);

void
on_mainwin_openfilelist_ok_button_clicked
                   (GtkButton *button,
                   gpointer *user_data);

void
on_mainwin_openfilelist_cancel_button_clicked
                   (GtkButton *button,
                   gpointer *user_data);

void
on_mainwin_overwrite_yes_no (gint answer,
                   gpointer *user_data);

void
on_mainwin_saveallfilelist_ok_button_clicked
                   (GtkButton *button,
                   gpointer *user_data);
void
on_mainwinmenu_file_savealltodisk_activate
                   (GtkMenuItem *menuitem,
                   gpointer *user_data);
void
on_mainwinmenu_file_savetodisk_activate
                   (GtkMenuItem *menuitem,
                   gpointer *user_data);

void
on_mainwinmenu_file_openfromdisk_activate
                   (GtkMenuItem *menuitem,
                   gpointer *user_data);


void
on_mainwinmenu_exit_activate (GtkMenuItem *menuitem,
                   gpointer *user_data);
void
on_mainwinmenu_selectnone_activate (GtkMenuItem *menuitem,
                   gpointer *user_data);
void
on_mainwinmenu_selectall_activate (GtkMenuItem *menuitem,
                   gpointer *user_data);

void
on_mainwinmenu_cut_activate (GtkMenuItem *menuitem,
                   gpointer *user_data);


void
on_mainwinmenu_copy_activate (GtkMenuItem *menuitem,
                   gpointer *user_data);


void
on_mainwinmenu_paste_activate (GtkMenuItem *menuitem,
                   gpointer *user_data);


void
on_mainwinmenu_clear_activate (GtkMenuItem *menuitem,
                   gpointer *user_data);


void
on_mainwinmenu_preferences_activate (GtkMenuItem *menuitem,
                   gpointer *user_data);


void
on_mainwinmenu_about_activate (GtkMenuItem *menuitem,
                   gpointer *user_data);

void
on_cliplistpopupmenu_new_activate (GtkMenuItem *menuitem,
                   gpointer *user_data);
void
on_cliplistpopupmenu_cut_activate (GtkMenuItem *menuitem,
                   gpointer *user_data);
void
on_cliplistpopupmenu_merge_activate (GtkMenuItem *menuitem,
                   gpointer *user_data);
void
on_cliplistpopupmenu_copy_activate (GtkMenuItem *menuitem,
                   gpointer *user_data);
void
on_cliplistpopupmenu_paste_activate (GtkMenuItem *menuitem,
                   gpointer *user_data);

void
on_cliplistpopupmenu_edit_activate (GtkMenuItem *menuitem,
                   gpointer *user_data);

void
on_cliplistpopupmenu_convert_activate (GtkMenuItem *menuitem,
                   gpointer *user_data);


void
on_toolbar_newitem_clicked (GtkButton *button,
                   gpointer *user_data);

void
on_toolbar_clear_clicked (GtkButton *button,
                   gpointer *user_data);

void
on_toolbar_deleteitem_clicked (GtkButton *button,
                   gpointer *user_data);

void
on_toolbar_edititem_clicked (GtkButton *button,
                   gpointer *user_data);


void
on_toolbar_preferences_clicked (GtkButton *button,
                   gpointer *user_data);


void
on_toolbar_refresh_clicked (GtkButton *button,
                   gpointer *user_data);


void
on_toolbar_quit_clicked (GtkButton *button,
                   gpointer *user_data);



void
on_cliplist_select_row  (GtkCList *clist,
                   gint row,
                   gint column,
                   GdkEvent *event,
                   gpointer *user_data);

void
on_cliplist_unselect_row (GtkCList *clist,
                   gint row,
                   gint column,
                   GdkEvent *event,
                   gpointer *user_data);




void *savefilestodisk_thread (gpointer *user_data);




void cliplist_row_removed (gpointer *user_data);
int
on_cliplist_event  (GtkCList *clist,
                   GdkEvent *event,
                   gpointer *user_data);


void clipboard_get_selection_cb (GtkClipboard *clipboard,
                   GtkSelectionData *selection_data,
                   guint info,
                   gpointer user_data_or_owner);

void clipboard_clear_selection_cb (GtkClipboard *clipboard,
                   gpointer user_data_or_owner);


gint primary_clipboard_idle_func(gpointer data);
gint clipboard_clipboard_idle_func(gpointer data);
gint get_new_item_from_clipboard_in_a_few_seconds_idle_func (gpointer user_data_or_owner);

#ifdef __cplusplus /* cpp compatibility */
}
#endif


