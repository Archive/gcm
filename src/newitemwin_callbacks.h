/* * C o p y r i g h t *(also read COPYING)* * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *  Copyright (C) 2003  Philip Van Hoof <me@freax.org>                         *
 *                                                                             *
 *  This program is free software; you can redistribute it and/or modify       *
 *  it under the terms of the GNU General Public License as published by       *
 *  the Free Software Foundation; either version 2 of the License, or          *
 *  (at your option) any later version.                                        *
 *                                                                             *
 *  This program is distributed in the hope that it will be useful,            *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *  GNU General Public License for more details.                               *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License          *
 *  along with this program; if not, write to the Free Software                *
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA  *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifdef HAVE_CONFIG_H
#  include "../config.h"
#endif


#ifdef __cplusplus
extern "C" {
#endif

/* gpointer Callbacks */

void newitemwin_show(GtkWidget *widget, gpointer *user_data);

void newitemwin_hide(GtkWidget *widget, gpointer *user_data);


void
on_newitemok_clicked                   (GtkButton       *button,
                                        gpointer         *user_data);
void
on_newitemcancel_clicked               (GtkButton       *button,
                                        gpointer         *user_data);

void
on_newitemmenu_edit_copy_activate      (GtkMenuItem     *menuitem,
                                        gpointer         *user_data);

void
on_newitemmenu_edit_paste_activate     (GtkMenuItem     *menuitem,
                                        gpointer         *user_data);

void
on_newitemmenu_edit_selectall_activate (GtkMenuItem     *menuitem,
                                        gpointer         *user_data);

void
on_newitemmenu_edit_clear_activate     (GtkMenuItem     *menuitem,
                                        gpointer         *user_data);

void
on_newitemmenu_edit_cut_activate       (GtkMenuItem     *menuitem,
                                        gpointer         *user_data);


#ifdef __cplusplus /* cpp compatibility */
}
#endif

