/* * C o p y r i g h t *(also read COPYING)* * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *  Copyright (C) 2003  Philip Van Hoof <me@freax.org>                         *
 *                                                                             *
 *  This program is free software; you can redistribute it and/or modify       *
 *  it under the terms of the GNU General Public License as published by       *
 *  the Free Software Foundation; either version 2 of the License, or          *
 *  (at your option) any later version.                                        *
 *                                                                             *
 *  This program is distributed in the hope that it will be useful,            *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *  GNU General Public License for more details.                               *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License          *
 *  along with this program; if not, write to the Free Software                *
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA  *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "prefswin.h"
#include "mainwin.h"
#include "prefswin_callbacks.h"

#include <gconf/gconf-client.h>


static void prefswin_class_init(PrefsWinClass *klass);
static void prefswin_init(PrefsWin *twin);
static void prefswin_destroy(GtkObject *object);

void
a_unregistered_setting_changed_callback(GConfClient *client,
                          guint        cnxn_id,
                          GConfEntry  *entry,
                          gpointer     user_data);

void
useplugins_changed_callback (GConfClient *client,
                          guint        cnxn_id,
                          GConfEntry  *entry,
                          gpointer     user_data);
void
beep_changed_callback (GConfClient *client,
                          guint        cnxn_id,
                          GConfEntry  *entry,
                          gpointer     user_data);
void
maxitems_changed_callback (GConfClient *client,
                          guint        cnxn_id,
                          GConfEntry  *entry,
                          gpointer     user_data);


void
collect_changed_callback(GConfClient *client,
                          guint        cnxn_id,
                          GConfEntry  *entry,
                          gpointer     user_data);

void
selectionsetting_changed_callback(GConfClient *client,
                          guint        cnxn_id,
                          GConfEntry  *entry,
                          gpointer     user_data);

static GtkWindowClass *parent_class = NULL;

GtkType prefswin_get_type(void)
{
   static GtkType prefswin_type = 0;
   
   if (!prefswin_type)
   {
      static const GtkTypeInfo prefswin_info =
      {	
         "PrefsWin",
         sizeof (PrefsWin),
         sizeof (PrefsWinClass),
         (GtkClassInitFunc) prefswin_class_init,
         (GtkObjectInitFunc) prefswin_init,
         NULL,
         NULL,
         (GtkClassInitFunc) NULL,
      };
      
      prefswin_type = gtk_type_unique (GTK_TYPE_WINDOW, &prefswin_info);
   }

   return (prefswin_type);
}

static void prefswin_class_init(PrefsWinClass *klass)
{
   GtkObjectClass *object_class;
   object_class = (GtkObjectClass*)klass;
   object_class->destroy = prefswin_destroy;
   parent_class = gtk_type_class(GTK_TYPE_WINDOW);
}

static void prefswin_init(PrefsWin *pwin)
{

}


static void prefswin_destroy(GtkObject *object)
{
   GtkWidget *pwin;
  
   g_return_if_fail(object != NULL);
   g_return_if_fail(GNOME_IS_PREFSWIN(object));
   
   pwin = GTK_WIDGET(object);
      
   gtk_widget_destroy(pwin);
}







/* Creates a new preferences GUI */
GtkWidget* prefswin_new (GtkWidget *windowparent)
{
  PrefsWin *pwin;
  gint coll_offset;
  pwin = gtk_type_new(GNOME_TYPE_PREFSWIN);
  GConfClient* client;

  client = gconf_client_get_default ();
  
  gtk_widget_set_size_request(GTK_WIDGET(pwin), 500, 400);
  pwin->tooltips = gtk_tooltips_new();
  pwin = gtk_type_new(GNOME_TYPE_PREFSWIN);
  g_signal_connect(G_OBJECT(pwin), "show", G_CALLBACK(prefswin_show), pwin);
  g_signal_connect(G_OBJECT(pwin), "hide", G_CALLBACK(prefswin_hide), pwin);
  pwin->windowparent = windowparent;
  gtk_window_set_title (GTK_WINDOW (pwin), _("Preferences"));
  pwin->sizegroup = gtk_size_group_new (GTK_SIZE_GROUP_VERTICAL);
  pwin->sizegroup2 = gtk_size_group_new (GTK_SIZE_GROUP_BOTH);

  
  gtk_window_set_transient_for (GTK_WINDOW(pwin), GTK_WINDOW(pwin->windowparent));

  gtk_window_set_policy (GTK_WINDOW (pwin), FALSE, FALSE, FALSE);
  pwin->tooltips = gtk_tooltips_new();

  pwin->tooltips = gtk_tooltips_new ();
  pwin->prefvbox = gtk_vbox_new (FALSE, 0);
  gtk_widget_show (pwin->prefvbox);
  gtk_container_add (GTK_CONTAINER (pwin), pwin->prefvbox);

  pwin->prefnbk = gtk_notebook_new ();
  gtk_widget_show (pwin->prefnbk);
  
  gtk_box_pack_start (GTK_BOX (pwin->prefvbox), pwin->prefnbk, FALSE, FALSE, 0);

  pwin->t1hbox = gtk_hbox_new (FALSE, 6);
  gtk_widget_show (pwin->t1hbox);
  gtk_container_add (GTK_CONTAINER (pwin->prefnbk), pwin->t1hbox);

  pwin->t1spacer = gtk_label_new("    ");
  gtk_widget_show(pwin->t1spacer);
  gtk_box_pack_start (GTK_BOX (pwin->t1hbox), pwin->t1spacer, FALSE, FALSE, 0);

  pwin->genvbox = gtk_vbox_new (FALSE, 0);
  gtk_widget_show (pwin->genvbox);
  gtk_container_add (GTK_CONTAINER (pwin->t1hbox), pwin->genvbox);

  pwin->seltypevbox=gtk_vbox_new (FALSE, 0);
  gtk_container_add (GTK_CONTAINER (pwin->genvbox), pwin->seltypevbox);
  gtk_widget_show(pwin->seltypevbox);
  
  pwin->seltypetoplabelh =gtk_hbox_new (FALSE, 6);
  gtk_widget_show (pwin->seltypetoplabelh);
  gtk_box_pack_start (GTK_BOX (pwin->seltypevbox), pwin->seltypetoplabelh, FALSE, FALSE, 0);
  pwin->seltypetoplabel = gtk_label_new(NULL);
  {
    gchar *trans = g_strdup_printf ("<span weight=\"bold\">%s</span>",_("Selection options"));
    gtk_label_set_markup (GTK_LABEL(pwin->seltypetoplabel), trans);
    g_free(trans);
  }
  gtk_box_pack_start (GTK_BOX (pwin->seltypetoplabelh), pwin->seltypetoplabel, FALSE, FALSE, 0);
  gtk_label_set_justify (GTK_LABEL (pwin->seltypetoplabel), GTK_JUSTIFY_LEFT);
  gtk_widget_show(pwin->seltypetoplabel);
 
  pwin->seltypehbox1=gtk_hbox_new (FALSE, 6);
  gtk_box_pack_start (GTK_BOX (pwin->seltypevbox), pwin->seltypehbox1, FALSE, FALSE, 0);
  gtk_widget_show(pwin->seltypehbox1);
  gtk_container_set_border_width (GTK_CONTAINER (pwin->seltypehbox1), 6);

  pwin->seltypehbox2=gtk_hbox_new (FALSE, 6);
  gtk_box_pack_start (GTK_BOX (pwin->seltypevbox), pwin->seltypehbox2, FALSE, FALSE, 0);
  gtk_widget_show(pwin->seltypehbox2);
  gtk_container_set_border_width (GTK_CONTAINER (pwin->seltypehbox2), 6);

  pwin->seltypehbox3=gtk_hbox_new (FALSE, 6);
  gtk_box_pack_start (GTK_BOX (pwin->seltypevbox), pwin->seltypehbox3, FALSE, FALSE, 0);
  gtk_widget_show(pwin->seltypehbox3);
  gtk_container_set_border_width (GTK_CONTAINER (pwin->seltypehbox3), 6);

  pwin->seltypehbox4=gtk_hbox_new (FALSE, 6);
  gtk_box_pack_start (GTK_BOX (pwin->seltypevbox), pwin->seltypehbox4, FALSE, FALSE, 0);
  gtk_widget_show(pwin->seltypehbox4);
  gtk_container_set_border_width (GTK_CONTAINER (pwin->seltypehbox4), 6);

  pwin->seltypevbox = gtk_vbox_new(FALSE, 0);
  gtk_widget_show(pwin->seltypevbox);
  gtk_box_pack_start (GTK_BOX (pwin->seltypehbox2), pwin->seltypevbox, FALSE, FALSE, 0);
  
  pwin->clipboard_only_radio = gtk_radio_button_new_with_mnemonic (NULL,_("Manage the _GNOME clipboard"));
  pwin->clipboard_and_primary_radio = gtk_radio_button_new_with_mnemonic_from_widget 
		(GTK_RADIO_BUTTON(pwin->clipboard_only_radio), _("Manage both the GNOME clipboard _and mouse-selection clipboard"));
  pwin->primary_only_radio = gtk_radio_button_new_with_mnemonic_from_widget (
		GTK_RADIO_BUTTON(pwin->clipboard_only_radio), _("Manage the mouse-selection clipboard"));
  gtk_widget_show(pwin->clipboard_only_radio);
  gtk_widget_show(pwin->clipboard_and_primary_radio);
  gtk_widget_show(pwin->primary_only_radio);
  
  gtk_box_pack_start (GTK_BOX (pwin->seltypevbox), pwin->clipboard_only_radio, FALSE, FALSE, 0);  
  gtk_box_pack_start (GTK_BOX (pwin->seltypevbox), pwin->clipboard_and_primary_radio, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX (pwin->seltypevbox), pwin->primary_only_radio, FALSE, FALSE, 0); 
 

/* category */
  pwin->colvbox = gtk_vbox_new (FALSE, 0);
  gtk_widget_show (pwin->colvbox);
  gtk_container_add (GTK_CONTAINER (pwin->genvbox), pwin->colvbox);
  pwin->coltoplabelh =gtk_hbox_new (FALSE, 6);
  gtk_widget_show (pwin->coltoplabelh);
  gtk_box_pack_start (GTK_BOX (pwin->colvbox), pwin->coltoplabelh, FALSE, FALSE, 0);
  pwin->coltoplabel = gtk_label_new(NULL);
  {
    gchar *trans = g_strdup_printf ("<span weight=\"bold\">%s</span>",_("Collection behaviour"));
    gtk_label_set_markup (GTK_LABEL(pwin->coltoplabel), trans);
    g_free(trans);
  }

  gtk_box_pack_start (GTK_BOX (pwin->coltoplabelh), pwin->coltoplabel, FALSE, FALSE, 0);
  gtk_label_set_justify (GTK_LABEL (pwin->coltoplabel), GTK_JUSTIFY_LEFT);
  gtk_widget_show(pwin->coltoplabel);
  pwin->colauto = gtk_check_button_new_with_label (_("Enable clipboard management"));
  gtk_tooltips_set_tip (pwin->tooltips, pwin->colauto, _(
	"Disabling this setting will stop GNOME Clipboard Manager from working correctly. If this setting is enabled,"
  "GNOME Clipboard Manager will automatically fetch clipboards (collecting) and will save them in its history "
  "(the clipboard shelf)"), NULL);

  gtk_widget_show (pwin->colauto);
  gtk_box_pack_start (GTK_BOX (pwin->colvbox), pwin->colauto, FALSE, FALSE, 0);
  pwin->colbeep = gtk_check_button_new_with_label (_("Beep when collecting"));
  gtk_tooltips_set_tip (pwin->tooltips, pwin->colbeep, _(
  "When this setting is enabled, GNOME Clipboard Manager will produce a beep when a new clipboard has been collected"), NULL);

  gtk_widget_show (pwin->colbeep);
  gtk_box_pack_start (GTK_BOX (pwin->colvbox), pwin->colbeep, FALSE, FALSE, 0);


  pwin->colmaxhbox = gtk_hbox_new (FALSE, 0);
  gtk_widget_show (pwin->colmaxhbox);
  gtk_container_add (GTK_CONTAINER (pwin->colvbox), pwin->colmaxhbox);

  pwin->colmaxlbl = gtk_label_new (_("Number of clipboard entries"));

  gtk_widget_show (pwin->colmaxlbl);
  gtk_box_pack_start (GTK_BOX (pwin->colmaxhbox), pwin->colmaxlbl, FALSE, FALSE, 0);

  pwin->colmaxspinadj = gtk_adjustment_new (1, 0, 99999, 10, 5, 5);
  pwin->colmaxspin = gtk_spin_button_new (GTK_ADJUSTMENT (pwin->colmaxspinadj), 1, 0);
  gtk_widget_show (pwin->colmaxspin);
  gtk_tooltips_set_tip (pwin->tooltips, pwin->colmaxspin, _(
	"GNOME Clipboard Manager will throw away older clipboards in its history and keep the new ones. Decrease this setting "
	"to reduce memory usage"), NULL);

  gtk_box_pack_start (GTK_BOX (pwin->colmaxhbox), pwin->colmaxspin, FALSE, FALSE, 0);

  pwin->prefnbkgeneral = gtk_label_new (_("General"));
  gtk_widget_show (pwin->prefnbkgeneral);
  gtk_notebook_set_tab_label (GTK_NOTEBOOK (pwin->prefnbk), gtk_notebook_get_nth_page (GTK_NOTEBOOK (pwin->prefnbk), 0),
	  pwin->prefnbkgeneral);
  /* General tabpage */


  pwin->t6hbox = gtk_hbox_new (FALSE, 6);
  gtk_widget_show (pwin->t6hbox);
  gtk_container_add (GTK_CONTAINER (pwin->prefnbk), pwin->t6hbox);

  pwin->t6spacer = gtk_label_new("    ");
  gtk_widget_show(pwin->t6spacer);
  
  gtk_box_pack_start (GTK_BOX (pwin->t6hbox), pwin->t6spacer, FALSE, FALSE, 0);

  
  pwin->plugtvbox = gtk_vbox_new (FALSE, 0);
  gtk_widget_show (pwin->plugtvbox);

  gtk_container_add (GTK_CONTAINER (pwin->t6hbox), pwin->plugtvbox);

  pwin->plugscrolledwindow = gtk_scrolled_window_new (NULL, NULL);
  gtk_widget_show (pwin->plugscrolledwindow);
  
  gtk_container_add (GTK_CONTAINER (pwin->plugtvbox), pwin->plugscrolledwindow);
  pwin->plugstore = gtk_list_store_new (PLUGN_COLUMNS, G_TYPE_BOOLEAN, G_TYPE_STRING, G_TYPE_STRING,G_TYPE_BOOLEAN,G_TYPE_BOOLEAN );
  
  pwin->plugtreeview = gtk_tree_view_new_with_model (GTK_TREE_MODEL(pwin->plugstore));
  
  gtk_widget_show (pwin->plugtreeview);
  gtk_container_add (GTK_CONTAINER (pwin->plugscrolledwindow), pwin->plugtreeview);

  pwin->plugdescolumn = gtk_tree_view_column_new();
  pwin->plugtext1cell =  gtk_cell_renderer_text_new ();
  gtk_tree_view_column_set_title (pwin->plugdescolumn, _("Description"));
  gtk_tree_view_column_pack_start(pwin->plugdescolumn, pwin->plugtext1cell, TRUE);
  gtk_tree_view_column_set_attributes(pwin->plugdescolumn, pwin->plugtext1cell,
	 "text", DES_COLUMN, NULL);

  pwin->plugfilencolumn = gtk_tree_view_column_new();
  pwin->plugtext2cell = gtk_cell_renderer_text_new ();
  gtk_tree_view_column_set_title (pwin->plugfilencolumn, _("Filename"));
  gtk_tree_view_column_pack_start(pwin->plugfilencolumn, pwin->plugtext2cell, TRUE);
  gtk_tree_view_column_set_attributes(pwin->plugfilencolumn, pwin->plugtext2cell,
	  "text", FILEN_COLUMN, NULL);


  pwin->plugenabledcell = gtk_cell_renderer_toggle_new ();

  coll_offset = gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (pwin->plugtreeview),
                                                           -1, _("Enable"),
                                                            pwin->plugenabledcell,
                                                            "active", ENABLED_COLUMN,
                                                            "visible", VISIBLE_COLUMN,
                                                            "activatable", WORLD_COLUMN,
                                                            NULL);
  pwin->plugenabledcolumn = gtk_tree_view_get_column (GTK_TREE_VIEW (pwin->plugtreeview), coll_offset - 1);
  gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN (pwin->plugenabledcolumn), GTK_TREE_VIEW_COLUMN_FIXED);
  gtk_tree_view_column_set_fixed_width (GTK_TREE_VIEW_COLUMN (pwin->plugenabledcolumn), 50);
  gtk_tree_view_column_set_clickable (GTK_TREE_VIEW_COLUMN (pwin->plugenabledcolumn), TRUE);

  g_signal_connect (G_OBJECT(pwin->plugenabledcell), "toggled",
              G_CALLBACK (plugin_enabled_item_toggled), pwin);


  /*gtk_tree_view_append_column (GTK_TREE_VIEW (pwin->plugtreeview), pwin->plugenabledcolumn);*/

  gtk_tree_view_append_column (GTK_TREE_VIEW (pwin->plugtreeview), pwin->plugdescolumn);

  gtk_tree_view_append_column (GTK_TREE_VIEW (pwin->plugtreeview), pwin->plugfilencolumn);


  pwin->plugbtnbox = gtk_hbox_new(TRUE, 0);
  gtk_widget_show(pwin->plugbtnbox); 
  gtk_box_pack_end (GTK_BOX (pwin->plugtvbox), pwin->plugbtnbox, FALSE, FALSE, 0);

  pwin->plugconfig = gtk_button_new_with_label (_("Configure"));
  gtk_widget_show (pwin->plugconfig); 
  
  gtk_box_pack_start (GTK_BOX (pwin->plugbtnbox), pwin->plugconfig, FALSE, FALSE, 0);

  pwin->plugabout = gtk_button_new_with_label (_("About"));
  gtk_widget_show (pwin->plugabout);  
  
  gtk_box_pack_start (GTK_BOX (pwin->plugbtnbox), pwin->plugabout, FALSE, FALSE, 0);


  pwin->prefnbkplug = gtk_label_new (_("Plugins"));
  gtk_widget_show (pwin->prefnbkplug);
  gtk_notebook_set_tab_label (GTK_NOTEBOOK (pwin->prefnbk), gtk_notebook_get_nth_page (GTK_NOTEBOOK (pwin->prefnbk), 1),
	  pwin->prefnbkplug);

/*eo plugins tab*/

  pwin->prefhsep = gtk_hseparator_new ();
  gtk_widget_show (pwin->prefhsep);
  
  gtk_box_pack_start (GTK_BOX (pwin->prefvbox), pwin->prefhsep, FALSE, TRUE, 0);

  pwin->prefhbtnbox = gtk_hbutton_box_new ();
  gtk_widget_show (pwin->prefhbtnbox);
  
  gtk_box_pack_end (GTK_BOX (pwin->prefvbox), pwin->prefhbtnbox, FALSE, FALSE, 0);
  gtk_button_box_set_layout (GTK_BUTTON_BOX (pwin->prefhbtnbox), GTK_BUTTONBOX_END);
  gtk_button_box_set_spacing (GTK_BUTTON_BOX (pwin->prefhbtnbox), 6);
  gtk_container_set_border_width (GTK_CONTAINER (pwin->prefhbtnbox), 6);
  

  pwin->close_button = gtk_button_new_from_stock (GNOME_STOCK_BUTTON_CLOSE);
  gtk_widget_show (pwin->close_button);
  gtk_container_add (GTK_CONTAINER (pwin->prefhbtnbox), pwin->close_button);
  GTK_WIDGET_SET_FLAGS (pwin->close_button, GTK_CAN_DEFAULT);


  gtk_widget_grab_default (pwin->close_button);

  gtk_object_set_data (GTK_OBJECT (pwin), "tooltips", pwin->tooltips);

  pwin->client = client;
  gconf_client_add_dir(client,
                       "/apps/gcm/prefs",
                       GCONF_CLIENT_PRELOAD_NONE,
                       NULL);

  g_signal_connect (G_OBJECT (pwin->plugconfig), "clicked",
                      G_CALLBACK (on_plugconfig_button_clicked),
                      pwin);

  g_signal_connect (G_OBJECT (pwin->plugabout), "clicked",
                      G_CALLBACK (on_plugabout_button_clicked),
                      pwin);


  g_signal_connect (G_OBJECT (pwin->close_button), "clicked",
                      G_CALLBACK (on_close_button_clicked),
                      pwin);


  gconf_client_notify_add(client, "/apps/gcm/prefs/useplugins",
                        (GConfClientNotifyFunc) useplugins_changed_callback,
                        pwin,
                        NULL, NULL);


  g_signal_connect (G_OBJECT (pwin->colbeep), "toggled",
                      G_CALLBACK (on_beep_changed),
                      pwin);
  gconf_client_notify_add(client, "/apps/gcm/prefs/beep_on_collect",
                        (GConfClientNotifyFunc) beep_changed_callback,
                        pwin,
                        NULL, NULL);

  g_signal_connect (G_OBJECT (pwin->colauto), "toggled",
                      G_CALLBACK (on_collect_changed),
                      pwin);
  gconf_client_notify_add(client, "/apps/gcm/prefs/auto_collect",
                        (GConfClientNotifyFunc) collect_changed_callback,
                        pwin,
                        NULL, NULL);


  g_signal_connect (G_OBJECT (pwin->colmaxspin), "changed",
                      G_CALLBACK (on_maxitems_changed),
                      pwin);
  gconf_client_notify_add(client, "/apps/gcm/prefs/maxitems",
                        (GConfClientNotifyFunc) maxitems_changed_callback,
                        pwin,
                        NULL, NULL);

  gconf_client_notify_add(client, "/apps/gcm/prefs/selection_setting",
                        (GConfClientNotifyFunc) selectionsetting_changed_callback,
                        pwin,
                        NULL, NULL);

  g_signal_connect (G_OBJECT (pwin->clipboard_only_radio), "toggled",
                      G_CALLBACK(on_clipboard_only_toggled),
                      pwin);


  g_signal_connect (G_OBJECT (pwin->clipboard_and_primary_radio), "toggled",
                      G_CALLBACK(on_clipboard_and_primary_toggled),
                      pwin);

  g_signal_connect (G_OBJECT (pwin->primary_only_radio), "toggled",
                      G_CALLBACK(on_primary_only_toggled),
                      pwin);



#ifdef DEVELOPMENT
		gconf_client_notify_add(client,"/apps/gcm/prefs/debug", 
                        (GConfClientNotifyFunc) a_unregistered_setting_changed_callback,
                        pwin,
                        NULL, NULL);
#endif
		 gconf_client_notify_add(client, "/apps/gcm/prefs/show_toolbar",
                        (GConfClientNotifyFunc) a_unregistered_setting_changed_callback,
                        pwin,
                        NULL, NULL);

		gconf_client_notify_add (client, "/apps/gcm/prefs/show_menu", 
                        (GConfClientNotifyFunc) a_unregistered_setting_changed_callback,
                        pwin,
                        NULL, NULL);

		gconf_client_notify_add (client, "/apps/gcm/prefs/show_cliplist_titles", 
                        (GConfClientNotifyFunc) a_unregistered_setting_changed_callback,
                        pwin,
                        NULL, NULL);

		gconf_client_notify_add (client, "/apps/gcm/prefs/show_statusbar", 
                        (GConfClientNotifyFunc) a_unregistered_setting_changed_callback,
                        pwin,
                        NULL, NULL);

		gconf_client_notify_add (client, "/apps/gcm/prefs/show_from", 
                        (GConfClientNotifyFunc) a_unregistered_setting_changed_callback,
                        pwin,
                        NULL, NULL);

		gconf_client_notify_add (client, "/apps/gcm/prefs/show_type", 
                        (GConfClientNotifyFunc) a_unregistered_setting_changed_callback,
                        pwin,
                        NULL, NULL);

		gconf_client_notify_add (client, "/apps/gcm/prefs/show_time", 
                        (GConfClientNotifyFunc) a_unregistered_setting_changed_callback,
                        pwin,
                        NULL, NULL);


		gconf_client_notify_add (client, "/apps/gcm/prefs/show_datap", 
                        (GConfClientNotifyFunc) a_unregistered_setting_changed_callback,
                        pwin,
                        NULL, NULL);


		gconf_client_notify_add (client, "/apps/gcm/prefs/multisel", 
                        (GConfClientNotifyFunc) a_unregistered_setting_changed_callback,
                        pwin,
                        NULL, NULL);

		gconf_client_notify_add (client, "/apps/gcm/prefs/manage_clipboard", 
                        (GConfClientNotifyFunc) a_unregistered_setting_changed_callback,
                        pwin,
                        NULL, NULL);

		gconf_client_notify_add (client, "/apps/gcm/prefs/manage_primary", 
                        (GConfClientNotifyFunc) a_unregistered_setting_changed_callback,
                        pwin,
                        NULL, NULL);

		gconf_client_notify_add (client, "/apps/gcm/prefs/manage_secondary", 
                        (GConfClientNotifyFunc) a_unregistered_setting_changed_callback,
                        pwin,
                        NULL, NULL);


		gconf_client_notify_add (client, "/apps/gcm/prefs/claim_clipboard", 
                        (GConfClientNotifyFunc) a_unregistered_setting_changed_callback,
                        pwin,
                        NULL, NULL);

		gconf_client_notify_add (client, "/apps/gcm/prefs/claim_primary", 
                        (GConfClientNotifyFunc) a_unregistered_setting_changed_callback,
                        pwin,
                        NULL, NULL);

		gconf_client_notify_add (client, "/apps/gcm/prefs/claim_secondary", 
                        (GConfClientNotifyFunc) a_unregistered_setting_changed_callback,
                        pwin,
                        NULL, NULL);


		gconf_client_notify_add (client, "/apps/gcm/prefs/unselall", 
                        (GConfClientNotifyFunc) a_unregistered_setting_changed_callback,
                        pwin,
                        NULL, NULL);

		gconf_client_notify_add (client, "/apps/gcm/prefs/followmode", 
                        (GConfClientNotifyFunc) a_unregistered_setting_changed_callback,
                        pwin,
                        NULL, NULL);

		gconf_client_notify_add (client, "/apps/gcm/prefs/selectnew", 
                        (GConfClientNotifyFunc) a_unregistered_setting_changed_callback,
                        pwin,
                        NULL, NULL);


  gtk_widget_grab_focus (pwin->close_button);
  gtk_window_set_wmclass (GTK_WINDOW (pwin), "prefswin", "gcm");
  return GTK_WIDGET(pwin);

}

void prefswin_fill_in_prefs (PrefsWin *pwin, gpointer p)
{
	Prefs *prefs = (Prefs*) p;
	if (prefs != NULL) {
		/*gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(pwin->pluguseplugins), prefs->useplugins); */
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(pwin->colbeep), prefs->beep_on_collect); 
		gtk_spin_button_set_value (GTK_SPIN_BUTTON(pwin->colmaxspin), prefs->maxitems);
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(pwin->colauto), prefs->auto_collect); 
	}
	
}

void
useplugins_changed_callback (GConfClient *client,
                          guint        cnxn_id,
                          GConfEntry  *entry,
                          gpointer     user_data)
{
	PrefsWin *pwin = GNOME_PREFSWIN(user_data);
	if (pwin!= NULL) {
		gboolean plugins = gconf_value_get_bool (gconf_entry_get_value (entry));
		MainWin *mwin = GNOME_MAINWIN(pwin->windowparent);
		/*gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(pwin->pluguseplugins), plugins);*/
		mwin->prefs->useplugins = plugins;
		reload_plugin_prefs (mwin, TRUE);
	}
	
}

void
beep_changed_callback (GConfClient *client,
                          guint        cnxn_id,
                          GConfEntry  *entry,
                          gpointer     user_data)
{
	PrefsWin *pwin = GNOME_PREFSWIN(user_data);
	if (pwin!= NULL) {
		gboolean beep = gconf_value_get_bool (gconf_entry_get_value (entry));
		MainWin *mwin = GNOME_MAINWIN(pwin->windowparent);
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(pwin->colbeep), beep); 
		mwin->prefs->beep_on_collect = beep;
		reload_ui_prefs (mwin, TRUE);
	}
}

void
maxitems_changed_callback (GConfClient *client,
                          guint        cnxn_id,
                          GConfEntry  *entry,
                          gpointer     user_data)
{
	PrefsWin *pwin = GNOME_PREFSWIN(user_data);
	if (pwin!= NULL) {
		gint amount = gconf_value_get_int (gconf_entry_get_value (entry));
		MainWin *mwin = GNOME_MAINWIN(pwin->windowparent);
		gtk_spin_button_set_value (GTK_SPIN_BUTTON(pwin->colmaxspin), amount);
		mwin->prefs->maxitems = amount;
		reload_maxitems_prefs (mwin, TRUE);
	}
}
void
collect_changed_callback(GConfClient *client,
                          guint        cnxn_id,
                          GConfEntry  *entry,
                          gpointer     user_data)
{
	PrefsWin *pwin = GNOME_PREFSWIN(user_data);
	if (pwin!= NULL) {
		gboolean collect = gconf_value_get_bool (gconf_entry_get_value (entry));
		MainWin *mwin = GNOME_MAINWIN(pwin->windowparent);
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(pwin->colauto), collect);
		mwin->prefs->auto_collect = collect;
		reload_collect_prefs (mwin, TRUE, TRUE);
	}

}


void
selectionsetting_changed_callback(GConfClient *client,
                          guint        cnxn_id,
                          GConfEntry  *entry,
                          gpointer     user_data)
{
	PrefsWin *pwin = GNOME_PREFSWIN(user_data);
	if (pwin!= NULL) {
		MainWin *mwin = GNOME_MAINWIN(pwin->windowparent);
		gint selectionsetting = gconf_value_get_int (gconf_entry_get_value (entry));
		mwin->prefs->selection_setting = selectionsetting;
		switch (selectionsetting) {
			case SELECTION_SETTING_CLIPBOARD_ONLY :
				/* Only X-event based management; get new item so that this management will start */
				reload_selection_prefs (mwin, TRUE, TRUE);
			break;
			case SELECTION_SETTING_CLIPBOARD_AND_PRIMARY :
				/* Both a poller which will automatically start and X-event based management */
				reload_selection_prefs (mwin, TRUE, TRUE);
			break;
			case SELECTION_SETTING_PRIMARY_ONLY :
				/* Only a poller which will automatically start */
				reload_selection_prefs (mwin, TRUE, FALSE);
			break;
			default:
				/* Unknown, so restart everything */
				reload_selection_prefs (mwin, TRUE, TRUE);
			break;
		}
		
	}
}


void
a_unregistered_setting_changed_callback(GConfClient *client,
                          guint        cnxn_id,
                          GConfEntry  *entry,
                          gpointer     user_data)
{
	PrefsWin *pwin = GNOME_PREFSWIN(user_data);
	MainWin *mwin = GNOME_MAINWIN(pwin->windowparent);
	if (mwin!=NULL) {
		prefs_free(mwin->prefs);
		mwin->prefs = getprefs_fromfile();
		reload_prefs(mwin);
	}
}
