/* * C o p y r i g h t *(also read COPYING)* * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *  Copyright (C) 2003  Philip Van Hoof <me@freax.org>                         *
 *                                                                             *
 *  This program is free software; you can redistribute it and/or modify       *
 *  it under the terms of the GNU General Public License as published by       *
 *  the Free Software Foundation; either version 2 of the License, or          *
 *  (at your option) any later version.                                        *
 *                                                                             *
 *  This program is distributed in the hope that it will be useful,            *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *  GNU General Public License for more details.                               *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License          *
 *  along with this program; if not, write to the Free Software                *
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA  *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */



#include <libgnome/gnome-config.h>
#include <libgnomeui/gnome-client.h>
#include "mainwin.h"
#include "plugin.h"
#include "controlsocket.h"

MainWin *mwin;
static GnomeClient *master_client = NULL;
static const char *program_argv0 = NULL;

static void 
interaction_function (GnomeClient *client, gint key, GnomeDialogType dialog_type, gpointer shutdown)
{

	if (GPOINTER_TO_INT (shutdown)) {
		if (mwin!=NULL) app_save_items_to_session (mwin);
	}
	gnome_interaction_key_return (key, FALSE);

}

static gboolean
client_save_yourself_cb (GnomeClient *client,
			 gint phase,
			 GnomeSaveStyle save_style,
			 gboolean shutdown,
			 GnomeInteractStyle interact_style,
			 gboolean fast,
			 gpointer data)
{
	const gchar *prefix;
	char *argv[] = { "rm", "-r", NULL };

	gnome_client_request_interaction (client, 
					  GNOME_DIALOG_NORMAL, 
					  interaction_function,
					  GINT_TO_POINTER (shutdown));
	prefix = gnome_client_get_config_prefix (client);

	argv[2] = gnome_config_get_real_path (prefix);
	gnome_client_set_discard_command (client, 3, argv);

	argv[0] = (char *) program_argv0;
	argv[1] = NULL; /* "--debug-session"; */

	gnome_client_set_clone_command (client, 1 /*2*/, argv);
	gnome_client_set_restart_command (client, 1 /*2*/, argv);


	return TRUE;
}


static void
client_die_cb (GnomeClient *client, gpointer data)
{
	g_print("Client Die\n");
	if (!client->save_yourself_emitted) {
		if (mwin!=NULL) app_save_items_to_session (mwin);
	}

	if (pluginsystem_ready ())
		cleanup_plugins ();

	cleanup_ctrlsocket ();
	gtk_main_quit ();
}

void
gcm_session_init (const char *argv0, MainWin *user_data)
{
	mwin = GNOME_MAINWIN(user_data);

	if (master_client)
		return;

	program_argv0 = argv0;

	master_client = gnome_master_client ();

	g_signal_connect (master_client, "save_yourself",
			  G_CALLBACK (client_save_yourself_cb),
			  NULL);
	g_signal_connect (master_client, "die",
			  G_CALLBACK (client_die_cb),
			  NULL);
}


gboolean
gcm_session_is_restored (void)
{
	gboolean restored;

	if (!master_client)
		return FALSE;

	restored = (gnome_client_get_flags (master_client) & GNOME_CLIENT_RESTORED) != 0;

	return restored;
}


gboolean
gcm_session_load (void)
{
	app_load_session_from_disk (mwin);
	return TRUE;
}
