/* * C o p y r i g h t *(also read COPYING)* * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *  Copyright (C) 2003  Philip Van Hoof <me@freax.org>                         *
 *                                                                             *
 *  This program is free software; you can redistribute it and/or modify       *
 *  it under the terms of the GNU General Public License as published by       *
 *  the Free Software Foundation; either version 2 of the License, or          *
 *  (at your option) any later version.                                        *
 *                                                                             *
 *  This program is distributed in the hope that it will be useful,            *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *  GNU General Public License for more details.                               *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License          *
 *  along with this program; if not, write to the Free Software                *
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA  *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "newitemwin.h"
#include "newitemwin_callbacks.h"
#include "mainwin.h"
/* Newitemwin Callbacks */

void newitemwin_show(GtkWidget *widget, gpointer *user_data)
{
}
void newitemwin_hide(GtkWidget *widget, gpointer *user_data)
{

}

void
on_newitemok_clicked                   (GtkButton       *button,
                                        gpointer         *user_data)
{
	NewItemWin *nwin = GNOME_NEWITEMWIN(user_data);
	GtkTextIter start, end;
	GtkTextBuffer *buffer;
	gchar *text;

	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (nwin->textview));
	gtk_text_buffer_get_bounds (buffer, &start, &end);
	text = gtk_text_iter_get_text (&start, &end);
	app_add_text_selection (GNOME_MAINWIN(nwin->windowparent), "Text", "New", text);
	g_free (text);
	gtk_widget_hide(GTK_WIDGET(nwin));
}

void
on_newitemcancel_clicked               (GtkButton       *button,
                                        gpointer         *user_data)
{
	NewItemWin *nwin = GNOME_NEWITEMWIN(user_data);
	gtk_widget_hide(GTK_WIDGET(nwin));
}

void
on_newitemmenu_edit_copy_activate      (GtkMenuItem     *menuitem,
                                        gpointer         *user_data)
{
	NewItemWin *nwin = GNOME_NEWITEMWIN(user_data);
	GtkTextBuffer *buffer;
	GtkClipboard *clipboard;

	clipboard = gtk_clipboard_get(GDK_NONE);
	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (nwin->textview));
	gtk_text_buffer_copy_clipboard (buffer, clipboard);
}

void
on_newitemmenu_edit_cut_activate      (GtkMenuItem     *menuitem,
                                        gpointer         *user_data)
{
	NewItemWin *nwin = GNOME_NEWITEMWIN(user_data);
	GtkTextBuffer *buffer;
	GtkClipboard *clipboard;

	clipboard = gtk_clipboard_get(GDK_NONE);
	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (nwin->textview));
	gtk_text_buffer_cut_clipboard (buffer, clipboard, TRUE);
}

void
on_newitemmenu_edit_paste_activate     (GtkMenuItem     *menuitem,
                                        gpointer         *user_data)
{
	NewItemWin *nwin = GNOME_NEWITEMWIN(user_data);
	GtkTextBuffer *buffer;
	GtkClipboard *clipboard;
    /*
<debugger>   buffer = self.source_view.get_buffer()
<debugger>   start, end = buffer.get_bounds()
<debugger>   insert_mark = buffer.get_insert()
<debugger>   selection_mark = buffer.get_selection_bound()
<debugger>   buffer.move_mark(insert_mark, end)
<debugger>   buffer.move_mark(selection_mark, start)  
    
    */
	clipboard = gtk_clipboard_get(GDK_NONE);
	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (nwin->textview));
	gtk_text_buffer_paste_clipboard (buffer, clipboard, NULL, TRUE);
}


void
on_newitemmenu_edit_selectall_activate (GtkMenuItem     *menuitem,
                                        gpointer         *user_data)
{
	/* fixme: GNOME 2.0 porting issue, I don't know how to port this */
	/* [2.0] gtk_editable_select_region(GTK_EDITABLE(nwin->itemtext), 0, -1); */
}


void
on_newitemmenu_edit_clear_activate     (GtkMenuItem     *menuitem,
                                        gpointer         *user_data)
{
	NewItemWin *nwin = GNOME_NEWITEMWIN(user_data);
	GtkTextBuffer *buffer;
	GtkClipboard *clipboard;
	GtkTextIter start, end;

	clipboard = gtk_clipboard_get(GDK_NONE);
	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (nwin->textview));
	gtk_text_buffer_get_bounds (buffer, &start, &end);
	gtk_text_buffer_delete (buffer, &start, &end);
}
