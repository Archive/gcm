/* * C o p y r i g h t *(also read COPYING)* * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *  Copyright (C) 2003  Philip Van Hoof <me@freax.org>                         *
 *                                                                             *
 *  This program is free software; you can redistribute it and/or modify       *
 *  it under the terms of the GNU General Public License as published by       *
 *  the Free Software Foundation; either version 2 of the License, or          *
 *  (at your option) any later version.                                        *
 *                                                                             *
 *  This program is distributed in the hope that it will be useful,            *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *  GNU General Public License for more details.                               *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License          *
 *  along with this program; if not, write to the Free Software                *
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA  *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
#include "main.h"
#include "session.h"
#include <gconf/gconf-client.h>
#include "mainwin.h"

gint t=0;
GdkAtom manager;
guint32 server_time;
GList* command_args;
gboolean show_mainwin=0;

static struct
poptOption gcm_options[] = {
	{
		NULL, '\0', POPT_ARG_INTL_DOMAIN, PACKAGE,
		0, NULL, NULL
	},
	{"show-mainwin", 's', POPT_ARG_NONE, &show_mainwin, 0, N_("Show the main window"), NULL},
	POPT_AUTOHELP {NULL}
};

gboolean claim_clipboard_manager_ownership (GtkWidget *mainwin)
{
	GNOME_MAINWIN(mainwin)->iammanager = TRUE;
	gtk_widget_add_events (mainwin, GDK_PROPERTY_CHANGE_MASK);
	server_time = gdk_x11_get_server_time (mainwin->window);
	gtk_selection_owner_set (mainwin, manager, server_time);
	gtk_widget_set_sensitive (mainwin, TRUE);
}

gint manager_idle_func(gpointer data)
{
	GDK_THREADS_ENTER();
	if (XGetSelectionOwner(gdk_display, gdk_x11_atom_to_xatom (manager))!=None) {
		g_print(_("Trying to get CLIPBOARD_MANAGER ownership\n"));
	} else {
		GtkWidget *mainwin = (GtkWidget*) data;
		claim_clipboard_manager_ownership (mainwin);
		app_get_new_item (GNOME_MAINWIN(mainwin));
		return FALSE;
	}
	GDK_THREADS_LEAVE();
	return TRUE;

}

int
main (int argc, char *argv[])
{
	GtkWidget *mainwin=NULL;
	GError* err = NULL;
	poptContext context;
	GConfClient* client = NULL;

	manager = gdk_atom_intern("CLIPBOARD_MANAGER", FALSE);
	g_thread_init(NULL);

	/* Initialize 18n */
	bindtextdomain (GETTEXT_PACKAGE, GNOMELOCALEDIR);
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (GETTEXT_PACKAGE);

	gnome_init_with_popt_table ("gnome-clipboard-manager", VERSION, argc, argv, 
							gcm_options, 0, &context);

	/* Initialize GConf */
	if (!gconf_init(argc, argv, &err)) {
		fprintf(stderr, _("Failed to init GConf: %s\n"), err->message);
		g_error_free(err); 
		err = NULL;
	} else {
		client = gconf_client_get_default();
		gconf_client_add_dir(client, "/apps/gcm", GCONF_CLIENT_PRELOAD_NONE, NULL);
	}
	poptFreeContext(context);

	mainwin = mainwin_new(mainwin);
	gtk_widget_realize (mainwin);

	GNOME_MAINWIN(mainwin)->argc = argc;
	GNOME_MAINWIN(mainwin)->argv = argv;

	app_update_menus(GNOME_MAINWIN(mainwin));

	/* Check for another CLIPBOARD MANAGER */
	if (XGetSelectionOwner(gdk_display, gdk_x11_atom_to_xatom (manager))!=None) {
		GNOME_MAINWIN(mainwin)->iammanager = FALSE;
		g_print(_("GCM: another application that does clipboard management is already running\n"));
		gtk_timeout_add(4000, manager_idle_func,(gpointer) mainwin); 
	} else {
		GNOME_MAINWIN(mainwin)->iammanager = TRUE;
		claim_clipboard_manager_ownership (mainwin);
		app_get_new_item (GNOME_MAINWIN(mainwin));
	}

	/* Initialize the Unix Socket for remoting */
	gcm_session_init (argv[0],(gpointer*)mainwin);

	if (gcm_session_is_restored ()) {
			if (!gcm_session_load ()) {
				g_print("GCM: Session not loaded\n");
			}
	}

	/* Show the mainwin if we are in -s mode */
	if (show_mainwin)
		  gtk_widget_show(mainwin);

	gdk_threads_enter();
	gtk_main ();
	gdk_threads_leave();

	return 0;
}

