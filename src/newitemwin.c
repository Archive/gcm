/* * C o p y r i g h t *(also read COPYING)* * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *  Copyright (C) 2003  Philip Van Hoof <me@freax.org>                         *
 *                                                                             *
 *  This program is free software; you can redistribute it and/or modify       *
 *  it under the terms of the GNU General Public License as published by       *
 *  the Free Software Foundation; either version 2 of the License, or          *
 *  (at your option) any later version.                                        *
 *                                                                             *
 *  This program is distributed in the hope that it will be useful,            *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *  GNU General Public License for more details.                               *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License          *
 *  along with this program; if not, write to the Free Software                *
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA  *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "newitemwin.h"
#include "mainwin.h"
#include "newitemwin_callbacks.h"

static void newitemwin_class_init(NewItemWinClass *klass);
static void newitemwin_init(NewItemWin *nwin);
static void newitemwin_destroy(GtkObject *object);

static GtkWindowClass *parent_class = NULL;
static GnomeUIInfo newitemmenu_edit_menu_uiinfo[] =
{
#define NEWITEMMENU_EDIT_CUT 0
  GNOMEUIINFO_MENU_CUT_ITEM (on_newitemmenu_edit_cut_activate, NULL),
#define NEWITEMMENU_EDIT_COPY 1
  GNOMEUIINFO_MENU_COPY_ITEM (on_newitemmenu_edit_copy_activate, NULL),
#define NEWITEMMENU_EDIT_PASTE 2
  GNOMEUIINFO_MENU_PASTE_ITEM (on_newitemmenu_edit_paste_activate, NULL),
#define NEWITEMMENU_EDIT_SELECTALL 3
  GNOMEUIINFO_MENU_SELECT_ALL_ITEM (on_newitemmenu_edit_selectall_activate, NULL),
#define NEWITEMMENU_EDIT_CLEAR 4
  GNOMEUIINFO_MENU_CLEAR_ITEM (on_newitemmenu_edit_clear_activate, NULL),
#define NEWITEMMENU_EDIT_END 5
  GNOMEUIINFO_END
};

static GnomeUIInfo newitemmenu_uiinfo[] =
{
#define NEWITEMMENU_EDIT 0
  GNOMEUIINFO_MENU_EDIT_TREE (newitemmenu_edit_menu_uiinfo),
#define NEWITEMMENU_END 1
  GNOMEUIINFO_END
};

GtkType newitemwin_get_type(void)
{
   static GtkType newitemwin_type = 0;
   
   if (!newitemwin_type)
   {
      static const GtkTypeInfo newitemwin_info =
      {	
         "NewItemWin",
         sizeof (NewItemWin),
         sizeof (NewItemWinClass),
         (GtkClassInitFunc) newitemwin_class_init,
         (GtkObjectInitFunc) newitemwin_init,
         NULL,
         NULL,
         (GtkClassInitFunc) NULL,
      };
      
      newitemwin_type = gtk_type_unique (GTK_TYPE_WINDOW, &newitemwin_info);
   }

   return (newitemwin_type);
}

static void newitemwin_class_init(NewItemWinClass *klass)
{
   GtkObjectClass *object_class;
   object_class = (GtkObjectClass*)klass;
   object_class->destroy = newitemwin_destroy;
   parent_class = gtk_type_class(GTK_TYPE_WINDOW);
}

static void newitemwin_init(NewItemWin *twin)
{

}


static void newitemwin_destroy(GtkObject *object)
{
   GtkWidget *nwin;
  
   g_return_if_fail(object != NULL);
   g_return_if_fail(GNOME_IS_NEWITEMWIN(object));
   
   nwin = GTK_WIDGET(object);
      
   gtk_widget_destroy(nwin);
}


/* Creates a new newitemwin */
GtkWidget* newitemwin_new (GtkWidget *windowparent)  {
  NewItemWin *nwin;

  nwin = gtk_type_new(GNOME_TYPE_NEWITEMWIN);
  g_signal_connect(G_OBJECT(nwin), "show", G_CALLBACK(newitemwin_show), nwin);
  g_signal_connect(G_OBJECT(nwin), "hide", G_CALLBACK(newitemwin_hide), nwin);
  g_signal_connect(G_OBJECT(nwin), "destroy", G_CALLBACK(newitemwin_hide), nwin);
  nwin->windowparent = windowparent;
  gtk_window_set_transient_for (GTK_WINDOW(nwin), GTK_WINDOW(nwin->windowparent));
  gtk_window_set_title (GTK_WINDOW (nwin), _("Create new item"));

  newitemmenu_edit_menu_uiinfo[NEWITEMMENU_EDIT_CUT].user_data = nwin;
  newitemmenu_edit_menu_uiinfo[NEWITEMMENU_EDIT_COPY].user_data = nwin;
  newitemmenu_edit_menu_uiinfo[NEWITEMMENU_EDIT_PASTE].user_data = nwin;
  newitemmenu_edit_menu_uiinfo[NEWITEMMENU_EDIT_SELECTALL].user_data = nwin;
  newitemmenu_edit_menu_uiinfo[NEWITEMMENU_EDIT_CLEAR].user_data = nwin;

  nwin->menubarvbox = gtk_vbox_new (FALSE, 0);
  gtk_widget_show (nwin->menubarvbox);
  gtk_container_add (GTK_CONTAINER (nwin), nwin->menubarvbox);

  nwin->menubardock = gtk_handle_box_new ();
  gtk_widget_show (nwin->menubardock);
  gtk_box_pack_start (GTK_BOX (nwin->menubarvbox), nwin->menubardock, FALSE, FALSE, 0);

  nwin->newitemmenu = gtk_menu_bar_new ();
  gtk_widget_show (nwin->newitemmenu);
  gtk_container_add (GTK_CONTAINER (nwin->menubardock), nwin->newitemmenu);
  gnome_app_fill_menu (GTK_MENU_SHELL (nwin->newitemmenu), newitemmenu_uiinfo,
                       NULL, FALSE, 0);

  nwin->scrolledwindow = gtk_scrolled_window_new (NULL, NULL);
  gtk_widget_show (nwin->scrolledwindow);
  gtk_box_pack_start (GTK_BOX (nwin->menubarvbox), nwin->scrolledwindow, TRUE, TRUE, 0);
  /* [2.0] gtk_widget_set_usize (nwin->scrolledwindow, 350, 150); */
  gtk_widget_set_size_request (nwin->scrolledwindow, 350, 150);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (nwin->scrolledwindow), GTK_POLICY_NEVER, GTK_POLICY_ALWAYS);
  
  /* [2.0] GNOME 2.0 porting issue (GtkText is replaced) 
  nwin->itemtext = gtk_text_new (NULL, NULL);
  gtk_widget_show (nwin->itemtext);
  gtk_container_add (GTK_CONTAINER (nwin->scrolledwindow), nwin->itemtext);
  GTK_WIDGET_SET_FLAGS (nwin->itemtext, GTK_CAN_DEFAULT);
  gtk_editable_set_editable(GTK_EDITABLE(nwin->itemtext), TRUE);
  */

  nwin->tag = gtk_text_tag_table_new ();
  nwin->textbuffer = gtk_text_buffer_new (nwin->tag);
  nwin->textview = gtk_text_view_new_with_buffer (nwin->textbuffer);
  gtk_text_view_set_editable (GTK_TEXT_VIEW(nwin->textview), TRUE);
  gtk_widget_show(nwin->textview);
  gtk_container_add (GTK_CONTAINER (nwin->scrolledwindow), nwin->textview);
  GTK_WIDGET_SET_FLAGS (nwin->textview, GTK_CAN_DEFAULT);


  nwin->tooltips = gtk_tooltips_new();
  nwin->hbtnbox = gtk_hbutton_box_new ();
  gtk_button_box_set_layout (GTK_BUTTON_BOX (nwin->hbtnbox), GTK_BUTTONBOX_END);
  gtk_container_set_border_width (GTK_CONTAINER (nwin->hbtnbox), 6);
  gtk_widget_show (nwin->hbtnbox);
  gtk_box_pack_end (GTK_BOX (nwin->menubarvbox), nwin->hbtnbox, FALSE, FALSE, 0);
  gtk_button_box_set_layout (GTK_BUTTON_BOX (nwin->hbtnbox), GTK_BUTTONBOX_END);
  gtk_button_box_set_spacing (GTK_BUTTON_BOX (nwin->hbtnbox), 6);
  
  nwin->newitemcancel =  gtk_button_new_from_stock (GNOME_STOCK_BUTTON_CANCEL);
  gtk_widget_show (nwin->newitemcancel);
  gtk_container_add (GTK_CONTAINER (nwin->hbtnbox), nwin->newitemcancel);
  GTK_WIDGET_SET_FLAGS (nwin->newitemcancel, GTK_CAN_DEFAULT);

  nwin->newitemok = gtk_button_new_from_stock  (GNOME_STOCK_BUTTON_OK);
  gtk_widget_show (nwin->newitemok);
  gtk_container_add (GTK_CONTAINER (nwin->hbtnbox), nwin->newitemok);
  GTK_WIDGET_SET_FLAGS (nwin->newitemok, GTK_CAN_DEFAULT);
  gtk_tooltips_set_tip (nwin->tooltips, nwin->newitemok, _("Add new item"), NULL);

  gtk_tooltips_set_tip (nwin->tooltips, nwin->newitemcancel, _("Close this window"), NULL);
  
  gtk_widget_grab_focus (nwin->textview);
  gtk_widget_grab_default (nwin->newitemok);

  
  g_signal_connect (G_OBJECT (nwin->newitemok), "clicked",
                      G_CALLBACK (on_newitemok_clicked),
                      nwin);

  g_signal_connect (G_OBJECT (nwin->newitemcancel), "clicked",
                      G_CALLBACK (on_newitemcancel_clicked),
                      nwin);
  gtk_window_set_wmclass (GTK_WINDOW (nwin), "newitemwin", "gcm");
  return GTK_WIDGET(nwin);
}


