#! /bin/sh
PREFIX=/usr
MAKEFLAGS="-j 10"
echo -en "\n- Running make distclean\n\n"
make distclean
echo -en "\n- Running ./autogen.sh --prefix=$PREFIX\n\n"
./autogen.sh --prefix=$PREFIX
echo -en "\n- Running make $MAKEFLAGS\n\n"
make $MAKEFLAGS
echo -en "\n- Running su -c \"make install\"\n\n"
su -c "make install"

