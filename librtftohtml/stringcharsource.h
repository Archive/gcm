// stringcharsource.h: interface for the StringCharSource class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_STRINGCHARSOURCE_H__E113F021_FB3F_11D6_858F_E817FA213732__INCLUDED_)
#define AFX_STRINGCHARSOURCE_H__E113F021_FB3F_11D6_858F_E817FA213732__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class StringCharSource : public CharSource  
{
public:
	char unGetChar(char ch);
	char getChar();
	bool endOfInput();
	StringCharSource(char *source);
	StringCharSource(char *source, int len);
	StringCharSource();
	virtual ~StringCharSource();

private:
	char * pos;
	char * source;
	int len;
};

#endif // !defined(AFX_STRINGCHARSOURCE_H__E113F021_FB3F_11D6_858F_E817FA213732__INCLUDED_)
