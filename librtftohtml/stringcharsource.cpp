/*Copyright 2001
 *
 * stringcharsource.cpp
 *
 * Author: Gregory Ford greg@reddfish.co.nz
 * RTF token parser based on:
 *      rtf2html by Dmitry Porapov <dpotapov@capitalsoft.com>,
 *      based on earlier work of Chuck Shotton.
 *      distributed under BSD-style license
 * RTF token lists and hashing algorithm based on
 *      RTF Tools, Release 1.10 
 *      6 April 1994	
 *      Paul DuBois	dubois@primate.wisc.edu 
 *
 * Copying permitted under GNU licence (see COPYING)
 */ 
// filecharsource.cpp: implementation of the FileCharSource class.
//
//////////////////////////////////////////////////////////////////////

#include "charsource.h"
#include "stringcharsource.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

StringCharSource::StringCharSource()
{
	source = 0;
	pos = 0;
}

StringCharSource::~StringCharSource()
{
	if ( source ) source = 0; // todo: maybe should free it 
}

StringCharSource::StringCharSource(char *source)
{
	this->source = source;
	this->pos = source;
}

StringCharSource::StringCharSource(char *source, int len)
{ 
        this->source = source;
        this->pos = source;
	this->len = len;
}

bool StringCharSource::endOfInput()
{
	if ( !pos || *pos == '\0' ) return true;
	return false;
}

char StringCharSource::getChar()
{
	char ch;
	if ( !pos ) return '\0';
	do{
		if ( *pos == '\0' ) return *pos;
		ch=*(pos++);
	} while ((ch=='\r')||(ch=='\n'));
	
	return ch;
}

char StringCharSource::unGetChar(char ch)
{
	if (!pos) return '\0';
	if ( (pos > source) ) return *(pos--);
	return *pos;
}
