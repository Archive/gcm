/*Copyright 2001
 *
 * rtftohtml.cpp
 *
 * Author: Philip Van Hoof - me@freax.org
 * RTF token parser based on:
 *      rtf2html by Dmitry Porapov <dpotapov@capitalsoft.com>,
 *      based on earlier work of Chuck Shotton.
 *      distributed under BSD-style license
 * RTF token lists and hashing algorithm based on
 *      RTF Tools, Release 1.10 
 *      6 April 1994	
 *      Paul DuBois	dubois@primate.wisc.edu 
 *
 * Copying permitted under GNU licence (see COPYING)
 */ 
// librtftohtml.cpp
#include "librtftohtml.h"
/*
#include <string>
#include <vector>
#include <stack>
#include <iostream>
using namespace std ;
#include <stdio.h>


#include "charoutput.h"
#include "linktracker.h"

#include "charsource.h"
#include "stringcharsource.h"

#include "tag.h"
#include "stylemap.h"
#include "token.h"
#include "tokeniser.h"

#include "graphicstate.h"
#include "destination.h"
#include "bodydestination.h"
#include "ignoredestination.h"
#include "stylesheetdestination.h"
#include "infodestination.h"

#include "rtfparser.h"
*/


char * rtftohtml( char *rtf, int len)
{
	char *p;
	StringCharSource *src;
	src = new StringCharSource( rtf, len );
	CharPointerOutput *dest = NULL;
	dest = new CharPointerOutput();

	RtfParser parser( dest );
	parser.parseDocument( src, dest);
	return (char*) dest->str.c_str();

}

