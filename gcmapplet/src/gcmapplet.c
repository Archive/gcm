/* * C o p y r i g h t *(also read COPYING)* * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *  Copyright (C) 2002  Philip Van Hoof <me@freax.org>                         *
 *                                                                             *
 *  This program is free software; you can redistribute it and/or modify       *
 *  it under the terms of the GNU General Public License as published by       *
 *  the Free Software Foundation; either version 2 of the License, or          *
 *  (at your option) any later version.                                        *
 *                                                                             *
 *  This program is distributed in the hope that it will be useful,            *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *  GNU General Public License for more details.                               *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License          *
 *  along with this program; if not, write to the Free Software                *
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA  *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/* gcmapplet.c  This is a gnome panel applet that allow users to
 * control the GNOME Clipboard Manager.
 */

/*GNOME_PANEL_DEBUG=1 BONOBO_ACTIVATION_DEBUG_OUTPUT=1 gnome-panel*/

#include <panel-applet.h>
#include "gcmapplet.h"
#include "config.h"
#include "../config.h"

gint session=0;
gboolean enter=FALSE;
#define IS_PANEL_HORIZONTAL(o) (o == PANEL_APPLET_ORIENT_UP || o == PANEL_APPLET_ORIENT_DOWN)


gboolean
popup_grab_on_window (GdkWindow *window,
		      guint32    activate_time)
{
  if ((gdk_pointer_grab (window, TRUE,
			 GDK_BUTTON_PRESS_MASK | GDK_BUTTON_RELEASE_MASK |
			 GDK_POINTER_MOTION_MASK,
			 NULL, NULL, activate_time) == 0))
    {
      if (gdk_keyboard_grab (window, TRUE,
			     activate_time) == 0)
	return TRUE;
      else
	{
	  gdk_pointer_ungrab (activate_time);
	  return FALSE;
	}
    }

  return FALSE;
}

static GnomeUIInfo itempopup_menu_uiinfo[] =
{
#define ITEMPOPUPMENU_EDITITEM 0
  {
    GNOME_APP_UI_ITEM, N_("_Edit"),
    NULL,
    (gpointer) on_edit_item_activate, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_SAVE_AS,
    0, (GdkModifierType) 0, NULL
  },

  #define ITEMPOPUPMENU_MOVE_UP 1
  {
    GNOME_APP_UI_ITEM, N_("Move item _up"),
    NULL,
    (gpointer) on_move_item_up_activate, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_UP,
    0, (GdkModifierType) 0, NULL
  },
#define ITEMPOPUPMENU_MOVE_DOWN 2
  {
    GNOME_APP_UI_ITEM, N_("Move item _down"),
    NULL,
    (gpointer) on_move_item_down_activate, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_DOWN,
    0, (GdkModifierType) 0, NULL
  },
  #define ITEMPOPUPMENU_FILE_END 3
  GNOMEUIINFO_END
};


GtkWidget * gcmapplet_update_itempopupmenu (gcmapplet_data *curr_data, itemclick_cb *d) {
		GtkWidget *popup;
		gint i;
	
		if (curr_data->itempopupmenu != NULL)
			gtk_widget_destroy(curr_data->itempopupmenu);
			
		for (i=0; i<ITEMPOPUPMENU_FILE_END; i++) {	
			itempopup_menu_uiinfo[i].user_data = d;
		}
		
		popup = gtk_menu_new ();
  		gnome_app_fill_menu (GTK_MENU_SHELL (popup), itempopup_menu_uiinfo,
                       NULL, FALSE, 0);	

		return popup;
}


void
on_move_item_up_activate (GtkMenuItem *menuitem,   gpointer user_data)
{
	itemclick_cb *d =(itemclick_cb*) user_data;
	gcmapplet_data *curr_data = d->curr_data;
	gcm_remote_move_selected_up (session);
	destroy_buttons (curr_data);
	redraw_buttons(curr_data);
}

void
on_move_item_down_activate (GtkMenuItem *menuitem,   gpointer user_data)
{
	itemclick_cb *d =(itemclick_cb*) user_data;
	gcmapplet_data *curr_data = d->curr_data;
	gcm_remote_move_selected_down (session);
	destroy_buttons (curr_data);
	redraw_buttons(curr_data);
}


void
on_edit_item_activate (GtkMenuItem *menuitem,   gpointer user_data) {
    if (user_data != NULL) {
		itemclick_cb *d = (itemclick_cb*) user_data;
		gcmapplet_data *curr_data = d->curr_data;
		gint row = d->i;
	
		gcm_remote_edit_item(session, row);	
	}
}

void
on_show_clicked (GtkButton *button, itemclick_cb *cb)
{
	gcm_remote_mainwin_show (session);
	gcmapplet_popup_hide (cb->curr_data);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(cb->curr_data->button), FALSE);
}

void
on_hide_clicked (GtkButton *button, itemclick_cb *cb)
{

	gcm_remote_mainwin_hide (session);
	gcmapplet_popup_hide (cb->curr_data);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(cb->curr_data->button), FALSE);

}

void
gcmapplet_popup_reposition (gcmapplet_data *p_curr_data)
{
	GtkRequisition requisition;

	gtk_widget_size_request (p_curr_data->popup, &p_curr_data->req);
	gdk_window_get_origin (p_curr_data->button->window, &p_curr_data->x, &p_curr_data->y);
	gdk_drawable_get_size (p_curr_data->button->window, &p_curr_data->width, &p_curr_data->height);
	

	  switch (p_curr_data->orientation) {
        case PANEL_APPLET_ORIENT_DOWN:
                p_curr_data->x += (p_curr_data->width - p_curr_data->req.width) / 2;
                p_curr_data->y += p_curr_data->height;
                break;
        case PANEL_APPLET_ORIENT_UP:
                p_curr_data->x += (p_curr_data->width - p_curr_data->req.width) / 2;
                p_curr_data->y -= p_curr_data->req.height;
                break;
        case PANEL_APPLET_ORIENT_LEFT:
                p_curr_data->x -= p_curr_data->req.width;
                p_curr_data->y += (p_curr_data->height - p_curr_data->req.height) / 2;
                break;
        case PANEL_APPLET_ORIENT_RIGHT:
                p_curr_data->x += p_curr_data->width;
				p_curr_data->y += (p_curr_data->height - p_curr_data->req.height) / 2;
                break;
        default:
                break;
	  }

	if (p_curr_data->x < 0) p_curr_data->x = 0;
	if (p_curr_data->y < 0) p_curr_data->y = 0;
	gtk_widget_size_request (GTK_WIDGET(p_curr_data->popup), &requisition);

	if ((p_curr_data->x + requisition.width) > gdk_screen_width ())
		p_curr_data->x = gdk_screen_width () - (requisition.width);

	if ((p_curr_data->y + requisition.height) > gdk_screen_height ())
		p_curr_data->y = gdk_screen_height () - (requisition.height);


	  gtk_window_move (GTK_WINDOW (p_curr_data->popup), p_curr_data->x, p_curr_data->y);
	  gtk_widget_show (p_curr_data->popup);
 



}



gboolean
popup_button_press_event (GtkWidget *widget,
			      GdkEvent  *event,
			      gcmapplet_data *data)
{
  GtkWidget *child;

  child = gtk_get_event_widget (event);
gnome_error_dialog("asd");
  /* We don't ask for button press events on the grab widget, so
   *  if an event is reported directly to the grab widget, it must
   *  be on a window outside the application (and thus we remove
   *  the popup window). Otherwise, we check if the widget is a child
   *  of the grab widget, and only remove the popup window if it
   *  is not.
   */
  if (child != widget)
    {
      while (child)
	{
	  if (child == widget)
	    return FALSE;
	  child = child->parent;
	}
    }

  gcmapplet_popup_hide (data);
  return TRUE;

}

gboolean
popup_key_press_event (GtkWidget *widget,
			      GdkEventKey * event,
			      gcmapplet_data *data)
{
	guint state = event->state & gtk_accelerator_get_default_mod_mask ();

  if (event->keyval == GDK_Escape && state == 0)
    {
      if (GTK_WIDGET_HAS_GRAB (data->popup))

      gcmapplet_popup_hide (data);
      
      return TRUE;
    }
  return FALSE;
}


gboolean
popup_event_after (GtkWidget *widget,
			      GdkEvent  *event,
			      gcmapplet_data *data)
{
	if (event->type != GDK_BUTTON_RELEASE)
		return FALSE;

	gcmapplet_popup_hide(data);
	return FALSE;
}
gboolean on_item_event (GtkButton *button,  GdkEvent        *event,
                                        gpointer         user_data)
{	
	itemclick_cb *d = (itemclick_cb*) user_data;
	gcmapplet_data *curr_data = d->curr_data;
	gint row = d->i;
	GdkEventButton *button_event = (GdkEventButton *) event;
	if (event->type == GDK_BUTTON_PRESS && button_event->button == 3) {
		
		GtkWidget *popup = gcmapplet_update_itempopupmenu (curr_data, d);

		gtk_menu_popup (GTK_MENU(popup), NULL, NULL, NULL, NULL, 3, 
				button_event->time);
		
		curr_data->itempopupmenu =popup;
	
	} else if (event->type == GDK_BUTTON_PRESS && button_event->button == 1) {
			on_item_clicked(button, d);
		
	}
	return FALSE;
}


void
on_item_clicked (GtkButton *button, itemclick_cb *cb)
{
	gcm_remote_select_none (session);
	gcm_remote_select_row (session, cb->i);
	gcmapplet_popup_hide (cb->curr_data);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(cb->curr_data->button), FALSE);
	
}

static gboolean
applet_button_release_event_cb (GtkWidget *widget, GdkEventButton *event, gcmapplet_data *data)
{

	if (event->button == 1) {
		if (data->popup == NULL) {
			gcmapplet_popup_show (data);
			redraw_buttons (data);
			draw_std_ctrls (data);
			gcmapplet_popup_reposition (data);
		}
	}
}

static gboolean
button_key_press_event_cb (GtkWidget *widget, GdkEventKey *event, gcmapplet_data *data)
{

        switch (event->keyval) {
        case GDK_Escape:
                /* Revert. */
                gcmapplet_popup_hide (data);
                return TRUE;

        case GDK_KP_Enter:
        case GDK_ISO_Enter:
        case GDK_3270_Enter:
        case GDK_Return:
        case GDK_space:
        case GDK_KP_Space:
                gcmapplet_popup_hide (data);
                return TRUE;

        default:
                break;
        }

        return FALSE;
}

static gboolean
applet_key_press_event_cb (GtkWidget *widget, GdkEventKey *event, gcmapplet_data *data)
{
        switch (event->keyval) {
        case GDK_Escape:
                /* Revert. */
                gcmapplet_popup_hide (data);
                return TRUE;

        case GDK_KP_Enter:
        case GDK_ISO_Enter:
        case GDK_3270_Enter:   
        case GDK_Return:
        case GDK_space:
        case GDK_KP_Space:
                /* Apply. */
                if (data->popup != NULL)
                        gcmapplet_popup_hide (data);
                else {
                        gcmapplet_popup_show (data);
						redraw_buttons (data);
						draw_std_ctrls (data);
						gcmapplet_popup_reposition (data);
				}
                return TRUE;

        default:
                break;
        }

        return FALSE;
}


void
gcmapplet_popup_hide (gcmapplet_data *p_curr_data)
{
	gdk_pointer_ungrab (gtk_get_current_event_time ());
	gdk_keyboard_ungrab (gtk_get_current_event_time ());

	if (p_curr_data->popup!=NULL) { 

		destroy_buttons (p_curr_data);
		if ((p_curr_data->popup!=NULL)&&GTK_IS_WIDGET(p_curr_data->popup))
			gtk_widget_destroy (GTK_WIDGET (p_curr_data->popup));
		p_curr_data->popup = NULL;
	}
}

void
gcmapplet_popup_show (gcmapplet_data *p_curr_data)
{
	GtkWidget      *frame;
	GtkWidget      *inner_frame;


	p_curr_data->popup = gtk_window_new (GTK_WINDOW_POPUP);
	g_object_ref (p_curr_data->popup);

	popup_grab_on_window (p_curr_data->popup->window,
		gtk_get_current_event_time ());

	gtk_grab_add (p_curr_data->popup);

	gtk_widget_set_events (p_curr_data->popup, GDK_KEY_PRESS_MASK);

	g_signal_connect (p_curr_data->popup, "key_press_event",
		    G_CALLBACK (popup_key_press_event), p_curr_data);
	g_signal_connect (p_curr_data->popup, "button_press_event",
		    G_CALLBACK (popup_button_press_event), p_curr_data);

	g_signal_connect (G_OBJECT (p_curr_data->popup), "event_after",
		G_CALLBACK (popup_event_after), p_curr_data);

	enter = TRUE;

	frame = gtk_frame_new (NULL);
	gtk_container_set_border_width (GTK_CONTAINER (frame), 0);
	gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_OUT);
	gtk_widget_show (frame);

	inner_frame = gtk_frame_new (NULL);
	gtk_container_set_border_width (GTK_CONTAINER (inner_frame), 0);
	gtk_frame_set_shadow_type (GTK_FRAME (inner_frame), GTK_SHADOW_NONE);
	gtk_widget_show (inner_frame);

	gtk_container_add (GTK_CONTAINER (p_curr_data->popup), frame);

	gtk_container_add (GTK_CONTAINER (frame), inner_frame);

	p_curr_data->inner_frame = inner_frame;
	p_curr_data->popvbox = gtk_vbox_new(FALSE, 0);
	gtk_widget_show(p_curr_data->popvbox);
	gtk_container_add (GTK_CONTAINER (p_curr_data->inner_frame), p_curr_data->popvbox);
	gtk_widget_show (inner_frame);


}



static gboolean
button_press_hack (GtkWidget      *widget,
                   GdkEventButton *event,
                   GtkWidget      *applet)
{
    if (event->button == 3 || event->button == 2) {
        gtk_propagate_event (applet, (GdkEvent *) event);

        return TRUE;
    }

    return FALSE;
}


void draw_std_ctrls  (gcmapplet_data *p_curr_data)
{
	/* Additional controls could be drawn here */
}

void destroy_buttons (gcmapplet_data *p_curr_data)
{
	if (p_curr_data->buttons != NULL) {
		GList *mlist = g_list_copy(p_curr_data->buttons);
		while (mlist) {
			GtkWidget *button = (GtkWidget*) mlist->data;
			if ((button != NULL)&& GTK_IS_WIDGET(button))
				gtk_widget_destroy(button);
			mlist = g_list_next(mlist);
		}
		p_curr_data->buttons=NULL;
	}
	
}

void redraw_buttons (gcmapplet_data *p_curr_data)
{
		GtkWidget *vbox = gtk_vbox_new(FALSE, 0);	
		gint i, cnt=0;

		cnt = gcm_remote_get_rowcnt(session);
		if (cnt > 0) {
		 for (i=0; i<cnt; i++) {
			gchar *bstr = (gchar*) gcm_remote_get_rowpreview (session, i);
			gchar *str = g_strndup(bstr, PREVIEWMAXLEN);
			g_free(bstr);
            
			if ((str != NULL)&&(g_utf8_validate (str, strlen(str), NULL))) {
			  GtkWidget *nbutton = gtk_button_new_with_label(str);
			  itemclick_cb *cb = g_new(itemclick_cb,1);

			  cb->curr_data = p_curr_data;
			  cb->i = i;
			  gtk_button_set_relief(GTK_BUTTON(nbutton), GTK_RELIEF_NONE);
			  gtk_box_pack_start (GTK_BOX(vbox), nbutton, TRUE, TRUE, 0);
			  			
			  g_signal_connect (G_OBJECT (nbutton), "event",
                      G_CALLBACK (on_item_event), cb);
				
			  gtk_widget_show(nbutton);
			  p_curr_data->buttons = g_list_append(p_curr_data->buttons, nbutton);
			  g_free(str);
			} else {
				
			}
		 }
		}

		gtk_widget_show(vbox);
		gtk_box_pack_start (GTK_BOX(p_curr_data->popvbox), vbox, TRUE, TRUE, 0);
		gtk_widget_show(p_curr_data->popvbox);

}

static gint
toggle_button_toggled_cb(GtkToggleButton *button, gcmapplet_data *p_curr_data)
{  
  if (p_curr_data != NULL) {
	gboolean toggled = gtk_toggle_button_get_active (button);
	if (toggled) {
		gcmapplet_popup_show (p_curr_data);
		redraw_buttons (p_curr_data);
		draw_std_ctrls (p_curr_data);
		gcmapplet_popup_reposition (p_curr_data);
	} else {
		gcmapplet_popup_hide (p_curr_data);	
	}
	return TRUE;
  } else {
	return FALSE;
  }
}

void
build_applet(gcmapplet_data *p_curr_data)
{
  GtkWidget *box;
  gchar *file;
 if (p_curr_data != NULL) {
	if (IS_PANEL_HORIZONTAL(p_curr_data->orientation))
		box = gtk_hbox_new (TRUE, 0);
	else
		box = gtk_vbox_new (TRUE, 0);
  
	p_curr_data->orientation = panel_applet_get_orient (PANEL_APPLET (p_curr_data->applet));
	p_curr_data->panel_size = panel_applet_get_size (PANEL_APPLET (p_curr_data->applet));
	
  gtk_widget_show (box);
  
  p_curr_data->box = box;

  p_curr_data->button = gtk_toggle_button_new ();
  gtk_button_set_relief(GTK_BUTTON(p_curr_data->button), GTK_RELIEF_NONE);
  gtk_widget_show(p_curr_data->button);

  if (p_curr_data->panel_size < SMALLSIZE) {   
	    file = gnome_program_locate_file (NULL, GNOME_FILE_DOMAIN_PIXMAP, "gcmapplet_small.png", FALSE, NULL);
  } else {
	    file = gnome_program_locate_file (NULL, GNOME_FILE_DOMAIN_PIXMAP, "gcmapplet.png", FALSE, NULL);
  }
  p_curr_data->image = gtk_image_new_from_file (file);
  g_free(file);
  gtk_widget_show (p_curr_data->image);
  gtk_container_add (GTK_CONTAINER (p_curr_data->button), p_curr_data->image);
  
  gtk_box_pack_start (GTK_BOX (box), p_curr_data->button, TRUE, TRUE, 0);
  
  g_signal_connect (G_OBJECT (p_curr_data->button), "toggled",
                        G_CALLBACK (toggle_button_toggled_cb),
                        p_curr_data);
 
  g_signal_connect (G_OBJECT (p_curr_data->button), "button_press_event",
                        G_CALLBACK (button_press_hack), p_curr_data->applet);
 
  g_signal_connect (G_OBJECT(p_curr_data->button), "key-press-event",
			G_CALLBACK (button_key_press_event_cb), p_curr_data);
 }
}

void
hide_userinterface (BonoboUIComponent *uic,  
                         gcmapplet_data     *curr_data,
                         const gchar       *verbname)
{
	gcm_remote_mainwin_hide (session);
}

void
show_userinterface (BonoboUIComponent *uic,  
                         gcmapplet_data     *curr_data,
                         const gchar       *verbname)
{
	gcm_remote_mainwin_hide (session);
	gcm_remote_mainwin_show (session);
}

void
show_preferences_dialog (BonoboUIComponent *uic,  
                         gcmapplet_data     *curr_data,
                         const gchar       *verbname)
{
	gcm_remote_prefswin_hide(session);
	gcm_remote_prefswin_show(session);
}

void
show_newitem_dialog (BonoboUIComponent *uic,  
                         gcmapplet_data     *curr_data,
                         const gchar       *verbname)
{
	gcm_remote_newitemwin_hide(session);
	gcm_remote_newitemwin_show(session);
}

void
saveall (BonoboUIComponent *uic,  
                         gcmapplet_data     *curr_data,
                         const gchar       *verbname)
{
	gcm_remote_save_all (session);
}

void
clearall (BonoboUIComponent *uic,  
                         gcmapplet_data     *curr_data,
                         const gchar       *verbname)
{
	gcm_remote_clear (session);
}

static void
help_cb (BonoboUIComponent *uic,
         gcmapplet_data     *curr_data,
         const char        *verb)
{
	/* TODO */
	g_print("Showing help\n");
}


static void applet_change_pixel_size(PanelApplet *applet, gint size, gpointer data)
{
  gcmapplet_data *curr_data = data;
  curr_data->panel_size = size;

  build_applet (curr_data);
  return;
}

static void applet_change_orient(PanelApplet *applet, PanelAppletOrient o, gpointer data)
{
	if  (data != NULL) {
		gcmapplet_data *curr_data = data;
		curr_data->orientation = o;
		gcmapplet_popup_hide(curr_data);
	}
	return;
}


static void
about (BonoboUIComponent *uic,
       gcmapplet_data     *curr_data,
       const char        *verb)
{
  static GtkWidget *about_box = NULL;
  GdkPixbuf   	   *pixbuf;
  GError      	   *error     = NULL;
  gchar            *file;
   
  const char *authors[] = {
	  _("Philip Van Hoof <me@freax.org>"),
	  NULL
  };

  const gchar *documenters[] = {
	  NULL
  };

  const gchar *translator_credits = _("translator_credits");

  if (about_box) {

	gtk_window_present (GTK_WINDOW (about_box));
	return;
  }
  file = g_strdup_printf("%s%s", PIXMAPPATH, "/gcmapplet.png");
  pixbuf = gdk_pixbuf_new_from_file (file, &error);
  
   
  if (error) {
  	  g_warning (G_STRLOC ": cannot open %s: %s", file, error->message);
	  g_error_free (error);
  }
  
  about_box = gnome_about_new (_("GNOME Clipboard Manager applet"),
			       GCMAPPLET_VERSION,
			       _("Copyright (C) 2002"),
			       _("Gnome Panel applet for controlling the "
			         "GNOME Clipboard Manager deamon "
				 "Released under GNU General Public Licence."),
			       authors,
			       documenters,
			       strcmp (translator_credits, "translator_credits") != 0 ? translator_credits : NULL,
			       pixbuf);
  
  if (pixbuf) 
  	gdk_pixbuf_unref (pixbuf);

  gtk_window_set_wmclass (GTK_WINDOW (about_box), "clipboard manager applet", "clipboard manager applet");
  gnome_window_icon_set_from_file (GTK_WINDOW (about_box), file);
  g_free (file);
 
  gtk_signal_connect(GTK_OBJECT(about_box), "destroy",
		     GTK_SIGNAL_FUNC(gtk_widget_destroyed), &about_box);
  gtk_widget_show(about_box);
  return;
}



static void
applet_destroy (GtkWidget *widget, gpointer data)
{
  gcmapplet_data *curr_data = data;
  GtkWidget *applet = curr_data->applet;

  g_return_if_fail (curr_data);
 
  g_free (curr_data);
  
}

static const BonoboUIVerb gcmapplet_menu_verbs [] = 
{
		BONOBO_UI_UNSAFE_VERB ("Preferences", show_preferences_dialog),
		BONOBO_UI_UNSAFE_VERB ("Help",        help_cb),
		BONOBO_UI_UNSAFE_VERB ("About",       about),
		BONOBO_UI_UNSAFE_VERB ("ShowGcm",     show_userinterface),
		BONOBO_UI_UNSAFE_VERB ("HideGcm",     hide_userinterface),
		BONOBO_UI_UNSAFE_VERB ("NewItem",	  show_newitem_dialog),
		BONOBO_UI_UNSAFE_VERB ("SaveAll",	  saveall),
		BONOBO_UI_UNSAFE_VERB ("ClearAll",	  clearall),
		BONOBO_UI_VERB_END
};

void
set_atk_name_description (GtkWidget *widget, const gchar *name,
                          const gchar *description)
{
  AtkObject *aobj;
  aobj = gtk_widget_get_accessible (widget);

  if (GTK_IS_ACCESSIBLE (aobj) == FALSE)
     return;
  atk_object_set_name (aobj, name);
  atk_object_set_description (aobj, description);
}

static void
make_applet_accessible (GtkWidget *applet)
{
  set_atk_name_description (applet, _("Clipboard Manager Applet"), NULL);
}

static gboolean
gcmapplet_applet_fill (PanelApplet *applet)
{
	
  gcmapplet_data *curr_data;
	
  if (!gcm_check_and_start_gcm(session)) {
	gnome_error_dialog(_("Warning: Cannot start the Clipboard Manager deamon"));
  }
		
	
  panel_applet_add_preferences (applet, "/schemas/apps/gcmapplet/prefs", NULL);
  curr_data = g_new0 (gcmapplet_data, 1);
  curr_data->popup = NULL;
  curr_data->buttons = NULL;
  curr_data->applet = GTK_WIDGET (applet);
 
  build_applet (curr_data);

  gtk_container_add (GTK_CONTAINER (curr_data->applet), curr_data->box);

  make_applet_accessible (GTK_WIDGET (applet));

  gtk_widget_show(GTK_WIDGET(applet));
 

  g_signal_connect (G_OBJECT (applet), "change_orient",
		    G_CALLBACK (applet_change_orient), curr_data);

  g_signal_connect (G_OBJECT (applet), "change_size",
		    G_CALLBACK (applet_change_pixel_size), curr_data);
		    
  g_signal_connect (G_OBJECT (applet), "destroy",
  		    G_CALLBACK (applet_destroy), curr_data);
  
    
  panel_applet_setup_menu_from_file (PANEL_APPLET (applet),
                                     NULL,
			             "GNOME_GcmApplet.xml",
                                     NULL,
			             gcmapplet_menu_verbs,
			             curr_data);
    
  gtk_widget_show_all (GTK_WIDGET (applet));
  return TRUE;
}

static gboolean
gcmapplet_applet_factory (PanelApplet *applet,
			   const gchar          *iid,
			   gpointer              data)
{
	gboolean retval = FALSE;

	retval = gcmapplet_applet_fill (applet); 
	return retval;
}

PANEL_APPLET_BONOBO_FACTORY ("OAFIID:GNOME_GcmApplet_Factory",
			     PANEL_TYPE_APPLET,
			     "gcmapplet",
			     "0",
			     gcmapplet_applet_factory,
			     NULL)

