/* * C o p y r i g h t *(also read COPYING)* * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *  Copyright (C) 2002  Philip Van Hoof <me@freax.org>                         *
 *                                                                             *
 *  This program is free software; you can redistribute it and/or modify       *
 *  it under the terms of the GNU General Public License as published by       *
 *  the Free Software Foundation; either version 2 of the License, or          *
 *  (at your option) any later version.                                        *
 *                                                                             *
 *  This program is distributed in the hope that it will be useful,            *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *  GNU General Public License for more details.                               *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License          *
 *  along with this program; if not, write to the Free Software                *
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA  *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
#include <gnome.h>
#include <gdk/gdk.h>

#include "../../libgcm/libgcm.h"
#include <panel-applet.h>
#include <panel-applet-gconf.h>

#define GCMAPPLET_VERSION "0.0.1"
#define SMALLSIZE 32
#define PREVIEWMAXLEN 25

typedef struct _gcmapplet_data gcmapplet_data;
typedef struct _itemclick_cb itemclick_cb;
	

struct _gcmapplet_data {
  GtkWidget *box;
  GtkWidget *applet;
  gint panel_size;
  GtkWidget *propwindow;
  GtkWidget *button;
  GtkWidget *image;
  GtkWidget *popup;
  GtkWidget *inner_frame;
  GtkWidget *popvbox;
  PanelAppletOrient  orientation;
  GList *buttons;
  GtkWidget *showbutton, *hidebutton, *sep;
  GtkRequisition  req;
  gint            x, y;
  gint            width, height;
  GtkWidget *itempopupmenu;

};

struct _itemclick_cb {
	gcmapplet_data *curr_data;
	gint i;
};



void draw_std_ctrls  (gcmapplet_data *p_curr_data);
void gcmapplet_popup_reposition (gcmapplet_data *p_curr_data);
void on_item_clicked (GtkButton *button, itemclick_cb *cb);
static gboolean applet_button_release_event_cb (GtkWidget *widget, GdkEventButton *event, gcmapplet_data *data);
static gboolean button_key_press_event_cb (GtkWidget *widget, GdkEventKey *event, gcmapplet_data *data);
static gboolean applet_key_press_event_cb (GtkWidget *widget, GdkEventKey *event, gcmapplet_data *data);
void gcmapplet_popup_hide (gcmapplet_data *p_curr_data);
void gcmapplet_popup_show (gcmapplet_data *p_curr_data);
static gboolean button_press_hack (GtkWidget      *widget, GdkEventButton *event, GtkWidget      *applet);
void destroy_buttons (gcmapplet_data *p_curr_data);
void redraw_buttons (gcmapplet_data *p_curr_data);
static gint toggle_button_toggled_cb(GtkToggleButton *button, gcmapplet_data *p_curr_data);
void build_applet(gcmapplet_data *p_curr_data);
void show_preferences_dialog (BonoboUIComponent *uic,   gcmapplet_data     *curr_data, const gchar       *verbname);
static void help_cb (BonoboUIComponent *uic, gcmapplet_data     *curr_data, const char        *verb);
static void applet_change_pixel_size(PanelApplet *applet, gint size, gpointer data);
static void applet_change_orient(PanelApplet *applet, PanelAppletOrient o, gpointer data);
static void about (BonoboUIComponent *uic, gcmapplet_data     *curr_data, const char        *verb);
static void applet_destroy (GtkWidget *widget, gpointer data);
static void make_applet_accessible (GtkWidget *applet);
static gboolean gcmapplet_applet_fill (PanelApplet *applet);
static gboolean gcmapplet_applet_factory (PanelApplet *applet, const gchar          *iid, gpointer              data);
void build_applet              (gcmapplet_data     *curr_data);
void show_preferences_dialog  (BonoboUIComponent *uic, gcmapplet_data     *curr_data, const char        *verbname);
void set_atk_name_description (GtkWidget         *widget, const char        *name, const char        *description);
void on_show_clicked (GtkButton *button, itemclick_cb *cb);
void on_hide_clicked (GtkButton *button, itemclick_cb *cb);
void on_edit_item_activate (GtkMenuItem *menuitem,   gpointer user_data);
void on_move_item_up_activate (GtkMenuItem *menuitem,   gpointer user_data);
void on_move_item_down_activate (GtkMenuItem *menuitem,   gpointer user_data);
GtkWidget * gcmapplet_update_itempopupmenu (gcmapplet_data *curr_data, itemclick_cb *d);
