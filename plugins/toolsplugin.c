#include "toolsplugin.h"
#include "../src/textitemwin.h"
#include "../src/mainwin.h"
#include <string.h>
#include <gnome.h>
#include <gconf/gconf-client.h>
#ifdef __cplusplus
extern "C" {
#endif

/* GCM Struct */

GcmPlugin toolsplugin =
{
	NULL,
	NULL,
	"Tools plugin",
	"toolsplugin",
	FALSE,
	plugin_init,
	plugin_about,
	plugin_configure,
	plugin_cleanup,

	plugin_on_mainwin_show,
	plugin_on_mainwin_hide,
	plugin_on_plugin_added,
	plugin_on_firsttime,
	plugin_on_reload_prefs,
	plugin_on_update_item,
	plugin_on_add_item,
	plugin_on_remove_item,
	plugin_on_selection_claimed,
	plugin_on_selectiondata_received,
	plugin_i_will_handle_that_target,
	plugin_handle_target_tab,
	plugin_handle_target_merge

};

  
/* Function to return a handle to the plugin */
GcmPlugin *get_gcmplugin_info(void) { return &toolsplugin; }
#ifdef __cplusplus
}
#endif
GtkWidget *toolsmenu;
GError *error;

GtkWidget *menuitem, *sep;
gulong handlerid;
MainWin *themainwin;

void
enabled_changed_callback (GConfClient *client,
                          guint        cnxn_id,
                          GConfEntry  *entry,
                          gpointer     user_data);
void
on_preferencesclose_clicked (GtkWidget     *button,
                             gpointer     *user_data);
void
on_preferencestoggle_changed		(GtkWidget     *toggle,
                                        gpointer     *user_data);
int 
plugin_on_cliplist_event               (GtkCList        *clist,
                                        GdkEvent        *event,
                                        gpointer         *user_data);
void 
on_toolsmenu_openindefault_activate    (GtkMenuItem     *menuitem,
                                        gpointer         *user_data);

void 
on_toolsmenu_openinmozilla_newwindow_activate    (GtkMenuItem     *menuitem,
                                        gpointer         *user_data);
void 
on_toolsmenu_openinmozilla_newtab_activate    (GtkMenuItem     *menuitem,
                                        gpointer         *user_data);
void 
on_toolsmenu_openinmozilla_existing_activate    (GtkMenuItem     *menuitem,
                                        gpointer         *user_data);
void 
on_toolsmenu_openinopera_existing_activate    (GtkMenuItem     *menuitem,
                                        gpointer         *user_data);
void 
on_toolsmenu_openinopera_newwindow_activate    (GtkMenuItem     *menuitem,
                                        gpointer         *user_data);
void 
on_toolsmenu_openingaleon_existing_activate    (GtkMenuItem     *menuitem,
                                        gpointer         *user_data);
void 
on_toolsmenu_openingaleon_newwindow_activate    (GtkMenuItem     *menuitem,
                                        gpointer         *user_data);
void 
on_toolsmenu_openingaleon_newtab_activate    (GtkMenuItem     *menuitem,
                                        gpointer         *user_data);
void 
on_toolsmenu_openinnetscape_existing_activate    (GtkMenuItem     *menuitem,
                                        gpointer         *user_data);
void 
on_toolsmenu_openinnetscape_newwindow_activate    (GtkMenuItem     *menuitem,
                                        gpointer         *user_data);
void 
on_toolsmenu_openinnetscape_newtab_activate    (GtkMenuItem     *menuitem,
                                        gpointer         *user_data);


static GnomeUIInfo toolsmenu_uiinfo[] =
{
#define TOOLSMENU_OPENINDEFAULT 0
{
    GNOME_APP_UI_ITEM, N_("Open in default browser"),
    NULL,
    (gpointer) on_toolsmenu_openindefault_activate, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_CONVERT,
    0, (GdkModifierType) 0, NULL
  },

#define TOOLSMENU_OPENINMOZILLA 1
{
    GNOME_APP_UI_ITEM, N_("Open in _Mozilla"),
    NULL,
    (gpointer) on_toolsmenu_openinmozilla_existing_activate, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_CONVERT,
    0, (GdkModifierType) 0, NULL
  },
#define TOOLSMENU_OPENINMOZILLANW 2
{
    GNOME_APP_UI_ITEM, N_("Open in new Mozilla window"),
    NULL,
    (gpointer) on_toolsmenu_openinmozilla_newwindow_activate, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_CONVERT,
    0, (GdkModifierType) 0, NULL
  },
#define TOOLSMENU_OPENINMOZILLANT 3
{
    GNOME_APP_UI_ITEM, N_("Open in new Mozilla tab"),
    NULL,
    (gpointer) on_toolsmenu_openinmozilla_newtab_activate, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_CONVERT,
    0, (GdkModifierType) 0, NULL
  },

#define TOOLSMENU_OPENINNETSCAPE 4
{
    GNOME_APP_UI_ITEM, N_("Open in _Netscape"),
    NULL,
    (gpointer) on_toolsmenu_openinnetscape_existing_activate, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_CONVERT,
    0, (GdkModifierType) 0, NULL
  },
#define TOOLSMENU_OPENINNETSCAPENW 5
{
    GNOME_APP_UI_ITEM, N_("Open in new Netscape window"),
    NULL,
    (gpointer) on_toolsmenu_openinnetscape_newwindow_activate, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_CONVERT,
    0, (GdkModifierType) 0, NULL
  },
#define TOOLSMENU_OPENINNETSCAPENT 6
{
    GNOME_APP_UI_ITEM, N_("Open in new Netscape tab"),
    NULL,
    (gpointer) on_toolsmenu_openinnetscape_newtab_activate, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_CONVERT,
    0, (GdkModifierType) 0, NULL
},

#define TOOLSMENU_OPENINGALEON 7
{
    GNOME_APP_UI_ITEM, N_("Open in _Galeon"),
    NULL,
    (gpointer) on_toolsmenu_openingaleon_existing_activate, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_CONVERT,
    0, (GdkModifierType) 0, NULL
  },
#define TOOLSMENU_OPENINGALEONNW 8
{
    GNOME_APP_UI_ITEM, N_("Open in new Galeon window"),
    NULL,
    (gpointer) on_toolsmenu_openingaleon_newwindow_activate, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_CONVERT,
    0, (GdkModifierType) 0, NULL
  },
#define TOOLSMENU_OPENINGALEONNT 9
{
    GNOME_APP_UI_ITEM, N_("Open in new Galeon tab"),
    NULL,
    (gpointer) on_toolsmenu_openingaleon_newtab_activate, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_CONVERT,
    0, (GdkModifierType) 0, NULL
  },

#define TOOLSMENU_OPENINOPERA 10
{
    GNOME_APP_UI_ITEM, N_("Open in _Opera"),
    NULL,
    (gpointer) on_toolsmenu_openinopera_existing_activate, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_CONVERT,
    0, (GdkModifierType) 0, NULL
  },
#define TOOLSMENU_OPENINOPERANW 11
{
    GNOME_APP_UI_ITEM, N_("Open in new Opera window"),
    NULL,
    (gpointer) on_toolsmenu_openinopera_newwindow_activate, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_CONVERT,
    0, (GdkModifierType) 0, NULL
  },
#define TOOLSMENU_END 12
  GNOMEUIINFO_END
};


void plugin_init(GtkWidget *mainwin)
{
	MainWin *mwin = (MainWin*)mainwin;
	gint i;
	gchar *key = PLUGIN_ENABLED_CONFIG_KEY(toolsplugin);
	GError *err=NULL;
	GConfClient *client;
	themainwin = mwin;
	client = gconf_client_get_default();

	gconf_client_notify_add(client, key,
                        (GConfClientNotifyFunc) enabled_changed_callback,
                        themainwin->prefswin, /* Give a pointer to the prefswin*/
                        NULL, NULL);
	
	toolsplugin.enabled = gconf_client_get_bool (client, key, &err);
	g_free(key);
	for (i=0; i<TOOLSMENU_END; i++) {
		toolsmenu_uiinfo[i].user_data = mainwin;
	}

	toolsmenu = gtk_menu_new ();
	gnome_app_fill_menu (GTK_MENU_SHELL (toolsmenu), toolsmenu_uiinfo, NULL, FALSE, 0);

	sep = gtk_menu_item_new ();
	menuitem = gtk_menu_item_new_with_label ("ToolsPlugin");
	gtk_menu_item_set_submenu (GTK_MENU_ITEM (menuitem), toolsmenu);

	gtk_menu_append (GTK_MENU(mwin->cliplistpopupmenu), sep);
	gtk_menu_append (GTK_MENU(mwin->cliplistpopupmenu), menuitem);
	if (toolsplugin.enabled) {
		gtk_widget_show(sep);
		gtk_widget_show(menuitem);
	}

	handlerid = g_signal_connect (G_OBJECT (mwin->cliplist), "event",
				G_CALLBACK (plugin_on_cliplist_event), mwin);

}

int 
plugin_on_cliplist_event                (GtkCList        *clist,
                                        GdkEvent        *event,
                                        gpointer         *user_data)
{
  if (toolsplugin.enabled) {
	/* Unused MainWin *mwin = (MainWin*)user_data;*/
	GdkEventButton *button_event = (GdkEventButton *) event;
	
	if (event->type == GDK_BUTTON_PRESS && button_event->button == 2) {

		gtk_menu_popup (GTK_MENU(toolsmenu), NULL, NULL, NULL, NULL, 2, 
						button_event->time);
		return TRUE;
	}
 }
	return FALSE;
}



gchar * get_first_selected_text (MainWin *mwin)
{
 	GList *selected;

	selected = GTK_CLIST(mwin->cliplist)->selection;
	if (selected) {
		while (selected) {
			Selection *item;
			GList *targets;
			item = gtk_clist_get_row_data(GTK_CLIST(mwin->cliplist),(gint) selected->data);

			if (item==NULL) continue;
			targets = g_list_copy(item->targets);
			if (targets==NULL) continue;
			while (targets) {
				gchar *str;
				GtkSelectionData *data = (GtkSelectionData*)targets->data;
				if (data==NULL)
					continue;
				if (data->target == gdk_atom_intern("COMPOUND_TEXT", FALSE)) {
					str = g_strdup(data->data);
					return(str);
					break;
				}
				targets=g_list_next(targets);
			}
			selected = g_list_next(selected);
		}
	}
  return NULL;
}




void 
on_toolsmenu_openindefault_activate    (GtkMenuItem     *menuitem,
                                        gpointer         *user_data)
{
 if (user_data != NULL) {
	MainWin *mwin = (MainWin*)user_data;
	gchar *text = get_first_selected_text (mwin);
	 if (text != NULL) {
		 g_print("gnome_url_show running: %s\n", text);
		 gnome_url_show (text, &error);
		 g_free(text);
	 }
	 
 }	
}

void 
on_toolsmenu_openingaleon_existing_activate    (GtkMenuItem     *menuitem,
                                        gpointer         *user_data)
{
 if (user_data != NULL) {
	MainWin *mwin = (MainWin*)user_data;
	gchar *text = get_first_selected_text (mwin);
	 if (text != NULL) {
		 /*-w : new window, -n: new tab, -x: existing*/
		 gchar *exec = g_strdup_printf("galeon -x '%s'", text);
		 g_print("toolsplugin running: %s\n", exec);
		 g_spawn_command_line_async (exec, &error);
		 g_free(exec);
		 g_free(text);
	 }
	 
 }
}

void 
on_toolsmenu_openingaleon_newtab_activate    (GtkMenuItem     *menuitem,
                                        gpointer         *user_data)
{
 if (user_data != NULL) {
	MainWin *mwin = (MainWin*)user_data;
	gchar *text = get_first_selected_text (mwin);
	 if (text != NULL) {
		 /*-w : new window, -n: new tab, -x: existing*/
		 gchar *exec = g_strdup_printf("galeon -n '%s'", text);
		 g_print("toolsplugin running: %s\n", exec);
		 g_spawn_command_line_async (exec, &error);
		 g_free(exec);
		 g_free(text);
	 }
 }
}
void 
on_toolsmenu_openingaleon_newwindow_activate    (GtkMenuItem     *menuitem,
                                        gpointer         *user_data)
{
 if (user_data != NULL) {
	MainWin *mwin = (MainWin*)user_data;
	gchar *text = get_first_selected_text (mwin);
	 if (text != NULL) {
		 /*-w : new window, -n: new tab, -x: existing*/
		 gchar *exec = g_strdup_printf("galeon -w '%s'", text);
		 g_print("toolsplugin running: %s\n", exec);
		 g_spawn_command_line_async (exec, &error);
		 g_free(exec);
		 g_free(text);
	 }
 }
}

void 
on_toolsmenu_openinopera_existing_activate    (GtkMenuItem     *menuitem,
                                        gpointer         *user_data)
{
 if (user_data != NULL) {
	MainWin *mwin = (MainWin*)user_data;
	gchar *text = get_first_selected_text (mwin);
	 if (text != NULL) {
		 /*new-window*/
		 gchar *exec = g_strdup_printf("opera -remote 'openURL(%s)'", text);
		 g_print("toolsplugin running: %s\n", exec);
		 g_spawn_command_line_async (exec, &error);
		 g_free(exec);
		 g_free(text);
	 }
 }
}
void 
on_toolsmenu_openinopera_newwindow_activate    (GtkMenuItem     *menuitem,
                                        gpointer         *user_data)
{
 if (user_data != NULL) {
	MainWin *mwin = (MainWin*)user_data;
	gchar *text = get_first_selected_text (mwin);
	 if (text != NULL) {
		 /*new-window*/
		 gchar *exec = g_strdup_printf("opera -remote 'openURL(%s, new-window)'", text);
		 g_print("toolsplugin running: %s\n", exec);
		 g_spawn_command_line_async (exec, &error);
		 g_free(exec);
		 g_free(text);
	 }
 }
}

void 
on_toolsmenu_openinnetscape_existing_activate    (GtkMenuItem     *menuitem,
                                        gpointer         *user_data)
{
 if (user_data != NULL) {
	MainWin *mwin = (MainWin*)user_data;
	gchar *text = get_first_selected_text (mwin);
	 if (text != NULL) {
		 /*new-window and new-tab; -no-about-splash*/
		 gchar *exec = g_strdup_printf("netscape -remote 'openURL(%s)'", text);
		 g_print("toolsplugin running: %s\n", exec);
		 g_spawn_command_line_async (exec, &error);
		 g_free(exec);
		 g_free(text);
	 }
 }
}

void 
on_toolsmenu_openinnetscape_newwindow_activate    (GtkMenuItem     *menuitem,
                                        gpointer         *user_data)
{
 if (user_data != NULL) {
	MainWin *mwin = (MainWin*)user_data;
	gchar *text = get_first_selected_text (mwin);
	 if (text != NULL) {
		 /*new-window and new-tab; -no-about-splash*/
		 gchar *exec = g_strdup_printf("netscape -remote 'openURL(%s, new-window)'", text);
		 g_print("toolsplugin running: %s\n", exec);
		 g_spawn_command_line_async (exec, &error);
		 g_free(exec);
		 g_free(text);
	 }
 }
}

void 
on_toolsmenu_openinnetscape_newtab_activate    (GtkMenuItem     *menuitem,
                                        gpointer         *user_data)
{
 if (user_data != NULL) {
	MainWin *mwin = (MainWin*)user_data;
	gchar *text = get_first_selected_text (mwin);
	 if (text != NULL) {
		 /*new-window and new-tab; -no-about-splash*/
		 gchar *exec = g_strdup_printf("netscape -remote 'openURL(%s, new-tab)'", text);
		 g_print("toolsplugin running: %s\n", exec);
		 g_spawn_command_line_async (exec, &error);
		 g_free(exec);
		 g_free(text);
	 }
 }
}

void 
on_toolsmenu_openinmozilla_existing_activate    (GtkMenuItem     *menuitem,
                                        gpointer         *user_data)
{
 if (user_data != NULL) {
	MainWin *mwin = (MainWin*)user_data;
	gchar *text = get_first_selected_text (mwin);
	 if (text != NULL) {
		 /*new-window and new-tab*/
		 gchar *exec = g_strdup_printf("mozilla -remote 'openURL(%s)'", text);
		 g_print("toolsplugin running: %s\n", exec);
		 g_spawn_command_line_async (exec, &error);
		 g_free(exec);
		 g_free(text);
	 }
 }
}

void 
on_toolsmenu_openinmozilla_newwindow_activate    (GtkMenuItem     *menuitem,
                                        gpointer         *user_data)
{
 if (user_data != NULL) {
	MainWin *mwin = (MainWin*)user_data;
	gchar *text = get_first_selected_text (mwin);
	 if (text != NULL) {
		 /*new-window and new-tab*/
		 gchar *exec = g_strdup_printf("mozilla -remote 'openURL(%s, new-window)'", text);
		 g_print("toolsplugin running: %s\n", exec);
		 g_spawn_command_line_async (exec, &error);
		 g_free(exec);
		 g_free(text);
	 }
 }
}
void 
on_toolsmenu_openinmozilla_newtab_activate    (GtkMenuItem     *menuitem,
                                        gpointer         *user_data)
{
 if (user_data != NULL) {
	MainWin *mwin = (MainWin*)user_data;
	gchar *text = get_first_selected_text (mwin);
	 if (text != NULL) {
		 /*new-window and new-tab*/
		 gchar *exec = g_strdup_printf("mozilla -remote 'openURL(%s, new-tab)'", text);
		 g_print("toolsplugin running: %s\n", exec);
		 g_spawn_command_line_async (exec, &error);
		 g_free(exec);
		 g_free(text);
	 }
 }
}


void plugin_about(GtkWidget *mainwin)
{
  GtkWidget *thewin;
  gchar *comment;
  const gchar *documenters[] = {
	  "Philip Van Hoof <me@freax.org>",
	  NULL	  
  };
  const gchar *translator_credits = _("translator_credits");
  const gchar *authors[] = {
	"Philip Van Hoof <me@freax.org>",
    NULL
  };
  

  comment = g_strdup (_("This plugin adds some tools to the GNOME Clipboard Manager"));

  thewin = gnome_about_new ( _("GNOME Clipboard Manager tools plugin"), "0.0.1",
                        "(C) 2000",
                        _("Released under the GNU General Public License.\n"),
                        authors,
                        documenters,
                        strcmp (translator_credits, "translator_credits") != 0 ? translator_credits : NULL,
                        NULL);


  gtk_widget_show(thewin);
}

void plugin_configure(GtkWidget *mainwin) 
{

	GtkWidget *win, *vbox, *bbox, *closeb;
	win = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_widget_set_size_request(GTK_WIDGET(win), 150, 80);
	gtk_window_set_title (GTK_WINDOW (win), _("ToolsPlugin configuration"));
	vbox = gtk_vbox_new (FALSE, 0);
	gtk_container_add (GTK_CONTAINER(win), vbox);
	gtk_widget_show (vbox);

	bbox = gtk_hbutton_box_new ();
	gtk_widget_show (bbox);

	gtk_box_pack_end (GTK_BOX (vbox), bbox, FALSE, FALSE, 0);
	gtk_button_box_set_layout (GTK_BUTTON_BOX (bbox), GTK_BUTTONBOX_END);
	gtk_button_box_set_spacing (GTK_BUTTON_BOX (bbox), 6);
	gtk_container_set_border_width (GTK_CONTAINER (bbox), 6);
 
	closeb = gtk_button_new_from_stock  (GNOME_STOCK_BUTTON_CLOSE);
	gtk_widget_show (closeb);
	gtk_container_add (GTK_CONTAINER (bbox), closeb);
	GTK_WIDGET_SET_FLAGS (closeb, GTK_CAN_DEFAULT);
	gtk_widget_grab_default (closeb);

	g_signal_connect (G_OBJECT (closeb), "clicked",
					  G_CALLBACK (on_preferencesclose_clicked),
					  win);

	gtk_widget_show(win);

}

void
on_preferencesclose_clicked (GtkWidget     *button,
                             gpointer     *user_data)
{
	gtk_widget_destroy(GTK_WIDGET(user_data));
}


void plugin_cleanup() { 
/* Cleanup menu */
	g_signal_handler_disconnect (G_OBJECT (themainwin->cliplist), handlerid);
	gtk_widget_destroy(sep);
	gtk_widget_destroy(menuitem);
}

void plugin_on_mainwin_show (GtkWidget *mainwin, gpointer *user_data){ }
void plugin_on_mainwin_hide (GtkWidget *mainwin, gpointer *user_data){ }
void plugin_on_plugin_added (GtkWidget *mainwin, GcmPlugin *p){ }
void plugin_on_save_prefs (gpointer *client, gpointer *prefs){ }
void plugin_on_get_prefs (gpointer *client, gpointer *prefs){ }
void plugin_on_firsttime (gpointer *client, gpointer *prefs){ }
void plugin_on_reload_prefs (GtkWidget *mainwin){ }
void plugin_on_update_item (GtkWidget *mainwin, gint row, gpointer *user_data){ }
void plugin_on_add_item (GtkWidget *mainwin, gpointer *item, gint row){ }
void plugin_on_remove_item (GtkWidget *mainwin, gpointer *item, gint row){ }
void plugin_on_selection_claimed (GtkWidget *mainwin, GtkClipboard *clipboard,
	GtkTargetEntry *ttargets, gpointer *item){ }
void plugin_on_selectiondata_received (GtkWidget *mainwin, gpointer *item, GtkSelectionData *data){ }
GcmPlugin *plugin_i_will_handle_that_target (GdkAtom target) { return NULL;}
gboolean plugin_handle_target_tab (gpointer *mytab, GtkSelectionData *data) { return FALSE; }
GtkSelectionData *plugin_handle_target_merge (GtkSelectionData *old, GtkSelectionData *in) {return old;}

void
enabled_changed_callback (GConfClient *client,
                          guint        cnxn_id,
                          GConfEntry  *entry,
                          gpointer     user_data)
{
	gboolean enabled = gconf_value_get_bool (gconf_entry_get_value (entry));
	GcmPlugin *pe = &toolsplugin;
	if (enabled) {
		gtk_widget_show(sep);
		gtk_widget_show(menuitem);
		themainwin->plugin_enable_plugin (themainwin, pe);
	} else {
		gtk_widget_hide(sep);
		gtk_widget_hide(menuitem);
		themainwin->plugin_disable_plugin (themainwin, pe);
	}

}
