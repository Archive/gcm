/* * C o p y r i g h t *(also read COPYING)* * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *  Copyright (C) 2003  Philip Van Hoof <me@freax.org>                         *
 *                                                                             *
 *  This program is free software; you can redistribute it and/or modify       *
 *  it under the terms of the GNU General Public License as published by       *
 *  the Free Software Foundation; either version 2 of the License, or          *
 *  (at your option) any later version.                                        *
 *                                                                             *
 *  This program is distributed in the hope that it will be useful,            *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *  GNU General Public License for more details.                               *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License          *
 *  along with this program; if not, write to the Free Software                *
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA  *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
#include "rtftohtmlplugin.h"
#include "../src/mainwin.h"
#include "../librtftohtml/librtftohtml.h"
#include <gconf/gconf-client.h>
#include <string.h>
#include <gnome.h>

#ifdef __cplusplus
extern "C" {
#endif

/* GCM Struct */

GcmPlugin rtftohtmlplugin =
{
	NULL,
	NULL,
	"Richtext To HTML plugin",
	"rtftohtmlplugin",
	FALSE,
	plugin_init,
	plugin_about,
	plugin_configure,
	plugin_cleanup,

	plugin_on_mainwin_show,
	plugin_on_mainwin_hide,
	plugin_on_plugin_added,
	plugin_on_firsttime,
	plugin_on_reload_prefs,
	plugin_on_update_item,
	plugin_on_add_item,
	plugin_on_remove_item,
	plugin_on_selection_claimed,
	plugin_on_selectiondata_received,
	plugin_i_will_handle_that_target,
	plugin_handle_target_tab,
	plugin_handle_target_merge
};

/* Function to return a handle to the plugin */
GcmPlugin *get_gcmplugin_info(void) { return &rtftohtmlplugin; }
#ifdef __cplusplus
}
#endif
GError *error;
gboolean enabled=FALSE;

MainWin *themwin;


void
enabled_changed_callback (GConfClient *client,
                          guint        cnxn_id,
                          GConfEntry  *entry,
                          gpointer     user_data);

void
on_preferencesclose_clicked (GtkWidget     *button,
                             gpointer     *user_data);

void
on_preferencestoggle_changed		(GtkWidget     *toggle,
                                        gpointer     *user_data);
void plugin_init(GtkWidget *mainwin)
{
	MainWin *mwin = (MainWin*)mainwin;
	GError *err=NULL;
	GConfClient *client;
	gchar *key = PLUGIN_ENABLED_CONFIG_KEY(rtftohtmlplugin);
	themwin = mwin;
	client = gconf_client_get_default();


	gconf_client_notify_add(client, key,
                        (GConfClientNotifyFunc) enabled_changed_callback,
                        themwin->prefswin,
                        NULL, NULL);
	rtftohtmlplugin.enabled = gconf_client_get_bool (client, key, &err);

	g_free(key);

}


void plugin_about(GtkWidget *mainwin)
{
  GtkWidget *thewin;

  gchar *comment;
  const gchar *documenters[] = {
	  "Philip Van Hoof <me@freax.org>",
	  NULL	  
  };
  const gchar *translator_credits = _("translator_credits");
  const gchar *authors[] = {
	"Philip Van Hoof <me@freax.org>",
    NULL
  };
  

  comment = g_strdup (_("This plugin adds converting the text/richtext-format to text/html to GNOME Clipboard Manager"));

  thewin = gnome_about_new ( _("GNOME Clipboard Manager rtltohtml plugin"), "0.0.1",
                        "(C) 2000",
                        _("Released under the GNU General Public License.\n"),
                        authors,
                        documenters,
                        strcmp (translator_credits, "translator_credits") != 0 ? translator_credits : NULL,
                        NULL);


  gtk_widget_show(thewin);
}

void plugin_configure(GtkWidget *mainwin) 
{
	GtkWidget *win, *vbox, *bbox, *closeb;
	win = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_widget_set_size_request(GTK_WIDGET(win), 150, 80);
	gtk_window_set_title (GTK_WINDOW (win), _("Richtext to HTML Plugin configuration"));
	vbox = gtk_vbox_new (FALSE, 0);
	gtk_container_add (GTK_CONTAINER(win), vbox);
	gtk_widget_show (vbox);

	bbox = gtk_hbutton_box_new ();
	gtk_widget_show (bbox);

	gtk_box_pack_end (GTK_BOX (vbox), bbox, FALSE, FALSE, 0);
	gtk_button_box_set_layout (GTK_BUTTON_BOX (bbox), GTK_BUTTONBOX_END);
	gtk_button_box_set_spacing (GTK_BUTTON_BOX (bbox), 6);
	gtk_container_set_border_width (GTK_CONTAINER (bbox), 6);
  
	closeb = gtk_button_new_from_stock  (GNOME_STOCK_BUTTON_CLOSE);
	gtk_widget_show (closeb);
	gtk_container_add (GTK_CONTAINER (bbox), closeb);
	GTK_WIDGET_SET_FLAGS (closeb, GTK_CAN_DEFAULT);
	gtk_widget_grab_default (closeb);

	g_signal_connect (G_OBJECT (closeb), "clicked",
					  G_CALLBACK (on_preferencesclose_clicked),
					  win);

	gtk_widget_show(win);
}

void
on_preferencesclose_clicked (GtkWidget     *button,
                             gpointer     *user_data)
{
	gtk_widget_destroy(GTK_WIDGET(user_data));
}


void plugin_cleanup() { }
void plugin_on_mainwin_show (GtkWidget *mainwin, gpointer *user_data){ }
void plugin_on_mainwin_hide (GtkWidget *mainwin, gpointer *user_data){ }
void plugin_on_plugin_added (GtkWidget *mainwin, GcmPlugin *p){ }
void plugin_on_firsttime (gpointer *client, gpointer *prefs){ }
void plugin_on_reload_prefs (GtkWidget *mainwin){ }
void plugin_on_update_item (GtkWidget *mainwin, gint row, gpointer *user_data){ }
void plugin_on_add_item (GtkWidget *mainwin, gpointer *item, gint row){ }
void plugin_on_remove_item (GtkWidget *mainwin, gpointer *item, gint row){ }
void plugin_on_selection_claimed (GtkWidget *mainwin, GtkClipboard *clipboard,
	GtkTargetEntry *ttargets, gpointer *item){ }

void plugin_on_selectiondata_received (GtkWidget *mainwin, gpointer *item, GtkSelectionData *data) {
	if ((rtftohtmlplugin.enabled) && (data->target == gdk_atom_intern("text/richtext", FALSE))) {
		Selection *titem = (Selection*)item;
		GtkSelectionData *datacopy = gtk_selection_data_copy(data);
		gsize size;
		guchar *ndata = (guchar*) rtftohtml ((char*) data->data, data->length);
		g_free (datacopy->data);
		datacopy->data = (guchar*) g_convert ((const gchar*) ndata, 
				strlen((char*)ndata), 
				"UCS-2", 
				"UTF-8" ,
				NULL, &size, NULL);
		datacopy->length = size;
		g_free(ndata);
		titem->targets=g_list_append(titem->targets, datacopy);
	}
}

GcmPlugin *plugin_i_will_handle_that_target (GdkAtom target) { return NULL; }
gboolean plugin_handle_target_tab (gpointer *mytab, GtkSelectionData *data) { return FALSE; }
GtkSelectionData *plugin_handle_target_merge (GtkSelectionData *old, GtkSelectionData *in) {return old;}

void
enabled_changed_callback (GConfClient *client,
                          guint        cnxn_id,
                          GConfEntry  *entry,
                          gpointer     user_data)
{
	gboolean enabled = gconf_value_get_bool (gconf_entry_get_value (entry));
	GcmPlugin *pe = &rtftohtmlplugin;
	if (enabled) {
		themwin->plugin_enable_plugin (themwin, pe);
	} else {
		themwin->plugin_disable_plugin (themwin, pe);
	}

}
