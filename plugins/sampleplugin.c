/* This file is not Copyrighted */
#include "sampleplugin.h"
#include "../src/textitemwin.h"
#include "../src/mainwin.h"
#include <gconf/gconf-client.h>
#include <string.h>
#include <gnome.h>

#ifdef __cplusplus
extern "C" {
#endif

/* GCM Struct */

GcmPlugin sampleplugin =
{
	NULL,
	NULL,
	"Hello World sample plugin",
	"sampleplugin",
	FALSE,
	plugin_init,
	plugin_about,
	plugin_configure,
	plugin_cleanup,
	plugin_on_mainwin_show,
	plugin_on_mainwin_hide,
	plugin_on_plugin_added,
	plugin_on_firsttime,
	plugin_on_reload_prefs,
	plugin_on_update_item,
	plugin_on_add_item,
	plugin_on_remove_item,
	plugin_on_selection_claimed,
	plugin_on_selectiondata_received,
	plugin_i_will_handle_that_target,
	plugin_handle_target_tab,
	plugin_handle_target_merge

};

  
/* Function to return a handle to the plugin */
GcmPlugin *get_gcmplugin_info(void) { return &sampleplugin; }
#ifdef __cplusplus
}
#endif

void
enabled_changed_callback (GConfClient *client,
                          guint        cnxn_id,
                          GConfEntry  *entry,
                          gpointer     user_data);
void
on_preferencesclose_clicked (GtkWidget     *button,
                             gpointer     *user_data);

MainWin *themainwin;

void plugin_init(GtkWidget *mainwin)
{
	/*
	 * mainwin	-	Is a pointer to the mainwin (MainWin, include mainwin.h)
	 * You can use this one to initialize the plugin
	 */
	MainWin *mwin = (MainWin*)mainwin;
	gchar *key = PLUGIN_ENABLED_CONFIG_KEY(sampleplugin);
	GError *err=NULL;
	GConfClient *client;
	themainwin = mwin;
	client = gconf_client_get_default();

	gconf_client_notify_add(client, key,
                        (GConfClientNotifyFunc) enabled_changed_callback,
                        themainwin->prefswin, /* Give a pointer to the prefswin*/
                        NULL, NULL);
	sampleplugin.enabled = gconf_client_get_bool (client, key, &err);
	g_free(key);

	if (sampleplugin.enabled)
		g_print("sampleplugin: Hello world from sampleplugin\n");

}

void plugin_about(GtkWidget *mainwin)
{
	/*
	 * mainwin	-	Is a pointer to the mainwin (MainWin, include mainwin.h)
	 * You can use this one to create and show an about-box for your plugin
	 */
  GtkWidget *thewin;
  gchar *comment;
  const gchar *documenters[] = {
	  "Philip Van Hoof <me@freax.org>",
	  NULL	  
  };
  const gchar *translator_credits = _("translator_credits");
  const gchar *authors[] = {
	"Philip Van Hoof <me@freax.org>",
    NULL
  };
  

  comment = g_strdup (_("This plugin is a sample plugin for GNOME Clipboard Manager"));

  thewin = gnome_about_new ( _("GNOME Clipboard Manager sample plugin"), "0.0.1",
                        "(C) 2000",
                        _("Released under the GNU General Public License.\n"),
                        authors,
                        documenters,
                        strcmp (translator_credits, "translator_credits") != 0 ? translator_credits : NULL,
                        NULL);


  gtk_widget_show(thewin);

}

void plugin_configure(GtkWidget *mainwin)
{
	/*
	 * mainwin	-	Is a pointer to the mainwin (MainWin, include mainwin.h)
	 * You can use this one to create and show a configure-box for your plugin
	 */
	GtkWidget *win, *vbox, *bbox, *closeb;
	win = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_widget_set_size_request(GTK_WIDGET(win), 150, 80);
	gtk_window_set_title (GTK_WINDOW (win), _("SamplePlugin configuration"));
	vbox = gtk_vbox_new (FALSE, 0);
	gtk_container_add (GTK_CONTAINER(win), vbox);
	gtk_widget_show (vbox);

	bbox = gtk_hbutton_box_new ();
	gtk_widget_show (bbox);
  
	gtk_box_pack_end (GTK_BOX (vbox), bbox, FALSE, FALSE, 0);
	gtk_button_box_set_layout (GTK_BUTTON_BOX (bbox), GTK_BUTTONBOX_END);
	gtk_button_box_set_spacing (GTK_BUTTON_BOX (bbox), 6);
	gtk_container_set_border_width (GTK_CONTAINER (bbox), 6);
  
	closeb = gtk_button_new_from_stock  (GNOME_STOCK_BUTTON_CLOSE);
	gtk_widget_show (closeb);
	gtk_container_add (GTK_CONTAINER (bbox), closeb);
	GTK_WIDGET_SET_FLAGS (closeb, GTK_CAN_DEFAULT);
	gtk_widget_grab_default (closeb);

	g_signal_connect (G_OBJECT (closeb), "clicked",
					  G_CALLBACK (on_preferencesclose_clicked),
					  win);

	gtk_widget_show(win);

}

void
on_preferencesclose_clicked (GtkWidget     *button,
                             gpointer     *user_data)
{
	gtk_widget_destroy(GTK_WIDGET(user_data));
}

void plugin_cleanup()
{
	/*
	 * Free all memory here
	 */
	if (sampleplugin.enabled)
		g_print("sampleplugin: Cleanup sampleplugin\n");
}


void plugin_on_mainwin_show(GtkWidget *mainwin, gpointer *user_data)
{
	if (sampleplugin.enabled)
		g_print("sampleplugin: plugin received a show event\n");
}

void plugin_on_mainwin_hide(GtkWidget *mainwin, gpointer *user_data)
{
	if (sampleplugin.enabled)
		g_print("sampleplugin: plugin received an hide event\n");
}

void plugin_on_plugin_added(GtkWidget *mainwin, GcmPlugin *p)
{
	/*
	 * mainwin	-	is the mainwin (MainWin)
	 * p is a pointer to the newly added plugin
	 *
	 * You can use this function, for example, to check if a specific
	 * plugin is being added. For example: Your plugin has support for
	 * something only if another plugin is loaded...
	 */
	if (sampleplugin.enabled)
		g_print("sampleplugin: Another plugin (%s) added\n", p->description);
}

void plugin_on_firsttime(gpointer *client, gpointer *prefs)
{
	/* 
	 * client 	-	is the GConfClient used
	 * prefs	-	is a Prefs struct, containing the new prefences
	 * This plugin call is called just before the save_prefs is called..
	 * So here you can manipulate the standard preferences values of gcm
	 */
	if (sampleplugin.enabled)
		g_print("sampleplugin: gcm started for the firsttime\n");
}

void plugin_on_reload_prefs (GtkWidget *mainwin)
{
	/*
	 * mainwin	-	is the mainwin (MainWin)
	 * the prefs are in mainwin->prefs ;) read mainwin.h !
	 */
	if (sampleplugin.enabled)
		g_print("sampleplugin: gcm reloaded it's prefs\n");
}

void plugin_on_update_item (GtkWidget *mainwin, gint row, gpointer *user_data)
{
	/*
	 * mainwin	-	is the mainwin (MainWin)
	 * item		-	is the item (Selection - include mainwin.h) which is updated
	 * row		-	the row in the cliplist that has been updated
	 */
	if (sampleplugin.enabled)
		g_print("sampleplugin: gcm on update item (row=%d)\n", row);
}

void plugin_on_add_item (GtkWidget *mainwin, gpointer *item, gint row)
{
	/*
	 * mainwin	-	is the mainwin (MainWin)
	 * item		-	is the item (Selection - include mainwin.h) which was added
	 * row		-	the row in the cliplist where the new item is added
	 */
	if (sampleplugin.enabled)
		g_print("sampleplugin: gcm added item at row=%d\n", row);
}

void plugin_on_remove_item (GtkWidget *mainwin, gpointer *item, gint row)
{
	/*
	 * mainwin	-	is the mainwin (MainWin)
	 * item		-	is the item (Selection - include mainwin.h) which will be removed
	 * row		-	the row in the cliplist where the item is removed
	 */
	if (sampleplugin.enabled)
		g_print("sampleplugin: gcm removed item at row=%d\n", row);
}

void plugin_on_selection_claimed (GtkWidget *mainwin, GtkClipboard *clipboard,
									  GtkTargetEntry *ttargets, gpointer *item)
{
	/*
	 * mainwin	-	is the mainwin (MainWin)
	 * item		-	is the item (Selection - include mainwin.h) which was claimed
	 * ttargets	-	the targets that are claimed
	 * clipboard -	the clipboard object used to claim on
	 */
	if (sampleplugin.enabled)
		g_print("sampleplugin: gcm claimed selection over an item\n");
}

void plugin_on_selectiondata_received (GtkWidget *mainwin, gpointer *item, GtkSelectionData *data)
{
	/*
	 * mainwin	-	is the mainwin (MainWin)
	 * item		-	is the item (Selection - include mainwin.h) to which the data will be added
	 * data 	-	the valid selectiondata (invalid can be that it is to large)
	 */
	if (sampleplugin.enabled)
		g_print("sampleplugin: gcm received 'a valid' selectiondata\n");
}

GcmPlugin *plugin_i_will_handle_that_target	(GdkAtom target)
{
	/*
	 * If your plugin handles a specific target then it should handle its tab and convert too
	 * and return itself in this function, else return NULL. Note that this function
	 * should always exist.
	 *
	 * if (i_accept_target(target))
	 * 		return &sampleplugin;
	 *
	 */
	if (sampleplugin.enabled) {
		g_print("sampleplugin: Answering if I will handle a target\n");
		return NULL;
		/*
		 * If you want to test this plugin, comment return NULL and checkout for example
		 * the COMPOUND_TEXT target in the Edit-window.
		 */
		if (gdk_atom_intern("COMPOUND_TEXT", FALSE) == target) {
			g_print("sampleplugin: I do\n");
			return &sampleplugin;
		}
	} else {
		return NULL;
	}
}

gboolean plugin_handle_target_tab (gpointer *mytab, GtkSelectionData *data)
{
	/*
	 * myTab is the TextItemTab (#include textitemwin.h)
	 * data is the GtkSelectionData to support
	 * Make sure to checkout the sources of textitemwin.c before
	 * writing a plugin that will handle a specified target!
	 *
	 * there are some properties that need to be set when the target
	 * is changed and/or handled by the plugin !
	 *
	 * mytab->codeset	-	is the encoding type to convert to while saving
	 * mytab->textview	-	should point to your textview widget (if yours can use
	 *					-	the copy, paste, select all features of the menu)
	 * mytab->changed	-	is a boolean which indicates wheter or not the target
	 *						has been changed vs. the original.
	 * mytab->tabwidget	-	The widget (a vbox?) that will be used in the tab
	 */
	if (sampleplugin.enabled) {
		TextItemTab *tab = (TextItemTab*) mytab;
		GtkWidget *vbox = gtk_vbox_new(FALSE, 0);
		GtkWidget *label = gtk_label_new("Hi, I am sampleplugin and I am\nresponsible for this target\n");
		tab->tabwidget = vbox;
		gtk_box_pack_start (GTK_BOX (tab->tabwidget), label, TRUE, FALSE, 0);
		gtk_widget_show(label);
		gtk_widget_show(tab->tabwidget);
		g_print("sampleplugin: Handling target\n");
		return TRUE;
	} else {
		return FALSE;
	}
}


GtkSelectionData *plugin_handle_target_merge (GtkSelectionData *old, GtkSelectionData *in)
{
	/*
	 * If the plugin handles the target, then it should also handle the mergin
	 * feature for that target.
	 *
	 * old		-	The previous generated GtkSelectionData (this happens in a loop of selected items)
	 * in 		-	The GtkSelectionData that must be merged with old and returned
	 * returns	-	A (allocated) GtkSelectionData
	 *
	 * Note that it is possible that you will have to free old in this function !
	 */
	if (sampleplugin.enabled)
		g_print("sampleplugin: merging old with new\n");

	return old;
}

void
enabled_changed_callback (GConfClient *client,
                          guint        cnxn_id,
                          GConfEntry  *entry,
                          gpointer     user_data)
{
	gboolean enabled = gconf_value_get_bool (gconf_entry_get_value (entry));
	GcmPlugin *pe = &sampleplugin;
	if (enabled) {
		themainwin->plugin_enable_plugin (themainwin, pe);
	} else {
		themainwin->plugin_disable_plugin (themainwin, pe);
	}

}
