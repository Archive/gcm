/* Copy this file to your plugin source dir and include it there */
#include "../src/plugin.h"

/* Functions to export */

#ifdef __cplusplus
extern "C" {
#endif

	void plugin_init							(GtkWidget *mainwin);
	void plugin_about							(GtkWidget *mainwin);
	void plugin_configure						(GtkWidget *mainwin);
	void plugin_cleanup							();
	void plugin_on_mainwin_show					(GtkWidget *mainwin, gpointer *user_data);
	void plugin_on_mainwin_hide					(GtkWidget *mainwin, gpointer *user_data);
	void plugin_on_plugin_added					(GtkWidget *mainwin, GcmPlugin *p);
	void plugin_on_firsttime					(gpointer *client, gpointer *prefs);
	void plugin_on_reload_prefs 				(GtkWidget *mainwin);
	void plugin_on_update_item 					(GtkWidget *mainwin, gint row, gpointer *user_data);
	void plugin_on_add_item 					(GtkWidget *mainwin, gpointer *item, gint row);
	void plugin_on_remove_item 					(GtkWidget *mainwin, gpointer *item, gint row);
	void plugin_on_selection_claimed 			(GtkWidget *mainwin, GtkClipboard *clipboard,
												 GtkTargetEntry *ttargets, gpointer *item);
	void plugin_on_selectiondata_received (GtkWidget *mainwin, gpointer *item,
											     GtkSelectionData *data);
	GcmPlugin *plugin_i_will_handle_that_target	(GdkAtom target);
	gboolean plugin_handle_target_tab			(gpointer *mytab, GtkSelectionData *data);

	GtkSelectionData
		*plugin_handle_target_merge				(GtkSelectionData *old, GtkSelectionData *in);


#ifdef __cplusplus
}
#endif

