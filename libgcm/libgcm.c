/* * C o p y r i g h t *(also read COPYING)* * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *  Copyright (C) 2003  Philip Van Hoof <me@freax.org>                         *
 *                                                                             *
 *  This program is free software; you can redistribute it and/or modify       *
 *  it under the terms of the GNU General Public License as published by       *
 *  the Free Software Foundation; either version 2 of the License, or          *
 *  (at your option) any later version.                                        *
 *                                                                             *
 *  This program is distributed in the hope that it will be useful,            *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *  GNU General Public License for more details.                               *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License          *
 *  along with this program; if not, write to the Free Software                *
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA  *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <errno.h>
#include <stdio.h>
#include <zlib.h>
#include <stdlib.h>
#include "libgcm.h"
#include "../src/mainwin.h"
#include "../src/controlsocket.h"

#define CHECK_ERR(err, msg) if (err != Z_OK) g_print("GCM ERROR: %s error: %d\n", msg, err);

#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif


static gpointer remote_read_packet(gint fd, ServerPktHeader * pkt_hdr)
{
	gpointer data = NULL;

	if (read(fd, pkt_hdr, sizeof (ServerPktHeader)) == sizeof (ServerPktHeader))
	{
		if (pkt_hdr->data_length)
		{
			data = g_malloc0(pkt_hdr->data_length);
			read(fd, data, pkt_hdr->data_length);
		}
	}
	return data;
}

static void remote_read_ack(gint fd)
{
	gpointer data;
	ServerPktHeader pkt_hdr;

	data = remote_read_packet(fd, &pkt_hdr);
	if (data)
		g_free(data);

}

static void remote_send_packet(gint fd, guint32 command, gpointer data, guint32 data_length)
{
	ClientPktHeader pkt_hdr;

	pkt_hdr.version = GCM_PROTOCOL_VERSION;
	pkt_hdr.command = command;
	pkt_hdr.data_length = data_length;
	write(fd, &pkt_hdr, sizeof (ClientPktHeader));
	if (data_length && data)
		write(fd, data, data_length);
}

static void remote_send_guint32(gint session, guint32 cmd, guint32 val)
{
	gint fd;

	if ((fd = gcm_connect_to_session(session)) == -1)
		return;
	remote_send_packet(fd, cmd, &val, sizeof (guint32));
	remote_read_ack(fd);
	close(fd);
}


/* Unsused a.t.m.
static void remote_send_boolean(gint session, guint32 cmd, gboolean val)
{
	gint fd;

	if ((fd = gcm_connect_to_session(session)) == -1)
		return;
	remote_send_packet(fd, cmd, &val, sizeof (gboolean));
	remote_read_ack(fd);
	close(fd);
}




static void remote_send_gfloat(gint session, guint32 cmd, gfloat value)
{
	gint fd;

	if ((fd = gcm_connect_to_session(session)) == -1)
		return;
	remote_send_packet(fd, cmd, &value, sizeof (gfloat));
	remote_read_ack(fd);
	close(fd);
}

static gboolean remote_get_gboolean(gint session, gint cmd)
{
	ServerPktHeader pkt_hdr;
	gboolean ret = FALSE;
	gpointer data;
	gint fd;

	if ((fd = gcm_connect_to_session(session)) == -1)
		return ret;
	remote_send_packet(fd, cmd, NULL, 0);
	data = remote_read_packet(fd, &pkt_hdr);
	if (data)
	{
		ret = *((gboolean *) data);
		g_free(data);
	}
	remote_read_ack(fd);
	close(fd);

	return ret;
}

static gfloat remote_get_gfloat(gint session, gint cmd)
{
	ServerPktHeader pkt_hdr;
	gpointer data;
	gint fd;
	gfloat ret = 0.0;

	if ((fd = gcm_connect_to_session(session)) == -1)
		return ret;
	remote_send_packet(fd, cmd, NULL, 0);
	data = remote_read_packet(fd, &pkt_hdr);
	if (data)
	{
		ret = *((gfloat *) data);
		g_free(data);
	}
	remote_read_ack(fd);
	close(fd);
	return ret;
}

*/

static void remote_send_string(gint session, guint32 cmd, gchar * string)
{
	gint fd;

	if ((fd = gcm_connect_to_session(session)) == -1)
		return;
	remote_send_packet(fd, cmd, string, string ? strlen(string) + 1 : 0);
	remote_read_ack(fd);
	close(fd);
}


static gboolean remote_cmd(gint session, guint32 cmd)
{
	gint fd;

	if ((fd = gcm_connect_to_session(session)) == -1)
		return FALSE;
	remote_send_packet(fd, cmd, NULL, 0);
	remote_read_ack(fd);
	close(fd);

	return TRUE;
}



static guint32 remote_get_gint(gint session, gint cmd)
{
	ServerPktHeader pkt_hdr;
	gpointer data;
	gint fd, ret = 0;

	if ((fd = gcm_connect_to_session(session)) == -1)
		return ret;
	remote_send_packet(fd, cmd, NULL, 0);
	data = remote_read_packet(fd, &pkt_hdr);
	if (data)
	{
		ret = *((gint *) data);
		g_free(data);
	}
	remote_read_ack(fd);
	close(fd);
	return ret;
}


gchar *remote_get_string(gint session, gint cmd)
{
	ServerPktHeader pkt_hdr;
	gpointer data;
	gint fd;

	if ((fd = gcm_connect_to_session(session)) == -1)
		return NULL;
	remote_send_packet(fd, cmd, NULL, 0);
	data = remote_read_packet(fd, &pkt_hdr);
	remote_read_ack(fd);
	close(fd);
	return data;
}

gchar *remote_get_string_pos(gint session, gint cmd, guint32 pos)
{
	ServerPktHeader pkt_hdr;
	gpointer data;
	gint fd;

	if ((fd = gcm_connect_to_session(session)) == -1)
		return NULL;
	remote_send_packet(fd, cmd, &pos, sizeof (guint32));
	data = remote_read_packet(fd, &pkt_hdr);
	remote_read_ack(fd);
	close(fd);
	return data;
}

gint gcm_connect_to_session(gint session)
{
	gint fd;
	uid_t stored_uid, euid;
	struct sockaddr_un saddr;

	if ((fd = socket(AF_UNIX, SOCK_STREAM, 0)) != -1)
	{
		saddr.sun_family = AF_UNIX;
		stored_uid = getuid();
		euid = geteuid();
		setuid(euid);
		sprintf(saddr.sun_path, "%s/gcm_%s.%d", g_get_tmp_dir(), g_get_user_name(), session);
		setreuid(stored_uid, euid);
		if (connect(fd, (struct sockaddr *) &saddr, sizeof (saddr)) != -1)
			return fd;
	}
	close(fd);
	return -1;
}

gint gcm_remote_get_version(gint session)
{
	if (gcm_check_and_start_gcm (session))
		return remote_get_gint(session, CMD_GET_VERSION);
	else return -1;
}

gint gcm_remote_get_rowcnt (gint session)
{
	if (gcm_check_and_start_gcm (session))
		return remote_get_gint(session, CMD_GET_ROWCNT);
	else return -1;
}

gchar *gcm_remote_get_rowpreview (gint session, gint row)
{
	if (gcm_check_and_start_gcm (session))
		return remote_get_string_pos(session, CMD_GET_ROWPREVIEW, row);
	else return NULL;
	
}
gchar *gcm_remote_get_rowdate (gint session, gint row)
{
	if (gcm_check_and_start_gcm (session))
		return remote_get_string_pos(session, CMD_GET_ROWDATE, row);
	else return NULL;
	
}
gchar *gcm_remote_get_rowfrom (gint session, gint row)
{
	if (gcm_check_and_start_gcm (session))
		return remote_get_string_pos(session, CMD_GET_ROWFROM, row);
	else return NULL;
	
}



gboolean gcm_remote_is_running(gint session)
{
	return remote_cmd(session, CMD_PING);
}

void gcm_remote_mainwin_hide(gint session)
{
	if (gcm_check_and_start_gcm (session))
		remote_cmd(session, CMD_MWIN_HIDE);
}

void gcm_remote_mainwin_show(gint session)
{
	if (gcm_check_and_start_gcm (session))
		remote_cmd(session, CMD_MWIN_SHOW);
}


void gcm_remote_select_row(gint session, gint row)
{
	if (gcm_check_and_start_gcm (session))
		remote_send_guint32(session,CMD_SELECT_ROW, row);
}

void gcm_remote_edit_item(gint session, gint row)
{
	if (gcm_check_and_start_gcm (session))
		remote_send_guint32(session,CMD_SHOW_TEXTITEMWIN, row);
}
void gcm_remote_exit_gcm(gint session, gint code)
{
	if (gcm_check_and_start_gcm (session))
		remote_send_guint32(session,CMD_EXIT_GCM, code);
}
void gcm_remote_move_selected_up (gint session)
{
	if (gcm_check_and_start_gcm (session))
		remote_cmd(session, CMD_MOVE_SELECTED_UP);
}
void gcm_remote_move_selected_down (gint session)
{
	if (gcm_check_and_start_gcm (session))
		remote_cmd(session, CMD_MOVE_SELECTED_DOWN);
}


void gcm_remote_newitemwin_show (gint session)
{
	if (gcm_check_and_start_gcm (session))
		remote_cmd(session, CMD_NEWWIN_SHOW);
}

void gcm_remote_newitemwin_hide (gint session)
{
	if (gcm_check_and_start_gcm (session))
		remote_cmd(session, CMD_NEWWIN_HIDE);
}

void gcm_remote_aboutwin_show (gint session)
{
	if (gcm_check_and_start_gcm (session))
		remote_cmd(session, CMD_ABOUT_SHOW);
}

void gcm_remote_prefswin_show (gint session)
{
	if (gcm_check_and_start_gcm (session))
		remote_cmd(session, CMD_PREFS_SHOW);
}

void gcm_remote_prefswin_hide (gint session)
{
	if (gcm_check_and_start_gcm (session))
		remote_cmd(session, CMD_PREFS_HIDE);
}

void gcm_remote_save_all (gint session)
{
	if (gcm_check_and_start_gcm (session))
		remote_cmd(session, CMD_SAVE_ALL);
}
void gcm_remote_save (gint session)
{
	if (gcm_check_and_start_gcm (session))
		remote_cmd(session, CMD_SAVE);
}
void gcm_remote_merge_selected (gint session)
{
	if (gcm_check_and_start_gcm (session))
		remote_cmd(session, CMD_MERGE_SELECTED);
}
void gcm_remote_delete_selected (gint session)
{
	if (gcm_check_and_start_gcm (session))
		remote_cmd(session, CMD_DELETE_SELECTED);
}
void gcm_remote_get_new (gint session)
{
	if (gcm_check_and_start_gcm (session))
		remote_cmd(session, CMD_GET_NEW);
}

void gcm_remote_select_all (gint session)
{
	if (gcm_check_and_start_gcm (session))
		remote_cmd(session, CMD_SELECT_ALL);
}
void gcm_remote_select_none (gint session)
{
	if (gcm_check_and_start_gcm (session))
		remote_cmd(session, CMD_SELECT_NONE);
}
void gcm_remote_clear (gint session)
{
	if (gcm_check_and_start_gcm (session))
		remote_cmd(session, CMD_CLEAR);
}
void gcm_remote_add_textitem (gint session, gchar *text)
{
	if (gcm_check_and_start_gcm (session))
		remote_send_string(session, CMD_ADD_TEXTSEL, text);
}

gboolean gcm_check_and_start_gcm (gint session)
{
	if (!gcm_remote_is_running(session)) {
		GError *error;
		if (!g_spawn_command_line_async ("gcm", &error)) {
			return FALSE;
		}
	}
	return TRUE;
}


gchar * gcm_decodeblock (gchar *s, gint *size, gint *exp)
{
	gint n, expected, real=0;
	gchar *p, *buf;
	n = DEC(*s);
	expected = ((n+2)/3)<<2;
	/*
	 * Expected is the number that is UUEncoded in the beginning of each line.
	 * UUEncoded text is larget then UUDecoded so it should be enough memory
	 * Note that a malicious created UUEncoded block could create a memoryfault
	 * So in the while we check if the amount of bytes that we have filled in
	 * is smaller then the allocated size for buf. (cuz we all h4t3 cr4ck3rs)
	 */ 
	buf = (gchar*) g_malloc0(sizeof(gchar) * expected);
	s++;
	p = s;
	while ((n > 0)&&(real<expected)) {
		/* Do the UUDecoding of the next three UUEncoded bytes */
		gint c1, c2, c3;
		c1 = DEC(*p) << 2 | DEC(p[1]) >> 4;
		c2 = DEC(p[1]) << 4 | DEC(p[2]) >> 2;
		c3 = DEC(p[2]) << 6 | DEC(p[3]);
		if (n >= 1) {
			buf[real]=c1;
			real++;
		}
		if (n >= 2) {
			buf[real]=c2;
			real++;
		}
		if (n >= 3) {
			buf[real]=c3;
			real++;
		}
		p += 4;
		n -= 3;
	}

	/*
	 * Set the byreference pointers so that the caller knows about the size
	 * of the decoded data.
	 */
	*exp = expected;
	*size = real;
	return buf;
}

gchar * gcm_uudecode(gchar *s, gint *size)
{
	guchar *p=s, *np, *pa, *result=NULL;
	gboolean firstloop=TRUE;
	gint si=0, i=0;

	/*
	 * Tokenize the buffer (this will replace all \n's with NULLs!)
	 */
	pa = strtok(p, "\n");
	if (pa == NULL) return NULL;
	np=pa;
	do { /* While we find a (\n) token */
			gint bsize, exp;
			gchar *str;
			str = gcm_decodeblock (np, &bsize, &exp);
			if (firstloop) {
				result = (gchar*) g_malloc0(sizeof(gchar) * (bsize));
				firstloop=FALSE;
			}
			result = g_realloc(result, si+bsize);
			g_memmove(result+si, str, bsize);
			si+=bsize;
			g_free(str);
			i++;
	} while ((np = strtok(NULL, "\n")) != NULL);
	*size = si;
	return result;
}

gchar * gcm_encodeblock (gchar *s, gint from, gint len)
{
	/*
	 * This encodes a block text which starts at s+from and ends at s+len
	 * It returns a GString and will always be UTF-8 encoded (this is why
	 * we UUEncode it, remember)
	 */
	GString *gstr=g_string_new("");
	gint i;
	for (i=0; i<len; i += 3) {
		gchar *p = &s[from+i];
		gint c1, c2, c3, c4;

		c1 = *p >> 2;
		c2 = ((*p << 4) & 060) | ((p[1] >> 4) & 017);
		c3 = ((p[1] << 2) & 074) | ((p[2] >> 6) & 03);
		c4 = p[2] & 077;

		gstr = g_string_append_c(gstr, ENC(c1));
		gstr = g_string_append_c(gstr, ENC(c2));
		gstr = g_string_append_c(gstr, ENC(c3));
		gstr = g_string_append_c(gstr, ENC(c4));
  }
	return gstr->str;
}
GString * gcm_uuencode (gchar *s, gint length)
{
	gint n, r, bs=45;
	gchar *t=s;
	GString *gstr = g_string_new("");
	if (length < bs) bs = length;

	r = length % bs;
	
	/*
	 * Write blocks of uuencoded data. Pack 45 bytes together. This will
	 * result in blocks of 60-size uuencoded bytes. Delimit them with a newline
	 * and put the expected size in front of the block (so one record is 60
	 * uuencoded bytes and delimited by a newline).
	 */
	for (n=0; n<(length-r); n+=45) {
		gchar *str = gcm_encodeblock (t, n, bs);
		gstr = g_string_append_c(gstr, '\n');
		gstr = g_string_append_c(gstr, ENC(bs));
		gstr = g_string_append(gstr, str);
		g_free(str);
	}

	{   /*
		 * The last block, which will contain the remaining bytes
		 * Put the size of the remaining bytes in front of the
		 * record so that uudecode can know how much memory to 
		 * allocate and how much uuencoded data to read in this record.
		 */
		gchar *str = gcm_encodeblock (t, n, r);
		gstr = g_string_append_c(gstr, '\n');
		gstr = g_string_append_c(gstr, ENC(r));
		gstr = g_string_append(gstr, str);
		g_free(str);
	}

	return gstr;
}


gint gcm_target_to_enum (GdkAtom target)
{ 
	/* Move most common to top ! */
	
	/* Known UTF-8 targets */
	if (target == gdk_atom_intern("TARGETS",FALSE)) return TARGETS_TARGET;
	if (target == gdk_atom_intern("COMPOUND_TEXT",FALSE)) return COMPOUND_TEXT_TARGET;
	if (target == gdk_atom_intern("TEXT",FALSE)) return TEXT_TARGET;
	if (target == gdk_atom_intern("UTF8_STRING",FALSE)) return UTF8_STRING_TARGET;
	if (target == gdk_atom_intern("STRING",FALSE)) return STRING_TARGET;
	if (target == gdk_atom_intern("UTF-8",FALSE)) return UTF_8_TARGET;
	if (target == gdk_atom_intern("text/plain;charset=UTF-8",FALSE)) return TEXTPLAIN_CHARSET_UTF_8_TARGET;
	if (target == gdk_atom_intern("text/plain;charset=utf-8",FALSE)) return TEXTPLAIN_CHARSET_UTF_8_TARGET;

	/* Known encoding targets */
	if (target == gdk_atom_intern ("TIMESTAMP",FALSE)) return TIMESTAMP_TARGET;
	if (target == gdk_atom_intern ("INTEGER", FALSE)) return INTEGER_TARGET;
	if (target == gdk_atom_intern ("ATOM", FALSE)) return ATOM_TARGET;
	if (target == gdk_atom_intern ("text/_moz_htmlcontext", FALSE)) return TEXT_MOZ_HTMLCONTEXT_TARGET;
	if (target == gdk_atom_intern ("text/_moz_htmlinfo", FALSE)) return TEXT_MOZ_HTMLINFO_TARGET;
	if (target == gdk_atom_intern ("text/unicode", FALSE)) return TEXTUNICODE_TARGET;

	/* Known undefined encoding targets */
	if (target == gdk_atom_intern ("text/html", FALSE)) return TEXTHTML_TARGET;
	if (target == gdk_atom_intern ("text/richtext", FALSE)) return TEXTRICHTEXT_TARGET;
	if (target == gdk_atom_intern ("ISO10646-1", FALSE)) return ISO10646_1_TARGET;

	/* Known UTF-16 targets */
	if (target == gdk_atom_intern ("text/plain;charset=UTF-16", FALSE)) return TEXTPLAIN_CHARSET_UTF_16_TARGET;
	if (target == gdk_atom_intern ("text/plain;charset=utf-16", FALSE)) return TEXTPLAIN_CHARSET_UTF_16_TARGET;
	
	/* Known OpenOffice.org targets */
	if (target == gdk_atom_intern ("application/x-openoffice;windows_formatname=\"Star Embed Source (XML)\"", FALSE)) return OOO_STAR_EMBED_SOURCE_TARGET;
	if (target == gdk_atom_intern ("application/x-openoffice;windows_formatname=\"Star Object Descriptor (XML)\"", FALSE)) return OOO_STAR_OBJECT_DESCRIPTOR_TARGET;
	if (target == gdk_atom_intern ("application/x-openoffice;windows_formatname=\"Drawing Format\"", FALSE)) return OOO_DRAWING_FORMAT_TARGET;
	if (target == gdk_atom_intern ("application/x-openoffice;windows_formatname=\"GDIMetaFile\"", FALSE)) return OOO_GDIMETAFILE_TARGET;
	if (target == gdk_atom_intern ("application/x-openoffice;windows_formatname=\"Image WMF\"", FALSE)) return OOO_IMAGE_WMF_TARGET;
	if (target == gdk_atom_intern ("application/x-openoffice;windows_formatname=\"Bitmap\"", FALSE)) return OOO_BITMAP_TARGET;	
	if (target == gdk_atom_intern ("application/x-openoffice;windows_formatname=\"Sylk\"", FALSE)) return OOO_SYLK_TARGET;
	if (target == gdk_atom_intern ("application/x-openoffice;windows_formatname=\"Link\"", FALSE)) return OOO_LINK_TARGET;
	if (target == gdk_atom_intern ("application/x-openoffice;windows_formatname=\"DIF\"", FALSE)) return OOO_DIF_TARGET;



	return OTHER_TARGET;
}


gpointer gcm_compress_selection (gpointer pin) 
{
	Selection *in = (Selection*) pin;
	if (in == NULL) return (gpointer)in;

	if (NOCOMPRESSION) {
		in->compressed = TRUE;
		return (gpointer)in;
	}

	if (in->compressed == TRUE) 
		return (gpointer)in;
	else {
		Selection *item=in;
		GList *targets=g_list_copy(item->targets);
		g_list_free(item->csizes);
		item->csizes = NULL;

		while(targets)
		{
			int retval=-1;
			uLong oldlen = retval;
			GtkSelectionData *data = (GtkSelectionData*)targets->data;
			if ((data!=NULL) && (data->length > 1000)) {
				uLong resultlen=data->length + (data->length * (10/100) + 12);
				Byte *compressed = (Byte*) calloc(resultlen, 1);
				int level = Z_DEFAULT_COMPRESSION;

				if (data->length > 20000) level=8;

				retval = compress2 (compressed, &resultlen, data->data, data->length, level);
				CHECK_ERR(retval, "compress");
				switch (retval) {
					case Z_OK:
						/*resultlen+=2;*/
						g_free(data->data);
						realloc(compressed, resultlen);
						data->data = (guchar*) compressed;
						oldlen = data->length;
						data->length = resultlen;
					break;
					default:
						free(compressed);
						/* The data is not compressed, retval is < 0 */
						oldlen = retval;
					break;
				}
			}
			item->csizes = g_list_append (item->csizes, (gpointer) oldlen);
			targets=g_list_next(targets);
		}
		g_list_free(targets);
		in->compressed = TRUE;
	}
		
		return (gpointer)in;
}

gpointer gcm_decompress_selection (gpointer pin) 
{
	Selection *in = (Selection*) pin;
	if (in == NULL) return in;

	if (NOCOMPRESSION) {
		in->compressed=FALSE;
		return (gpointer)in;
	}

	if (in->compressed == FALSE) 
		return (gpointer)in;
	else {
		Selection *item=in;
		GList *targets=g_list_copy(item->targets);
		GList *csizes=g_list_copy(item->csizes);

		while ((targets) && (csizes))
		{
			GtkSelectionData *data = (GtkSelectionData*)targets->data;
			uLong csize = (uLong)csizes->data;
			if ((data!=NULL) & ((int)csize > 0)) {
				Byte *uncompressed = (Byte*) calloc(csize, 1);
				uLongf resultlen=csize;
				int retval;
				retval = uncompress (uncompressed, &resultlen, (Byte*) data->data, (uLong) data->length);
				CHECK_ERR(retval, "uncompress");
				switch (retval) {
					case Z_OK:
						g_free(data->data);
						/*realloc(uncompressed, resultlen);*/
						data->data = (guchar*) uncompressed;
						data->length = resultlen;
					break;
					default:
						g_print(_("GCM ERROR: Error during the (de)compression has occured, removing this target\n"));
						targets = g_list_remove (targets, targets->data);
						csizes = g_list_remove(csizes, csizes->data);
						free(uncompressed);
						continue;
					break;
				}
			} /* Else, the data is not compressed */
			targets=g_list_next(targets);
			csizes=g_list_next(csizes);
		}
		g_list_free(targets);
		g_list_free(csizes);
		in->compressed = FALSE;
		return (gpointer)in;
	}
	return (gpointer)in;
}



gboolean gcm_compare_selections (gpointer pa, gpointer pb)
{
	Selection *a = (Selection*)pa,
			  *b = (Selection*)pb;
	GList *al, *bl;
	gboolean acompres=FALSE, bcompres=FALSE;
	if (a->compressed) { a = gcm_decompress_selection(a); acompres=TRUE; }
	if (b->compressed) { b = gcm_decompress_selection(b); bcompres=TRUE; }
	if ((a==NULL)||(b==NULL))
		return FALSE;
	if (g_list_length (a->targets) != g_list_length(b->targets))
		return FALSE;

	al = g_list_copy(a->targets);
	bl = g_list_copy(b->targets);
	while ((al) && (bl)) {
		GtkSelectionData *aldata = (GtkSelectionData*)al->data;
		GtkSelectionData *bldata = (GtkSelectionData*)bl->data;
		if ((aldata==NULL)||(bldata==NULL))
			return FALSE;
		if (aldata->length != bldata->length)
			return FALSE;
		al = g_list_next (al);
		bl = g_list_next (bl);
	}
	if (acompres) a = gcm_compress_selection (a);
	if (bcompres) b = gcm_compress_selection (b);
	return TRUE;
}

GtkSelectionData *gcm_parse_target(xmlDocPtr doc, xmlNodePtr cur) 
{
	GtkSelectionData *data=(GtkSelectionData*) g_malloc0(sizeof(GtkSelectionData));

	cur = cur->xmlChildrenNode;
	
	while (cur != NULL) {

		if (cur->name != NULL) {
			if ((!xmlStrcmp(cur->name, (const xmlChar *)"selection")))
				data->selection=gdk_atom_intern (xmlNodeListGetString(doc, cur->xmlChildrenNode, 1), FALSE);
			if ((!xmlStrcmp(cur->name, (const xmlChar *)"target"))){
				data->target=gdk_atom_intern (xmlNodeListGetString(doc, cur->xmlChildrenNode, 1), FALSE);
			}
			if ((!xmlStrcmp(cur->name, (const xmlChar *)"type")))
				data->type=gdk_atom_intern (xmlNodeListGetString(doc, cur->xmlChildrenNode, 1), FALSE);
			if ((!xmlStrcmp(cur->name, (const xmlChar *)"format")))
				data->format=(gint)atoi((char*)xmlNodeListGetString(doc, cur->xmlChildrenNode, 1));
			if ((!xmlStrcmp(cur->name, (const xmlChar *)"data"))) {
				gint size;
				guchar *str = (guchar*) gcm_uudecode(xmlNodeListGetString(doc, cur->xmlChildrenNode, 1), &size);
				
				data->data = str;
				data->length = size;
			}
		}
		cur = cur->next;
	}
	return data;
}

GList *gcm_parse_targets (xmlDocPtr doc, xmlNodePtr cur) 
{
	GList *targets=NULL; 
	while (cur)
	{
		GtkSelectionData *target;
		target=gcm_parse_target(doc, cur);
		if (target != NULL) targets = g_list_append(targets, target);
			else gnome_error_dialog(_("Invalid target in file."));
		cur = cur -> next;
	}
	return targets;
}

gpointer gcm_parse_selection(xmlDocPtr doc, xmlNodePtr cur)
{
	Selection *item;
	item = (Selection*) malloc(sizeof(Selection));
	
	memset(item, 0, sizeof(Selection));
	item->time=NULL;
	item->from=NULL;
	item->compressed = FALSE;

	item->targets=NULL;
	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if (cur->name != NULL) {
			if ((!xmlStrcmp(cur->name, (const xmlChar *)"time")))
				item->time = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
			if ((!xmlStrcmp(cur->name, (const xmlChar *)"from")))
				item->from = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
			if ((!xmlStrcmp(cur->name, (const xmlChar *)"targets"))) {
				item->targets = gcm_parse_targets(doc, cur->xmlChildrenNode);
			}
		}
		if (item->time == NULL) {
			time_t timeval=time(0);
			char *tim = malloc (32);
			strftime(tim, 32, "%H:%M:%S", localtime (&timeval));
			item->time = tim;
		}
		if (item->from == NULL) item->from = g_strdup(_("Unknown"));

		cur = cur->next;
	}

	return(gpointer)item;
}

gboolean gcm_save_items_to_disk(gchar *path, gpointer psa)
{
	gchar *c=NULL;
	SaveAsData *sa = (SaveAsData*)psa;
	gint i=0;
	xmlDocPtr rootnode;

	rootnode = xmlNewDoc((xmlChar *) "1.0");
	
	rootnode->children = xmlNewDocNode(rootnode, NULL, "gcm", NULL);
	while (sa->items) {
		xmlNodePtr itemnode, targetsnode;
		Selection *item;
		GList *targets=NULL;
		gint t=0;

		i++; c=g_strdup_printf("%d", i);
		item = (Selection*) sa->items->data;
		item = gcm_decompress_selection(item);
		itemnode = xmlNewChild(rootnode->children, NULL, "item", NULL);
			xmlSetProp(itemnode, "id", c);

			xmlNewChild (itemnode,NULL,"time", item->time);
			xmlNewChild (itemnode,NULL,"from", item->from);

			targetsnode = xmlNewChild(itemnode, NULL, "targets", NULL);

			targets = g_list_copy(item->targets);
			while (targets) {
				gchar *x,*l,*f;
				GString *uu;
				xmlNodePtr targetnode, cdata, datanode;
				GtkSelectionData *data = (GtkSelectionData*)targets->data;

				uu = gcm_uuencode(data->data, data->length);
			
				t++;
				cdata = xmlNewCDataBlock (rootnode, uu->str, uu->len);
				x=g_strdup_printf("%d", t);
				l=g_strdup_printf("%d", data->length);
				f=g_strdup_printf("%d", data->format);
					targetnode = xmlNewChild(targetsnode, NULL, "target", NULL);
						xmlSetProp(targetnode, "id", x);
						xmlNewChild (targetnode,NULL,"selection",gdk_atom_name(data->selection));
						xmlNewChild (targetnode,NULL,"target",gdk_atom_name(data->target));
						xmlNewChild (targetnode,NULL,"type",gdk_atom_name(data->type));
						xmlNewChild (targetnode,NULL,"format",f);
						datanode = xmlNewChild (targetnode,NULL,"data", "");
						xmlAddChild (datanode,cdata);
						xmlNewChild (targetnode,NULL,"length",l);
						g_free(l);g_free(x);g_free(f);
				g_string_free(uu, TRUE);
				targets = g_list_next(targets);
			}
			g_list_free(targets);
			
		sa->items = g_list_next(sa->items);
	}
	g_free(c);
	return xmlSaveFile(path, rootnode);
}
gchar* gcm_create_datapreview (GtkWidget *host, gchar *in, gint space, gint maxlen)
{
	gint width, cwidth, len, sub;
	gchar *text, *thetext;
	gboolean happend=FALSE;
	gint x, y, height, depth;
	GtkWidget *col=host;
	GtkStyle *style = gtk_widget_get_style (host);
	GdkFont *my_font = gtk_style_get_font (style);

	text = g_strndup(in, maxlen);

	gdk_window_get_geometry( col->window, 
		&x, &y, &cwidth, &height, &depth);

		cwidth-=space;
		sub=3;

		width = gdk_string_width (my_font, text);
		len = strlen(text);

		while (width >= cwidth) {
			happend=TRUE;
			if (text!=NULL) g_free(text);
			if (len > sub) len-=sub;
			text = g_strndup(in, len);
			if (len <= sub) goto done;
			width = gdk_string_width (my_font, text);
		}
done:

	if (happend) thetext = g_strdup_printf("%s...", text);
		else thetext = g_strdup(text);

	g_free(text);

	for (sub=0;sub<strlen(thetext);++sub)
					if (thetext[sub]=='\n') thetext[sub]=' ';


	return thetext;
}
