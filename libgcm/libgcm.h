/* * C o p y r i g h t *(also read COPYING)* * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *  Copyright (C) 2002  Philip Van Hoof <me@freax.org>                         *
 *                                                                             *
 *  This program is free software; you can redistribute it and/or modify       *
 *  it under the terms of the GNU General Public License as published by       *
 *  the Free Software Foundation; either version 2 of the License, or          *
 *  (at your option) any later version.                                        *
 *                                                                             *
 *  This program is distributed in the hope that it will be useful,            *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *  GNU General Public License for more details.                               *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License          *
 *  along with this program; if not, write to the Free Software                *
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA  *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
#include <gnome.h>

#include <libxml/xmlmemory.h>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xmlIO.h>

#define ENC(c) ((c) ? ((c) & 077) + ' ': '`')
#define DEC(c)	(((c) - ' ') & 077)


#ifdef __cplusplus
extern "C" {
#endif

#define NOCOMPRESSION FALSE

enum {
	CONVERT_METHOD_COPY,
	CONVERT_METHOD_CNT
};

	enum {
		STREAM_ERROR_NOT_XML,
		STREAM_ERROR_EMPTY,
		STREAM_ERROR_FORMAT,
		STREAM_ERROR_OK
	};
	
	enum {
		LOAD_ERROR_NOT_XML,
		LOAD_ERROR_EMPTY,
		LOAD_ERROR_FORMAT,
		LOAD_ERROR_OK
	};

/* gcm_target_to_enum will return one of these */
	enum {
START_DEF_ENCODING_TARGETS,

  START_UTF8_TARGETS,
	COMPOUND_TEXT_TARGET,
	TEXT_TARGET,
	UTF8_STRING_TARGET,
	STRING_TARGET,
	OTHER_TARGET,
	UTF_8_TARGET,
	TEXTPLAIN_CHARSET_UTF_8_TARGET,
  END_UTF8_TARGETS,
	
  START_UTF16_TARGETS,
	TEXTPLAIN_CHARSET_UTF_16_TARGET,
  END_UTF16_TARGETS,
	
  START_OOO_TARGETS,
	OOO_STAR_EMBED_SOURCE_TARGET,
	OOO_DRAWING_FORMAT_TARGET,
	OOO_GDIMETAFILE_TARGET,
	OOO_IMAGE_WMF_TARGET,
	OOO_BITMAP_TARGET,
	OOO_STAR_OBJECT_DESCRIPTOR_TARGET,
	OOO_SYLK_TARGET,
	OOO_DIF_TARGET,
	OOO_LINK_TARGET,
  END_OOO_TARGETS,

  START_UNICODE_TARGETS,
	TEXTUNICODE_TARGET,
  END_UNICODE_TARGETS,

	TIMESTAMP_TARGET,
	TARGETS_TARGET,
	INTEGER_TARGET,
	ATOM_TARGET,
	TEXT_MOZ_HTMLCONTEXT_TARGET,
	TEXT_MOZ_HTMLINFO_TARGET,

END_DEF_ENCODING_TARGETS,
	
START_UNDEF_ENCODING_TARGETS,
	TEXTHTML_TARGET,
	ISO10646_1_TARGET,
	TEXTRICHTEXT_TARGET,
END_UNDEF_ENCODING_TARGETS

};

	gint gcm_connect_to_session (gint session);

	/* Remote functions */
	gboolean gcm_remote_is_running (gint session);
	gint gcm_remote_get_version (gint session);
	gint gcm_remote_get_rowcnt (gint session);
	gchar *gcm_remote_get_rowpreview (gint session, gint row);
	gchar *gcm_remote_get_rowfrom (gint session, gint row);
	gchar *gcm_remote_get_rowdate (gint session, gint row);

	/* Another application can use these functions to ... */
	/* ... hide the mainwin */
	void gcm_remote_mainwin_hide (gint session);
	/* ... show the mainwin */
	void gcm_remote_mainwin_show (gint session);
	/* ... show the preferences window */
	void gcm_remote_prefswin_show (gint session);
	/* ... hide the preferences window */
	void gcm_remote_prefswin_hide (gint session);
	/* ... show the create new item window */
	void gcm_remote_newitemwin_show (gint session);
	/* ... hide the create new item window */
	void gcm_remote_newitemwin_hide (gint session);
	/* ... show the about window */
	void gcm_remote_aboutwin_show (gint session);

		/* ... select a row (and, thus, claim selectionownership for the item) */
	void gcm_remote_select_row (gint session, gint row);
	/* ... show the edit window and load the item at row */
	void gcm_remote_edit_item(gint session, gint row);
	/* ... stop clipboard management by GNOME Clipboard Manager (exit code is code) */
	void gcm_remote_exit_gcm(gint session, gint code);

	/* ... show the save all window */
	void gcm_remote_save_all (gint session);
	/* ... show the save selected window */
	void gcm_remote_save (gint session);
	/* ... merge the selected item to a new item */
	void gcm_remote_merge_selected (gint session);
	/* ... delete the selected items */
	void gcm_remote_delete_selected (gint session);
	/* ... get the current selection from the current selectionowner */
	void gcm_remote_get_new (gint sesssion);

	/* ... select all collected items */
	void gcm_remote_select_all (gint session);
	/* ... unselect all */
	void gcm_remote_select_none (gint session);
	/* ... delete all */
	void gcm_remote_clear (gint session);
	/* ... add a text-only item */
	void gcm_remote_add_textitem (gint session, gchar *text);

	/* General library functions */
	
	/* Will start gcm if gcm is not yet running */
	gboolean gcm_check_and_start_gcm (gint session);

/*
 * Will decode one block of UUEncoded text
 *
 * s		: is a pointer to the next token (after a \n) to decode
 * size		: is a pointer to a gint where the size of the decoded block is stored (there can be NULLs so strlen is incorrect)
 * exp		: is a pointer to a gint where the size of the encoded block is stored
 * returns	: a pointer to a newly allocated block of memory that can contain NULLs (use size to know it's length)
 */
	gchar * gcm_decodeblock (gchar *s, gint *size, gint *exp);

/*
 * Will decode a UUEncoded text
 *
 * s		: is a pointer to a buffer that contains the uuencoded text. Note that all \n's will be replaced with NULLs
 * size		: is a pointer to a gint where the size of the decoded block is stored (there can be NULLs so strlen is incorrect)
 * returns	: a pointer to a newly allocated block of memory that can contain NULLs (use size to know it's length)
 */
	gchar * gcm_uudecode(gchar *s, gint *size);

	/* Will encode stuff to one UUEncoded block text */
	gchar * gcm_encodeblock (gchar *s, gint from, gint len);

	/* Will encode stuff to UUEncoded text */
	GString * gcm_uuencode (gchar *s, gint length);

/* This is a helper function that turns a target into an integer (which one is defined in the enum) */
	gint gcm_target_to_enum (GdkAtom target);

	gpointer gcm_compress_selection (gpointer pin);
	gpointer gcm_decompress_selection (gpointer pin);
	gboolean gcm_compare_selections (gpointer pa, gpointer pb);

	GList *gcm_parse_targets (xmlDocPtr doc, xmlNodePtr cur);
	GtkSelectionData *gcm_parse_target(xmlDocPtr doc, xmlNodePtr cur);
	gpointer gcm_parse_selection(xmlDocPtr doc, xmlNodePtr cur);
	gboolean gcm_save_items_to_disk(gchar *path, gpointer psa);

	gchar* gcm_create_datapreview (GtkWidget *host, gchar *in, gint space, gint maxlen);

#ifdef __cplusplus
};
#endif

