
#include <gnome.h>
#include "../libgcm/libgcm.h"

gint session=0; /* Default session */

int main() {

	if (!gcm_remote_is_running(session)) {
	  GError *error;
	  if (!g_spawn_command_line_async ("gcm -s&", &error)) {
		  gnome_error_dialog(_("Warning: Cannot start the Clipboard Manager deamon"));
	  }
	}

//	gcm_remote_mainwin_hide(session);
	gcm_remote_mainwin_show(session);

}

